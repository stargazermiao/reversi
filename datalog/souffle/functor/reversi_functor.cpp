/**
 * @file reversi_functor.cpp
 * @author Yihao Sun (ysun67@syr.edu)
 * @brief Some souffle User-Defined functor for reversi project
 * @version 0.1
 * @date 2022-03-15
 * 
 * @copyright Copyright (c) 2022
 * 
 */


#include <cstdint>
#include <cstdlib>
#include <cstring>
#include <iostream>
#include <fstream>
#include <vector>
#include <souffle/RamTypes.h>
#include <sstream>
#include <string>
// #include <regex>
// #include "souffle/RamTypes.h"
// #include "souffle/RecordTable.h"
// #include "souffle/SymbolTable.h"
#include "souffle/CompiledSouffle.h"

#include"z3++.h"

/**
* @brief encode an normal string into a string can be used in csv/tsv file
* 
* @param input 
* @return std::string 
*/
std::string encode_csv_str(std::string input) {
    std::stringstream output_s;
    for (char _c : input) {
        if (_c == '\n') {
            output_s << "\\n";
        }
        else if (_c == '\t') {
            output_s << "\\t";
        }
        else if (_c == ',') {
            output_s << "__COMMA__";
        }
        else {
            output_s <<  _c;
        }
    }
    return output_s.str();
}

std::string replace_all(std::string str, const std::string& from, const std::string& to) {
    size_t start_pos = 0;
    while((start_pos = str.find(from, start_pos)) != std::string::npos) {
        str.replace(start_pos, from.length(), to);
        start_pos += to.length(); // Handles case where 'to' is a substring of 'from'
    }
    return str;
}

/**
 * @brief convert and souffle list into c++ vector
 * 
 * @param souffle_lst 
 * @return std::vector<int> 
 */
std::vector<int> souffle_lst_to_vector(souffle::RecordTable* recordTable, souffle::RamDomain souffle_lst) {
    std::vector<int> result;
    souffle::RamDomain rest = souffle_lst;
    while(rest != 0) {
        // [x, l]
        const souffle::RamDomain* upacked_lst = recordTable->unpack(rest, 2);
        result.push_back(upacked_lst[0]);
        rest = upacked_lst[1];
    }
    return result;
}

extern "C" {

    /**
     * @brief generate a random int32 number in range 0 ~ <x>
     * 
     * @param x 
     * @return int32_t 
     */
    int32_t random32(int32_t x) {
        return random() % x;
    }


    // const char* file_to_str(const char* fpath)
    // {
    //     std::ifstream input_fs(fpath);
    //     std::stringstream buffer;
    //     buffer << input_fs.rdbuf();
    //     char* file_content_c = new char[buffer.str().size()+1];
    //     strcpy(file_content_c, buffer.str().c_str());
    //     return file_content_c;
    // }


    /**
     * @brief read everything of a file into souffle symbol
     * 
     * @param symbolTable 
     * @param recordTable 
     * @param fpath_arg the path of input file
     * @return souffle::RamDomain 
     */ 
    souffle::RamDomain file_to_str(souffle::SymbolTable* symbolTable, souffle::RecordTable* recordTable,
        souffle::RamDomain fpath_arg)
    {
        const std::string& fpath = symbolTable->decode(fpath_arg);
        std::ifstream input_fs(fpath);
        std::stringstream buffer;
        buffer << input_fs.rdbuf();
        return symbolTable->encode(encode_csv_str(buffer.str()));
    }

    /**
     * @brief encode an string so it can be used in normal csv/tsv file
     * 
     * @param symbolTable 
     * @param recordTable
     * @param str
     * @return souffle::RamDomain 
     */
    souffle::RamDomain csv_encoding(souffle::SymbolTable* symbolTable, souffle::RecordTable* recordTable,
        souffle::RamDomain str)
    {
        std::string fpath = symbolTable->decode(str);
        return symbolTable->encode(encode_csv_str(fpath));
    }

    /**
     * @brief solve formula fmul declared in collafl
     *         (cur >> x) ^ (prev >> y) + z < 2 ^16;
     * NOTE: switch to z3!
     * @param symbolTable 
     * @param recordTable 
     * @param prev_label 
     * @param cur_label 
     * @return souffle::RamDomain 
     */
    souffle::RamDomain solve_fmul_collafl(
        souffle::SymbolTable* symbolTable,
        souffle::RecordTable* recordTable,
        int32_t prev_label,
        int32_t cur_label,
        // int32_t max_size,
        souffle::RamDomain assigned) {
        // convert `assigned` from souffle list into c++ vector
        std::vector<int> assigned_hash_vec = souffle_lst_to_vector(recordTable, assigned);
        z3::context ctx;
        z3::expr x = z3::zext(ctx.bv_const("x", 16), 16);
        z3::expr y = z3::zext(ctx.bv_const("y", 16), 16);
        z3::expr z = z3::zext(ctx.bv_const("z", 16), 16);
        z3::expr hash_val =  (z3::lshr(cur_label, x) ^ (z3::lshr(prev_label, y))) + z;
        z3::solver _solver(ctx);
        _solver.add(x > 0);
        _solver.add(x < 16);
        _solver.add(y > 0);
        _solver.add(y < 16);
        _solver.add(z > 0);
        _solver.add(z < 16);
        _solver.add(hash_val < 65536);
        // exclude used hash
        for (auto used: assigned_hash_vec) {
            _solver.add(hash_val != used);
        }
        auto _solver_res = _solver.check();
        if(_solver_res != z3::sat) {
            // [-1, -1, -1, -1] means not found
            souffle::RamDomain not_found[4] = {-1, -1, -1, -1}; 
            return recordTable->pack(not_found, 4);
        } else {
            z3::model m = _solver.get_model();
            int32_t solved_x = m.eval(x).get_numeral_int();
            int32_t solved_y = m.eval(y).get_numeral_int();
            int32_t solved_z = m.eval(z).get_numeral_int();
            int32_t solved_hashv = m.eval(hash_val).get_numeral_int();
            souffle::RamDomain solved_res[4] = {solved_x, solved_y, solved_z, solved_hashv};
            return recordTable->pack(solved_res, 4);
        }
    }

    /**
     * @brief format a string (similary to python format string)
     * using curly brace to wrap a place holder
     * e.g. "{foo} is {bar}"  
     * using a list comma seperated tuple to define value for place holder 
     * e.g. "(foo,'apple'),(bar,'red')"
     * @param symbolTable 
     * @param recordTable 
     * @param str_template 
     * @param placeholder
     * @param value     value to repace 
     * @return souffle::RamDomain 
     */
    souffle::RamDomain format(souffle::SymbolTable* symbolTable, souffle::RecordTable* recordTable,
        souffle::RamDomain str_template, souffle::RamDomain placeholder, souffle::RamDomain value) {
        assert(symbolTable && "NULL symbol table");
        assert(recordTable && "NULL record table");
        const std::string& template_s = symbolTable->decode(str_template);
        const std::string& placeholder_raw_s = symbolTable->decode(placeholder);
        const std::string& placeholder_s = "{" + template_s + "}";
        const std::string& value_s = symbolTable->decode(value);
        std::string res = replace_all(template_s, placeholder_s, value_s);
        return symbolTable->encode(res);
    }
}
 