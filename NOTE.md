# Note on  ddisasm

## profile

**most time consuming**
`def_used_return_val_reg` cost 89.2% of running time

**most memory cosuming**
`function_inference.in_function_initial** cost  95.7% of tuples.

## use define analysis

**declared rules**
- def
  - used in `pe_binaries`
- used
- def_used
  - `arm_binaries`
  - `data_access_analysis`
  - `debug_status`
  - `elf_binaries`
  - `pe_binaries`
  - `pointer_reatribution`
  - `register_type_analysis`
  - `relative_jump_table`
  - `symbolization`
  - `value_analysis`
  - `arch_x86_32`
- def_used_intra
- flow_def
  - `value_analysis`
- local_next                        *
- inter_procedural_jump             *
- block_next                        *
- must_def
- defined_in_block
  - `binaries`
- block_last_def
- last_def
  - `binaries`
- def_used_return_val_reg
  - `value_analysis`
- def_used_for_address
  - `symbolization`
  - `value_analysis`
- straight_line_last_def
  - output for other
- straight_line_def_used
  - output for other

**related file**
- `pe_binaries`  ❌ 
- `arm_binaries` ❌ 
- `data_access_analysis`  ✔
- `debug_status`  ❌
- `elf_binaries`  ✔ 
- `pointer_reatribution`  ✔ 
- `register_type_analysis`  ✔ 
- `relative_jump_table`  ✔ 
- `symbolization`  ✔ 
- `value_analysis`  ✔ 
- `arch_x86_32`  ✔ 

**input rules**
- code_in_block
- may_fallthrough
- unconditional_jump
- direct_jump
- direct_call
- code
- block_last_instruction
- block
- arch.delay_slot
- jump_table_start
- relative_address
- instruction_get_dest_op
- op_regdirect_contains_reg
- instruction_get_operation
- arch.call_operation
- arch.function_non_maintained_reg
- cmp_immediate_to_reg
- arch.jump_equal_operation
- arch.jump_unequal_operation
- arch.condition_mov
- instruction_get_src_op
- instruction_get_op
- op_indirect_contains_reg
- block_last_def
- fde_addresses
- arch.return_val_reg
- arch.return
- possible_ea


**Other**
- in x86_32, `def` has special case defined in `arch_x86_32.dl`
