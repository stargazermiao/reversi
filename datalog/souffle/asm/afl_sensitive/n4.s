
  xorq %r10, %r10
  xorq %r11, %r11
  movq %rcx, %r10
  xorq __afl_pppprev_loc(%rip), %rcx
  xorq __afl_ppprev_loc(%rip), %rcx
  xorq __afl_pprev_loc(%rip), %rcx
  xorq __afl_prev_loc(%rip), %rcx
  movq __afl_ppprev_loc(%rip), %r11
  movq %r11, __afl_pppprev_loc(%rip)
  movq __afl_pprev_loc(%rip), %r11
  movq %r11, __afl_ppprev_loc(%rip)
  movq __afl_prev_loc(%rip), %r11
  movq %r11, __afl_pprev_loc(%rip)
  movq %r10, __afl_prev_loc(%rip)
  shrq $1, __afl_prev_loc(%rip)

  addb $1, (%rdx, %rcx, 1)
  adcb $0, (%rdx, %rcx, 1)
  nop
