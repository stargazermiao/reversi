; TODO: rewrite when we have 0-arity

; //===- cfg.dl ----------------------------------------*- datalog -*-===//
; //
; //  Copyright (C) 2019 GrammaTech, Inc.
; //
; //  This code is licensed under the GNU Affero General Public License
; //  as published by the Free Software Foundation, either version 3 of
; //  the License, or (at your option) any later version. See the
; //  LICENSE.txt file in the project root for license terms or visit
; //  https://www.gnu.org/licenses/agpl.txt.
; //
; //  This program is distributed in the hope that it will be useful,
; //  but WITHOUT ANY WARRANTY; without even the implied warranty of
; //  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
; //  GNU Affero General Public License for more details.
; //
; //  This project is sponsored by the Office of Naval Research, One Liberty
; //  Center, 875 N. Randolph Street, Arlington, VA 22203 under contract #
; //  N68335-17-C-0700.  The content of the information does not necessarily
; //  reflect the position or policy of the Government and no official
; //  endorsement should be inferred.
; //
; //===----------------------------------------------------------------------===//
; /**
; This module generates the control flow graph of the disassembled code
; */

; [(symbolic_operand_data EA 1 Initial_memory) <-- (symbolic_operand EA 1 Initial_memory "data")]
; [(symbol_minus_symbol_jump_table_first DataAddress TableStart Size Symbol1 Symbol2 1)
;  <--
;  (symbol_minus_symbol_jump_table DataAddress TableStart Size Symbol1 Symbol2 "first" 1)]
; [(symbol_minus_symbol_jump_table_second DataAddress TableStart Size Symbol1 Symbol2 1)
;  <--
;  (symbol_minus_symbol_jump_table DataAddress TableStart Size Symbol1 Symbol2 "second" 1)]

(return 0)
(true 0)
(false 0)
(branch 0)
(call 0)
(fallthrough 0)
(arch.pointer_size 4)
; .decl cfg_edge(src:address,dest:address,conditional:symbol,indirect:symbol,type:symbol)
; .output cfg_edge

; // edges whose destination we do not know
; .decl cfg_edge_to_top(src:address,conditional:symbol,type:symbol)
; .output cfg_edge_to_top

; .decl cfg_edge_to_symbol(src:address,symbol:symbol)
; .output cfg_edge_to_symbol

; /////////////////////////////////////////////////////////////////////////////////
; // jump table identification
; .decl jump_table(Src:address,Data:address)

; //use the data access pattern
; jump_table(EA,Initial_memory):-
;     code(EA),
;     indirect_jump(EA),
;     arch.pointer_size(Pt_size),
;     data_access_pattern(Initial_memory,Pt_size,as(Pt_size,number),EA),
;     symbolic_data(Initial_memory,Pt_size,Dest_block),
;     refined_block(Dest_block).
[(jump_table EA Initial_memory)
 <--
 (code EA)
 (indirect_jump EA)
 (arch.pointer_size Pt_size)
 (data_access_pattern Initial_memory Pt_size Pt_size EA)
 (symbolic_data Initial_memory Pt_size Dest_block)
 (refined_block Dest_block)]

; //use just the constant in the jump
; jump_table(EA,Initial_memory):-
;     code(EA),
;     indirect_jump(EA),
;     arch.pointer_size(Pt_size),
;     symbolic_operand(EA,1,Initial_memory,"data"),
;     symbolic_data(Initial_memory,Pt_size,Dest_block),
;     refined_block(Dest_block).
[(jump_table EA Initial_memory)
 <--
 (code EA)
 (indirect_jump EA)
 (arch.pointer_size Pt_size)
 (symbolic_operand_data EA 1 Initial_memory)
 (symbolic_data Initial_memory Pt_size Dest_block)
 (refined_block Dest_block)]

; jump_table(EA,Memory+Pt_size):-
;     jump_table(EA,Memory),
;     arch.pointer_size(Pt_size),
;     symbolic_data(Memory+Pt_size,Pt_size,Dest_block),
;     refined_block(Dest_block),
;     !data_access_pattern(Memory+Pt_size,_,_,_).
[(jump_table EA {+ Memory Pt_size})
 <--
 (jump_table EA Memory)
 (arch.pointer_size Pt_size)
 (symbolic_data {+ Memory Pt_size} Pt_size Dest_block)
 (refined_block Dest_block)
 (~ data_access_pattern {+ Memory Pt_size} _ _ _)]

; // we have another access to the same table (the access can be before or after)
; jump_table(EA,Memory+Pt_size):-
;     jump_table(EA,Memory),
;     arch.pointer_size(Pt_size),
;     symbolic_data(Memory,Pt_size,Content_prev),
;     symbolic_data(Memory+Pt_size,Pt_size,Content_next),
;     refined_block(Content_next),
;     data_access_pattern(Memory+Pt_size,Pt_size,as(Pt_size,number),EA),
;     (
;         EA < Content_prev,
;         EA < Content_next
;         ;
;         EA > Content_prev,
;         EA > Content_next
;     ).
[(jump_table EA {+ Memory Pt_size})
 <--
 (jump_table EA Memory)
 (arch.pointer_size Pt_size)
 (symbolic_data Memory Pt_size Content_prev)
 (symbolic_data {+ Memory Pt_size} Pt_size Content_next)
 (refined_block Content_next)
 (data_access_pattern {+ Memory Pt_size} Pt_size Pt_size EA)
 (or (and (< EA Content_prev)
          (< EA Content_next))
     (and (> EA Content_prev)
          (> EA Content_next)))]

; // Resolve easy jumps that access a straightforward jump table
; .decl resolved_jump(Src:address,Dest:address)
; .output resolved_jump

; resolved_jump(EA,Dest):-
;     jump_table(EA,Memory),
;     code(EA),
;     symbolic_data(Memory,_,Dest),
;     refined_block(Dest).
[(resolved_jump EA Dest)
 <--
 (jump_table EA Memory)
 (code EA)
 (symbolic_data Memory _ Dest)
 (refined_block Dest)]

; // Symbol-Symbol jump tables
; resolved_jump(EA,Dest):-
;     jump_table_start(EA,Size,TableStart,_,_),
;     symbol_minus_symbol_jump_table(DataAddress,TableStart,Size,Symbol1,Symbol2,Reference,1),
;     symbol_minus_symbol(DataAddress,Size,Symbol1,Symbol2,1),
;     (
;         Reference = "first", Dest = Symbol2;
;         Reference = "second", Dest = Symbol1
;     ).
[(resolved_jump EA Symbol2)
 <--
 (jump_table_start EA Size TableStart _ _)
 (symbol_minus_symbol_jump_table_first DataAddress TableStart Size Symbol1 Symbol2 1)
 (symbol_minus_symbol DataAddress Size Symbol1 Symbol2 1)]

[(resolved_jump EA Symbol1)
 <--
 (jump_table_start EA Size TableStart _ _)
 (symbol_minus_symbol_jump_table_second DataAddress TableStart Size Symbol1 Symbol2 /1)
 (symbol_minus_symbol DataAddress Size Symbol1 Symbol2 1)] 

; //////////////////////////////////////////////////////////////////////////////
; // edge creation

; cfg_edge(Src,Dest,Conditional,Indirect,"branch"):-
;     refined_block_control_instruction(Src,EA),
;     (
;         direct_jump(EA,Dest), Indirect = "false";
;         pc_relative_jump(EA,Dest), Indirect = "false";
;         resolved_jump(EA,Dest), Indirect = "true"
;     ),
;     refined_block(Dest),
;     (
;         unconditional_jump(EA), Conditional = "false";
;         !unconditional_jump(EA),Conditional = "true"
;     ).
[(cfg_edge Src Dest Conditional Indirect (branch 0))
 <--
 (refined_block_control_instruction Src EA)
 (or (and (direct_jump EA Dest)
          (= Indirect (false 0)))
     (and (pc_relative_jump EA Dest)
          (= Indirect (false 0)))
     (and (resolved_jump EA Dest)
          (= Indirect (true 0))))
 (refined_block Dest)
 (or (and (unconditional_jump EA)
          (= Conditional (false 0)))
     (and (~ unconditional_jump EA)
          (= Conditional (true 0))))]

; cfg_edge(Src,Dest,"false","false","call"):-
;     refined_block_control_instruction(Src,EA),
;     (
;         direct_call(EA,Dest);
;         pc_relative_call(EA,Dest)
;     ),
;     refined_block(Dest).
[(cfg_edge Src Dest (false 0) (false 0) (call 0))
 <--
 (refined_block_control_instruction Src EA)
 (or (direct_call EA Dest)
     (pc_relative_call EA Dest))
 (refined_block Dest)]

; cfg_edge(Src,Dest,"false","false","fallthrough"):-
;     refined_block_control_instruction(Src,EA),
;     may_fallthrough(EA,Dest),
;     !no_return_call(EA),
;     !nop_block(Src),
;     code_in_refined_block(Dest,Dest),
;     next_refined_block_in_section(Src,Dest).
[(cfg_edge Src Dest (false 0) (false 0) (fallthrough 0))
 <--
 (refined_block_control_instruction Src EA)
 (may_fallthrough EA Dest)
 (~ no_return_call EA)
 (~ nop_block Src)
 (code_in_refined_block Dest Dest)
 (next_refined_block_in_section Src Dest)
 (next_refined_block_in_section Src Dest)]

; cfg_edge(Src,Dest,"false","false","fallthrough"):-
;      nop_block(Src),
;      cfg_edge(_,Src,_,_,_),
;      next_refined_block_in_section(Src,Dest).
[(cfg_edge Src Dest (false 0) (false 0) (fallthrough 0))
 <--
 (nop_block Src)
 (cfg_edge _ Src _ _ _)
 (next_refined_block_in_section Src Dest)]

; cfg_edge(ReturnBlock,NextBlock,"false","false","return"):-
;     cfg_edge(Caller,Block,_,_,"call"),
;     cfg_edge(Caller,NextBlock,_,_,"fallthrough"),
;     function_inference.in_function(Block,FunctionEntry),
;     function_inference.in_function(ReturnBlock,FunctionEntry),
;     refined_block_control_instruction(ReturnBlock,Insn),
;     arch.return(Insn).
[(cfg_edge ReturnBlock NextBlock (false 0) (false 0) (return 0))
 <--
 (cfg_edge Caller Block _ _ (call 0))
 (cfg_edge Caller NextBlock _ _ (fallthrough 0))
 (function_inference.in_function Block FunctionEntry)
 (function_inference.in_function ReturnBlock FunctionEntry)
 (refined_block_control_instruction ReturnBlock Insn)
 (arch.return Insn)]

; cfg_edge_to_top(Src,Conditional,"branch"):-
;     refined_block_control_instruction(Src,EA),
;     (
;         reg_jump(EA,_);
;         indirect_jump(EA)
;     ),
;     !resolved_jump(EA,_),
;     (
;         unconditional_jump(EA), Conditional = "false";
;         !unconditional_jump(EA), Conditional = "true"
;     ).
[(cfg_edge_to_top Src Conditional (branch 0))
 <--
 (refined_block_control_instruction Src EA)
 (or (reg_jump EA _)
     (indirect_jump EA))
 (~ resolved_jump EA _)
 (or (and (unconditional_jump EA)
          (= Conditional (false 0)))
     (and (~ unconditional_jump EA)
          (= Conditional (true 0))))]

; cfg_edge_to_top(Src,"false","call"):-
;     refined_block_control_instruction(Src,EA),
;     (
;         reg_call(EA,_);
;         indirect_call(EA)
;     ).
[(cfg_edge_to_top Src (false 0) (call 0))
 <--
 (refined_block_control_instruction Src EA)
 (or (reg_call EA _)
     (indirect_call EA))]

; // a return to top if no other return exists
; cfg_edge_to_top(Src,"false","return"):-
;     refined_block_control_instruction(Src,Insn),
;     arch.return(Insn),
;     !cfg_edge(Src,_,_,_,"return").
[(cfg_edge_to_top Src (false 0) (return 0))
 <--
 (refined_block_control_instruction Src Insn)
 (arch.return Insn)
 (~ cfg_edge Src _ _ _ (return 0))]

; cfg_edge_to_symbol(Src,Symbol):-
;     refined_block_control_instruction(Src,EA),
;     plt_entry(EA,Symbol),
;     !defined_symbol(_,_,_,_,_,_,_,Symbol),
;     !inferred_symbol_name(_,Symbol,_,_).
[(defined_symbol _ _ _ _ _ _ _ Symbol) --> (defined_symbol8 Symbol)]
[(inferred_symbol_name _ Symbol _ _) --> (inferred_symbol_name2 Symbol)]
[(cfg_edge_to_symbol Src Symbol)
 <--
 (refined_block_control_instruction Src EA)
 (plt_entry EA Symbol)
 (~ defined_symbol8 Symbol)
 (~ inferred_symbol_name2 Symbol)]

; // pc relative jump with undefined destination
; cfg_edge_to_top(Src,Conditional,"branch"):-
;     refined_block_control_instruction(Src,EA),
;     pc_relative_jump(EA,Dest),
;     !refined_block(Dest),
;     (
;         unconditional_jump(EA), Conditional = "false";
;         !unconditional_jump(EA),Conditional = "true"
;     ).
[(cfg_edge_to_top Src Conditional (branch 0))
 <--
 (refined_block_control_instruction Src EA)
 (pc_relative_jump EA Dest)
 (~ refined_block Dest)
 (or (and (unconditional_jump EA)
          (= Conditional (false 0)))
     (and (~ unconditional_jump EA)
          (= Conditional (true 0))))]
