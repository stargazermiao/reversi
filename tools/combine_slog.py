'''
combine all slog file under a folder into one file

Yihao Sun
2021 Syracuse
'''

import os

target_file = [
    "code_in_block",
    "may_fallthrough",
    "unconditional_jump",
    "direct_jump",
    "direct_call",
    "code",
    "block_last_instruction",
    "block",
    "arch.delay_slot",
    "jump_table_start",
    "relative_address",
    "instruction_get_dest_op",
    "op_regdirect_contains_reg",
    "arch.call_operation",
    "arch.function_non_maintained_reg",
    "cmp_immediate_to_reg",
    "arch.jump_equal_operation",
    "jump_unequal_operation",
    "arch.condition_mov",
    "instruction_get_src_op",
    "instruction_get_op",
    "op_indirect_contains_reg",
    "block_last_def",
    "fde_addresses",
    "arch.return_val_reg",
    "arch.return",
    "possible_ea",
    "instruction"
]

def gen_default_name(dir_path):
    p_name = dir_path.split('/')[-1]
    return f'./{p_name}_combined.slog'

def combine_files(dir_path, combined_name=None):
    if combined_name is None:
        combined_name = gen_default_name(dir_path)
    with open(combined_name, 'w+') as output_file:
        for root, dirs, files in os.walk(dir_path):
            for fname in files:
                if not fname.endswith('.slog'):
                    continue
                if not fname[:-5] in target_file:
                    continue
                with open(f'{root}/{fname}', 'r') as input_file:
                    output_file.write(input_file.read())
                    output_file.write('\n')

if __name__ == '__main__':
    if os.path.exists('facts/vfunc_heap.slog'):
        os.remove('facts/vfunc_heap.slog')
    combine_files('facts/vfunc_heap_slog', 'facts/vfunc_heap.slog')

