'''
a naive and ad hoc way to input csv into slog interpreter

Yihao Sun
2021 Syracuse
'''

import os

def fact_to_slog(fpath, spath):
    def fact_str(fs):
        fs = fs.strip()
        # neg
        if fs.startswith('-') and fs[1:].isnumeric():
            return fs
        elif fs.isnumeric():
            return fs
        else:
            return f'"{fs}"'
    if fpath.endswith('.facts'):
        rel_name = fpath.split('/')[-1][:-6]
    elif fpath.endswith('.csv'):
        rel_name = fpath.split('/')[-1][:-4]
    else:
        return
    facts_row = []
    with open(fpath, 'r') as f:
        for line in f.readlines():
            facts_row.append(line.split('\t'))
    with open(spath, 'w+') as f:
        slog_str = ""
        for fact in facts_row:
            fact = map(fact_str, fact)
            new_row = f"({rel_name} {' '.join(fact)})\n"
            slog_str = slog_str + new_row
        f.write(slog_str)

if __name__ == "__main__":
    base_dir = 'facts'
    souffle_dir = 'vfunc_except_souffle'
    slog_dir = 'vfunc_except_slog'
    for fname in os.listdir(f'{base_dir}/{souffle_dir}'):
        if fname.endswith('.facts'):
            if os.path.exists(f'{base_dir}/{souffle_dir}/{fname[:-6]}.csv'):
                continue
            fact_to_slog(
                f'{base_dir}/{souffle_dir}/{fname}', 
                f'{base_dir}/{slog_dir}/{fname[:-6]}.slog'
            )
        elif fname.endswith('.csv'):
            fact_to_slog(
                f'{base_dir}/{souffle_dir}/{fname}', 
                f'{base_dir}/{slog_dir}/{fname[:-4]}.slog'
            )

