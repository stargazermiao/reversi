"""
gtirb rewrite must work with setup tool

Yihao Sun
"""

from setuptools import setup
import setuptools


setup(
    name="drew",
    version=0.1,
    author="Yihao Sun",
    author_email="ysun67@syr.edu",
    description="An datalog GTIRB rewrite pass",
    packages=setuptools.find_packages(),
    install_requires = [
        "gtirb-rewriting",
        "capstone-gt",
        "gtirb-capstone >= 1.0.1",
        "gtirb-functions",
        "gtirb >= 1.10.4",
        "mcasm > 0.1.2",
        "more-itertools",
        "dataclasses ; python_version<'3.7.0'",
        "packaging",
        "entrypoints",
        "typing-extensions",
    ],
    entry_points = {
        "gtirb_rewriting": {
            "drew-pass=drew:DrewPassDriver"
        }
    },
    classifiers=["Programming Language :: Python :: 3"],
    url="https://github.com/harp-lab/reversi/",
    license="GPLv3",
)

