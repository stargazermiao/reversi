// binary rewriting rule
// this will be mainly used for input of python

// supported syntax are "ATT" and "INTEL"
// .decl asm_syntax(syntax: symbol)
// // .input asm_syntax
// .output asm_syntax

// declare the symbol defined in other module
.decl foreign_symbol(name: symbol, lib: symbol)
// .input foreign_symbol
.output foreign_symbol

// define `.lcomm` local variable in bss segmentation
.decl local_common_block(name: symbol, size: number)
//.input local_common_block
.output local_common_block

// `.comm`
// Note: By convention, use of the TD storage mapping class is restricted to common blocks no more than four (4) bytes long.
// A common block can be aligned by using the Number parameter, which is specified as the log base 2 of the alignment desired.
.decl common_block(name: symbol, size: number,align_base: number)
// .input common_block
.output common_block

// declare a labeld string in inserted asm code
.decl ascii_string(label: symbol, str: symbol)
// .input ascii_string
.output ascii_string

// user can have place holder in their asm code snippit to insert  
.decl placeholder(asm_name: symbol, pname: symbol)
// .input placeholder
.output placeholder

// the instructions file want to insert into bianries, instruction is represented by a string
// name of insturction can be used as place holder for later insertion.
.decl asm_snippet(name: symbol, path: symbol)
// .input asm_snippet
.output asm_snippet

// insert some instructions at
// prioiry is the order of instrumentation, some code may need use the symbol defined in previous 
.decl insert_at(EA: unsigned, instruction: symbol, priority: number)
.output insert_at

// insert a whole function to a binary
.decl insert_function(name: symbol, asm:symbol)
// .input insert_function
.output insert_function

// during an rewriting process, some function might need to call label/procedure
// in other function, since our insertion is asm code based, user have to declare
// the dependency by themselves
// NOTE: in future DSL like asm template we maybe can solve this?
.decl function_depend_on(func_name: symbol, pred_func_name: symbol)

//////////////////////////
// some API will be post processed, do not dircectly use this
// instance a place hodler at an insertion at ... has asm snippet ... with value ...
// NOTE: value must be symbol, if you want use number use `to_string`
.decl replace_placeholder_at(EA: unsigned, asm_name: symbol, placeholder_name: symbol, value: symbol)
.output replace_placeholder_at
.decl replace_function_placeholder(func_name: symbol, placeholder_name: symbol, value: symbol)
.output replace_function_placeholder


// sort and get function rewriting order from dependency relation
.decl function_order(func_name: symbol, order: number)
.output function_order
function_order(fname, 0) :-
    insert_function(fname, _),
    !function_depend_on(fname, _).

function_order(fname, prev_order+1) :-
    insert_function(fname, _),
    function_depend_on(fname, prev_fname),
    function_order(prev_fname, prev_order).
