
# REVERSI

Reversi is next generation binary analysis platform, it's powered by datalog to enable declarative binary analysis experience.

> gtirb-rewriting --run drew-pass --dl=../datalog/souffle/instrument.dl --fact=../example/fuzz_test/ddisasm ../facts/fuzz_test.ir output.gtirb
