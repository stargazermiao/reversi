//  This is the test souffle version of binary instrument

// some user defined functor
.functor random32(x: number): number

///////////////////////////

// rule input from ddisasm
// Fuzz only need instrument on the real possible program branch, so use refined block here
// normal block is a super set

.decl refined_block(block:unsigned)
.input refined_block(filename="refined_block.csv")
.decl code_section(name: symbol)
.input code_section(filename="code_section.csv")
.decl section(name:symbol,size:unsigned,addr:unsigned)
.input section(filename="section.csv")
.decl block_information(block:unsigned,size:unsigned,endAddress:unsigned)
.input block_information(filename="block_information.csv")


//////////////////////////
// some API will be post processed, do not dircectly use this
// instance a place hodler at an insertion at ... has asm snippet ... with value ...
// NOTE: value must be symbol, if you want use number use `to_string`
.decl replace_placeholder_at(EA: unsigned, asm_name: symbol, placeholder_name: symbol, value: symbol)
.output replace_placeholder_at
.decl replace_function_placeholder(func_name: symbol, placeholder_name: symbol, value: symbol)
.output replace_function_placeholder

///////////////////////////

// supported syntax are "ATT" and "INTEL"
.decl asm_syntax(syntax: symbol)
// .input asm_syntax
.output asm_syntax
asm_syntax("ATT").

// declare the symbol defined in other module
.decl foreign_symbol(name: symbol, lib: symbol)
// .input foreign_symbol
.output foreign_symbol

// define `.lcomm` local variable in bss segmentation
.decl local_common_block(name: symbol, size: number)
//.input local_common_block
.output local_common_block

// `.comm`
// Note: By convention, use of the TD storage mapping class is restricted to common blocks no more than four (4) bytes long.
// A common block can be aligned by using the Number parameter, which is specified as the log base 2 of the alignment desired.
.decl common_block(name: symbol, size: number,align_base: number)
// .input common_block
.output common_block

// declare a labeld string in inserted asm code
.decl ascii_string(label: symbol, str: symbol)
// .input ascii_string
.output ascii_string

// user can have place holder in their asm code snippit to insert  
.decl placeholder(asm_name: symbol, pname: symbol)
// .input placeholder
.output placeholder

// the instructions file want to insert into bianries, instruction is represented by a string
// name of insturction can be used as place holder for later insertion.
.decl asm_snippet(name: symbol, path: symbol)
// .input asm_snippet
.output asm_snippet

// insert some instructions at
// prioiry is the order of instrumentation, some code may need use the symbol defined in previous 
.decl insert_at(EA: unsigned, instruction: symbol, priority: number)
.output insert_at

// insert a whole function to a binary
.decl insert_function(name: symbol, asm:symbol)
// .input insert_function
.output insert_function

// add a lobel for a basic block
.decl basic_block_label(addr: unsigned, label: symbol)

foreign_symbol("getenv", "libc.so.6").
foreign_symbol("atoi", "libc.so.6").
foreign_symbol("shmat", "libc.so.6").
foreign_symbol("write", "libc.so.6").
foreign_symbol("read", "libc.so.6").
foreign_symbol("fork", "libc.so.6").
foreign_symbol("waitpid", "libc.so.6").
foreign_symbol("close", "libc.so.6").
foreign_symbol("_exit", "libc.so.6").

asm_snippet("trampoline", "/home/stargazermiao/workspace/PL/reversi/datalog/souffle/asm/trampoline_64.s").
asm_snippet("payload", "/home/stargazermiao/workspace/PL/reversi/datalog/souffle/asm/payload_64.s").

placeholder("trampoline", "unique_mark").

local_common_block("__afl_area_ptr", 8).
local_common_block("__afl_prev_loc", 8).
local_common_block("__afl_fork_pid", 4).
local_common_block("__afl_temp", 4).
local_common_block("__afl_setup_failure", 1).

common_block("__afl_global_area_ptr", 8, 8).

ascii_string(".AFL_SHM_ENV", "__AFL_SHM_ID").

// Because of GTIRB library problem, an insertion can only be done in a label
// which should be changed later!

// code block in text segmentation
.decl text_block(addr: unsigned)
text_block(block_start) :-
    refined_block(block_start),
    section(".text", size, sec_start_addr),
    block_start > sec_start_addr,
    block_start < sec_start_addr + size.

.decl lowest_block(addr: unsigned)
lowest_block(min_addr) :- min_addr = min addr : text_block(addr).

// AFL map size
.decl map_size(size: number)
map_size(65535).

// instrument trampoline at the start of each basic block
insert_at(block_start, "trampoline", 1),
basic_block_label(block_start, block_mark) :-
    map_size(m_size),
    text_block(block_start),
    // !lowest_block(block_start),
    block_mark = cat("$", to_string(@random32(m_size))).

insert_function("afl_payload", "payload").


/////////////////////////////////////////////////////////////////////
// some post process
replace_placeholder_at(block_start, "trampoline", "unique_mark", block_mark) :-
    basic_block_label(block_start, block_mark).

// here is code for sanity check
.decl error(reason: symbol)
.output error

// if a asm snippit has placeholder but inserted without instance it
error("Please provide value for all place holder !") :-
    insert_at(addr, asm_name, _),
    placeholder(asm_name, pname),
    !replace_placeholder_at(addr, asm_name, pname, _).
