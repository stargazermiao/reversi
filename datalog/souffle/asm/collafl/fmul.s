
  xorq %r10, %r10
  xorq %r11, %r11

  movq %rcx, %r10
  movq %rcx, %r11
  mov __coll_afl_y(%rip), %cl
  shrq %cl, __afl_prev_loc(%rip)
  mov __coll_afl_x(%rip), %cl
  shrq %cl, %r10
  xorq __afl_prev_loc(%rip), %r10
  addq __coll_afl_z(%rip), %r10
  movq %r11, __afl_prev_loc(%rip)

  addb $1, (%rdx, %r10, 1)
  adcb $0, (%rdx, %r10, 1)
  nop
