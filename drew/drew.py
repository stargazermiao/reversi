"""
drew

datalog rewrite tool based on grammatech tool kit

Yihao Sun
"""

import argparse
import logging
import os
import tempfile
from typing import Sequence, Optional, Iterator

import capstone_gt
import gtirb
import gtirb_functions
from gtirb_rewriting import Constraints, Pass, RewritingContext, Scope, Patch, X86Syntax
import gtirb_rewriting
import gtirb_rewriting.driver

# logging.basicConfig(level=logging.DEBUG)

SUPPORTED_SYNTAX = ['INTEL', 'ATT']

FUNCTOR_PATH = os.path.join(os.path.dirname(os.path.abspath(__file__)),
                            "../datalog/souffle/functor/libfunctors.so")

SOUFFLE = "souffle"
# DDISASM = ""


def decode_csv_str(csv_s: str):
    """
    in our souffle code, string used in fact file will be encoded(escape `,` `\n`, `\t`)
    decode back here
    """
    return csv_s.replace('\\t', '\t').replace('\\n', '\n').replace('__COMMA__', ',')


class SingleAddressScope(Scope):
    """ insert at an specify address """

    def __init__(self, module: gtirb.Module, addr: int):
        self.addr = addr

    def _function_matches(
        self, module: gtirb.Module, func: gtirb_functions.Function
    ) -> bool:
        return True

    def _block_matches(
        self,
        module: gtirb.Module,
        func: gtirb_functions.Function,
        block: gtirb.CodeBlock,
    ) -> bool:
        return block.contains_address(self.addr)

    def _needs_disassembly(self) -> bool:
        return True

    def _potential_offsets(
        self,
        func: gtirb_functions.Function,
        block: gtirb.CodeBlock,
        disassembly: Optional[Sequence[capstone_gt.CsInsn]],
    ) -> Iterator[int]:
        yield self.addr - block.address


class AsmFilePatch(Patch):
    """ an gtrib rewrite functor to fetch asm code from file """

    def __init__(self, asm_path, asm_syntax, placeholder_map=None):
        self.constraints = Constraints(x86_syntax=asm_syntax)
        self.asm_path = asm_path
        self.placeholder_map = placeholder_map

    def get_asm(self, insertion_context, *args) -> Optional[str]:
        with open(self.asm_path, 'r') as asm_f:
            asm_raw = asm_f.read()
        replaced_asm = asm_raw
        # check if need replace the placeholder
        if self.placeholder_map:
            for placeholder, value in self.placeholder_map.items():
                replaced_asm = replaced_asm.replace(f"{{{placeholder}}}",
                                                    decode_csv_str(value))
        return replaced_asm


class StringPatch(Patch):
    """ A patch for insert string to .text segmentation """

    def __init__(self, label: str, str_data: str, asm_synatx):
        self.constraints = Constraints(x86_syntax=asm_synatx)
        self.label = label
        self.str_data = str_data

    def get_asm(self, insertion_context, *args) -> Optional[str]:
        return f'''
        .string "{self.str_data}"
        '''


class DrewPass(Pass):
    """ a custome rewrite pass based on datalog output """

    def __init__(self, asm_map, insert_map, function_map, insert_strings, local_comms,
                 global_comms, foreign_symbols, syntax, placeholder_map):
        super().__init__()
        self.asm_map = asm_map
        self.insert_map = insert_map
        self.function_map = function_map
        self.insert_strings = insert_strings
        self.local_comms = local_comms
        self.global_comms = global_comms
        self.foreign_symbols = foreign_symbols
        if syntax == 'ATT':
            self.asm_syntax = X86Syntax.ATT
        else:
            self.asm_syntax = X86Syntax.INTEL
        self.placeholder_map = placeholder_map

    def get_section(self, module: gtirb.Module, sec_name: str):
        for sec in module.sections:
            if sec.name == sec_name:
                return sec
        return None

    def begin_module(self, module: gtirb.Module, functions: Sequence[gtirb_functions.Function], rewriting_ctx: RewritingContext) -> None:
        # update plt for foreign symbol will be used

        for foreign_sym in self.foreign_symbols:
            rewriting_ctx.get_or_insert_extern_symbol(foreign_sym['symbol'],
                                                      foreign_sym['lib_name'])

        # add lcomm var into bss section
        bss_section = self.get_section(module, '.bss')
        assert bss_section
        # inserted data offset start at the end of bss section
        original_bss_bot_addr = bss_section.address + bss_section.size
        cur_bss_offset = 0
        new_bss_bi = []
        for lcomm in self.local_comms:
            lc_bi = gtirb.ByteInterval(
                address=cur_bss_offset + original_bss_bot_addr,
                size=lcomm['size'],
                section=bss_section)
            cur_bss_offset += lcomm['size']
            lc_datablock = gtirb.DataBlock(
                size=lcomm['size'], byte_interval=lc_bi)
            lc_bi.blocks.add(lc_datablock)
            lc_sym = gtirb.Symbol(
                name=lcomm['symbol'], payload=lc_datablock, module=module)
            module.symbols.add(lc_sym)
            # lc_sym_const = gtirb.symbolicexpression.SymAddrConst(0, lc_sym)
            # lc_bi.symbolic_expressions = {0: lc_sym_const}
            # bss_section.byte_intervals.add(lc_bi)
            new_bss_bi.append(lc_bi)

        for comm in self.global_comms:
            c_bi = gtirb.ByteInterval(
                address=cur_bss_offset + original_bss_bot_addr,
                size=comm['size'],
                section=bss_section)
            cur_bss_offset += comm['size']
            c_datablock = gtirb.DataBlock(
                size=comm['size'], byte_interval=c_bi)
            c_bi.blocks.add(c_datablock)
            c_sym = gtirb.Symbol(
                name=comm['symbol'], payload=c_datablock, module=module)
            module.symbols.add(c_sym)
            # bss_section.byte_intervals.add(c_bi)
            new_bss_bi.append(lc_bi)
            module.aux_data['elfSymbolInfo'].data[c_sym] = (
                0, 'OBJECT', 'GLOBAL', 'DEFAULT', 0)
        # after we insert byte interval in bss everything need offset!
        for _bi in module.byte_intervals:
            if _bi.address >= original_bss_bot_addr:
                _bi.address += cur_bss_offset
        for _bi in new_bss_bi:
            bss_section.byte_intervals.add(_bi)

        # insert string object to the end of .text segmentation
        text_section = self.get_section(module, '.text')
        assert text_section
        original_text_bot_addr = text_section.address + text_section.size
        cur_text_offset = 0
        new_text_bi = []
        for str_meta in self.insert_strings:
            str_bytes = str_meta['str_data'].encode('ascii') + b'\0'
            str_size = len(str_bytes)
            s_bi = gtirb.ByteInterval(
                address=cur_text_offset + original_text_bot_addr,
                size=str_size,
                contents=str_bytes,
                section=text_section
            )
            cur_text_offset += len(str_bytes)
            s_datablock = gtirb.DataBlock(size=str_size, byte_interval=s_bi)
            s_bi.blocks.add(s_datablock)
            s_sym = gtirb.Symbol(
                name=str_meta['label'], payload=s_datablock, module=module)
            module.symbols.add(s_sym)
            module.aux_data['encodings'].data[s_datablock] = "string"
            new_text_bi.append(s_bi)
        for _bi in module.byte_intervals:
            if _bi.address >= original_text_bot_addr:
                _bi.address += cur_text_offset
        for _bi in new_text_bi:
            text_section.byte_intervals.add(_bi)

        # print(self.insert_map)
        for name, func in self.function_map.items():
            fpath = func["path"]
            if (-1, name) in self.placeholder_map:
                print(self.placeholder_map[(-1, name)])
                patch = AsmFilePatch(fpath, self.asm_syntax,
                                     self.placeholder_map[(-1, name)])
            else:
                print(f"register func {name}, but no placeholder")
                patch = AsmFilePatch(fpath, self.asm_syntax)
            rewriting_ctx.register_insert_function(name, patch, func["order"])
        for addr, insert_file in self.insert_map.items():
            g_block_candidates = list(module.code_blocks_on(addr))
            if len(g_block_candidates) == 0:
                print(f"Address {addr} is not in any code block!")
            # assume all code block is resolved after ddisasm =.=
            # maybe check this after2
            # g_block = g_block_candidates[0]
            if (addr, insert_file) in self.placeholder_map:
                patch = AsmFilePatch(self.asm_map[insert_file], self.asm_syntax,
                                     self.placeholder_map[(addr, insert_file)])
            else:
                print(f"no related replacement {(addr, insert_file)}")
                patch = AsmFilePatch(
                    self.asm_map[insert_file], self.asm_syntax)
            rewriting_ctx.register_insert(
                SingleAddressScope(module, addr), patch)

    # def end_module(self, module: gtirb.Module, functions: Sequence[gtirb_functions.Function]) -> None:
    #     print(module.aux_data['encodings'])
    #     for sym in module.symbols:
    #         if sym not in module.aux_data['elfSymbolInfo'].data:
    #             continue
    #         print(f"{sym.name} ::  {module.aux_data['elfSymbolInfo'].data[sym]}")
            # if sym.name == "_edata":
            #     print(module.aux_data['elfSymbolInfo'].data[sym])
            # if sym.name == "__afl_global_area_ptr":
            #     print(module.aux_data['elfSymbolInfo'].data[sym])


class DrewPassDriver(gtirb_rewriting.driver.PassDriver):
    """ rewrite pass driver """

    def __run_souffle(self, fact_folder, tmp_dir_path, dl_file):
        cmd = f"souffle -F {fact_folder} -D {tmp_dir_path} -lfunctors -lz3 {dl_file}"
        os.system(cmd)
        print("finish running souffle...")

    # def __run_ddisasm(self, bin_path):
    #     cmd = ""

    def __read_input_instrument_data(self, tmp_dir_path):
        """ read in output of souffle """
        asm_map = {}
        insert_map = {}
        function_map = {}
        insert_strings = []
        foreign_symbols = []
        local_comms = []
        global_comms = []
        placeholder_map = {}
        with open(f"{tmp_dir_path}/asm_snippet.csv", "r") as asm_f:
            for line in asm_f:
                instrument_name = line.split('\t')[0].strip()
                asm_path = line.split('\t')[1].strip()
                asm_map[instrument_name] = asm_path
        with open(f"{tmp_dir_path}/insert_function.csv", "r") as asm_f:
            for line in asm_f:
                func_name = line.split('\t')[0].strip()
                instrument_name = line.split('\t')[1].strip()
                func_path = asm_map[instrument_name]
                function_map[func_name] = {"path": func_path, "order": 0}
        with open(f"{tmp_dir_path}/function_order.csv", "r") as ord_f:
            for line in ord_f:
                func_name = line.split('\t')[0].strip()
                func_order = int(line.split('\t')[1].strip())
                function_map[func_name][func_name] = func_order
        with open(f"{tmp_dir_path}/insert_at.csv", "r") as insert_f:
            for line in insert_f:
                # print(line)
                addr = int(line.split('\t')[0].strip())
                asm_name = line.split('\t')[1].strip()
                insert_map[addr] = asm_name
        with open(f"{tmp_dir_path}/ascii_string.csv", "r") as string_f:
            for line in string_f:
                label = line.split('\t')[0].strip()
                str_data = line.split('\t')[1].strip()
                insert_strings.append({"label": label, "str_data": str_data})
        with open(f"{tmp_dir_path}/local_common_block.csv", "r") as lcomm_f:
            for line in lcomm_f:
                symbol = line.split("\t")[0].strip()
                size = int(line.split("\t")[1].strip())
                local_comms.append({"symbol": symbol, "size": size})
        with open(f"{tmp_dir_path}/common_block.csv", "r") as lcomm_f:
            for line in lcomm_f:
                symbol = line.split("\t")[0].strip()
                size = int(line.split("\t")[1].strip())
                align_base = int(line.split("\t")[2].strip())
                global_comms.append(
                    {"symbol": symbol, "size": size, "align_base": align_base})
        with open(f"{tmp_dir_path}/foreign_symbol.csv", "r") as foreign_f:
            for line in foreign_f:
                symbol = line.split("\t")[0].strip()
                lib_name = line.split("\t")[1].strip()
                foreign_symbols.append(
                    {"symbol": symbol, "lib_name": lib_name})
        with open(f"{tmp_dir_path}/fuzzer.asm_syntax.csv", "r") as syntax_f:
            syntax_s = syntax_f.read().strip()
            if syntax_s not in SUPPORTED_SYNTAX:
                print(f'Syntax {syntax_s} is not supported,'
                      f' please choose from {SUPPORTED_SYNTAX}')
                exit(1)
        with open(f"{tmp_dir_path}/replace_placeholder_at.csv", "r") as placeholder_f:
            for line in placeholder_f:
                addr = int(line.split("\t")[0].strip())
                asm_name = line.split("\t")[1].strip()
                # asm_path = asm_map[asm_name]
                placeholder_name = line.split("\t")[2].strip()
                value = line.split("\t")[3].strip()
                if (addr, asm_name) in placeholder_map:
                    placeholder_map[(addr, asm_name)][placeholder_name] = value
                else:
                    placeholder_map[(addr, asm_name)] = {
                        placeholder_name: value}
        with open(f"{tmp_dir_path}/replace_function_placeholder.csv", "r") as placeholder_f:
            for line in placeholder_f:
                func_name = line.split("\t")[0].strip()
                asm_name = function_map[func_name]["path"]
                # asm_path = asm_map[asm_name]
                placeholder_name = line.split("\t")[1].strip()
                value = line.split("\t")[2].strip()

                if (-1, func_name) in placeholder_map:
                    placeholder_map[(-1, func_name)][placeholder_name] = value
                else:
                    placeholder_map[(-1, func_name)] = {placeholder_name: value}
        print(f"Total insertion in text {len(insert_map.keys())}")
        print(function_map)
        # print(placeholder_map)
        return {
            "asm_map": asm_map,
            "insert_map": insert_map,
            "function_map": function_map,
            "insert_strings": insert_strings,
            "local_comms": local_comms,
            "global_comms": global_comms,
            "foreign_symbols": foreign_symbols,
            "syntax": syntax_s,
            "placeholder_map": placeholder_map
        }

    def add_options(self, cmd_parser) -> None:
        cmd_parser.add_argument("--dl", metavar="dl", type=str, help="Input datalog script.",
                                required=True)
        cmd_parser.add_argument("--fact", metavar="fact",
                                type=str, help="Input fact file.", required=True)
        cmd_parser.add_argument("--debug_dir", metavar="fout",
                                type=str, help="", required=True)
        # cmd_parser.add_argument("--output", metavar="output", type=str, help="Output binary path.")

    def create_pass(self, args: argparse.Namespace, ir: gtirb.IR) -> Pass:
        if args.debug_dir:
            tmp_dir_name = args.debug_dir
        else:
            tmp_dir = tempfile.TemporaryDirectory()
            tmp_dir_name = tmp_dir.name
        self.__run_souffle(args.fact, tmp_dir_name, args.dl)
        parsed_input = self.__read_input_instrument_data(tmp_dir_name)
        return DrewPass(**parsed_input)

    def description(self) -> Optional[str]:
        return "DREW: a datalog binary instrumentation/rewrite tool."


if __name__ == "__main__":
    gtirb_rewriting.driver.main(DrewPassDriver)
