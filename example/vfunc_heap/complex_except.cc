
#include <iostream> 
using namespace std;
 
int INIT = 0;

struct MyException : public exception {
   const char * what () const throw () {
      return "C++ Exception";
   }
};

class Shape {
   protected:
      int width, height;
   public:
      Shape( int a=0, int b=0)
      {
         width = a;
         height = b;
      }
      virtual int area() { return 0; };
};
class Rectangle: public Shape{
   private:
      int c;
   public:
      Rectangle( int a=0, int b=0):Shape(a, b) { c = 0; }
      int area () override
      { 
         cout << "Rectangle class area :" <<endl;
         return (width * height); 
      }
};
class Triangle: public Shape{
   public:
      Triangle( int a=0, int b=0):Shape(a, b) { }
      int area () override
      { 
         cout << "Triangle class area :" <<endl;
         return (width * height / 2); 
      }
};

int main( )
{
   for (int i=0 ; i < 10 ; i++) {
      cout << "count down" << i + INIT << endl;
   }
   Shape *shape;
   Rectangle *rec = new Rectangle(10,7);
   Triangle *tri = new Triangle(10,5);
 
   shape = rec;
   shape->area();
 
   shape = tri;
   shape->area();

   try {
      throw MyException();
   } catch(MyException& e) {
      std::cout << "MyException caught" << std::endl;
      std::cout << e.what() << std::endl;
   } catch(std::exception& e) {
      //Other errors
   }
   
   return 0;
}
