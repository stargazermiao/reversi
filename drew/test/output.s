#===================================
.intel_syntax noprefix
#===================================

nop
nop
nop
nop
nop
nop
nop
nop

#===================================
.text
#===================================

.align 16
#-----------------------------------
.globl payload
.type payload, @function
#-----------------------------------
payload:
__afl_maybe_log:

            lahf 
            seto AL
            mov RDX,QWORD PTR [RIP+__afl_area_ptr]
            test RDX,RDX
            je __afl_setup
__afl_store:

            xor RCX,QWORD PTR [RIP+__afl_prev_loc]
            xor QWORD PTR [RIP+__afl_prev_loc],RCX
            shr QWORD PTR [RIP+__afl_prev_loc],1
            add BYTE PTR [RDX+RCX*1],1
            adc BYTE PTR [RDX+RCX*1],0
__afl_return:

            add AL,127
            sahf 
            ret 
__afl_setup:

            cmp BYTE PTR [RIP+__afl_setup_failure],0
            jne __afl_return

            mov RDX,QWORD PTR [RIP+__afl_global_area_ptr@GOTPCREL]
            mov RDX,QWORD PTR [RDX]
            test RDX,RDX
            je __afl_setup_first

            mov QWORD PTR [RIP+__afl_area_ptr],RDX
            jmp __afl_store
__afl_setup_first:

            lea RSP,QWORD PTR [RSP-352]
            mov QWORD PTR [RSP],RAX
            mov QWORD PTR [RSP+8],RCX
            mov QWORD PTR [RSP+16],RDI
            mov QWORD PTR [RSP+32],RSI
            mov QWORD PTR [RSP+40],R8
            mov QWORD PTR [RSP+48],R9
            mov QWORD PTR [RSP+56],R10
            mov QWORD PTR [RSP+64],R11
            movq QWORD PTR [RSP+96],XMM0
            movq QWORD PTR [RSP+112],XMM1
            movq QWORD PTR [RSP+128],XMM2
            movq QWORD PTR [RSP+144],XMM3
            movq QWORD PTR [RSP+160],XMM4
            movq QWORD PTR [RSP+176],XMM5
            movq QWORD PTR [RSP+192],XMM6
            movq QWORD PTR [RSP+208],XMM7
            movq QWORD PTR [RSP+224],XMM8
            movq QWORD PTR [RSP+240],XMM9
            movq QWORD PTR [RSP+256],XMM10
            movq QWORD PTR [RSP+272],XMM11
            movq QWORD PTR [RSP+288],XMM12
            movq QWORD PTR [RSP+304],XMM13
            movq QWORD PTR [RSP+320],XMM14
            movq QWORD PTR [RSP+336],XMM15
            push R12
            mov R12,RSP
            sub RSP,16
            and RSP,-16
            lea RDI,QWORD PTR [RIP+.AFL_SHM_ENV]
            call getenv@PLT

            test RAX,RAX
            je __afl_setup_abort

            mov RDI,RAX
            call atoi@PLT

            xor RDX,RDX
            xor RSI,RSI
            mov RDI,RAX
            call shmat@PLT

            cmp RAX,-1
            je __afl_setup_abort

            mov BYTE PTR [RAX],1
            mov RDX,RAX
            mov QWORD PTR [RIP+__afl_area_ptr],RAX
            mov RDX,QWORD PTR [RIP+__afl_global_area_ptr@GOTPCREL]
            mov QWORD PTR [RDX],RAX
            mov RDX,RAX
__afl_forkserver:

            push RDX
            push RDX
            mov RDX,4
            lea RSI,QWORD PTR [RIP+__afl_temp]
            mov RDI,199
            call write@PLT

            cmp RAX,4
            jne __afl_fork_resume
__afl_fork_wait_loop:

            mov RDX,4
            lea RSI,QWORD PTR [RIP+__afl_temp]
            mov RDI,198
            call read@PLT

            cmp RAX,4
            jne __afl_die

            call fork@PLT

            cmp RAX,0
            jl __afl_die

            je __afl_fork_resume

            mov DWORD PTR [RIP+__afl_fork_pid],EAX
            mov RDX,4
            lea RSI,QWORD PTR [RIP+__afl_fork_pid]
            mov RDI,199
            call write@PLT

            mov RDX,0
            lea RSI,QWORD PTR [RIP+__afl_temp]
            mov RDI,QWORD PTR [RIP+__afl_fork_pid]
            call waitpid@PLT

            cmp RAX,0
            jle __afl_die

            mov RDX,4
            lea RSI,QWORD PTR [RIP+__afl_temp]
            mov RDI,199
            call write@PLT

            jmp __afl_fork_wait_loop
__afl_fork_resume:

            mov RDI,198
            call close@PLT

            mov RDI,199
            call close@PLT

            pop RDX
            pop RDX
            mov RSP,R12
            pop R12
            mov RAX,QWORD PTR [RSP]
            mov RCX,QWORD PTR [RSP+8]
            mov RDI,QWORD PTR [RSP+16]
            mov RSI,QWORD PTR [RSP+32]
            mov R8,QWORD PTR [RSP+40]
            mov R9,QWORD PTR [RSP+48]
            mov R10,QWORD PTR [RSP+56]
            mov R11,QWORD PTR [RSP+64]
            movq XMM0,QWORD PTR [RSP+96]
            movq XMM1,QWORD PTR [RSP+112]
            movq XMM2,QWORD PTR [RSP+128]
            movq XMM3,QWORD PTR [RSP+144]
            movq XMM4,QWORD PTR [RSP+160]
            movq XMM5,QWORD PTR [RSP+176]
            movq XMM6,QWORD PTR [RSP+192]
            movq XMM7,QWORD PTR [RSP+208]
            movq XMM8,QWORD PTR [RSP+224]
            movq XMM9,QWORD PTR [RSP+240]
            movq XMM10,QWORD PTR [RSP+256]
            movq XMM11,QWORD PTR [RSP+272]
            movq XMM12,QWORD PTR [RSP+288]
            movq XMM13,QWORD PTR [RSP+304]
            movq XMM14,QWORD PTR [RSP+320]
            movq XMM15,QWORD PTR [RSP+336]
            lea RSP,QWORD PTR [RSP+352]
            jmp __afl_store
__afl_die:

            xor RAX,RAX
            call _exit@PLT
__afl_setup_abort:

            inc BYTE PTR [RIP+__afl_setup_failure]
            mov RSP,R12
            pop R12
            mov RAX,QWORD PTR [RSP]
            mov RCX,QWORD PTR [RSP+8]
            mov RDI,QWORD PTR [RSP+16]
            mov RSI,QWORD PTR [RSP+32]
            mov R8,QWORD PTR [RSP+40]
            mov R9,QWORD PTR [RSP+48]
            mov R10,QWORD PTR [RSP+56]
            mov R11,QWORD PTR [RSP+64]
            movq XMM0,QWORD PTR [RSP+96]
            movq XMM1,QWORD PTR [RSP+112]
            movq XMM2,QWORD PTR [RSP+128]
            movq XMM3,QWORD PTR [RSP+144]
            movq XMM4,QWORD PTR [RSP+160]
            movq XMM5,QWORD PTR [RSP+176]
            movq XMM6,QWORD PTR [RSP+192]
            movq XMM7,QWORD PTR [RSP+208]
            movq XMM8,QWORD PTR [RSP+224]
            movq XMM9,QWORD PTR [RSP+240]
            movq XMM10,QWORD PTR [RSP+256]
            movq XMM11,QWORD PTR [RSP+272]
            movq XMM12,QWORD PTR [RSP+288]
            movq XMM13,QWORD PTR [RSP+304]
            movq XMM14,QWORD PTR [RSP+320]
            movq XMM15,QWORD PTR [RSP+336]
            lea RSP,QWORD PTR [RSP+352]
            jmp __afl_return
.align 1
          .byte 0xf
          .byte 0x1f
          .byte 0x80
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
.align 1
          .byte 0xf
          .byte 0x1f
          .byte 0x80
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
.align 2
          .byte 0x66
          .byte 0xf
          .byte 0x1f
          .byte 0x44
          .byte 0x0
          .byte 0x0
.align 1
          .byte 0xf
          .byte 0x1f
          .byte 0x80
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
.align 2
          .byte 0x66
          .byte 0x2e
          .byte 0xf
          .byte 0x1f
          .byte 0x84
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
.align 1
          .byte 0x66
          .byte 0x66
          .byte 0x2e
          .byte 0xf
          .byte 0x1f
          .byte 0x84
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0x0
          .byte 0xf
          .byte 0x1f
          .byte 0x40
          .byte 0x0
.align 8
#-----------------------------------
.globl vuln
.type vuln, @function
#-----------------------------------
vuln:

            lea RSP,QWORD PTR [RSP-152]
            mov QWORD PTR [RSP],RDX
            mov QWORD PTR [RSP+8],RCX
            mov QWORD PTR [RSP+16],RAX
            mov RCX,48786
            call __afl_maybe_log

            mov RAX,QWORD PTR [RSP+16]
            mov RCX,QWORD PTR [RSP+8]
            mov RDX,QWORD PTR [RSP]
            lea RSP,QWORD PTR [RSP+152]
            push RBP
            mov RBP,RSP
            sub RSP,16
            mov QWORD PTR [RBP-8],RDI
            call rand@PLT
.align 1

            lea RSP,QWORD PTR [RSP-152]
            mov QWORD PTR [RSP],RDX
            mov QWORD PTR [RSP+8],RCX
            mov QWORD PTR [RSP+16],RAX
            mov RCX,6772
            call __afl_maybe_log

            mov RAX,QWORD PTR [RSP+16]
            mov RCX,QWORD PTR [RSP+8]
            mov RDX,QWORD PTR [RSP]
            lea RSP,QWORD PTR [RSP+152]
            mov ECX,100
            cdq 
            idiv ECX
            add EDX,1
            mov DWORD PTR [RBP-12],EDX
            mov ESI,DWORD PTR [RBP-12]
            lea RDI,QWORD PTR [RIP+.L_2004]
            mov AL,0
            call printf@PLT
.align 8

            lea RSP,QWORD PTR [RSP-152]
            mov QWORD PTR [RSP],RDX
            mov QWORD PTR [RSP+8],RCX
            mov QWORD PTR [RSP+16],RAX
            mov RCX,30121
            call __afl_maybe_log

            mov RAX,QWORD PTR [RSP+16]
            mov RCX,QWORD PTR [RSP+8]
            mov RDX,QWORD PTR [RSP]
            lea RSP,QWORD PTR [RSP+152]
            mov RAX,QWORD PTR [RBP-8]
            movsx EAX,BYTE PTR [RAX]
            cmp EAX,67
            jne .L_1209
.align 8

            lea RSP,QWORD PTR [RSP-152]
            mov QWORD PTR [RSP],RDX
            mov QWORD PTR [RSP+8],RCX
            mov QWORD PTR [RSP+16],RAX
            mov RCX,9521
            call __afl_maybe_log

            mov RAX,QWORD PTR [RSP+16]
            mov RCX,QWORD PTR [RSP+8]
            mov RDX,QWORD PTR [RSP]
            lea RSP,QWORD PTR [RSP+152]
            cmp DWORD PTR [RBP-12],25
            jne .L_1209
.align 2

            lea RSP,QWORD PTR [RSP-152]
            mov QWORD PTR [RSP],RDX
            mov QWORD PTR [RSP+8],RCX
            mov QWORD PTR [RSP+16],RAX
            mov RCX,2303
            call __afl_maybe_log

            mov RAX,QWORD PTR [RSP+16]
            mov RCX,QWORD PTR [RSP+8]
            mov RDX,QWORD PTR [RSP]
            lea RSP,QWORD PTR [RSP+152]
            mov EDI,11
            call raise@PLT
.align 4

            lea RSP,QWORD PTR [RSP-152]
            mov QWORD PTR [RSP],RDX
            mov QWORD PTR [RSP+8],RCX
            mov QWORD PTR [RSP+16],RAX
            mov RCX,57207
            call __afl_maybe_log

            mov RAX,QWORD PTR [RSP+16]
            mov RCX,QWORD PTR [RSP+8]
            mov RDX,QWORD PTR [RSP]
            lea RSP,QWORD PTR [RSP+152]
            jmp .L_1245
.align 1
.L_1209:

            lea RSP,QWORD PTR [RSP-152]
            mov QWORD PTR [RSP],RDX
            mov QWORD PTR [RSP+8],RCX
            mov QWORD PTR [RSP+16],RAX
            mov RCX,43644
            call __afl_maybe_log

            mov RAX,QWORD PTR [RSP+16]
            mov RCX,QWORD PTR [RSP+8]
            mov RDX,QWORD PTR [RSP]
            lea RSP,QWORD PTR [RSP+152]
            mov RAX,QWORD PTR [RBP-8]
            movsx EAX,BYTE PTR [RAX]
            cmp EAX,70
            jne .L_1232
.align 1

            lea RSP,QWORD PTR [RSP-152]
            mov QWORD PTR [RSP],RDX
            mov QWORD PTR [RSP+8],RCX
            mov QWORD PTR [RSP+16],RAX
            mov RCX,9092
            call __afl_maybe_log

            mov RAX,QWORD PTR [RSP+16]
            mov RCX,QWORD PTR [RSP+8]
            mov RDX,QWORD PTR [RSP]
            lea RSP,QWORD PTR [RSP+152]
            cmp DWORD PTR [RBP-12],90
            jne .L_1232
.align 1

            lea RSP,QWORD PTR [RSP-152]
            mov QWORD PTR [RSP],RDX
            mov QWORD PTR [RSP+8],RCX
            mov QWORD PTR [RSP+16],RAX
            mov RCX,22700
            call __afl_maybe_log

            mov RAX,QWORD PTR [RSP+16]
            mov RCX,QWORD PTR [RSP+8]
            mov RDX,QWORD PTR [RSP]
            lea RSP,QWORD PTR [RSP+152]
            mov EDI,11
            call raise@PLT
.align 1

            lea RSP,QWORD PTR [RSP-152]
            mov QWORD PTR [RSP],RDX
            mov QWORD PTR [RSP+8],RCX
            mov QWORD PTR [RSP+16],RAX
            mov RCX,2507
            call __afl_maybe_log

            mov RAX,QWORD PTR [RSP+16]
            mov RCX,QWORD PTR [RSP+8]
            mov RDX,QWORD PTR [RSP]
            lea RSP,QWORD PTR [RSP+152]
            jmp .L_1240
.align 2
.L_1232:

            lea RSP,QWORD PTR [RSP-152]
            mov QWORD PTR [RSP],RDX
            mov QWORD PTR [RSP+8],RCX
            mov QWORD PTR [RSP+16],RAX
            mov RCX,32848
            call __afl_maybe_log

            mov RAX,QWORD PTR [RSP+16]
            mov RCX,QWORD PTR [RSP+8]
            mov RDX,QWORD PTR [RSP]
            lea RSP,QWORD PTR [RSP+152]
            lea RDI,QWORD PTR [RIP+.L_2022]
            mov AL,0
            call printf@PLT
.align 8
.L_1240:

            lea RSP,QWORD PTR [RSP-152]
            mov QWORD PTR [RSP],RDX
            mov QWORD PTR [RSP+8],RCX
            mov QWORD PTR [RSP+16],RAX
            mov RCX,16912
            call __afl_maybe_log

            mov RAX,QWORD PTR [RSP+16]
            mov RCX,QWORD PTR [RSP+8]
            mov RDX,QWORD PTR [RSP]
            lea RSP,QWORD PTR [RSP+152]
            jmp .L_1245
.align 1
.L_1245:

            lea RSP,QWORD PTR [RSP-152]
            mov QWORD PTR [RSP],RDX
            mov QWORD PTR [RSP+8],RCX
            mov QWORD PTR [RSP+16],RAX
            mov RCX,62350
            call __afl_maybe_log

            mov RAX,QWORD PTR [RSP+16]
            mov RCX,QWORD PTR [RSP+8]
            mov RDX,QWORD PTR [RSP]
            lea RSP,QWORD PTR [RSP+152]
            xor EAX,EAX
            add RSP,16
            pop RBP
            ret 
.align 1

            lea RSP,QWORD PTR [RSP-152]
            mov QWORD PTR [RSP],RDX
            mov QWORD PTR [RSP+8],RCX
            mov QWORD PTR [RSP+16],RAX
            mov RCX,1649
            call __afl_maybe_log

            mov RAX,QWORD PTR [RSP+16]
            mov RCX,QWORD PTR [RSP+8]
            mov RDX,QWORD PTR [RSP]
            lea RSP,QWORD PTR [RSP+152]
            nop
            nop
            nop
.align 8
#-----------------------------------
.globl main
.type main, @function
#-----------------------------------
main:

            lea RSP,QWORD PTR [RSP-152]
            mov QWORD PTR [RSP],RDX
            mov QWORD PTR [RSP+8],RCX
            mov QWORD PTR [RSP+16],RAX
            mov RCX,29442
            call __afl_maybe_log

            mov RAX,QWORD PTR [RSP+16]
            mov RCX,QWORD PTR [RSP+8]
            mov RDX,QWORD PTR [RSP]
            lea RSP,QWORD PTR [RSP+152]
            push RBP
            mov RBP,RSP
            sub RSP,80
            mov RAX,QWORD PTR FS:[40]
            mov QWORD PTR [RBP-8],RAX
            mov DWORD PTR [RBP-52],0
            mov DWORD PTR [RBP-56],EDI
            mov QWORD PTR [RBP-64],RSI
            lea RDI,QWORD PTR [RBP-48]
            xor ESI,ESI
            mov EDX,40
            call memset@PLT
.align 1

            lea RSP,QWORD PTR [RSP-152]
            mov QWORD PTR [RSP],RDX
            mov QWORD PTR [RSP+8],RCX
            mov QWORD PTR [RSP+16],RAX
            mov RCX,18897
            call __afl_maybe_log

            mov RAX,QWORD PTR [RSP+16]
            mov RCX,QWORD PTR [RSP+8]
            mov RDX,QWORD PTR [RSP]
            lea RSP,QWORD PTR [RSP+152]
            mov QWORD PTR [RBP-72],0
            mov RAX,QWORD PTR [RBP-64]
            mov RDI,QWORD PTR [RAX+8]
            lea RSI,QWORD PTR [RIP+.L_202f]
            call fopen@PLT
.align 1

            lea RSP,QWORD PTR [RSP-152]
            mov QWORD PTR [RSP],RDX
            mov QWORD PTR [RSP+8],RCX
            mov QWORD PTR [RSP+16],RAX
            mov RCX,33558
            call __afl_maybe_log

            mov RAX,QWORD PTR [RSP+16]
            mov RCX,QWORD PTR [RSP+8]
            mov RDX,QWORD PTR [RSP]
            lea RSP,QWORD PTR [RSP+152]
            mov QWORD PTR [RBP-72],RAX
            cmp QWORD PTR [RBP-72],0
            je .L_12ed
.align 2

            lea RSP,QWORD PTR [RSP-152]
            mov QWORD PTR [RSP],RDX
            mov QWORD PTR [RSP+8],RCX
            mov QWORD PTR [RSP+16],RAX
            mov RCX,41384
            call __afl_maybe_log

            mov RAX,QWORD PTR [RSP+16]
            mov RCX,QWORD PTR [RSP+8]
            mov RDX,QWORD PTR [RSP]
            lea RSP,QWORD PTR [RSP+152]
            mov RDI,QWORD PTR [RBP-72]
            lea RSI,QWORD PTR [RIP+.L_2031]
            lea RDX,QWORD PTR [RBP-48]
            mov AL,0
            call __isoc99_fscanf@PLT
.align 4

            lea RSP,QWORD PTR [RSP-152]
            mov QWORD PTR [RSP],RDX
            mov QWORD PTR [RSP+8],RCX
            mov QWORD PTR [RSP+16],RAX
            mov RCX,6867
            call __afl_maybe_log

            mov RAX,QWORD PTR [RSP+16]
            mov RCX,QWORD PTR [RSP+8]
            mov RDX,QWORD PTR [RSP]
            lea RSP,QWORD PTR [RSP+152]
            lea RSI,QWORD PTR [RBP-48]
            lea RDI,QWORD PTR [RIP+.L_2034]
            mov AL,0
            call printf@PLT
.align 2

            lea RSP,QWORD PTR [RSP-152]
            mov QWORD PTR [RSP],RDX
            mov QWORD PTR [RSP+8],RCX
            mov QWORD PTR [RSP+16],RAX
            mov RCX,63838
            call __afl_maybe_log

            mov RAX,QWORD PTR [RSP+16]
            mov RCX,QWORD PTR [RSP+8]
            mov RDX,QWORD PTR [RSP]
            lea RSP,QWORD PTR [RSP+152]
            lea RDI,QWORD PTR [RBP-48]
            call vuln
.align 1

            lea RSP,QWORD PTR [RSP-152]
            mov QWORD PTR [RSP],RDX
            mov QWORD PTR [RSP+8],RCX
            mov QWORD PTR [RSP+16],RAX
            mov RCX,24796
            call __afl_maybe_log

            mov RAX,QWORD PTR [RSP+16]
            mov RCX,QWORD PTR [RSP+8]
            mov RDX,QWORD PTR [RSP]
            lea RSP,QWORD PTR [RSP+152]
            mov RDI,QWORD PTR [RBP-72]
            call fclose@PLT
.align 8

            lea RSP,QWORD PTR [RSP-152]
            mov QWORD PTR [RSP],RDX
            mov QWORD PTR [RSP+8],RCX
            mov QWORD PTR [RSP+16],RAX
            mov RCX,22036
            call __afl_maybe_log

            mov RAX,QWORD PTR [RSP+16]
            mov RCX,QWORD PTR [RSP+8]
            mov RDX,QWORD PTR [RSP]
            lea RSP,QWORD PTR [RSP+152]
            jmp .L_12fb
.align 1
.L_12ed:

            lea RSP,QWORD PTR [RSP-152]
            mov QWORD PTR [RSP],RDX
            mov QWORD PTR [RSP+8],RCX
            mov QWORD PTR [RSP+16],RAX
            mov RCX,15382
            call __afl_maybe_log

            mov RAX,QWORD PTR [RSP+16]
            mov RCX,QWORD PTR [RSP+8]
            mov RDX,QWORD PTR [RSP]
            lea RSP,QWORD PTR [RSP+152]
            lea RDI,QWORD PTR [RIP+.L_203f]
            mov AL,0
            call printf@PLT
.align 1
.L_12fb:

            lea RSP,QWORD PTR [RSP-152]
            mov QWORD PTR [RSP],RDX
            mov QWORD PTR [RSP+8],RCX
            mov QWORD PTR [RSP+16],RAX
            mov RCX,42129
            call __afl_maybe_log

            mov RAX,QWORD PTR [RSP+16]
            mov RCX,QWORD PTR [RSP+8]
            mov RDX,QWORD PTR [RSP]
            lea RSP,QWORD PTR [RSP+152]
            mov RAX,QWORD PTR FS:[40]
            mov RCX,QWORD PTR [RBP-8]
            cmp RAX,RCX
            jne .L_1319
.align 1

            lea RSP,QWORD PTR [RSP-152]
            mov QWORD PTR [RSP],RDX
            mov QWORD PTR [RSP+8],RCX
            mov QWORD PTR [RSP+16],RAX
            mov RCX,60393
            call __afl_maybe_log

            mov RAX,QWORD PTR [RSP+16]
            mov RCX,QWORD PTR [RSP+8]
            mov RDX,QWORD PTR [RSP]
            lea RSP,QWORD PTR [RSP+152]
            xor EAX,EAX
            add RSP,80
            pop RBP
            ret 
.align 1
.L_1319:

            lea RSP,QWORD PTR [RSP-152]
            mov QWORD PTR [RSP],RDX
            mov QWORD PTR [RSP+8],RCX
            mov QWORD PTR [RSP+16],RAX
            mov RCX,58431
            call __afl_maybe_log

            mov RAX,QWORD PTR [RSP+16]
            mov RCX,QWORD PTR [RSP+8]
            mov RDX,QWORD PTR [RSP]
            lea RSP,QWORD PTR [RSP+152]
            call __stack_chk_fail@PLT
.align 2

            lea RSP,QWORD PTR [RSP-152]
            mov QWORD PTR [RSP],RDX
            mov QWORD PTR [RSP+8],RCX
            mov QWORD PTR [RSP+16],RAX
            mov RCX,5429
            call __afl_maybe_log

            mov RAX,QWORD PTR [RSP+16]
            mov RCX,QWORD PTR [RSP+8]
            mov RDX,QWORD PTR [RSP]
            lea RSP,QWORD PTR [RSP+152]
            nop
            nop
.AFL_SHM_ENV:
          .string "__AFL_SHM_ID"
#===================================
# end section .text
#===================================

#===================================
.section .interp ,"a",@progbits
#===================================

.align 1
          .byte 0x2f
          .byte 0x6c
          .byte 0x69
          .byte 0x62
          .byte 0x36
          .byte 0x34
          .byte 0x2f
          .byte 0x6c
          .byte 0x64
          .byte 0x2d
          .byte 0x6c
          .byte 0x69
          .byte 0x6e
          .byte 0x75
          .byte 0x78
          .byte 0x2d
          .byte 0x78
          .byte 0x38
          .byte 0x36
          .byte 0x2d
          .byte 0x36
          .byte 0x34
          .byte 0x2e
          .byte 0x73
          .byte 0x6f
          .byte 0x2e
          .byte 0x32
          .byte 0x0
#===================================
# end section .interp
#===================================

#===================================
.section .note.ABI-tag ,"a"
#===================================

.align 4
__abi_tag:
#===================================
# end section .note.ABI-tag
#===================================

#===================================
.section .rodata ,"a",@progbits
#===================================

.align 4
          .byte 0x1
          .byte 0x0
          .byte 0x2
          .byte 0x0
.L_2004:
          .string "Data is generated, num is %d\n"
.L_2022:
          .string "it is good!\n"
.L_202f:
          .string "r"
.L_2031:
          .string "%s"
.L_2034:
          .string "buf is %s\n"
.L_203f:
          .string "bad file!"
#===================================
# end section .rodata
#===================================

#===================================
.section .init_array ,"wa"
#===================================

.align 8
__init_array_start:
__frame_dummy_init_array_entry:
#===================================
# end section .init_array
#===================================

#===================================
.section .fini_array ,"wa"
#===================================

.align 8
__init_array_end:
__do_global_dtors_aux_fini_array_entry:
#===================================
# end section .fini_array
#===================================

#===================================
.data
#===================================

.align 8
#-----------------------------------
.weak data_start
.type data_start, @notype
#-----------------------------------
data_start:
          .zero 8
          .quad 0
#           : WARNING:0: no symbol for address 0x1bce 
#===================================
# end section .data
#===================================

#===================================
.bss
#===================================

.align 1
#-----------------------------------
.globl _edata
.type _edata, @notype
#-----------------------------------
_edata:
completed.0:
          .zero 8
#-----------------------------------
.globl _end
.type _end, @notype
#-----------------------------------
_end:
__afl_area_ptr:
          .zero 8
__afl_prev_loc:
          .zero 8
__afl_fork_pid:
          .zero 4
__afl_temp:
          .zero 4
__afl_setup_failure:
          .zero 1
#-----------------------------------
.globl __afl_global_area_ptr
.type __afl_global_area_ptr, @object
#-----------------------------------
__afl_global_area_ptr:
          .zero 8
#===================================
# end section .bss
#===================================
#-----------------------------------
.globl write
.type write, @function
#-----------------------------------
#-----------------------------------
.globl atoi
.type atoi, @function
#-----------------------------------
#-----------------------------------
.weak __gmon_start__
.type __gmon_start__, @notype
#-----------------------------------
#-----------------------------------
.globl fclose
.type fclose, @function
#-----------------------------------
#-----------------------------------
.weak _ITM_registerTMCloneTable
.type _ITM_registerTMCloneTable, @notype
#-----------------------------------
#-----------------------------------
.weak _ITM_deregisterTMCloneTable
.type _ITM_deregisterTMCloneTable, @notype
#-----------------------------------
#-----------------------------------
.globl memset
.type memset, @function
#-----------------------------------
#-----------------------------------
.globl waitpid
.type waitpid, @function
#-----------------------------------
#-----------------------------------
.globl close
.type close, @function
#-----------------------------------
#-----------------------------------
.globl printf
.type printf, @function
#-----------------------------------
#-----------------------------------
.globl __isoc99_fscanf
.type __isoc99_fscanf, @function
#-----------------------------------
#-----------------------------------
.globl raise
.type raise, @function
#-----------------------------------
#-----------------------------------
.globl _exit
.type _exit, @function
#-----------------------------------
#-----------------------------------
.globl __libc_start_main
.type __libc_start_main, @function
#-----------------------------------
#-----------------------------------
.globl rand
.type rand, @function
#-----------------------------------
#-----------------------------------
.globl __stack_chk_fail
.type __stack_chk_fail, @function
#-----------------------------------
#-----------------------------------
.weak __cxa_finalize
.type __cxa_finalize, @function
#-----------------------------------
#-----------------------------------
.globl getenv
.type getenv, @function
#-----------------------------------
#-----------------------------------
.globl fopen
.type fopen, @function
#-----------------------------------
#-----------------------------------
.globl read
.type read, @function
#-----------------------------------
#-----------------------------------
.globl shmat
.type shmat, @function
#-----------------------------------
#-----------------------------------
.globl fork
.type fork, @function
#-----------------------------------
