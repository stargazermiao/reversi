
#include <iostream>
#include <ostream>
#include <vector>
#include "z3++.h"

int main() {
    std::vector<int> assigned_hash_vec = {};
    z3::context ctx;
    z3::expr x = z3::zext(ctx.bv_const("x", 16), 16);
    z3::expr y = z3::zext(ctx.bv_const("y", 16), 16);
    z3::expr z = z3::zext(ctx.bv_const("z", 16), 16);
    z3::expr hash_val =  (z3::lshr(3421, x) ^ (z3::lshr(2231, y))) + z;
    z3::solver _solver(ctx);
    _solver.add(x > 0);
    _solver.add(x < 16);
    _solver.add(y > 0);
    _solver.add(y < 16);
    _solver.add(z > 0);
    _solver.add(z < 16);
    _solver.add(hash_val < 65536);
    // exclude used hash
    for (auto used: assigned_hash_vec) {
        _solver.add(hash_val != used);
    }
    auto _solver_res = _solver.check();
    if (_solver_res != z3::sat) {
        std::cout << "unsat" << std::endl;
        return 0;
    }
    auto m = _solver.get_model();
    std::cout << m.eval(x).get_numeral_int() << std::endl;
    std::cout << m.eval(y).get_numeral_int() << std::endl;
    std::cout << m.eval(z).get_numeral_int() << std::endl;
    std::cout << m.eval(hash_val).get_numeral_int() << std::endl;
    // if(_solver_res == z3::unsat or _solver_res == z3::unknown) {
    //     // [-1, -1, -1, -1] means not found
    //     souffle::RamDomain not_found[4] = {-1, -1, -1, -1}; 
    //     return recordTable->pack(not_found, 4);
    // } else {
    //     z3::model m = _solver.get_model();
    //     int32_t solved_x = m.eval(x).get_numeral_int();
    //     int32_t solved_y = m.eval(y).get_numeral_int();
    //     int32_t solved_z = m.eval(z).get_numeral_int();
    //     int32_t solved_hashv = m.eval(hash_val).get_numeral_int();
    //     souffle::RamDomain solved_res[4] = {solved_x, solved_y, solved_z, solved_hashv};
    //     return recordTable->pack(solved_res, 4);
    // }
    return 0;
}
