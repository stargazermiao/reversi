
#include "souffle/CompiledSouffle.h"

extern "C" {}

namespace souffle {
static const RamDomain RAM_BIT_SHIFT_MASK = RAM_DOMAIN_SIZE - 1;
struct t_btree_uui__0_1_2__111 {
  using t_tuple = Tuple<RamDomain, 3>;
  struct t_comparator_0 {
    int operator()(const t_tuple &a, const t_tuple &b) const {
      return (ramBitCast<RamUnsigned>(a[0]) < ramBitCast<RamUnsigned>(b[0]))
                 ? -1
             : (ramBitCast<RamUnsigned>(a[0]) > ramBitCast<RamUnsigned>(b[0]))
                 ? 1
                 : ((ramBitCast<RamUnsigned>(a[1]) <
                     ramBitCast<RamUnsigned>(b[1]))
                        ? -1
                    : (ramBitCast<RamUnsigned>(a[1]) >
                       ramBitCast<RamUnsigned>(b[1]))
                        ? 1
                        : ((ramBitCast<RamSigned>(a[2]) <
                            ramBitCast<RamSigned>(b[2]))
                               ? -1
                           : (ramBitCast<RamSigned>(a[2]) >
                              ramBitCast<RamSigned>(b[2]))
                               ? 1
                               : (0)));
    }
    bool less(const t_tuple &a, const t_tuple &b) const {
      return (ramBitCast<RamUnsigned>(a[0]) < ramBitCast<RamUnsigned>(b[0])) ||
             (ramBitCast<RamUnsigned>(a[0]) == ramBitCast<RamUnsigned>(b[0])) &&
                 ((ramBitCast<RamUnsigned>(a[1]) <
                   ramBitCast<RamUnsigned>(b[1])) ||
                  (ramBitCast<RamUnsigned>(a[1]) ==
                   ramBitCast<RamUnsigned>(b[1])) &&
                      ((ramBitCast<RamSigned>(a[2]) <
                        ramBitCast<RamSigned>(b[2]))));
    }
    bool equal(const t_tuple &a, const t_tuple &b) const {
      return (ramBitCast<RamUnsigned>(a[0]) == ramBitCast<RamUnsigned>(b[0])) &&
             (ramBitCast<RamUnsigned>(a[1]) == ramBitCast<RamUnsigned>(b[1])) &&
             (ramBitCast<RamSigned>(a[2]) == ramBitCast<RamSigned>(b[2]));
    }
  };
  using t_ind_0 = btree_set<t_tuple, t_comparator_0>;
  t_ind_0 ind_0;
  using iterator = t_ind_0::iterator;
  struct context {
    t_ind_0::operation_hints hints_0_lower;
    t_ind_0::operation_hints hints_0_upper;
  };
  context createContext() { return context(); }
  bool insert(const t_tuple &t) {
    context h;
    return insert(t, h);
  }
  bool insert(const t_tuple &t, context &h) {
    if (ind_0.insert(t, h.hints_0_lower)) {
      return true;
    } else
      return false;
  }
  bool insert(const RamDomain *ramDomain) {
    RamDomain data[3];
    std::copy(ramDomain, ramDomain + 3, data);
    const t_tuple &tuple = reinterpret_cast<const t_tuple &>(data);
    context h;
    return insert(tuple, h);
  }
  bool insert(RamDomain a0, RamDomain a1, RamDomain a2) {
    RamDomain data[3] = {a0, a1, a2};
    return insert(data);
  }
  bool contains(const t_tuple &t, context &h) const {
    return ind_0.contains(t, h.hints_0_lower);
  }
  bool contains(const t_tuple &t) const {
    context h;
    return contains(t, h);
  }
  std::size_t size() const { return ind_0.size(); }
  iterator find(const t_tuple &t, context &h) const {
    return ind_0.find(t, h.hints_0_lower);
  }
  iterator find(const t_tuple &t) const {
    context h;
    return find(t, h);
  }
  range<iterator> lowerUpperRange_000(const t_tuple & /* lower */,
                                      const t_tuple & /* upper */,
                                      context & /* h */) const {
    return range<iterator>(ind_0.begin(), ind_0.end());
  }
  range<iterator> lowerUpperRange_000(const t_tuple & /* lower */,
                                      const t_tuple & /* upper */) const {
    return range<iterator>(ind_0.begin(), ind_0.end());
  }
  range<t_ind_0::iterator> lowerUpperRange_111(const t_tuple &lower,
                                               const t_tuple &upper,
                                               context &h) const {
    t_comparator_0 comparator;
    int cmp = comparator(lower, upper);
    if (cmp == 0) {
      auto pos = ind_0.find(lower, h.hints_0_lower);
      auto fin = ind_0.end();
      if (pos != fin) {
        fin = pos;
        ++fin;
      }
      return make_range(pos, fin);
    }
    if (cmp > 0) {
      return make_range(ind_0.end(), ind_0.end());
    }
    return make_range(ind_0.lower_bound(lower, h.hints_0_lower),
                      ind_0.upper_bound(upper, h.hints_0_upper));
  }
  range<t_ind_0::iterator> lowerUpperRange_111(const t_tuple &lower,
                                               const t_tuple &upper) const {
    context h;
    return lowerUpperRange_111(lower, upper, h);
  }
  bool empty() const { return ind_0.empty(); }
  std::vector<range<iterator>> partition() const {
    return ind_0.getChunks(400);
  }
  void purge() { ind_0.clear(); }
  iterator begin() const { return ind_0.begin(); }
  iterator end() const { return ind_0.end(); }
  void printStatistics(std::ostream &o) const {
    o << " arity 3 direct b-tree index 0 lex-order [0,1,2]\n";
    ind_0.printStats(o);
  }
};
struct t_btree_u__0__1 {
  using t_tuple = Tuple<RamDomain, 1>;
  struct t_comparator_0 {
    int operator()(const t_tuple &a, const t_tuple &b) const {
      return (ramBitCast<RamUnsigned>(a[0]) < ramBitCast<RamUnsigned>(b[0]))
                 ? -1
             : (ramBitCast<RamUnsigned>(a[0]) > ramBitCast<RamUnsigned>(b[0]))
                 ? 1
                 : (0);
    }
    bool less(const t_tuple &a, const t_tuple &b) const {
      return (ramBitCast<RamUnsigned>(a[0]) < ramBitCast<RamUnsigned>(b[0]));
    }
    bool equal(const t_tuple &a, const t_tuple &b) const {
      return (ramBitCast<RamUnsigned>(a[0]) == ramBitCast<RamUnsigned>(b[0]));
    }
  };
  using t_ind_0 = btree_set<t_tuple, t_comparator_0>;
  t_ind_0 ind_0;
  using iterator = t_ind_0::iterator;
  struct context {
    t_ind_0::operation_hints hints_0_lower;
    t_ind_0::operation_hints hints_0_upper;
  };
  context createContext() { return context(); }
  bool insert(const t_tuple &t) {
    context h;
    return insert(t, h);
  }
  bool insert(const t_tuple &t, context &h) {
    if (ind_0.insert(t, h.hints_0_lower)) {
      return true;
    } else
      return false;
  }
  bool insert(const RamDomain *ramDomain) {
    RamDomain data[1];
    std::copy(ramDomain, ramDomain + 1, data);
    const t_tuple &tuple = reinterpret_cast<const t_tuple &>(data);
    context h;
    return insert(tuple, h);
  }
  bool insert(RamDomain a0) {
    RamDomain data[1] = {a0};
    return insert(data);
  }
  bool contains(const t_tuple &t, context &h) const {
    return ind_0.contains(t, h.hints_0_lower);
  }
  bool contains(const t_tuple &t) const {
    context h;
    return contains(t, h);
  }
  std::size_t size() const { return ind_0.size(); }
  iterator find(const t_tuple &t, context &h) const {
    return ind_0.find(t, h.hints_0_lower);
  }
  iterator find(const t_tuple &t) const {
    context h;
    return find(t, h);
  }
  range<iterator> lowerUpperRange_0(const t_tuple & /* lower */,
                                    const t_tuple & /* upper */,
                                    context & /* h */) const {
    return range<iterator>(ind_0.begin(), ind_0.end());
  }
  range<iterator> lowerUpperRange_0(const t_tuple & /* lower */,
                                    const t_tuple & /* upper */) const {
    return range<iterator>(ind_0.begin(), ind_0.end());
  }
  range<t_ind_0::iterator> lowerUpperRange_1(const t_tuple &lower,
                                             const t_tuple &upper,
                                             context &h) const {
    t_comparator_0 comparator;
    int cmp = comparator(lower, upper);
    if (cmp == 0) {
      auto pos = ind_0.find(lower, h.hints_0_lower);
      auto fin = ind_0.end();
      if (pos != fin) {
        fin = pos;
        ++fin;
      }
      return make_range(pos, fin);
    }
    if (cmp > 0) {
      return make_range(ind_0.end(), ind_0.end());
    }
    return make_range(ind_0.lower_bound(lower, h.hints_0_lower),
                      ind_0.upper_bound(upper, h.hints_0_upper));
  }
  range<t_ind_0::iterator> lowerUpperRange_1(const t_tuple &lower,
                                             const t_tuple &upper) const {
    context h;
    return lowerUpperRange_1(lower, upper, h);
  }
  bool empty() const { return ind_0.empty(); }
  std::vector<range<iterator>> partition() const {
    return ind_0.getChunks(400);
  }
  void purge() { ind_0.clear(); }
  iterator begin() const { return ind_0.begin(); }
  iterator end() const { return ind_0.end(); }
  void printStatistics(std::ostream &o) const {
    o << " arity 1 direct b-tree index 0 lex-order [0]\n";
    ind_0.printStats(o);
  }
};
struct t_btree_uui__0_2_1__101__111 {
  using t_tuple = Tuple<RamDomain, 3>;
  struct t_comparator_0 {
    int operator()(const t_tuple &a, const t_tuple &b) const {
      return (ramBitCast<RamUnsigned>(a[0]) < ramBitCast<RamUnsigned>(b[0]))
                 ? -1
             : (ramBitCast<RamUnsigned>(a[0]) > ramBitCast<RamUnsigned>(b[0]))
                 ? 1
                 : ((ramBitCast<RamSigned>(a[2]) < ramBitCast<RamSigned>(b[2]))
                        ? -1
                    : (ramBitCast<RamSigned>(a[2]) >
                       ramBitCast<RamSigned>(b[2]))
                        ? 1
                        : ((ramBitCast<RamUnsigned>(a[1]) <
                            ramBitCast<RamUnsigned>(b[1]))
                               ? -1
                           : (ramBitCast<RamUnsigned>(a[1]) >
                              ramBitCast<RamUnsigned>(b[1]))
                               ? 1
                               : (0)));
    }
    bool less(const t_tuple &a, const t_tuple &b) const {
      return (ramBitCast<RamUnsigned>(a[0]) < ramBitCast<RamUnsigned>(b[0])) ||
             (ramBitCast<RamUnsigned>(a[0]) == ramBitCast<RamUnsigned>(b[0])) &&
                 ((ramBitCast<RamSigned>(a[2]) < ramBitCast<RamSigned>(b[2])) ||
                  (ramBitCast<RamSigned>(a[2]) ==
                   ramBitCast<RamSigned>(b[2])) &&
                      ((ramBitCast<RamUnsigned>(a[1]) <
                        ramBitCast<RamUnsigned>(b[1]))));
    }
    bool equal(const t_tuple &a, const t_tuple &b) const {
      return (ramBitCast<RamUnsigned>(a[0]) == ramBitCast<RamUnsigned>(b[0])) &&
             (ramBitCast<RamSigned>(a[2]) == ramBitCast<RamSigned>(b[2])) &&
             (ramBitCast<RamUnsigned>(a[1]) == ramBitCast<RamUnsigned>(b[1]));
    }
  };
  using t_ind_0 = btree_set<t_tuple, t_comparator_0>;
  t_ind_0 ind_0;
  using iterator = t_ind_0::iterator;
  struct context {
    t_ind_0::operation_hints hints_0_lower;
    t_ind_0::operation_hints hints_0_upper;
  };
  context createContext() { return context(); }
  bool insert(const t_tuple &t) {
    context h;
    return insert(t, h);
  }
  bool insert(const t_tuple &t, context &h) {
    if (ind_0.insert(t, h.hints_0_lower)) {
      return true;
    } else
      return false;
  }
  bool insert(const RamDomain *ramDomain) {
    RamDomain data[3];
    std::copy(ramDomain, ramDomain + 3, data);
    const t_tuple &tuple = reinterpret_cast<const t_tuple &>(data);
    context h;
    return insert(tuple, h);
  }
  bool insert(RamDomain a0, RamDomain a1, RamDomain a2) {
    RamDomain data[3] = {a0, a1, a2};
    return insert(data);
  }
  bool contains(const t_tuple &t, context &h) const {
    return ind_0.contains(t, h.hints_0_lower);
  }
  bool contains(const t_tuple &t) const {
    context h;
    return contains(t, h);
  }
  std::size_t size() const { return ind_0.size(); }
  iterator find(const t_tuple &t, context &h) const {
    return ind_0.find(t, h.hints_0_lower);
  }
  iterator find(const t_tuple &t) const {
    context h;
    return find(t, h);
  }
  range<iterator> lowerUpperRange_000(const t_tuple & /* lower */,
                                      const t_tuple & /* upper */,
                                      context & /* h */) const {
    return range<iterator>(ind_0.begin(), ind_0.end());
  }
  range<iterator> lowerUpperRange_000(const t_tuple & /* lower */,
                                      const t_tuple & /* upper */) const {
    return range<iterator>(ind_0.begin(), ind_0.end());
  }
  range<t_ind_0::iterator> lowerUpperRange_101(const t_tuple &lower,
                                               const t_tuple &upper,
                                               context &h) const {
    t_comparator_0 comparator;
    int cmp = comparator(lower, upper);
    if (cmp > 0) {
      return make_range(ind_0.end(), ind_0.end());
    }
    return make_range(ind_0.lower_bound(lower, h.hints_0_lower),
                      ind_0.upper_bound(upper, h.hints_0_upper));
  }
  range<t_ind_0::iterator> lowerUpperRange_101(const t_tuple &lower,
                                               const t_tuple &upper) const {
    context h;
    return lowerUpperRange_101(lower, upper, h);
  }
  range<t_ind_0::iterator> lowerUpperRange_111(const t_tuple &lower,
                                               const t_tuple &upper,
                                               context &h) const {
    t_comparator_0 comparator;
    int cmp = comparator(lower, upper);
    if (cmp == 0) {
      auto pos = ind_0.find(lower, h.hints_0_lower);
      auto fin = ind_0.end();
      if (pos != fin) {
        fin = pos;
        ++fin;
      }
      return make_range(pos, fin);
    }
    if (cmp > 0) {
      return make_range(ind_0.end(), ind_0.end());
    }
    return make_range(ind_0.lower_bound(lower, h.hints_0_lower),
                      ind_0.upper_bound(upper, h.hints_0_upper));
  }
  range<t_ind_0::iterator> lowerUpperRange_111(const t_tuple &lower,
                                               const t_tuple &upper) const {
    context h;
    return lowerUpperRange_111(lower, upper, h);
  }
  bool empty() const { return ind_0.empty(); }
  std::vector<range<iterator>> partition() const {
    return ind_0.getChunks(400);
  }
  void purge() { ind_0.clear(); }
  iterator begin() const { return ind_0.begin(); }
  iterator end() const { return ind_0.end(); }
  void printStatistics(std::ostream &o) const {
    o << " arity 3 direct b-tree index 0 lex-order [0,2,1]\n";
    ind_0.printStats(o);
  }
};
struct t_btree_uu__0_1__11__10 {
  using t_tuple = Tuple<RamDomain, 2>;
  struct t_comparator_0 {
    int operator()(const t_tuple &a, const t_tuple &b) const {
      return (ramBitCast<RamUnsigned>(a[0]) < ramBitCast<RamUnsigned>(b[0]))
                 ? -1
             : (ramBitCast<RamUnsigned>(a[0]) > ramBitCast<RamUnsigned>(b[0]))
                 ? 1
                 : ((ramBitCast<RamUnsigned>(a[1]) <
                     ramBitCast<RamUnsigned>(b[1]))
                        ? -1
                    : (ramBitCast<RamUnsigned>(a[1]) >
                       ramBitCast<RamUnsigned>(b[1]))
                        ? 1
                        : (0));
    }
    bool less(const t_tuple &a, const t_tuple &b) const {
      return (ramBitCast<RamUnsigned>(a[0]) < ramBitCast<RamUnsigned>(b[0])) ||
             (ramBitCast<RamUnsigned>(a[0]) == ramBitCast<RamUnsigned>(b[0])) &&
                 ((ramBitCast<RamUnsigned>(a[1]) <
                   ramBitCast<RamUnsigned>(b[1])));
    }
    bool equal(const t_tuple &a, const t_tuple &b) const {
      return (ramBitCast<RamUnsigned>(a[0]) == ramBitCast<RamUnsigned>(b[0])) &&
             (ramBitCast<RamUnsigned>(a[1]) == ramBitCast<RamUnsigned>(b[1]));
    }
  };
  using t_ind_0 = btree_set<t_tuple, t_comparator_0>;
  t_ind_0 ind_0;
  using iterator = t_ind_0::iterator;
  struct context {
    t_ind_0::operation_hints hints_0_lower;
    t_ind_0::operation_hints hints_0_upper;
  };
  context createContext() { return context(); }
  bool insert(const t_tuple &t) {
    context h;
    return insert(t, h);
  }
  bool insert(const t_tuple &t, context &h) {
    if (ind_0.insert(t, h.hints_0_lower)) {
      return true;
    } else
      return false;
  }
  bool insert(const RamDomain *ramDomain) {
    RamDomain data[2];
    std::copy(ramDomain, ramDomain + 2, data);
    const t_tuple &tuple = reinterpret_cast<const t_tuple &>(data);
    context h;
    return insert(tuple, h);
  }
  bool insert(RamDomain a0, RamDomain a1) {
    RamDomain data[2] = {a0, a1};
    return insert(data);
  }
  bool contains(const t_tuple &t, context &h) const {
    return ind_0.contains(t, h.hints_0_lower);
  }
  bool contains(const t_tuple &t) const {
    context h;
    return contains(t, h);
  }
  std::size_t size() const { return ind_0.size(); }
  iterator find(const t_tuple &t, context &h) const {
    return ind_0.find(t, h.hints_0_lower);
  }
  iterator find(const t_tuple &t) const {
    context h;
    return find(t, h);
  }
  range<iterator> lowerUpperRange_00(const t_tuple & /* lower */,
                                     const t_tuple & /* upper */,
                                     context & /* h */) const {
    return range<iterator>(ind_0.begin(), ind_0.end());
  }
  range<iterator> lowerUpperRange_00(const t_tuple & /* lower */,
                                     const t_tuple & /* upper */) const {
    return range<iterator>(ind_0.begin(), ind_0.end());
  }
  range<t_ind_0::iterator> lowerUpperRange_11(const t_tuple &lower,
                                              const t_tuple &upper,
                                              context &h) const {
    t_comparator_0 comparator;
    int cmp = comparator(lower, upper);
    if (cmp == 0) {
      auto pos = ind_0.find(lower, h.hints_0_lower);
      auto fin = ind_0.end();
      if (pos != fin) {
        fin = pos;
        ++fin;
      }
      return make_range(pos, fin);
    }
    if (cmp > 0) {
      return make_range(ind_0.end(), ind_0.end());
    }
    return make_range(ind_0.lower_bound(lower, h.hints_0_lower),
                      ind_0.upper_bound(upper, h.hints_0_upper));
  }
  range<t_ind_0::iterator> lowerUpperRange_11(const t_tuple &lower,
                                              const t_tuple &upper) const {
    context h;
    return lowerUpperRange_11(lower, upper, h);
  }
  range<t_ind_0::iterator> lowerUpperRange_10(const t_tuple &lower,
                                              const t_tuple &upper,
                                              context &h) const {
    t_comparator_0 comparator;
    int cmp = comparator(lower, upper);
    if (cmp > 0) {
      return make_range(ind_0.end(), ind_0.end());
    }
    return make_range(ind_0.lower_bound(lower, h.hints_0_lower),
                      ind_0.upper_bound(upper, h.hints_0_upper));
  }
  range<t_ind_0::iterator> lowerUpperRange_10(const t_tuple &lower,
                                              const t_tuple &upper) const {
    context h;
    return lowerUpperRange_10(lower, upper, h);
  }
  bool empty() const { return ind_0.empty(); }
  std::vector<range<iterator>> partition() const {
    return ind_0.getChunks(400);
  }
  void purge() { ind_0.clear(); }
  iterator begin() const { return ind_0.begin(); }
  iterator end() const { return ind_0.end(); }
  void printStatistics(std::ostream &o) const {
    o << " arity 2 direct b-tree index 0 lex-order [0,1]\n";
    ind_0.printStats(o);
  }
};
struct t_btree_i__0__1 {
  using t_tuple = Tuple<RamDomain, 1>;
  struct t_comparator_0 {
    int operator()(const t_tuple &a, const t_tuple &b) const {
      return (ramBitCast<RamSigned>(a[0]) < ramBitCast<RamSigned>(b[0])) ? -1
             : (ramBitCast<RamSigned>(a[0]) > ramBitCast<RamSigned>(b[0]))
                 ? 1
                 : (0);
    }
    bool less(const t_tuple &a, const t_tuple &b) const {
      return (ramBitCast<RamSigned>(a[0]) < ramBitCast<RamSigned>(b[0]));
    }
    bool equal(const t_tuple &a, const t_tuple &b) const {
      return (ramBitCast<RamSigned>(a[0]) == ramBitCast<RamSigned>(b[0]));
    }
  };
  using t_ind_0 = btree_set<t_tuple, t_comparator_0>;
  t_ind_0 ind_0;
  using iterator = t_ind_0::iterator;
  struct context {
    t_ind_0::operation_hints hints_0_lower;
    t_ind_0::operation_hints hints_0_upper;
  };
  context createContext() { return context(); }
  bool insert(const t_tuple &t) {
    context h;
    return insert(t, h);
  }
  bool insert(const t_tuple &t, context &h) {
    if (ind_0.insert(t, h.hints_0_lower)) {
      return true;
    } else
      return false;
  }
  bool insert(const RamDomain *ramDomain) {
    RamDomain data[1];
    std::copy(ramDomain, ramDomain + 1, data);
    const t_tuple &tuple = reinterpret_cast<const t_tuple &>(data);
    context h;
    return insert(tuple, h);
  }
  bool insert(RamDomain a0) {
    RamDomain data[1] = {a0};
    return insert(data);
  }
  bool contains(const t_tuple &t, context &h) const {
    return ind_0.contains(t, h.hints_0_lower);
  }
  bool contains(const t_tuple &t) const {
    context h;
    return contains(t, h);
  }
  std::size_t size() const { return ind_0.size(); }
  iterator find(const t_tuple &t, context &h) const {
    return ind_0.find(t, h.hints_0_lower);
  }
  iterator find(const t_tuple &t) const {
    context h;
    return find(t, h);
  }
  range<iterator> lowerUpperRange_0(const t_tuple & /* lower */,
                                    const t_tuple & /* upper */,
                                    context & /* h */) const {
    return range<iterator>(ind_0.begin(), ind_0.end());
  }
  range<iterator> lowerUpperRange_0(const t_tuple & /* lower */,
                                    const t_tuple & /* upper */) const {
    return range<iterator>(ind_0.begin(), ind_0.end());
  }
  range<t_ind_0::iterator> lowerUpperRange_1(const t_tuple &lower,
                                             const t_tuple &upper,
                                             context &h) const {
    t_comparator_0 comparator;
    int cmp = comparator(lower, upper);
    if (cmp == 0) {
      auto pos = ind_0.find(lower, h.hints_0_lower);
      auto fin = ind_0.end();
      if (pos != fin) {
        fin = pos;
        ++fin;
      }
      return make_range(pos, fin);
    }
    if (cmp > 0) {
      return make_range(ind_0.end(), ind_0.end());
    }
    return make_range(ind_0.lower_bound(lower, h.hints_0_lower),
                      ind_0.upper_bound(upper, h.hints_0_upper));
  }
  range<t_ind_0::iterator> lowerUpperRange_1(const t_tuple &lower,
                                             const t_tuple &upper) const {
    context h;
    return lowerUpperRange_1(lower, upper, h);
  }
  bool empty() const { return ind_0.empty(); }
  std::vector<range<iterator>> partition() const {
    return ind_0.getChunks(400);
  }
  void purge() { ind_0.clear(); }
  iterator begin() const { return ind_0.begin(); }
  iterator end() const { return ind_0.end(); }
  void printStatistics(std::ostream &o) const {
    o << " arity 1 direct b-tree index 0 lex-order [0]\n";
    ind_0.printStats(o);
  }
};
struct t_btree_uiui__0_1_2_3__1000__1111 {
  using t_tuple = Tuple<RamDomain, 4>;
  struct t_comparator_0 {
    int operator()(const t_tuple &a, const t_tuple &b) const {
      return (ramBitCast<RamUnsigned>(a[0]) < ramBitCast<RamUnsigned>(b[0]))
                 ? -1
             : (ramBitCast<RamUnsigned>(a[0]) > ramBitCast<RamUnsigned>(b[0]))
                 ? 1
                 : ((ramBitCast<RamSigned>(a[1]) < ramBitCast<RamSigned>(b[1]))
                        ? -1
                    : (ramBitCast<RamSigned>(a[1]) >
                       ramBitCast<RamSigned>(b[1]))
                        ? 1
                        : ((ramBitCast<RamUnsigned>(a[2]) <
                            ramBitCast<RamUnsigned>(b[2]))
                               ? -1
                           : (ramBitCast<RamUnsigned>(a[2]) >
                              ramBitCast<RamUnsigned>(b[2]))
                               ? 1
                               : ((ramBitCast<RamSigned>(a[3]) <
                                   ramBitCast<RamSigned>(b[3]))
                                      ? -1
                                  : (ramBitCast<RamSigned>(a[3]) >
                                     ramBitCast<RamSigned>(b[3]))
                                      ? 1
                                      : (0))));
    }
    bool less(const t_tuple &a, const t_tuple &b) const {
      return (ramBitCast<RamUnsigned>(a[0]) < ramBitCast<RamUnsigned>(b[0])) ||
             (ramBitCast<RamUnsigned>(a[0]) == ramBitCast<RamUnsigned>(b[0])) &&
                 ((ramBitCast<RamSigned>(a[1]) < ramBitCast<RamSigned>(b[1])) ||
                  (ramBitCast<RamSigned>(a[1]) ==
                   ramBitCast<RamSigned>(b[1])) &&
                      ((ramBitCast<RamUnsigned>(a[2]) <
                        ramBitCast<RamUnsigned>(b[2])) ||
                       (ramBitCast<RamUnsigned>(a[2]) ==
                        ramBitCast<RamUnsigned>(b[2])) &&
                           ((ramBitCast<RamSigned>(a[3]) <
                             ramBitCast<RamSigned>(b[3])))));
    }
    bool equal(const t_tuple &a, const t_tuple &b) const {
      return (ramBitCast<RamUnsigned>(a[0]) == ramBitCast<RamUnsigned>(b[0])) &&
             (ramBitCast<RamSigned>(a[1]) == ramBitCast<RamSigned>(b[1])) &&
             (ramBitCast<RamUnsigned>(a[2]) == ramBitCast<RamUnsigned>(b[2])) &&
             (ramBitCast<RamSigned>(a[3]) == ramBitCast<RamSigned>(b[3]));
    }
  };
  using t_ind_0 = btree_set<t_tuple, t_comparator_0>;
  t_ind_0 ind_0;
  using iterator = t_ind_0::iterator;
  struct context {
    t_ind_0::operation_hints hints_0_lower;
    t_ind_0::operation_hints hints_0_upper;
  };
  context createContext() { return context(); }
  bool insert(const t_tuple &t) {
    context h;
    return insert(t, h);
  }
  bool insert(const t_tuple &t, context &h) {
    if (ind_0.insert(t, h.hints_0_lower)) {
      return true;
    } else
      return false;
  }
  bool insert(const RamDomain *ramDomain) {
    RamDomain data[4];
    std::copy(ramDomain, ramDomain + 4, data);
    const t_tuple &tuple = reinterpret_cast<const t_tuple &>(data);
    context h;
    return insert(tuple, h);
  }
  bool insert(RamDomain a0, RamDomain a1, RamDomain a2, RamDomain a3) {
    RamDomain data[4] = {a0, a1, a2, a3};
    return insert(data);
  }
  bool contains(const t_tuple &t, context &h) const {
    return ind_0.contains(t, h.hints_0_lower);
  }
  bool contains(const t_tuple &t) const {
    context h;
    return contains(t, h);
  }
  std::size_t size() const { return ind_0.size(); }
  iterator find(const t_tuple &t, context &h) const {
    return ind_0.find(t, h.hints_0_lower);
  }
  iterator find(const t_tuple &t) const {
    context h;
    return find(t, h);
  }
  range<iterator> lowerUpperRange_0000(const t_tuple & /* lower */,
                                       const t_tuple & /* upper */,
                                       context & /* h */) const {
    return range<iterator>(ind_0.begin(), ind_0.end());
  }
  range<iterator> lowerUpperRange_0000(const t_tuple & /* lower */,
                                       const t_tuple & /* upper */) const {
    return range<iterator>(ind_0.begin(), ind_0.end());
  }
  range<t_ind_0::iterator> lowerUpperRange_1000(const t_tuple &lower,
                                                const t_tuple &upper,
                                                context &h) const {
    t_comparator_0 comparator;
    int cmp = comparator(lower, upper);
    if (cmp > 0) {
      return make_range(ind_0.end(), ind_0.end());
    }
    return make_range(ind_0.lower_bound(lower, h.hints_0_lower),
                      ind_0.upper_bound(upper, h.hints_0_upper));
  }
  range<t_ind_0::iterator> lowerUpperRange_1000(const t_tuple &lower,
                                                const t_tuple &upper) const {
    context h;
    return lowerUpperRange_1000(lower, upper, h);
  }
  range<t_ind_0::iterator> lowerUpperRange_1111(const t_tuple &lower,
                                                const t_tuple &upper,
                                                context &h) const {
    t_comparator_0 comparator;
    int cmp = comparator(lower, upper);
    if (cmp == 0) {
      auto pos = ind_0.find(lower, h.hints_0_lower);
      auto fin = ind_0.end();
      if (pos != fin) {
        fin = pos;
        ++fin;
      }
      return make_range(pos, fin);
    }
    if (cmp > 0) {
      return make_range(ind_0.end(), ind_0.end());
    }
    return make_range(ind_0.lower_bound(lower, h.hints_0_lower),
                      ind_0.upper_bound(upper, h.hints_0_upper));
  }
  range<t_ind_0::iterator> lowerUpperRange_1111(const t_tuple &lower,
                                                const t_tuple &upper) const {
    context h;
    return lowerUpperRange_1111(lower, upper, h);
  }
  bool empty() const { return ind_0.empty(); }
  std::vector<range<iterator>> partition() const {
    return ind_0.getChunks(400);
  }
  void purge() { ind_0.clear(); }
  iterator begin() const { return ind_0.begin(); }
  iterator end() const { return ind_0.end(); }
  void printStatistics(std::ostream &o) const {
    o << " arity 4 direct b-tree index 0 lex-order [0,1,2,3]\n";
    ind_0.printStats(o);
  }
};
struct t_btree_uu__1_0__0__11__10__01 {
  using t_tuple = Tuple<RamDomain, 2>;
  struct t_comparator_0 {
    int operator()(const t_tuple &a, const t_tuple &b) const {
      return (ramBitCast<RamUnsigned>(a[1]) < ramBitCast<RamUnsigned>(b[1]))
                 ? -1
             : (ramBitCast<RamUnsigned>(a[1]) > ramBitCast<RamUnsigned>(b[1]))
                 ? 1
                 : ((ramBitCast<RamUnsigned>(a[0]) <
                     ramBitCast<RamUnsigned>(b[0]))
                        ? -1
                    : (ramBitCast<RamUnsigned>(a[0]) >
                       ramBitCast<RamUnsigned>(b[0]))
                        ? 1
                        : (0));
    }
    bool less(const t_tuple &a, const t_tuple &b) const {
      return (ramBitCast<RamUnsigned>(a[1]) < ramBitCast<RamUnsigned>(b[1])) ||
             (ramBitCast<RamUnsigned>(a[1]) == ramBitCast<RamUnsigned>(b[1])) &&
                 ((ramBitCast<RamUnsigned>(a[0]) <
                   ramBitCast<RamUnsigned>(b[0])));
    }
    bool equal(const t_tuple &a, const t_tuple &b) const {
      return (ramBitCast<RamUnsigned>(a[1]) == ramBitCast<RamUnsigned>(b[1])) &&
             (ramBitCast<RamUnsigned>(a[0]) == ramBitCast<RamUnsigned>(b[0]));
    }
  };
  using t_ind_0 = btree_set<t_tuple, t_comparator_0>;
  t_ind_0 ind_0;
  struct t_comparator_1 {
    int operator()(const t_tuple &a, const t_tuple &b) const {
      return (ramBitCast<RamUnsigned>(a[0]) < ramBitCast<RamUnsigned>(b[0]))
                 ? -1
             : (ramBitCast<RamUnsigned>(a[0]) > ramBitCast<RamUnsigned>(b[0]))
                 ? 1
                 : (0);
    }
    bool less(const t_tuple &a, const t_tuple &b) const {
      return (ramBitCast<RamUnsigned>(a[0]) < ramBitCast<RamUnsigned>(b[0]));
    }
    bool equal(const t_tuple &a, const t_tuple &b) const {
      return (ramBitCast<RamUnsigned>(a[0]) == ramBitCast<RamUnsigned>(b[0]));
    }
  };
  using t_ind_1 = btree_multiset<t_tuple, t_comparator_1>;
  t_ind_1 ind_1;
  using iterator = t_ind_0::iterator;
  struct context {
    t_ind_0::operation_hints hints_0_lower;
    t_ind_0::operation_hints hints_0_upper;
    t_ind_1::operation_hints hints_1_lower;
    t_ind_1::operation_hints hints_1_upper;
  };
  context createContext() { return context(); }
  bool insert(const t_tuple &t) {
    context h;
    return insert(t, h);
  }
  bool insert(const t_tuple &t, context &h) {
    if (ind_0.insert(t, h.hints_0_lower)) {
      ind_1.insert(t, h.hints_1_lower);
      return true;
    } else
      return false;
  }
  bool insert(const RamDomain *ramDomain) {
    RamDomain data[2];
    std::copy(ramDomain, ramDomain + 2, data);
    const t_tuple &tuple = reinterpret_cast<const t_tuple &>(data);
    context h;
    return insert(tuple, h);
  }
  bool insert(RamDomain a0, RamDomain a1) {
    RamDomain data[2] = {a0, a1};
    return insert(data);
  }
  bool contains(const t_tuple &t, context &h) const {
    return ind_0.contains(t, h.hints_0_lower);
  }
  bool contains(const t_tuple &t) const {
    context h;
    return contains(t, h);
  }
  std::size_t size() const { return ind_0.size(); }
  iterator find(const t_tuple &t, context &h) const {
    return ind_0.find(t, h.hints_0_lower);
  }
  iterator find(const t_tuple &t) const {
    context h;
    return find(t, h);
  }
  range<iterator> lowerUpperRange_00(const t_tuple & /* lower */,
                                     const t_tuple & /* upper */,
                                     context & /* h */) const {
    return range<iterator>(ind_0.begin(), ind_0.end());
  }
  range<iterator> lowerUpperRange_00(const t_tuple & /* lower */,
                                     const t_tuple & /* upper */) const {
    return range<iterator>(ind_0.begin(), ind_0.end());
  }
  range<t_ind_0::iterator> lowerUpperRange_11(const t_tuple &lower,
                                              const t_tuple &upper,
                                              context &h) const {
    t_comparator_0 comparator;
    int cmp = comparator(lower, upper);
    if (cmp == 0) {
      auto pos = ind_0.find(lower, h.hints_0_lower);
      auto fin = ind_0.end();
      if (pos != fin) {
        fin = pos;
        ++fin;
      }
      return make_range(pos, fin);
    }
    if (cmp > 0) {
      return make_range(ind_0.end(), ind_0.end());
    }
    return make_range(ind_0.lower_bound(lower, h.hints_0_lower),
                      ind_0.upper_bound(upper, h.hints_0_upper));
  }
  range<t_ind_0::iterator> lowerUpperRange_11(const t_tuple &lower,
                                              const t_tuple &upper) const {
    context h;
    return lowerUpperRange_11(lower, upper, h);
  }
  range<t_ind_1::iterator> lowerUpperRange_10(const t_tuple &lower,
                                              const t_tuple &upper,
                                              context &h) const {
    t_comparator_1 comparator;
    int cmp = comparator(lower, upper);
    if (cmp > 0) {
      return make_range(ind_1.end(), ind_1.end());
    }
    return make_range(ind_1.lower_bound(lower, h.hints_1_lower),
                      ind_1.upper_bound(upper, h.hints_1_upper));
  }
  range<t_ind_1::iterator> lowerUpperRange_10(const t_tuple &lower,
                                              const t_tuple &upper) const {
    context h;
    return lowerUpperRange_10(lower, upper, h);
  }
  range<t_ind_0::iterator> lowerUpperRange_01(const t_tuple &lower,
                                              const t_tuple &upper,
                                              context &h) const {
    t_comparator_0 comparator;
    int cmp = comparator(lower, upper);
    if (cmp > 0) {
      return make_range(ind_0.end(), ind_0.end());
    }
    return make_range(ind_0.lower_bound(lower, h.hints_0_lower),
                      ind_0.upper_bound(upper, h.hints_0_upper));
  }
  range<t_ind_0::iterator> lowerUpperRange_01(const t_tuple &lower,
                                              const t_tuple &upper) const {
    context h;
    return lowerUpperRange_01(lower, upper, h);
  }
  bool empty() const { return ind_0.empty(); }
  std::vector<range<iterator>> partition() const {
    return ind_0.getChunks(400);
  }
  void purge() {
    ind_0.clear();
    ind_1.clear();
  }
  iterator begin() const { return ind_0.begin(); }
  iterator end() const { return ind_0.end(); }
  void printStatistics(std::ostream &o) const {
    o << " arity 2 direct b-tree index 0 lex-order [1,0]\n";
    ind_0.printStats(o);
    o << " arity 2 direct b-tree index 1 lex-order [0]\n";
    ind_1.printStats(o);
  }
};
struct t_btree_ui__0_1__11 {
  using t_tuple = Tuple<RamDomain, 2>;
  struct t_comparator_0 {
    int operator()(const t_tuple &a, const t_tuple &b) const {
      return (ramBitCast<RamUnsigned>(a[0]) < ramBitCast<RamUnsigned>(b[0]))
                 ? -1
             : (ramBitCast<RamUnsigned>(a[0]) > ramBitCast<RamUnsigned>(b[0]))
                 ? 1
                 : ((ramBitCast<RamSigned>(a[1]) < ramBitCast<RamSigned>(b[1]))
                        ? -1
                    : (ramBitCast<RamSigned>(a[1]) >
                       ramBitCast<RamSigned>(b[1]))
                        ? 1
                        : (0));
    }
    bool less(const t_tuple &a, const t_tuple &b) const {
      return (ramBitCast<RamUnsigned>(a[0]) < ramBitCast<RamUnsigned>(b[0])) ||
             (ramBitCast<RamUnsigned>(a[0]) == ramBitCast<RamUnsigned>(b[0])) &&
                 ((ramBitCast<RamSigned>(a[1]) < ramBitCast<RamSigned>(b[1])));
    }
    bool equal(const t_tuple &a, const t_tuple &b) const {
      return (ramBitCast<RamUnsigned>(a[0]) == ramBitCast<RamUnsigned>(b[0])) &&
             (ramBitCast<RamSigned>(a[1]) == ramBitCast<RamSigned>(b[1]));
    }
  };
  using t_ind_0 = btree_set<t_tuple, t_comparator_0>;
  t_ind_0 ind_0;
  using iterator = t_ind_0::iterator;
  struct context {
    t_ind_0::operation_hints hints_0_lower;
    t_ind_0::operation_hints hints_0_upper;
  };
  context createContext() { return context(); }
  bool insert(const t_tuple &t) {
    context h;
    return insert(t, h);
  }
  bool insert(const t_tuple &t, context &h) {
    if (ind_0.insert(t, h.hints_0_lower)) {
      return true;
    } else
      return false;
  }
  bool insert(const RamDomain *ramDomain) {
    RamDomain data[2];
    std::copy(ramDomain, ramDomain + 2, data);
    const t_tuple &tuple = reinterpret_cast<const t_tuple &>(data);
    context h;
    return insert(tuple, h);
  }
  bool insert(RamDomain a0, RamDomain a1) {
    RamDomain data[2] = {a0, a1};
    return insert(data);
  }
  bool contains(const t_tuple &t, context &h) const {
    return ind_0.contains(t, h.hints_0_lower);
  }
  bool contains(const t_tuple &t) const {
    context h;
    return contains(t, h);
  }
  std::size_t size() const { return ind_0.size(); }
  iterator find(const t_tuple &t, context &h) const {
    return ind_0.find(t, h.hints_0_lower);
  }
  iterator find(const t_tuple &t) const {
    context h;
    return find(t, h);
  }
  range<iterator> lowerUpperRange_00(const t_tuple & /* lower */,
                                     const t_tuple & /* upper */,
                                     context & /* h */) const {
    return range<iterator>(ind_0.begin(), ind_0.end());
  }
  range<iterator> lowerUpperRange_00(const t_tuple & /* lower */,
                                     const t_tuple & /* upper */) const {
    return range<iterator>(ind_0.begin(), ind_0.end());
  }
  range<t_ind_0::iterator> lowerUpperRange_11(const t_tuple &lower,
                                              const t_tuple &upper,
                                              context &h) const {
    t_comparator_0 comparator;
    int cmp = comparator(lower, upper);
    if (cmp == 0) {
      auto pos = ind_0.find(lower, h.hints_0_lower);
      auto fin = ind_0.end();
      if (pos != fin) {
        fin = pos;
        ++fin;
      }
      return make_range(pos, fin);
    }
    if (cmp > 0) {
      return make_range(ind_0.end(), ind_0.end());
    }
    return make_range(ind_0.lower_bound(lower, h.hints_0_lower),
                      ind_0.upper_bound(upper, h.hints_0_upper));
  }
  range<t_ind_0::iterator> lowerUpperRange_11(const t_tuple &lower,
                                              const t_tuple &upper) const {
    context h;
    return lowerUpperRange_11(lower, upper, h);
  }
  bool empty() const { return ind_0.empty(); }
  std::vector<range<iterator>> partition() const {
    return ind_0.getChunks(400);
  }
  void purge() { ind_0.clear(); }
  iterator begin() const { return ind_0.begin(); }
  iterator end() const { return ind_0.end(); }
  void printStatistics(std::ostream &o) const {
    o << " arity 2 direct b-tree index 0 lex-order [0,1]\n";
    ind_0.printStats(o);
  }
};
struct t_btree_uiuu__0_1_2_3__1111 {
  using t_tuple = Tuple<RamDomain, 4>;
  struct t_comparator_0 {
    int operator()(const t_tuple &a, const t_tuple &b) const {
      return (ramBitCast<RamUnsigned>(a[0]) < ramBitCast<RamUnsigned>(b[0]))
                 ? -1
             : (ramBitCast<RamUnsigned>(a[0]) > ramBitCast<RamUnsigned>(b[0]))
                 ? 1
                 : ((ramBitCast<RamSigned>(a[1]) < ramBitCast<RamSigned>(b[1]))
                        ? -1
                    : (ramBitCast<RamSigned>(a[1]) >
                       ramBitCast<RamSigned>(b[1]))
                        ? 1
                        : ((ramBitCast<RamUnsigned>(a[2]) <
                            ramBitCast<RamUnsigned>(b[2]))
                               ? -1
                           : (ramBitCast<RamUnsigned>(a[2]) >
                              ramBitCast<RamUnsigned>(b[2]))
                               ? 1
                               : ((ramBitCast<RamUnsigned>(a[3]) <
                                   ramBitCast<RamUnsigned>(b[3]))
                                      ? -1
                                  : (ramBitCast<RamUnsigned>(a[3]) >
                                     ramBitCast<RamUnsigned>(b[3]))
                                      ? 1
                                      : (0))));
    }
    bool less(const t_tuple &a, const t_tuple &b) const {
      return (ramBitCast<RamUnsigned>(a[0]) < ramBitCast<RamUnsigned>(b[0])) ||
             (ramBitCast<RamUnsigned>(a[0]) == ramBitCast<RamUnsigned>(b[0])) &&
                 ((ramBitCast<RamSigned>(a[1]) < ramBitCast<RamSigned>(b[1])) ||
                  (ramBitCast<RamSigned>(a[1]) ==
                   ramBitCast<RamSigned>(b[1])) &&
                      ((ramBitCast<RamUnsigned>(a[2]) <
                        ramBitCast<RamUnsigned>(b[2])) ||
                       (ramBitCast<RamUnsigned>(a[2]) ==
                        ramBitCast<RamUnsigned>(b[2])) &&
                           ((ramBitCast<RamUnsigned>(a[3]) <
                             ramBitCast<RamUnsigned>(b[3])))));
    }
    bool equal(const t_tuple &a, const t_tuple &b) const {
      return (ramBitCast<RamUnsigned>(a[0]) == ramBitCast<RamUnsigned>(b[0])) &&
             (ramBitCast<RamSigned>(a[1]) == ramBitCast<RamSigned>(b[1])) &&
             (ramBitCast<RamUnsigned>(a[2]) == ramBitCast<RamUnsigned>(b[2])) &&
             (ramBitCast<RamUnsigned>(a[3]) == ramBitCast<RamUnsigned>(b[3]));
    }
  };
  using t_ind_0 = btree_set<t_tuple, t_comparator_0>;
  t_ind_0 ind_0;
  using iterator = t_ind_0::iterator;
  struct context {
    t_ind_0::operation_hints hints_0_lower;
    t_ind_0::operation_hints hints_0_upper;
  };
  context createContext() { return context(); }
  bool insert(const t_tuple &t) {
    context h;
    return insert(t, h);
  }
  bool insert(const t_tuple &t, context &h) {
    if (ind_0.insert(t, h.hints_0_lower)) {
      return true;
    } else
      return false;
  }
  bool insert(const RamDomain *ramDomain) {
    RamDomain data[4];
    std::copy(ramDomain, ramDomain + 4, data);
    const t_tuple &tuple = reinterpret_cast<const t_tuple &>(data);
    context h;
    return insert(tuple, h);
  }
  bool insert(RamDomain a0, RamDomain a1, RamDomain a2, RamDomain a3) {
    RamDomain data[4] = {a0, a1, a2, a3};
    return insert(data);
  }
  bool contains(const t_tuple &t, context &h) const {
    return ind_0.contains(t, h.hints_0_lower);
  }
  bool contains(const t_tuple &t) const {
    context h;
    return contains(t, h);
  }
  std::size_t size() const { return ind_0.size(); }
  iterator find(const t_tuple &t, context &h) const {
    return ind_0.find(t, h.hints_0_lower);
  }
  iterator find(const t_tuple &t) const {
    context h;
    return find(t, h);
  }
  range<iterator> lowerUpperRange_0000(const t_tuple & /* lower */,
                                       const t_tuple & /* upper */,
                                       context & /* h */) const {
    return range<iterator>(ind_0.begin(), ind_0.end());
  }
  range<iterator> lowerUpperRange_0000(const t_tuple & /* lower */,
                                       const t_tuple & /* upper */) const {
    return range<iterator>(ind_0.begin(), ind_0.end());
  }
  range<t_ind_0::iterator> lowerUpperRange_1111(const t_tuple &lower,
                                                const t_tuple &upper,
                                                context &h) const {
    t_comparator_0 comparator;
    int cmp = comparator(lower, upper);
    if (cmp == 0) {
      auto pos = ind_0.find(lower, h.hints_0_lower);
      auto fin = ind_0.end();
      if (pos != fin) {
        fin = pos;
        ++fin;
      }
      return make_range(pos, fin);
    }
    if (cmp > 0) {
      return make_range(ind_0.end(), ind_0.end());
    }
    return make_range(ind_0.lower_bound(lower, h.hints_0_lower),
                      ind_0.upper_bound(upper, h.hints_0_upper));
  }
  range<t_ind_0::iterator> lowerUpperRange_1111(const t_tuple &lower,
                                                const t_tuple &upper) const {
    context h;
    return lowerUpperRange_1111(lower, upper, h);
  }
  bool empty() const { return ind_0.empty(); }
  std::vector<range<iterator>> partition() const {
    return ind_0.getChunks(400);
  }
  void purge() { ind_0.clear(); }
  iterator begin() const { return ind_0.begin(); }
  iterator end() const { return ind_0.end(); }
  void printStatistics(std::ostream &o) const {
    o << " arity 4 direct b-tree index 0 lex-order [0,1,2,3]\n";
    ind_0.printStats(o);
  }
};
struct t_btree_uiuu__1_0_2_3__1111__0100 {
  using t_tuple = Tuple<RamDomain, 4>;
  struct t_comparator_0 {
    int operator()(const t_tuple &a, const t_tuple &b) const {
      return (ramBitCast<RamSigned>(a[1]) < ramBitCast<RamSigned>(b[1])) ? -1
             : (ramBitCast<RamSigned>(a[1]) > ramBitCast<RamSigned>(b[1]))
                 ? 1
                 : ((ramBitCast<RamUnsigned>(a[0]) <
                     ramBitCast<RamUnsigned>(b[0]))
                        ? -1
                    : (ramBitCast<RamUnsigned>(a[0]) >
                       ramBitCast<RamUnsigned>(b[0]))
                        ? 1
                        : ((ramBitCast<RamUnsigned>(a[2]) <
                            ramBitCast<RamUnsigned>(b[2]))
                               ? -1
                           : (ramBitCast<RamUnsigned>(a[2]) >
                              ramBitCast<RamUnsigned>(b[2]))
                               ? 1
                               : ((ramBitCast<RamUnsigned>(a[3]) <
                                   ramBitCast<RamUnsigned>(b[3]))
                                      ? -1
                                  : (ramBitCast<RamUnsigned>(a[3]) >
                                     ramBitCast<RamUnsigned>(b[3]))
                                      ? 1
                                      : (0))));
    }
    bool less(const t_tuple &a, const t_tuple &b) const {
      return (ramBitCast<RamSigned>(a[1]) < ramBitCast<RamSigned>(b[1])) ||
             (ramBitCast<RamSigned>(a[1]) == ramBitCast<RamSigned>(b[1])) &&
                 ((ramBitCast<RamUnsigned>(a[0]) <
                   ramBitCast<RamUnsigned>(b[0])) ||
                  (ramBitCast<RamUnsigned>(a[0]) ==
                   ramBitCast<RamUnsigned>(b[0])) &&
                      ((ramBitCast<RamUnsigned>(a[2]) <
                        ramBitCast<RamUnsigned>(b[2])) ||
                       (ramBitCast<RamUnsigned>(a[2]) ==
                        ramBitCast<RamUnsigned>(b[2])) &&
                           ((ramBitCast<RamUnsigned>(a[3]) <
                             ramBitCast<RamUnsigned>(b[3])))));
    }
    bool equal(const t_tuple &a, const t_tuple &b) const {
      return (ramBitCast<RamSigned>(a[1]) == ramBitCast<RamSigned>(b[1])) &&
             (ramBitCast<RamUnsigned>(a[0]) == ramBitCast<RamUnsigned>(b[0])) &&
             (ramBitCast<RamUnsigned>(a[2]) == ramBitCast<RamUnsigned>(b[2])) &&
             (ramBitCast<RamUnsigned>(a[3]) == ramBitCast<RamUnsigned>(b[3]));
    }
  };
  using t_ind_0 = btree_set<t_tuple, t_comparator_0>;
  t_ind_0 ind_0;
  using iterator = t_ind_0::iterator;
  struct context {
    t_ind_0::operation_hints hints_0_lower;
    t_ind_0::operation_hints hints_0_upper;
  };
  context createContext() { return context(); }
  bool insert(const t_tuple &t) {
    context h;
    return insert(t, h);
  }
  bool insert(const t_tuple &t, context &h) {
    if (ind_0.insert(t, h.hints_0_lower)) {
      return true;
    } else
      return false;
  }
  bool insert(const RamDomain *ramDomain) {
    RamDomain data[4];
    std::copy(ramDomain, ramDomain + 4, data);
    const t_tuple &tuple = reinterpret_cast<const t_tuple &>(data);
    context h;
    return insert(tuple, h);
  }
  bool insert(RamDomain a0, RamDomain a1, RamDomain a2, RamDomain a3) {
    RamDomain data[4] = {a0, a1, a2, a3};
    return insert(data);
  }
  bool contains(const t_tuple &t, context &h) const {
    return ind_0.contains(t, h.hints_0_lower);
  }
  bool contains(const t_tuple &t) const {
    context h;
    return contains(t, h);
  }
  std::size_t size() const { return ind_0.size(); }
  iterator find(const t_tuple &t, context &h) const {
    return ind_0.find(t, h.hints_0_lower);
  }
  iterator find(const t_tuple &t) const {
    context h;
    return find(t, h);
  }
  range<iterator> lowerUpperRange_0000(const t_tuple & /* lower */,
                                       const t_tuple & /* upper */,
                                       context & /* h */) const {
    return range<iterator>(ind_0.begin(), ind_0.end());
  }
  range<iterator> lowerUpperRange_0000(const t_tuple & /* lower */,
                                       const t_tuple & /* upper */) const {
    return range<iterator>(ind_0.begin(), ind_0.end());
  }
  range<t_ind_0::iterator> lowerUpperRange_1111(const t_tuple &lower,
                                                const t_tuple &upper,
                                                context &h) const {
    t_comparator_0 comparator;
    int cmp = comparator(lower, upper);
    if (cmp == 0) {
      auto pos = ind_0.find(lower, h.hints_0_lower);
      auto fin = ind_0.end();
      if (pos != fin) {
        fin = pos;
        ++fin;
      }
      return make_range(pos, fin);
    }
    if (cmp > 0) {
      return make_range(ind_0.end(), ind_0.end());
    }
    return make_range(ind_0.lower_bound(lower, h.hints_0_lower),
                      ind_0.upper_bound(upper, h.hints_0_upper));
  }
  range<t_ind_0::iterator> lowerUpperRange_1111(const t_tuple &lower,
                                                const t_tuple &upper) const {
    context h;
    return lowerUpperRange_1111(lower, upper, h);
  }
  range<t_ind_0::iterator> lowerUpperRange_0100(const t_tuple &lower,
                                                const t_tuple &upper,
                                                context &h) const {
    t_comparator_0 comparator;
    int cmp = comparator(lower, upper);
    if (cmp > 0) {
      return make_range(ind_0.end(), ind_0.end());
    }
    return make_range(ind_0.lower_bound(lower, h.hints_0_lower),
                      ind_0.upper_bound(upper, h.hints_0_upper));
  }
  range<t_ind_0::iterator> lowerUpperRange_0100(const t_tuple &lower,
                                                const t_tuple &upper) const {
    context h;
    return lowerUpperRange_0100(lower, upper, h);
  }
  bool empty() const { return ind_0.empty(); }
  std::vector<range<iterator>> partition() const {
    return ind_0.getChunks(400);
  }
  void purge() { ind_0.clear(); }
  iterator begin() const { return ind_0.begin(); }
  iterator end() const { return ind_0.end(); }
  void printStatistics(std::ostream &o) const {
    o << " arity 4 direct b-tree index 0 lex-order [1,0,2,3]\n";
    ind_0.printStats(o);
  }
};
struct t_btree_uuiuu__0_1_2_3_4__11111 {
  using t_tuple = Tuple<RamDomain, 5>;
  struct t_comparator_0 {
    int operator()(const t_tuple &a, const t_tuple &b) const {
      return (ramBitCast<RamUnsigned>(a[0]) < ramBitCast<RamUnsigned>(b[0]))
                 ? -1
             : (ramBitCast<RamUnsigned>(a[0]) > ramBitCast<RamUnsigned>(b[0]))
                 ? 1
                 : ((ramBitCast<RamUnsigned>(a[1]) <
                     ramBitCast<RamUnsigned>(b[1]))
                        ? -1
                    : (ramBitCast<RamUnsigned>(a[1]) >
                       ramBitCast<RamUnsigned>(b[1]))
                        ? 1
                        : ((ramBitCast<RamSigned>(a[2]) <
                            ramBitCast<RamSigned>(b[2]))
                               ? -1
                           : (ramBitCast<RamSigned>(a[2]) >
                              ramBitCast<RamSigned>(b[2]))
                               ? 1
                               : ((ramBitCast<RamUnsigned>(a[3]) <
                                   ramBitCast<RamUnsigned>(b[3]))
                                      ? -1
                                  : (ramBitCast<RamUnsigned>(a[3]) >
                                     ramBitCast<RamUnsigned>(b[3]))
                                      ? 1
                                      : ((ramBitCast<RamUnsigned>(a[4]) <
                                          ramBitCast<RamUnsigned>(b[4]))
                                             ? -1
                                         : (ramBitCast<RamUnsigned>(a[4]) >
                                            ramBitCast<RamUnsigned>(b[4]))
                                             ? 1
                                             : (0)))));
    }
    bool less(const t_tuple &a, const t_tuple &b) const {
      return (ramBitCast<RamUnsigned>(a[0]) < ramBitCast<RamUnsigned>(b[0])) ||
             (ramBitCast<RamUnsigned>(a[0]) == ramBitCast<RamUnsigned>(b[0])) &&
                 ((ramBitCast<RamUnsigned>(a[1]) <
                   ramBitCast<RamUnsigned>(b[1])) ||
                  (ramBitCast<RamUnsigned>(a[1]) ==
                   ramBitCast<RamUnsigned>(b[1])) &&
                      ((ramBitCast<RamSigned>(a[2]) <
                        ramBitCast<RamSigned>(b[2])) ||
                       (ramBitCast<RamSigned>(a[2]) ==
                        ramBitCast<RamSigned>(b[2])) &&
                           ((ramBitCast<RamUnsigned>(a[3]) <
                             ramBitCast<RamUnsigned>(b[3])) ||
                            (ramBitCast<RamUnsigned>(a[3]) ==
                             ramBitCast<RamUnsigned>(b[3])) &&
                                ((ramBitCast<RamUnsigned>(a[4]) <
                                  ramBitCast<RamUnsigned>(b[4]))))));
    }
    bool equal(const t_tuple &a, const t_tuple &b) const {
      return (ramBitCast<RamUnsigned>(a[0]) == ramBitCast<RamUnsigned>(b[0])) &&
             (ramBitCast<RamUnsigned>(a[1]) == ramBitCast<RamUnsigned>(b[1])) &&
             (ramBitCast<RamSigned>(a[2]) == ramBitCast<RamSigned>(b[2])) &&
             (ramBitCast<RamUnsigned>(a[3]) == ramBitCast<RamUnsigned>(b[3])) &&
             (ramBitCast<RamUnsigned>(a[4]) == ramBitCast<RamUnsigned>(b[4]));
    }
  };
  using t_ind_0 = btree_set<t_tuple, t_comparator_0>;
  t_ind_0 ind_0;
  using iterator = t_ind_0::iterator;
  struct context {
    t_ind_0::operation_hints hints_0_lower;
    t_ind_0::operation_hints hints_0_upper;
  };
  context createContext() { return context(); }
  bool insert(const t_tuple &t) {
    context h;
    return insert(t, h);
  }
  bool insert(const t_tuple &t, context &h) {
    if (ind_0.insert(t, h.hints_0_lower)) {
      return true;
    } else
      return false;
  }
  bool insert(const RamDomain *ramDomain) {
    RamDomain data[5];
    std::copy(ramDomain, ramDomain + 5, data);
    const t_tuple &tuple = reinterpret_cast<const t_tuple &>(data);
    context h;
    return insert(tuple, h);
  }
  bool insert(RamDomain a0, RamDomain a1, RamDomain a2, RamDomain a3,
              RamDomain a4) {
    RamDomain data[5] = {a0, a1, a2, a3, a4};
    return insert(data);
  }
  bool contains(const t_tuple &t, context &h) const {
    return ind_0.contains(t, h.hints_0_lower);
  }
  bool contains(const t_tuple &t) const {
    context h;
    return contains(t, h);
  }
  std::size_t size() const { return ind_0.size(); }
  iterator find(const t_tuple &t, context &h) const {
    return ind_0.find(t, h.hints_0_lower);
  }
  iterator find(const t_tuple &t) const {
    context h;
    return find(t, h);
  }
  range<iterator> lowerUpperRange_00000(const t_tuple & /* lower */,
                                        const t_tuple & /* upper */,
                                        context & /* h */) const {
    return range<iterator>(ind_0.begin(), ind_0.end());
  }
  range<iterator> lowerUpperRange_00000(const t_tuple & /* lower */,
                                        const t_tuple & /* upper */) const {
    return range<iterator>(ind_0.begin(), ind_0.end());
  }
  range<t_ind_0::iterator> lowerUpperRange_11111(const t_tuple &lower,
                                                 const t_tuple &upper,
                                                 context &h) const {
    t_comparator_0 comparator;
    int cmp = comparator(lower, upper);
    if (cmp == 0) {
      auto pos = ind_0.find(lower, h.hints_0_lower);
      auto fin = ind_0.end();
      if (pos != fin) {
        fin = pos;
        ++fin;
      }
      return make_range(pos, fin);
    }
    if (cmp > 0) {
      return make_range(ind_0.end(), ind_0.end());
    }
    return make_range(ind_0.lower_bound(lower, h.hints_0_lower),
                      ind_0.upper_bound(upper, h.hints_0_upper));
  }
  range<t_ind_0::iterator> lowerUpperRange_11111(const t_tuple &lower,
                                                 const t_tuple &upper) const {
    context h;
    return lowerUpperRange_11111(lower, upper, h);
  }
  bool empty() const { return ind_0.empty(); }
  std::vector<range<iterator>> partition() const {
    return ind_0.getChunks(400);
  }
  void purge() { ind_0.clear(); }
  iterator begin() const { return ind_0.begin(); }
  iterator end() const { return ind_0.end(); }
  void printStatistics(std::ostream &o) const {
    o << " arity 5 direct b-tree index 0 lex-order [0,1,2,3,4]\n";
    ind_0.printStats(o);
  }
};
struct t_btree_uiui__0_1_2_3__1110__1111 {
  using t_tuple = Tuple<RamDomain, 4>;
  struct t_comparator_0 {
    int operator()(const t_tuple &a, const t_tuple &b) const {
      return (ramBitCast<RamUnsigned>(a[0]) < ramBitCast<RamUnsigned>(b[0]))
                 ? -1
             : (ramBitCast<RamUnsigned>(a[0]) > ramBitCast<RamUnsigned>(b[0]))
                 ? 1
                 : ((ramBitCast<RamSigned>(a[1]) < ramBitCast<RamSigned>(b[1]))
                        ? -1
                    : (ramBitCast<RamSigned>(a[1]) >
                       ramBitCast<RamSigned>(b[1]))
                        ? 1
                        : ((ramBitCast<RamUnsigned>(a[2]) <
                            ramBitCast<RamUnsigned>(b[2]))
                               ? -1
                           : (ramBitCast<RamUnsigned>(a[2]) >
                              ramBitCast<RamUnsigned>(b[2]))
                               ? 1
                               : ((ramBitCast<RamSigned>(a[3]) <
                                   ramBitCast<RamSigned>(b[3]))
                                      ? -1
                                  : (ramBitCast<RamSigned>(a[3]) >
                                     ramBitCast<RamSigned>(b[3]))
                                      ? 1
                                      : (0))));
    }
    bool less(const t_tuple &a, const t_tuple &b) const {
      return (ramBitCast<RamUnsigned>(a[0]) < ramBitCast<RamUnsigned>(b[0])) ||
             (ramBitCast<RamUnsigned>(a[0]) == ramBitCast<RamUnsigned>(b[0])) &&
                 ((ramBitCast<RamSigned>(a[1]) < ramBitCast<RamSigned>(b[1])) ||
                  (ramBitCast<RamSigned>(a[1]) ==
                   ramBitCast<RamSigned>(b[1])) &&
                      ((ramBitCast<RamUnsigned>(a[2]) <
                        ramBitCast<RamUnsigned>(b[2])) ||
                       (ramBitCast<RamUnsigned>(a[2]) ==
                        ramBitCast<RamUnsigned>(b[2])) &&
                           ((ramBitCast<RamSigned>(a[3]) <
                             ramBitCast<RamSigned>(b[3])))));
    }
    bool equal(const t_tuple &a, const t_tuple &b) const {
      return (ramBitCast<RamUnsigned>(a[0]) == ramBitCast<RamUnsigned>(b[0])) &&
             (ramBitCast<RamSigned>(a[1]) == ramBitCast<RamSigned>(b[1])) &&
             (ramBitCast<RamUnsigned>(a[2]) == ramBitCast<RamUnsigned>(b[2])) &&
             (ramBitCast<RamSigned>(a[3]) == ramBitCast<RamSigned>(b[3]));
    }
  };
  using t_ind_0 = btree_set<t_tuple, t_comparator_0>;
  t_ind_0 ind_0;
  using iterator = t_ind_0::iterator;
  struct context {
    t_ind_0::operation_hints hints_0_lower;
    t_ind_0::operation_hints hints_0_upper;
  };
  context createContext() { return context(); }
  bool insert(const t_tuple &t) {
    context h;
    return insert(t, h);
  }
  bool insert(const t_tuple &t, context &h) {
    if (ind_0.insert(t, h.hints_0_lower)) {
      return true;
    } else
      return false;
  }
  bool insert(const RamDomain *ramDomain) {
    RamDomain data[4];
    std::copy(ramDomain, ramDomain + 4, data);
    const t_tuple &tuple = reinterpret_cast<const t_tuple &>(data);
    context h;
    return insert(tuple, h);
  }
  bool insert(RamDomain a0, RamDomain a1, RamDomain a2, RamDomain a3) {
    RamDomain data[4] = {a0, a1, a2, a3};
    return insert(data);
  }
  bool contains(const t_tuple &t, context &h) const {
    return ind_0.contains(t, h.hints_0_lower);
  }
  bool contains(const t_tuple &t) const {
    context h;
    return contains(t, h);
  }
  std::size_t size() const { return ind_0.size(); }
  iterator find(const t_tuple &t, context &h) const {
    return ind_0.find(t, h.hints_0_lower);
  }
  iterator find(const t_tuple &t) const {
    context h;
    return find(t, h);
  }
  range<iterator> lowerUpperRange_0000(const t_tuple & /* lower */,
                                       const t_tuple & /* upper */,
                                       context & /* h */) const {
    return range<iterator>(ind_0.begin(), ind_0.end());
  }
  range<iterator> lowerUpperRange_0000(const t_tuple & /* lower */,
                                       const t_tuple & /* upper */) const {
    return range<iterator>(ind_0.begin(), ind_0.end());
  }
  range<t_ind_0::iterator> lowerUpperRange_1110(const t_tuple &lower,
                                                const t_tuple &upper,
                                                context &h) const {
    t_comparator_0 comparator;
    int cmp = comparator(lower, upper);
    if (cmp > 0) {
      return make_range(ind_0.end(), ind_0.end());
    }
    return make_range(ind_0.lower_bound(lower, h.hints_0_lower),
                      ind_0.upper_bound(upper, h.hints_0_upper));
  }
  range<t_ind_0::iterator> lowerUpperRange_1110(const t_tuple &lower,
                                                const t_tuple &upper) const {
    context h;
    return lowerUpperRange_1110(lower, upper, h);
  }
  range<t_ind_0::iterator> lowerUpperRange_1111(const t_tuple &lower,
                                                const t_tuple &upper,
                                                context &h) const {
    t_comparator_0 comparator;
    int cmp = comparator(lower, upper);
    if (cmp == 0) {
      auto pos = ind_0.find(lower, h.hints_0_lower);
      auto fin = ind_0.end();
      if (pos != fin) {
        fin = pos;
        ++fin;
      }
      return make_range(pos, fin);
    }
    if (cmp > 0) {
      return make_range(ind_0.end(), ind_0.end());
    }
    return make_range(ind_0.lower_bound(lower, h.hints_0_lower),
                      ind_0.upper_bound(upper, h.hints_0_upper));
  }
  range<t_ind_0::iterator> lowerUpperRange_1111(const t_tuple &lower,
                                                const t_tuple &upper) const {
    context h;
    return lowerUpperRange_1111(lower, upper, h);
  }
  bool empty() const { return ind_0.empty(); }
  std::vector<range<iterator>> partition() const {
    return ind_0.getChunks(400);
  }
  void purge() { ind_0.clear(); }
  iterator begin() const { return ind_0.begin(); }
  iterator end() const { return ind_0.end(); }
  void printStatistics(std::ostream &o) const {
    o << " arity 4 direct b-tree index 0 lex-order [0,1,2,3]\n";
    ind_0.printStats(o);
  }
};
struct t_btree_ui__0_1__11__10 {
  using t_tuple = Tuple<RamDomain, 2>;
  struct t_comparator_0 {
    int operator()(const t_tuple &a, const t_tuple &b) const {
      return (ramBitCast<RamUnsigned>(a[0]) < ramBitCast<RamUnsigned>(b[0]))
                 ? -1
             : (ramBitCast<RamUnsigned>(a[0]) > ramBitCast<RamUnsigned>(b[0]))
                 ? 1
                 : ((ramBitCast<RamSigned>(a[1]) < ramBitCast<RamSigned>(b[1]))
                        ? -1
                    : (ramBitCast<RamSigned>(a[1]) >
                       ramBitCast<RamSigned>(b[1]))
                        ? 1
                        : (0));
    }
    bool less(const t_tuple &a, const t_tuple &b) const {
      return (ramBitCast<RamUnsigned>(a[0]) < ramBitCast<RamUnsigned>(b[0])) ||
             (ramBitCast<RamUnsigned>(a[0]) == ramBitCast<RamUnsigned>(b[0])) &&
                 ((ramBitCast<RamSigned>(a[1]) < ramBitCast<RamSigned>(b[1])));
    }
    bool equal(const t_tuple &a, const t_tuple &b) const {
      return (ramBitCast<RamUnsigned>(a[0]) == ramBitCast<RamUnsigned>(b[0])) &&
             (ramBitCast<RamSigned>(a[1]) == ramBitCast<RamSigned>(b[1]));
    }
  };
  using t_ind_0 = btree_set<t_tuple, t_comparator_0>;
  t_ind_0 ind_0;
  using iterator = t_ind_0::iterator;
  struct context {
    t_ind_0::operation_hints hints_0_lower;
    t_ind_0::operation_hints hints_0_upper;
  };
  context createContext() { return context(); }
  bool insert(const t_tuple &t) {
    context h;
    return insert(t, h);
  }
  bool insert(const t_tuple &t, context &h) {
    if (ind_0.insert(t, h.hints_0_lower)) {
      return true;
    } else
      return false;
  }
  bool insert(const RamDomain *ramDomain) {
    RamDomain data[2];
    std::copy(ramDomain, ramDomain + 2, data);
    const t_tuple &tuple = reinterpret_cast<const t_tuple &>(data);
    context h;
    return insert(tuple, h);
  }
  bool insert(RamDomain a0, RamDomain a1) {
    RamDomain data[2] = {a0, a1};
    return insert(data);
  }
  bool contains(const t_tuple &t, context &h) const {
    return ind_0.contains(t, h.hints_0_lower);
  }
  bool contains(const t_tuple &t) const {
    context h;
    return contains(t, h);
  }
  std::size_t size() const { return ind_0.size(); }
  iterator find(const t_tuple &t, context &h) const {
    return ind_0.find(t, h.hints_0_lower);
  }
  iterator find(const t_tuple &t) const {
    context h;
    return find(t, h);
  }
  range<iterator> lowerUpperRange_00(const t_tuple & /* lower */,
                                     const t_tuple & /* upper */,
                                     context & /* h */) const {
    return range<iterator>(ind_0.begin(), ind_0.end());
  }
  range<iterator> lowerUpperRange_00(const t_tuple & /* lower */,
                                     const t_tuple & /* upper */) const {
    return range<iterator>(ind_0.begin(), ind_0.end());
  }
  range<t_ind_0::iterator> lowerUpperRange_11(const t_tuple &lower,
                                              const t_tuple &upper,
                                              context &h) const {
    t_comparator_0 comparator;
    int cmp = comparator(lower, upper);
    if (cmp == 0) {
      auto pos = ind_0.find(lower, h.hints_0_lower);
      auto fin = ind_0.end();
      if (pos != fin) {
        fin = pos;
        ++fin;
      }
      return make_range(pos, fin);
    }
    if (cmp > 0) {
      return make_range(ind_0.end(), ind_0.end());
    }
    return make_range(ind_0.lower_bound(lower, h.hints_0_lower),
                      ind_0.upper_bound(upper, h.hints_0_upper));
  }
  range<t_ind_0::iterator> lowerUpperRange_11(const t_tuple &lower,
                                              const t_tuple &upper) const {
    context h;
    return lowerUpperRange_11(lower, upper, h);
  }
  range<t_ind_0::iterator> lowerUpperRange_10(const t_tuple &lower,
                                              const t_tuple &upper,
                                              context &h) const {
    t_comparator_0 comparator;
    int cmp = comparator(lower, upper);
    if (cmp > 0) {
      return make_range(ind_0.end(), ind_0.end());
    }
    return make_range(ind_0.lower_bound(lower, h.hints_0_lower),
                      ind_0.upper_bound(upper, h.hints_0_upper));
  }
  range<t_ind_0::iterator> lowerUpperRange_10(const t_tuple &lower,
                                              const t_tuple &upper) const {
    context h;
    return lowerUpperRange_10(lower, upper, h);
  }
  bool empty() const { return ind_0.empty(); }
  std::vector<range<iterator>> partition() const {
    return ind_0.getChunks(400);
  }
  void purge() { ind_0.clear(); }
  iterator begin() const { return ind_0.begin(); }
  iterator end() const { return ind_0.end(); }
  void printStatistics(std::ostream &o) const {
    o << " arity 2 direct b-tree index 0 lex-order [0,1]\n";
    ind_0.printStats(o);
  }
};
struct
    t_btree_uuiiuuuuuu__3_0_1_2_4_5_6_7_8_9__1111111111__1001000000__0001000000 {
  using t_tuple = Tuple<RamDomain, 10>;
  Table<t_tuple> dataTable;
  Lock insert_lock;
  struct t_comparator_0 {
    int operator()(const t_tuple *a, const t_tuple *b) const {
      return (ramBitCast<RamSigned>((*a)[3]) < ramBitCast<RamSigned>((*b)[3]))
                 ? -1
                 : ((ramBitCast<RamSigned>((*a)[3]) >
                     ramBitCast<RamSigned>((*b)[3]))
                        ? 1
                        : ((ramBitCast<RamUnsigned>((*a)[0]) <
                            ramBitCast<RamUnsigned>((*b)[0]))
                               ? -1
                               : ((ramBitCast<RamUnsigned>((*a)[0]) >
                                   ramBitCast<RamUnsigned>((*b)[0]))
                                      ? 1
                                      : ((ramBitCast<RamUnsigned>((*a)[1]) <
                                          ramBitCast<RamUnsigned>((*b)[1]))
                                             ? -1
                                             : ((ramBitCast<RamUnsigned>(
                                                     (*a)[1]) >
                                                 ramBitCast<RamUnsigned>(
                                                     (*b)[1]))
                                                    ? 1
                                                    : ((ramBitCast<RamSigned>(
                                                            (*a)[2]) <
                                                        ramBitCast<RamSigned>(
                                                            (*b)[2]))
                                                           ? -1
                                                           : ((ramBitCast<
                                                                   RamSigned>(
                                                                   (*a)[2]) >
                                                               ramBitCast<
                                                                   RamSigned>(
                                                                   (*b)[2]))
                                                                  ? 1
                                                                  : (
                                                                        (ramBitCast<
                                                                             RamUnsigned>(
                                                                             (*a)[4]) <
                                                                         ramBitCast<
                                                                             RamUnsigned>(
                                                                             (*b)[4]))
                                                                            ? -1
                                                                            : (
                                                                                  (ramBitCast<RamUnsigned>((*a)[4]) > ramBitCast<RamUnsigned>((*b)[4])) ? 1
                                                                                                                                                        : (
                                                                                                                                                              (ramBitCast<RamUnsigned>((*a)[5]) < ramBitCast<RamUnsigned>((*b)[5])) ? -1
                                                                                                                                                                                                                                    : (
                                                                                                                                                                                                                                          (ramBitCast<RamUnsigned>((*a)[5]) > ramBitCast<RamUnsigned>((*b)[5])) ? 1
                                                                                                                                                                                                                                                                                                                : (
                                                                                                                                                                                                                                                                                                                      (
                                                                                                                                                                                                                                                                                                                          ramBitCast<RamUnsigned>((*a)[6]) < ramBitCast<
                                                                                                                                                                                                                                                                                                                                                                 RamUnsigned>((
                                                                                                                                                                                                                                                                                                                                                                 *b)[6]))
                                                                                                                                                                                                                                                                                                                          ? -1
                                                                                                                                                                                                                                                                                                                          : ((
                                                                                                                                                                                                                                                                                                                                 ramBitCast<
                                                                                                                                                                                                                                                                                                                                     RamUnsigned>(
                                                                                                                                                                                                                                                                                                                                     (*a)[6]) > ramBitCast<RamUnsigned>((*b)[6]))
                                                                                                                                                                                                                                                                                                                                 ? 1
                                                                                                                                                                                                                                                                                                                                 : ((ramBitCast<RamUnsigned>((*a)[7]) < ramBitCast<RamUnsigned>((*b)[7])) ? -1
                                                                                                                                                                                                                                                                                                                                                                                                          : ((ramBitCast<RamUnsigned>((*a)[7]) > ramBitCast<RamUnsigned>((*b)[7])) ? 1
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   : (
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         (
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             ramBitCast<
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 RamUnsigned>(
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 (*a)[8]) < ramBitCast<RamUnsigned>((*b)[8]))
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             ? -1
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             : (
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   (ramBitCast<RamUnsigned>((*a)[8]) > ramBitCast<
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           RamUnsigned>((
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           *b)[8]))
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                       ? 1
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                       : (
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             (ramBitCast<
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  RamUnsigned>(
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  (*a)[9]) <
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              ramBitCast<
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  RamUnsigned>(
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  (*b)[9]))
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 ? -1
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 : ((ramBitCast<
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         RamUnsigned>(
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         (*a)[9]) >
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     ramBitCast<
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         RamUnsigned>(
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         (*b)[9]))
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        ? 1
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        : (0))))))))))))))))))));
    }
    bool less(const t_tuple *a, const t_tuple *b) const {
      return ramBitCast<RamSigned>((*a)[3]) < ramBitCast<RamSigned>((*b)[3]) ||
             (ramBitCast<RamSigned>((*a)[3]) ==
                  ramBitCast<RamSigned>((*b)[3]) &&
              (ramBitCast<RamUnsigned>((*a)[0]) <
                   ramBitCast<RamUnsigned>((*b)[0]) ||
               (ramBitCast<RamUnsigned>((*a)[0]) ==
                    ramBitCast<RamUnsigned>((*b)[0]) &&
                (ramBitCast<RamUnsigned>((*a)[1]) <
                     ramBitCast<RamUnsigned>((*b)[1]) ||
                 (ramBitCast<RamUnsigned>((*a)[1]) ==
                      ramBitCast<RamUnsigned>((*b)[1]) &&
                  (ramBitCast<RamSigned>((*a)[2]) <
                       ramBitCast<RamSigned>((*b)[2]) ||
                   (ramBitCast<RamSigned>((*a)[2]) ==
                        ramBitCast<RamSigned>((*b)[2]) &&
                    (ramBitCast<RamUnsigned>((*a)[4]) <
                         ramBitCast<RamUnsigned>((*b)[4]) ||
                     (ramBitCast<RamUnsigned>((*a)[4]) ==
                          ramBitCast<RamUnsigned>((*b)[4]) &&
                      (ramBitCast<RamUnsigned>((*a)[5]) <
                           ramBitCast<RamUnsigned>((*b)[5]) ||
                       (ramBitCast<RamUnsigned>((*a)[5]) ==
                            ramBitCast<RamUnsigned>((*b)[5]) &&
                        (ramBitCast<RamUnsigned>((*a)[6]) <
                             ramBitCast<RamUnsigned>((*b)[6]) ||
                         (ramBitCast<RamUnsigned>((*a)[6]) ==
                              ramBitCast<RamUnsigned>((*b)[6]) &&
                          (ramBitCast<RamUnsigned>((*a)[7]) <
                               ramBitCast<RamUnsigned>((*b)[7]) ||
                           (ramBitCast<RamUnsigned>((*a)[7]) ==
                                ramBitCast<RamUnsigned>((*b)[7]) &&
                            (ramBitCast<RamUnsigned>((*a)[8]) <
                                 ramBitCast<RamUnsigned>((*b)[8]) ||
                             (ramBitCast<RamUnsigned>((*a)[8]) ==
                                  ramBitCast<RamUnsigned>((*b)[8]) &&
                              (ramBitCast<RamUnsigned>((*a)[9]) <
                               ramBitCast<RamUnsigned>(
                                   (*b)[9])))))))))))))))))));
    }
    bool equal(const t_tuple *a, const t_tuple *b) const {
      return ramBitCast<RamSigned>((*a)[3]) == ramBitCast<RamSigned>((*b)[3]) &&
             ramBitCast<RamUnsigned>((*a)[0]) ==
                 ramBitCast<RamUnsigned>((*b)[0]) &&
             ramBitCast<RamUnsigned>((*a)[1]) ==
                 ramBitCast<RamUnsigned>((*b)[1]) &&
             ramBitCast<RamSigned>((*a)[2]) == ramBitCast<RamSigned>((*b)[2]) &&
             ramBitCast<RamUnsigned>((*a)[4]) ==
                 ramBitCast<RamUnsigned>((*b)[4]) &&
             ramBitCast<RamUnsigned>((*a)[5]) ==
                 ramBitCast<RamUnsigned>((*b)[5]) &&
             ramBitCast<RamUnsigned>((*a)[6]) ==
                 ramBitCast<RamUnsigned>((*b)[6]) &&
             ramBitCast<RamUnsigned>((*a)[7]) ==
                 ramBitCast<RamUnsigned>((*b)[7]) &&
             ramBitCast<RamUnsigned>((*a)[8]) ==
                 ramBitCast<RamUnsigned>((*b)[8]) &&
             ramBitCast<RamUnsigned>((*a)[9]) ==
                 ramBitCast<RamUnsigned>((*b)[9]);
    }
  };
  using t_ind_0 = btree_set<const t_tuple *, t_comparator_0>;
  t_ind_0 ind_0;
  using iterator_0 = IterDerefWrapper<typename t_ind_0::iterator>;
  using iterator = iterator_0;
  struct context {
    t_ind_0::operation_hints hints_0_lower;
    t_ind_0::operation_hints hints_0_upper;
  };
  context createContext() { return context(); }
  bool insert(const t_tuple &t) {
    context h;
    return insert(t, h);
  }
  bool insert(const t_tuple &t, context &h) {
    const t_tuple *masterCopy = nullptr;
    {
      auto lease = insert_lock.acquire();
      if (contains(t, h))
        return false;
      masterCopy = &dataTable.insert(t);
      ind_0.insert(masterCopy, h.hints_0_lower);
    }
    return true;
  }
  bool insert(const RamDomain *ramDomain) {
    RamDomain data[10];
    std::copy(ramDomain, ramDomain + 10, data);
    const t_tuple &tuple = reinterpret_cast<const t_tuple &>(data);
    context h;
    return insert(tuple, h);
  }
  bool insert(RamDomain a0, RamDomain a1, RamDomain a2, RamDomain a3,
              RamDomain a4, RamDomain a5, RamDomain a6, RamDomain a7,
              RamDomain a8, RamDomain a9) {
    RamDomain data[10] = {a0, a1, a2, a3, a4, a5, a6, a7, a8, a9};
    return insert(data);
  }
  bool contains(const t_tuple &t, context &h) const {
    return ind_0.contains(&t, h.hints_0_lower);
  }
  bool contains(const t_tuple &t) const {
    context h;
    return contains(t, h);
  }
  std::size_t size() const { return ind_0.size(); }
  iterator find(const t_tuple &t, context &h) const {
    return ind_0.find(&t, h.hints_0_lower);
  }
  iterator find(const t_tuple &t) const {
    context h;
    return find(t, h);
  }
  range<iterator> lowerUpperRange_0(const t_tuple &lower, const t_tuple &upper,
                                    context &h) const {
    return range<iterator>(ind_0.begin(), ind_0.end());
  }
  range<iterator> lowerUpperRange_0(const t_tuple &lower,
                                    const t_tuple &upper) const {
    return range<iterator>(ind_0.begin(), ind_0.end());
  }
  range<iterator_0> lowerUpperRange_1111111111(const t_tuple &lower,
                                               const t_tuple &upper,
                                               context &h) const {
    t_comparator_0 comparator;
    int cmp = comparator(&lower, &upper);
    if (cmp == 0) {
      auto pos = find(lower, h);
      auto fin = end();
      if (pos != fin) {
        fin = pos;
        ++fin;
      }
      return make_range(pos, fin);
    }
    if (cmp > 0) {
      return range<iterator_0>(ind_0.end(), ind_0.end());
    }
    return range<iterator_0>(ind_0.lower_bound(&lower, h.hints_0_lower),
                             ind_0.upper_bound(&upper, h.hints_0_upper));
  }
  range<iterator_0> lowerUpperRange_1111111111(const t_tuple &lower,
                                               const t_tuple &upper) const {
    context h;
    return lowerUpperRange_1111111111(lower, upper, h);
  }
  range<iterator_0> lowerUpperRange_1001000000(const t_tuple &lower,
                                               const t_tuple &upper,
                                               context &h) const {
    t_comparator_0 comparator;
    int cmp = comparator(&lower, &upper);
    if (cmp > 0) {
      return range<iterator_0>(ind_0.end(), ind_0.end());
    }
    return range<iterator_0>(ind_0.lower_bound(&lower, h.hints_0_lower),
                             ind_0.upper_bound(&upper, h.hints_0_upper));
  }
  range<iterator_0> lowerUpperRange_1001000000(const t_tuple &lower,
                                               const t_tuple &upper) const {
    context h;
    return lowerUpperRange_1001000000(lower, upper, h);
  }
  range<iterator_0> lowerUpperRange_0001000000(const t_tuple &lower,
                                               const t_tuple &upper,
                                               context &h) const {
    t_comparator_0 comparator;
    int cmp = comparator(&lower, &upper);
    if (cmp > 0) {
      return range<iterator_0>(ind_0.end(), ind_0.end());
    }
    return range<iterator_0>(ind_0.lower_bound(&lower, h.hints_0_lower),
                             ind_0.upper_bound(&upper, h.hints_0_upper));
  }
  range<iterator_0> lowerUpperRange_0001000000(const t_tuple &lower,
                                               const t_tuple &upper) const {
    context h;
    return lowerUpperRange_0001000000(lower, upper, h);
  }
  bool empty() const { return ind_0.empty(); }
  std::vector<range<iterator>> partition() const {
    std::vector<range<iterator>> res;
    for (const auto &cur : ind_0.getChunks(400)) {
      res.push_back(make_range(derefIter(cur.begin()), derefIter(cur.end())));
    }
    return res;
  }
  void purge() {
    ind_0.clear();
    dataTable.clear();
  }
  iterator begin() const { return ind_0.begin(); }
  iterator end() const { return ind_0.end(); }
  void printStatistics(std::ostream &o) const {
    o << " arity 10 indirect b-tree index 0 lex-order [3,0,1,2,4,5,6,7,8,9]\n";
    ind_0.printStats(o);
  }
};
struct t_btree_uiu__0_1_2__111 {
  using t_tuple = Tuple<RamDomain, 3>;
  struct t_comparator_0 {
    int operator()(const t_tuple &a, const t_tuple &b) const {
      return (ramBitCast<RamUnsigned>(a[0]) < ramBitCast<RamUnsigned>(b[0]))
                 ? -1
             : (ramBitCast<RamUnsigned>(a[0]) > ramBitCast<RamUnsigned>(b[0]))
                 ? 1
                 : ((ramBitCast<RamSigned>(a[1]) < ramBitCast<RamSigned>(b[1]))
                        ? -1
                    : (ramBitCast<RamSigned>(a[1]) >
                       ramBitCast<RamSigned>(b[1]))
                        ? 1
                        : ((ramBitCast<RamUnsigned>(a[2]) <
                            ramBitCast<RamUnsigned>(b[2]))
                               ? -1
                           : (ramBitCast<RamUnsigned>(a[2]) >
                              ramBitCast<RamUnsigned>(b[2]))
                               ? 1
                               : (0)));
    }
    bool less(const t_tuple &a, const t_tuple &b) const {
      return (ramBitCast<RamUnsigned>(a[0]) < ramBitCast<RamUnsigned>(b[0])) ||
             (ramBitCast<RamUnsigned>(a[0]) == ramBitCast<RamUnsigned>(b[0])) &&
                 ((ramBitCast<RamSigned>(a[1]) < ramBitCast<RamSigned>(b[1])) ||
                  (ramBitCast<RamSigned>(a[1]) ==
                   ramBitCast<RamSigned>(b[1])) &&
                      ((ramBitCast<RamUnsigned>(a[2]) <
                        ramBitCast<RamUnsigned>(b[2]))));
    }
    bool equal(const t_tuple &a, const t_tuple &b) const {
      return (ramBitCast<RamUnsigned>(a[0]) == ramBitCast<RamUnsigned>(b[0])) &&
             (ramBitCast<RamSigned>(a[1]) == ramBitCast<RamSigned>(b[1])) &&
             (ramBitCast<RamUnsigned>(a[2]) == ramBitCast<RamUnsigned>(b[2]));
    }
  };
  using t_ind_0 = btree_set<t_tuple, t_comparator_0>;
  t_ind_0 ind_0;
  using iterator = t_ind_0::iterator;
  struct context {
    t_ind_0::operation_hints hints_0_lower;
    t_ind_0::operation_hints hints_0_upper;
  };
  context createContext() { return context(); }
  bool insert(const t_tuple &t) {
    context h;
    return insert(t, h);
  }
  bool insert(const t_tuple &t, context &h) {
    if (ind_0.insert(t, h.hints_0_lower)) {
      return true;
    } else
      return false;
  }
  bool insert(const RamDomain *ramDomain) {
    RamDomain data[3];
    std::copy(ramDomain, ramDomain + 3, data);
    const t_tuple &tuple = reinterpret_cast<const t_tuple &>(data);
    context h;
    return insert(tuple, h);
  }
  bool insert(RamDomain a0, RamDomain a1, RamDomain a2) {
    RamDomain data[3] = {a0, a1, a2};
    return insert(data);
  }
  bool contains(const t_tuple &t, context &h) const {
    return ind_0.contains(t, h.hints_0_lower);
  }
  bool contains(const t_tuple &t) const {
    context h;
    return contains(t, h);
  }
  std::size_t size() const { return ind_0.size(); }
  iterator find(const t_tuple &t, context &h) const {
    return ind_0.find(t, h.hints_0_lower);
  }
  iterator find(const t_tuple &t) const {
    context h;
    return find(t, h);
  }
  range<iterator> lowerUpperRange_000(const t_tuple & /* lower */,
                                      const t_tuple & /* upper */,
                                      context & /* h */) const {
    return range<iterator>(ind_0.begin(), ind_0.end());
  }
  range<iterator> lowerUpperRange_000(const t_tuple & /* lower */,
                                      const t_tuple & /* upper */) const {
    return range<iterator>(ind_0.begin(), ind_0.end());
  }
  range<t_ind_0::iterator> lowerUpperRange_111(const t_tuple &lower,
                                               const t_tuple &upper,
                                               context &h) const {
    t_comparator_0 comparator;
    int cmp = comparator(lower, upper);
    if (cmp == 0) {
      auto pos = ind_0.find(lower, h.hints_0_lower);
      auto fin = ind_0.end();
      if (pos != fin) {
        fin = pos;
        ++fin;
      }
      return make_range(pos, fin);
    }
    if (cmp > 0) {
      return make_range(ind_0.end(), ind_0.end());
    }
    return make_range(ind_0.lower_bound(lower, h.hints_0_lower),
                      ind_0.upper_bound(upper, h.hints_0_upper));
  }
  range<t_ind_0::iterator> lowerUpperRange_111(const t_tuple &lower,
                                               const t_tuple &upper) const {
    context h;
    return lowerUpperRange_111(lower, upper, h);
  }
  bool empty() const { return ind_0.empty(); }
  std::vector<range<iterator>> partition() const {
    return ind_0.getChunks(400);
  }
  void purge() { ind_0.clear(); }
  iterator begin() const { return ind_0.begin(); }
  iterator end() const { return ind_0.end(); }
  void printStatistics(std::ostream &o) const {
    o << " arity 3 direct b-tree index 0 lex-order [0,1,2]\n";
    ind_0.printStats(o);
  }
};
struct t_btree_uuu__0_1_2__111 {
  using t_tuple = Tuple<RamDomain, 3>;
  struct t_comparator_0 {
    int operator()(const t_tuple &a, const t_tuple &b) const {
      return (ramBitCast<RamUnsigned>(a[0]) < ramBitCast<RamUnsigned>(b[0]))
                 ? -1
             : (ramBitCast<RamUnsigned>(a[0]) > ramBitCast<RamUnsigned>(b[0]))
                 ? 1
                 : ((ramBitCast<RamUnsigned>(a[1]) <
                     ramBitCast<RamUnsigned>(b[1]))
                        ? -1
                    : (ramBitCast<RamUnsigned>(a[1]) >
                       ramBitCast<RamUnsigned>(b[1]))
                        ? 1
                        : ((ramBitCast<RamUnsigned>(a[2]) <
                            ramBitCast<RamUnsigned>(b[2]))
                               ? -1
                           : (ramBitCast<RamUnsigned>(a[2]) >
                              ramBitCast<RamUnsigned>(b[2]))
                               ? 1
                               : (0)));
    }
    bool less(const t_tuple &a, const t_tuple &b) const {
      return (ramBitCast<RamUnsigned>(a[0]) < ramBitCast<RamUnsigned>(b[0])) ||
             (ramBitCast<RamUnsigned>(a[0]) == ramBitCast<RamUnsigned>(b[0])) &&
                 ((ramBitCast<RamUnsigned>(a[1]) <
                   ramBitCast<RamUnsigned>(b[1])) ||
                  (ramBitCast<RamUnsigned>(a[1]) ==
                   ramBitCast<RamUnsigned>(b[1])) &&
                      ((ramBitCast<RamUnsigned>(a[2]) <
                        ramBitCast<RamUnsigned>(b[2]))));
    }
    bool equal(const t_tuple &a, const t_tuple &b) const {
      return (ramBitCast<RamUnsigned>(a[0]) == ramBitCast<RamUnsigned>(b[0])) &&
             (ramBitCast<RamUnsigned>(a[1]) == ramBitCast<RamUnsigned>(b[1])) &&
             (ramBitCast<RamUnsigned>(a[2]) == ramBitCast<RamUnsigned>(b[2]));
    }
  };
  using t_ind_0 = btree_set<t_tuple, t_comparator_0>;
  t_ind_0 ind_0;
  using iterator = t_ind_0::iterator;
  struct context {
    t_ind_0::operation_hints hints_0_lower;
    t_ind_0::operation_hints hints_0_upper;
  };
  context createContext() { return context(); }
  bool insert(const t_tuple &t) {
    context h;
    return insert(t, h);
  }
  bool insert(const t_tuple &t, context &h) {
    if (ind_0.insert(t, h.hints_0_lower)) {
      return true;
    } else
      return false;
  }
  bool insert(const RamDomain *ramDomain) {
    RamDomain data[3];
    std::copy(ramDomain, ramDomain + 3, data);
    const t_tuple &tuple = reinterpret_cast<const t_tuple &>(data);
    context h;
    return insert(tuple, h);
  }
  bool insert(RamDomain a0, RamDomain a1, RamDomain a2) {
    RamDomain data[3] = {a0, a1, a2};
    return insert(data);
  }
  bool contains(const t_tuple &t, context &h) const {
    return ind_0.contains(t, h.hints_0_lower);
  }
  bool contains(const t_tuple &t) const {
    context h;
    return contains(t, h);
  }
  std::size_t size() const { return ind_0.size(); }
  iterator find(const t_tuple &t, context &h) const {
    return ind_0.find(t, h.hints_0_lower);
  }
  iterator find(const t_tuple &t) const {
    context h;
    return find(t, h);
  }
  range<iterator> lowerUpperRange_000(const t_tuple & /* lower */,
                                      const t_tuple & /* upper */,
                                      context & /* h */) const {
    return range<iterator>(ind_0.begin(), ind_0.end());
  }
  range<iterator> lowerUpperRange_000(const t_tuple & /* lower */,
                                      const t_tuple & /* upper */) const {
    return range<iterator>(ind_0.begin(), ind_0.end());
  }
  range<t_ind_0::iterator> lowerUpperRange_111(const t_tuple &lower,
                                               const t_tuple &upper,
                                               context &h) const {
    t_comparator_0 comparator;
    int cmp = comparator(lower, upper);
    if (cmp == 0) {
      auto pos = ind_0.find(lower, h.hints_0_lower);
      auto fin = ind_0.end();
      if (pos != fin) {
        fin = pos;
        ++fin;
      }
      return make_range(pos, fin);
    }
    if (cmp > 0) {
      return make_range(ind_0.end(), ind_0.end());
    }
    return make_range(ind_0.lower_bound(lower, h.hints_0_lower),
                      ind_0.upper_bound(upper, h.hints_0_upper));
  }
  range<t_ind_0::iterator> lowerUpperRange_111(const t_tuple &lower,
                                               const t_tuple &upper) const {
    context h;
    return lowerUpperRange_111(lower, upper, h);
  }
  bool empty() const { return ind_0.empty(); }
  std::vector<range<iterator>> partition() const {
    return ind_0.getChunks(400);
  }
  void purge() { ind_0.clear(); }
  iterator begin() const { return ind_0.begin(); }
  iterator end() const { return ind_0.end(); }
  void printStatistics(std::ostream &o) const {
    o << " arity 3 direct b-tree index 0 lex-order [0,1,2]\n";
    ind_0.printStats(o);
  }
};
struct t_btree_uu__0_1__11 {
  using t_tuple = Tuple<RamDomain, 2>;
  struct t_comparator_0 {
    int operator()(const t_tuple &a, const t_tuple &b) const {
      return (ramBitCast<RamUnsigned>(a[0]) < ramBitCast<RamUnsigned>(b[0]))
                 ? -1
             : (ramBitCast<RamUnsigned>(a[0]) > ramBitCast<RamUnsigned>(b[0]))
                 ? 1
                 : ((ramBitCast<RamUnsigned>(a[1]) <
                     ramBitCast<RamUnsigned>(b[1]))
                        ? -1
                    : (ramBitCast<RamUnsigned>(a[1]) >
                       ramBitCast<RamUnsigned>(b[1]))
                        ? 1
                        : (0));
    }
    bool less(const t_tuple &a, const t_tuple &b) const {
      return (ramBitCast<RamUnsigned>(a[0]) < ramBitCast<RamUnsigned>(b[0])) ||
             (ramBitCast<RamUnsigned>(a[0]) == ramBitCast<RamUnsigned>(b[0])) &&
                 ((ramBitCast<RamUnsigned>(a[1]) <
                   ramBitCast<RamUnsigned>(b[1])));
    }
    bool equal(const t_tuple &a, const t_tuple &b) const {
      return (ramBitCast<RamUnsigned>(a[0]) == ramBitCast<RamUnsigned>(b[0])) &&
             (ramBitCast<RamUnsigned>(a[1]) == ramBitCast<RamUnsigned>(b[1]));
    }
  };
  using t_ind_0 = btree_set<t_tuple, t_comparator_0>;
  t_ind_0 ind_0;
  using iterator = t_ind_0::iterator;
  struct context {
    t_ind_0::operation_hints hints_0_lower;
    t_ind_0::operation_hints hints_0_upper;
  };
  context createContext() { return context(); }
  bool insert(const t_tuple &t) {
    context h;
    return insert(t, h);
  }
  bool insert(const t_tuple &t, context &h) {
    if (ind_0.insert(t, h.hints_0_lower)) {
      return true;
    } else
      return false;
  }
  bool insert(const RamDomain *ramDomain) {
    RamDomain data[2];
    std::copy(ramDomain, ramDomain + 2, data);
    const t_tuple &tuple = reinterpret_cast<const t_tuple &>(data);
    context h;
    return insert(tuple, h);
  }
  bool insert(RamDomain a0, RamDomain a1) {
    RamDomain data[2] = {a0, a1};
    return insert(data);
  }
  bool contains(const t_tuple &t, context &h) const {
    return ind_0.contains(t, h.hints_0_lower);
  }
  bool contains(const t_tuple &t) const {
    context h;
    return contains(t, h);
  }
  std::size_t size() const { return ind_0.size(); }
  iterator find(const t_tuple &t, context &h) const {
    return ind_0.find(t, h.hints_0_lower);
  }
  iterator find(const t_tuple &t) const {
    context h;
    return find(t, h);
  }
  range<iterator> lowerUpperRange_00(const t_tuple & /* lower */,
                                     const t_tuple & /* upper */,
                                     context & /* h */) const {
    return range<iterator>(ind_0.begin(), ind_0.end());
  }
  range<iterator> lowerUpperRange_00(const t_tuple & /* lower */,
                                     const t_tuple & /* upper */) const {
    return range<iterator>(ind_0.begin(), ind_0.end());
  }
  range<t_ind_0::iterator> lowerUpperRange_11(const t_tuple &lower,
                                              const t_tuple &upper,
                                              context &h) const {
    t_comparator_0 comparator;
    int cmp = comparator(lower, upper);
    if (cmp == 0) {
      auto pos = ind_0.find(lower, h.hints_0_lower);
      auto fin = ind_0.end();
      if (pos != fin) {
        fin = pos;
        ++fin;
      }
      return make_range(pos, fin);
    }
    if (cmp > 0) {
      return make_range(ind_0.end(), ind_0.end());
    }
    return make_range(ind_0.lower_bound(lower, h.hints_0_lower),
                      ind_0.upper_bound(upper, h.hints_0_upper));
  }
  range<t_ind_0::iterator> lowerUpperRange_11(const t_tuple &lower,
                                              const t_tuple &upper) const {
    context h;
    return lowerUpperRange_11(lower, upper, h);
  }
  bool empty() const { return ind_0.empty(); }
  std::vector<range<iterator>> partition() const {
    return ind_0.getChunks(400);
  }
  void purge() { ind_0.clear(); }
  iterator begin() const { return ind_0.begin(); }
  iterator end() const { return ind_0.end(); }
  void printStatistics(std::ostream &o) const {
    o << " arity 2 direct b-tree index 0 lex-order [0,1]\n";
    ind_0.printStats(o);
  }
};
struct t_btree_uuuui__0_1_2_3_4__11111__10000 {
  using t_tuple = Tuple<RamDomain, 5>;
  struct t_comparator_0 {
    int operator()(const t_tuple &a, const t_tuple &b) const {
      return (ramBitCast<RamUnsigned>(a[0]) < ramBitCast<RamUnsigned>(b[0]))
                 ? -1
             : (ramBitCast<RamUnsigned>(a[0]) > ramBitCast<RamUnsigned>(b[0]))
                 ? 1
                 : ((ramBitCast<RamUnsigned>(a[1]) <
                     ramBitCast<RamUnsigned>(b[1]))
                        ? -1
                    : (ramBitCast<RamUnsigned>(a[1]) >
                       ramBitCast<RamUnsigned>(b[1]))
                        ? 1
                        : ((ramBitCast<RamUnsigned>(a[2]) <
                            ramBitCast<RamUnsigned>(b[2]))
                               ? -1
                           : (ramBitCast<RamUnsigned>(a[2]) >
                              ramBitCast<RamUnsigned>(b[2]))
                               ? 1
                               : ((ramBitCast<RamUnsigned>(a[3]) <
                                   ramBitCast<RamUnsigned>(b[3]))
                                      ? -1
                                  : (ramBitCast<RamUnsigned>(a[3]) >
                                     ramBitCast<RamUnsigned>(b[3]))
                                      ? 1
                                      : ((ramBitCast<RamSigned>(a[4]) <
                                          ramBitCast<RamSigned>(b[4]))
                                             ? -1
                                         : (ramBitCast<RamSigned>(a[4]) >
                                            ramBitCast<RamSigned>(b[4]))
                                             ? 1
                                             : (0)))));
    }
    bool less(const t_tuple &a, const t_tuple &b) const {
      return (ramBitCast<RamUnsigned>(a[0]) < ramBitCast<RamUnsigned>(b[0])) ||
             (ramBitCast<RamUnsigned>(a[0]) == ramBitCast<RamUnsigned>(b[0])) &&
                 ((ramBitCast<RamUnsigned>(a[1]) <
                   ramBitCast<RamUnsigned>(b[1])) ||
                  (ramBitCast<RamUnsigned>(a[1]) ==
                   ramBitCast<RamUnsigned>(b[1])) &&
                      ((ramBitCast<RamUnsigned>(a[2]) <
                        ramBitCast<RamUnsigned>(b[2])) ||
                       (ramBitCast<RamUnsigned>(a[2]) ==
                        ramBitCast<RamUnsigned>(b[2])) &&
                           ((ramBitCast<RamUnsigned>(a[3]) <
                             ramBitCast<RamUnsigned>(b[3])) ||
                            (ramBitCast<RamUnsigned>(a[3]) ==
                             ramBitCast<RamUnsigned>(b[3])) &&
                                ((ramBitCast<RamSigned>(a[4]) <
                                  ramBitCast<RamSigned>(b[4]))))));
    }
    bool equal(const t_tuple &a, const t_tuple &b) const {
      return (ramBitCast<RamUnsigned>(a[0]) == ramBitCast<RamUnsigned>(b[0])) &&
             (ramBitCast<RamUnsigned>(a[1]) == ramBitCast<RamUnsigned>(b[1])) &&
             (ramBitCast<RamUnsigned>(a[2]) == ramBitCast<RamUnsigned>(b[2])) &&
             (ramBitCast<RamUnsigned>(a[3]) == ramBitCast<RamUnsigned>(b[3])) &&
             (ramBitCast<RamSigned>(a[4]) == ramBitCast<RamSigned>(b[4]));
    }
  };
  using t_ind_0 = btree_set<t_tuple, t_comparator_0>;
  t_ind_0 ind_0;
  using iterator = t_ind_0::iterator;
  struct context {
    t_ind_0::operation_hints hints_0_lower;
    t_ind_0::operation_hints hints_0_upper;
  };
  context createContext() { return context(); }
  bool insert(const t_tuple &t) {
    context h;
    return insert(t, h);
  }
  bool insert(const t_tuple &t, context &h) {
    if (ind_0.insert(t, h.hints_0_lower)) {
      return true;
    } else
      return false;
  }
  bool insert(const RamDomain *ramDomain) {
    RamDomain data[5];
    std::copy(ramDomain, ramDomain + 5, data);
    const t_tuple &tuple = reinterpret_cast<const t_tuple &>(data);
    context h;
    return insert(tuple, h);
  }
  bool insert(RamDomain a0, RamDomain a1, RamDomain a2, RamDomain a3,
              RamDomain a4) {
    RamDomain data[5] = {a0, a1, a2, a3, a4};
    return insert(data);
  }
  bool contains(const t_tuple &t, context &h) const {
    return ind_0.contains(t, h.hints_0_lower);
  }
  bool contains(const t_tuple &t) const {
    context h;
    return contains(t, h);
  }
  std::size_t size() const { return ind_0.size(); }
  iterator find(const t_tuple &t, context &h) const {
    return ind_0.find(t, h.hints_0_lower);
  }
  iterator find(const t_tuple &t) const {
    context h;
    return find(t, h);
  }
  range<iterator> lowerUpperRange_00000(const t_tuple & /* lower */,
                                        const t_tuple & /* upper */,
                                        context & /* h */) const {
    return range<iterator>(ind_0.begin(), ind_0.end());
  }
  range<iterator> lowerUpperRange_00000(const t_tuple & /* lower */,
                                        const t_tuple & /* upper */) const {
    return range<iterator>(ind_0.begin(), ind_0.end());
  }
  range<t_ind_0::iterator> lowerUpperRange_11111(const t_tuple &lower,
                                                 const t_tuple &upper,
                                                 context &h) const {
    t_comparator_0 comparator;
    int cmp = comparator(lower, upper);
    if (cmp == 0) {
      auto pos = ind_0.find(lower, h.hints_0_lower);
      auto fin = ind_0.end();
      if (pos != fin) {
        fin = pos;
        ++fin;
      }
      return make_range(pos, fin);
    }
    if (cmp > 0) {
      return make_range(ind_0.end(), ind_0.end());
    }
    return make_range(ind_0.lower_bound(lower, h.hints_0_lower),
                      ind_0.upper_bound(upper, h.hints_0_upper));
  }
  range<t_ind_0::iterator> lowerUpperRange_11111(const t_tuple &lower,
                                                 const t_tuple &upper) const {
    context h;
    return lowerUpperRange_11111(lower, upper, h);
  }
  range<t_ind_0::iterator> lowerUpperRange_10000(const t_tuple &lower,
                                                 const t_tuple &upper,
                                                 context &h) const {
    t_comparator_0 comparator;
    int cmp = comparator(lower, upper);
    if (cmp > 0) {
      return make_range(ind_0.end(), ind_0.end());
    }
    return make_range(ind_0.lower_bound(lower, h.hints_0_lower),
                      ind_0.upper_bound(upper, h.hints_0_upper));
  }
  range<t_ind_0::iterator> lowerUpperRange_10000(const t_tuple &lower,
                                                 const t_tuple &upper) const {
    context h;
    return lowerUpperRange_10000(lower, upper, h);
  }
  bool empty() const { return ind_0.empty(); }
  std::vector<range<iterator>> partition() const {
    return ind_0.getChunks(400);
  }
  void purge() { ind_0.clear(); }
  iterator begin() const { return ind_0.begin(); }
  iterator end() const { return ind_0.end(); }
  void printStatistics(std::ostream &o) const {
    o << " arity 5 direct b-tree index 0 lex-order [0,1,2,3,4]\n";
    ind_0.printStats(o);
  }
};
struct t_btree_uuuuui__1_2_3_0_4_5__111111__011100 {
  using t_tuple = Tuple<RamDomain, 6>;
  struct t_comparator_0 {
    int operator()(const t_tuple &a, const t_tuple &b) const {
      return (ramBitCast<RamUnsigned>(a[1]) < ramBitCast<RamUnsigned>(b[1]))
                 ? -1
             : (ramBitCast<RamUnsigned>(a[1]) > ramBitCast<RamUnsigned>(b[1]))
                 ? 1
                 : ((ramBitCast<RamUnsigned>(a[2]) <
                     ramBitCast<RamUnsigned>(b[2]))
                        ? -1
                    : (ramBitCast<RamUnsigned>(a[2]) >
                       ramBitCast<RamUnsigned>(b[2]))
                        ? 1
                        : ((ramBitCast<RamUnsigned>(a[3]) <
                            ramBitCast<RamUnsigned>(b[3]))
                               ? -1
                           : (ramBitCast<RamUnsigned>(a[3]) >
                              ramBitCast<RamUnsigned>(b[3]))
                               ? 1
                               : ((ramBitCast<RamUnsigned>(a[0]) <
                                   ramBitCast<RamUnsigned>(b[0]))
                                      ? -1
                                  : (ramBitCast<RamUnsigned>(a[0]) >
                                     ramBitCast<RamUnsigned>(b[0]))
                                      ? 1
                                      : ((ramBitCast<RamUnsigned>(a[4]) <
                                          ramBitCast<RamUnsigned>(b[4]))
                                             ? -1
                                         : (ramBitCast<RamUnsigned>(a[4]) >
                                            ramBitCast<RamUnsigned>(b[4]))
                                             ? 1
                                             : ((ramBitCast<RamSigned>(a[5]) <
                                                 ramBitCast<RamSigned>(b[5]))
                                                    ? -1
                                                : (ramBitCast<RamSigned>(a[5]) >
                                                   ramBitCast<RamSigned>(b[5]))
                                                    ? 1
                                                    : (0))))));
    }
    bool less(const t_tuple &a, const t_tuple &b) const {
      return (ramBitCast<RamUnsigned>(a[1]) < ramBitCast<RamUnsigned>(b[1])) ||
             (ramBitCast<RamUnsigned>(a[1]) == ramBitCast<RamUnsigned>(b[1])) &&
                 ((ramBitCast<RamUnsigned>(a[2]) <
                   ramBitCast<RamUnsigned>(b[2])) ||
                  (ramBitCast<RamUnsigned>(a[2]) ==
                   ramBitCast<RamUnsigned>(b[2])) &&
                      ((ramBitCast<RamUnsigned>(a[3]) <
                        ramBitCast<RamUnsigned>(b[3])) ||
                       (ramBitCast<RamUnsigned>(a[3]) ==
                        ramBitCast<RamUnsigned>(b[3])) &&
                           ((ramBitCast<RamUnsigned>(a[0]) <
                             ramBitCast<RamUnsigned>(b[0])) ||
                            (ramBitCast<RamUnsigned>(a[0]) ==
                             ramBitCast<RamUnsigned>(b[0])) &&
                                ((ramBitCast<RamUnsigned>(a[4]) <
                                  ramBitCast<RamUnsigned>(b[4])) ||
                                 (ramBitCast<RamUnsigned>(a[4]) ==
                                  ramBitCast<RamUnsigned>(b[4])) &&
                                     ((ramBitCast<RamSigned>(a[5]) <
                                       ramBitCast<RamSigned>(b[5])))))));
    }
    bool equal(const t_tuple &a, const t_tuple &b) const {
      return (ramBitCast<RamUnsigned>(a[1]) == ramBitCast<RamUnsigned>(b[1])) &&
             (ramBitCast<RamUnsigned>(a[2]) == ramBitCast<RamUnsigned>(b[2])) &&
             (ramBitCast<RamUnsigned>(a[3]) == ramBitCast<RamUnsigned>(b[3])) &&
             (ramBitCast<RamUnsigned>(a[0]) == ramBitCast<RamUnsigned>(b[0])) &&
             (ramBitCast<RamUnsigned>(a[4]) == ramBitCast<RamUnsigned>(b[4])) &&
             (ramBitCast<RamSigned>(a[5]) == ramBitCast<RamSigned>(b[5]));
    }
  };
  using t_ind_0 = btree_set<t_tuple, t_comparator_0>;
  t_ind_0 ind_0;
  using iterator = t_ind_0::iterator;
  struct context {
    t_ind_0::operation_hints hints_0_lower;
    t_ind_0::operation_hints hints_0_upper;
  };
  context createContext() { return context(); }
  bool insert(const t_tuple &t) {
    context h;
    return insert(t, h);
  }
  bool insert(const t_tuple &t, context &h) {
    if (ind_0.insert(t, h.hints_0_lower)) {
      return true;
    } else
      return false;
  }
  bool insert(const RamDomain *ramDomain) {
    RamDomain data[6];
    std::copy(ramDomain, ramDomain + 6, data);
    const t_tuple &tuple = reinterpret_cast<const t_tuple &>(data);
    context h;
    return insert(tuple, h);
  }
  bool insert(RamDomain a0, RamDomain a1, RamDomain a2, RamDomain a3,
              RamDomain a4, RamDomain a5) {
    RamDomain data[6] = {a0, a1, a2, a3, a4, a5};
    return insert(data);
  }
  bool contains(const t_tuple &t, context &h) const {
    return ind_0.contains(t, h.hints_0_lower);
  }
  bool contains(const t_tuple &t) const {
    context h;
    return contains(t, h);
  }
  std::size_t size() const { return ind_0.size(); }
  iterator find(const t_tuple &t, context &h) const {
    return ind_0.find(t, h.hints_0_lower);
  }
  iterator find(const t_tuple &t) const {
    context h;
    return find(t, h);
  }
  range<iterator> lowerUpperRange_000000(const t_tuple & /* lower */,
                                         const t_tuple & /* upper */,
                                         context & /* h */) const {
    return range<iterator>(ind_0.begin(), ind_0.end());
  }
  range<iterator> lowerUpperRange_000000(const t_tuple & /* lower */,
                                         const t_tuple & /* upper */) const {
    return range<iterator>(ind_0.begin(), ind_0.end());
  }
  range<t_ind_0::iterator> lowerUpperRange_111111(const t_tuple &lower,
                                                  const t_tuple &upper,
                                                  context &h) const {
    t_comparator_0 comparator;
    int cmp = comparator(lower, upper);
    if (cmp == 0) {
      auto pos = ind_0.find(lower, h.hints_0_lower);
      auto fin = ind_0.end();
      if (pos != fin) {
        fin = pos;
        ++fin;
      }
      return make_range(pos, fin);
    }
    if (cmp > 0) {
      return make_range(ind_0.end(), ind_0.end());
    }
    return make_range(ind_0.lower_bound(lower, h.hints_0_lower),
                      ind_0.upper_bound(upper, h.hints_0_upper));
  }
  range<t_ind_0::iterator> lowerUpperRange_111111(const t_tuple &lower,
                                                  const t_tuple &upper) const {
    context h;
    return lowerUpperRange_111111(lower, upper, h);
  }
  range<t_ind_0::iterator> lowerUpperRange_011100(const t_tuple &lower,
                                                  const t_tuple &upper,
                                                  context &h) const {
    t_comparator_0 comparator;
    int cmp = comparator(lower, upper);
    if (cmp > 0) {
      return make_range(ind_0.end(), ind_0.end());
    }
    return make_range(ind_0.lower_bound(lower, h.hints_0_lower),
                      ind_0.upper_bound(upper, h.hints_0_upper));
  }
  range<t_ind_0::iterator> lowerUpperRange_011100(const t_tuple &lower,
                                                  const t_tuple &upper) const {
    context h;
    return lowerUpperRange_011100(lower, upper, h);
  }
  bool empty() const { return ind_0.empty(); }
  std::vector<range<iterator>> partition() const {
    return ind_0.getChunks(400);
  }
  void purge() { ind_0.clear(); }
  iterator begin() const { return ind_0.begin(); }
  iterator end() const { return ind_0.end(); }
  void printStatistics(std::ostream &o) const {
    o << " arity 6 direct b-tree index 0 lex-order [1,2,3,0,4,5]\n";
    ind_0.printStats(o);
  }
};
struct t_btree_u__0__2__1 {
  using t_tuple = Tuple<RamDomain, 1>;
  struct t_comparator_0 {
    int operator()(const t_tuple &a, const t_tuple &b) const {
      return (ramBitCast<RamUnsigned>(a[0]) < ramBitCast<RamUnsigned>(b[0]))
                 ? -1
             : (ramBitCast<RamUnsigned>(a[0]) > ramBitCast<RamUnsigned>(b[0]))
                 ? 1
                 : (0);
    }
    bool less(const t_tuple &a, const t_tuple &b) const {
      return (ramBitCast<RamUnsigned>(a[0]) < ramBitCast<RamUnsigned>(b[0]));
    }
    bool equal(const t_tuple &a, const t_tuple &b) const {
      return (ramBitCast<RamUnsigned>(a[0]) == ramBitCast<RamUnsigned>(b[0]));
    }
  };
  using t_ind_0 = btree_set<t_tuple, t_comparator_0>;
  t_ind_0 ind_0;
  using iterator = t_ind_0::iterator;
  struct context {
    t_ind_0::operation_hints hints_0_lower;
    t_ind_0::operation_hints hints_0_upper;
  };
  context createContext() { return context(); }
  bool insert(const t_tuple &t) {
    context h;
    return insert(t, h);
  }
  bool insert(const t_tuple &t, context &h) {
    if (ind_0.insert(t, h.hints_0_lower)) {
      return true;
    } else
      return false;
  }
  bool insert(const RamDomain *ramDomain) {
    RamDomain data[1];
    std::copy(ramDomain, ramDomain + 1, data);
    const t_tuple &tuple = reinterpret_cast<const t_tuple &>(data);
    context h;
    return insert(tuple, h);
  }
  bool insert(RamDomain a0) {
    RamDomain data[1] = {a0};
    return insert(data);
  }
  bool contains(const t_tuple &t, context &h) const {
    return ind_0.contains(t, h.hints_0_lower);
  }
  bool contains(const t_tuple &t) const {
    context h;
    return contains(t, h);
  }
  std::size_t size() const { return ind_0.size(); }
  iterator find(const t_tuple &t, context &h) const {
    return ind_0.find(t, h.hints_0_lower);
  }
  iterator find(const t_tuple &t) const {
    context h;
    return find(t, h);
  }
  range<iterator> lowerUpperRange_0(const t_tuple & /* lower */,
                                    const t_tuple & /* upper */,
                                    context & /* h */) const {
    return range<iterator>(ind_0.begin(), ind_0.end());
  }
  range<iterator> lowerUpperRange_0(const t_tuple & /* lower */,
                                    const t_tuple & /* upper */) const {
    return range<iterator>(ind_0.begin(), ind_0.end());
  }
  range<t_ind_0::iterator> lowerUpperRange_2(const t_tuple &lower,
                                             const t_tuple &upper,
                                             context &h) const {
    t_comparator_0 comparator;
    int cmp = comparator(lower, upper);
    if (cmp > 0) {
      return make_range(ind_0.end(), ind_0.end());
    }
    return make_range(ind_0.lower_bound(lower, h.hints_0_lower),
                      ind_0.upper_bound(upper, h.hints_0_upper));
  }
  range<t_ind_0::iterator> lowerUpperRange_2(const t_tuple &lower,
                                             const t_tuple &upper) const {
    context h;
    return lowerUpperRange_2(lower, upper, h);
  }
  range<t_ind_0::iterator> lowerUpperRange_1(const t_tuple &lower,
                                             const t_tuple &upper,
                                             context &h) const {
    t_comparator_0 comparator;
    int cmp = comparator(lower, upper);
    if (cmp == 0) {
      auto pos = ind_0.find(lower, h.hints_0_lower);
      auto fin = ind_0.end();
      if (pos != fin) {
        fin = pos;
        ++fin;
      }
      return make_range(pos, fin);
    }
    if (cmp > 0) {
      return make_range(ind_0.end(), ind_0.end());
    }
    return make_range(ind_0.lower_bound(lower, h.hints_0_lower),
                      ind_0.upper_bound(upper, h.hints_0_upper));
  }
  range<t_ind_0::iterator> lowerUpperRange_1(const t_tuple &lower,
                                             const t_tuple &upper) const {
    context h;
    return lowerUpperRange_1(lower, upper, h);
  }
  bool empty() const { return ind_0.empty(); }
  std::vector<range<iterator>> partition() const {
    return ind_0.getChunks(400);
  }
  void purge() { ind_0.clear(); }
  iterator begin() const { return ind_0.begin(); }
  iterator end() const { return ind_0.end(); }
  void printStatistics(std::ostream &o) const {
    o << " arity 1 direct b-tree index 0 lex-order [0]\n";
    ind_0.printStats(o);
  }
};
struct t_btree_uiu__0_1_2__110__111 {
  using t_tuple = Tuple<RamDomain, 3>;
  struct t_comparator_0 {
    int operator()(const t_tuple &a, const t_tuple &b) const {
      return (ramBitCast<RamUnsigned>(a[0]) < ramBitCast<RamUnsigned>(b[0]))
                 ? -1
             : (ramBitCast<RamUnsigned>(a[0]) > ramBitCast<RamUnsigned>(b[0]))
                 ? 1
                 : ((ramBitCast<RamSigned>(a[1]) < ramBitCast<RamSigned>(b[1]))
                        ? -1
                    : (ramBitCast<RamSigned>(a[1]) >
                       ramBitCast<RamSigned>(b[1]))
                        ? 1
                        : ((ramBitCast<RamUnsigned>(a[2]) <
                            ramBitCast<RamUnsigned>(b[2]))
                               ? -1
                           : (ramBitCast<RamUnsigned>(a[2]) >
                              ramBitCast<RamUnsigned>(b[2]))
                               ? 1
                               : (0)));
    }
    bool less(const t_tuple &a, const t_tuple &b) const {
      return (ramBitCast<RamUnsigned>(a[0]) < ramBitCast<RamUnsigned>(b[0])) ||
             (ramBitCast<RamUnsigned>(a[0]) == ramBitCast<RamUnsigned>(b[0])) &&
                 ((ramBitCast<RamSigned>(a[1]) < ramBitCast<RamSigned>(b[1])) ||
                  (ramBitCast<RamSigned>(a[1]) ==
                   ramBitCast<RamSigned>(b[1])) &&
                      ((ramBitCast<RamUnsigned>(a[2]) <
                        ramBitCast<RamUnsigned>(b[2]))));
    }
    bool equal(const t_tuple &a, const t_tuple &b) const {
      return (ramBitCast<RamUnsigned>(a[0]) == ramBitCast<RamUnsigned>(b[0])) &&
             (ramBitCast<RamSigned>(a[1]) == ramBitCast<RamSigned>(b[1])) &&
             (ramBitCast<RamUnsigned>(a[2]) == ramBitCast<RamUnsigned>(b[2]));
    }
  };
  using t_ind_0 = btree_set<t_tuple, t_comparator_0>;
  t_ind_0 ind_0;
  using iterator = t_ind_0::iterator;
  struct context {
    t_ind_0::operation_hints hints_0_lower;
    t_ind_0::operation_hints hints_0_upper;
  };
  context createContext() { return context(); }
  bool insert(const t_tuple &t) {
    context h;
    return insert(t, h);
  }
  bool insert(const t_tuple &t, context &h) {
    if (ind_0.insert(t, h.hints_0_lower)) {
      return true;
    } else
      return false;
  }
  bool insert(const RamDomain *ramDomain) {
    RamDomain data[3];
    std::copy(ramDomain, ramDomain + 3, data);
    const t_tuple &tuple = reinterpret_cast<const t_tuple &>(data);
    context h;
    return insert(tuple, h);
  }
  bool insert(RamDomain a0, RamDomain a1, RamDomain a2) {
    RamDomain data[3] = {a0, a1, a2};
    return insert(data);
  }
  bool contains(const t_tuple &t, context &h) const {
    return ind_0.contains(t, h.hints_0_lower);
  }
  bool contains(const t_tuple &t) const {
    context h;
    return contains(t, h);
  }
  std::size_t size() const { return ind_0.size(); }
  iterator find(const t_tuple &t, context &h) const {
    return ind_0.find(t, h.hints_0_lower);
  }
  iterator find(const t_tuple &t) const {
    context h;
    return find(t, h);
  }
  range<iterator> lowerUpperRange_000(const t_tuple & /* lower */,
                                      const t_tuple & /* upper */,
                                      context & /* h */) const {
    return range<iterator>(ind_0.begin(), ind_0.end());
  }
  range<iterator> lowerUpperRange_000(const t_tuple & /* lower */,
                                      const t_tuple & /* upper */) const {
    return range<iterator>(ind_0.begin(), ind_0.end());
  }
  range<t_ind_0::iterator> lowerUpperRange_110(const t_tuple &lower,
                                               const t_tuple &upper,
                                               context &h) const {
    t_comparator_0 comparator;
    int cmp = comparator(lower, upper);
    if (cmp > 0) {
      return make_range(ind_0.end(), ind_0.end());
    }
    return make_range(ind_0.lower_bound(lower, h.hints_0_lower),
                      ind_0.upper_bound(upper, h.hints_0_upper));
  }
  range<t_ind_0::iterator> lowerUpperRange_110(const t_tuple &lower,
                                               const t_tuple &upper) const {
    context h;
    return lowerUpperRange_110(lower, upper, h);
  }
  range<t_ind_0::iterator> lowerUpperRange_111(const t_tuple &lower,
                                               const t_tuple &upper,
                                               context &h) const {
    t_comparator_0 comparator;
    int cmp = comparator(lower, upper);
    if (cmp == 0) {
      auto pos = ind_0.find(lower, h.hints_0_lower);
      auto fin = ind_0.end();
      if (pos != fin) {
        fin = pos;
        ++fin;
      }
      return make_range(pos, fin);
    }
    if (cmp > 0) {
      return make_range(ind_0.end(), ind_0.end());
    }
    return make_range(ind_0.lower_bound(lower, h.hints_0_lower),
                      ind_0.upper_bound(upper, h.hints_0_upper));
  }
  range<t_ind_0::iterator> lowerUpperRange_111(const t_tuple &lower,
                                               const t_tuple &upper) const {
    context h;
    return lowerUpperRange_111(lower, upper, h);
  }
  bool empty() const { return ind_0.empty(); }
  std::vector<range<iterator>> partition() const {
    return ind_0.getChunks(400);
  }
  void purge() { ind_0.clear(); }
  iterator begin() const { return ind_0.begin(); }
  iterator end() const { return ind_0.end(); }
  void printStatistics(std::ostream &o) const {
    o << " arity 3 direct b-tree index 0 lex-order [0,1,2]\n";
    ind_0.printStats(o);
  }
};

class Sf_test_def : public SouffleProgram {
private:
  static inline bool regex_wrapper(const std::string &pattern,
                                   const std::string &text) {
    bool result = false;
    try {
      result = std::regex_match(text, std::regex(pattern));
    } catch (...) {
      std::cerr << "warning: wrong pattern provided for match(\"" << pattern
                << "\",\"" << text << "\").\n";
    }
    return result;
  }

private:
  static inline std::string substr_wrapper(const std::string &str, size_t idx,
                                           size_t len) {
    std::string result;
    try {
      result = str.substr(idx, len);
    } catch (...) {
      std::cerr << "warning: wrong index position provided by substr(\"";
      std::cerr << str << "\"," << (int32_t)idx << "," << (int32_t)len
                << ") functor.\n";
    }
    return result;
  }

public:
  // -- initialize symbol table --
  SymbolTable symTable{
      R"_(CALL)_", R"_(EAX)_", R"_(ECX)_", R"_(EDX)_", R"_(JE)_",
      R"_(JZ)_",   R"_(JNE)_", R"_(JNZ)_", R"_(RAX)_",
  }; // -- initialize record table --
  RecordTable recordTable;
  // -- Table: @delta_block_last_def
  Own<t_btree_uui__0_1_2__111> rel_1_delta_block_last_def =
      mk<t_btree_uui__0_1_2__111>();
  // -- Table: @delta_last_def
  Own<t_btree_uui__0_1_2__111> rel_2_delta_last_def =
      mk<t_btree_uui__0_1_2__111>();
  // -- Table: @new_block_last_def
  Own<t_btree_uui__0_1_2__111> rel_3_new_block_last_def =
      mk<t_btree_uui__0_1_2__111>();
  // -- Table: @new_last_def
  Own<t_btree_uui__0_1_2__111> rel_4_new_last_def =
      mk<t_btree_uui__0_1_2__111>();
  // -- Table: block
  Own<t_btree_u__0__1> rel_5_block = mk<t_btree_u__0__1>();
  souffle::RelationWrapper<0, t_btree_u__0__1, Tuple<RamDomain, 1>, 1, 0>
      wrapper_rel_5_block;
  // -- Table: block_last_def
  Own<t_btree_uui__0_2_1__101__111> rel_6_block_last_def =
      mk<t_btree_uui__0_2_1__101__111>();
  souffle::RelationWrapper<1, t_btree_uui__0_2_1__101__111, Tuple<RamDomain, 3>,
                           3, 0>
      wrapper_rel_6_block_last_def;
  // -- Table: block_last_instruction
  Own<t_btree_uu__0_1__11__10> rel_7_block_last_instruction =
      mk<t_btree_uu__0_1__11__10>();
  souffle::RelationWrapper<2, t_btree_uu__0_1__11__10, Tuple<RamDomain, 2>, 2,
                           0>
      wrapper_rel_7_block_last_instruction;
  // -- Table: block_next
  Own<t_btree_uu__0_1__11__10> rel_8_block_next = mk<t_btree_uu__0_1__11__10>();
  souffle::RelationWrapper<3, t_btree_uu__0_1__11__10, Tuple<RamDomain, 2>, 2,
                           0>
      wrapper_rel_8_block_next;
  // -- Table: call_operation
  Own<t_btree_i__0__1> rel_9_call_operation = mk<t_btree_i__0__1>();
  souffle::RelationWrapper<4, t_btree_i__0__1, Tuple<RamDomain, 1>, 1, 0>
      wrapper_rel_9_call_operation;
  // -- Table: cmp_immediate_to_reg
  Own<t_btree_uiui__0_1_2_3__1000__1111> rel_10_cmp_immediate_to_reg =
      mk<t_btree_uiui__0_1_2_3__1000__1111>();
  souffle::RelationWrapper<5, t_btree_uiui__0_1_2_3__1000__1111,
                           Tuple<RamDomain, 4>, 4, 0>
      wrapper_rel_10_cmp_immediate_to_reg;
  // -- Table: code
  Own<t_btree_u__0__1> rel_11_code = mk<t_btree_u__0__1>();
  souffle::RelationWrapper<6, t_btree_u__0__1, Tuple<RamDomain, 1>, 1, 0>
      wrapper_rel_11_code;
  // -- Table: code_in_block
  Own<t_btree_uu__1_0__0__11__10__01> rel_12_code_in_block =
      mk<t_btree_uu__1_0__0__11__10__01>();
  souffle::RelationWrapper<7, t_btree_uu__1_0__0__11__10__01,
                           Tuple<RamDomain, 2>, 2, 0>
      wrapper_rel_12_code_in_block;
  // -- Table: conditional_mov
  Own<t_btree_u__0__1> rel_13_conditional_mov = mk<t_btree_u__0__1>();
  souffle::RelationWrapper<8, t_btree_u__0__1, Tuple<RamDomain, 1>, 1, 0>
      wrapper_rel_13_conditional_mov;
  // -- Table: def
  Own<t_btree_ui__0_1__11> rel_14_def = mk<t_btree_ui__0_1__11>();
  souffle::RelationWrapper<9, t_btree_ui__0_1__11, Tuple<RamDomain, 2>, 2, 0>
      wrapper_rel_14_def;
  // -- Table: def_used
  Own<t_btree_uiuu__0_1_2_3__1111> rel_15_def_used =
      mk<t_btree_uiuu__0_1_2_3__1111>();
  souffle::RelationWrapper<10, t_btree_uiuu__0_1_2_3__1111, Tuple<RamDomain, 4>,
                           4, 0>
      wrapper_rel_15_def_used;
  // -- Table: def_used_intra
  Own<t_btree_uiuu__1_0_2_3__1111__0100> rel_16_def_used_intra =
      mk<t_btree_uiuu__1_0_2_3__1111__0100>();
  souffle::RelationWrapper<11, t_btree_uiuu__1_0_2_3__1111__0100,
                           Tuple<RamDomain, 4>, 4, 0>
      wrapper_rel_16_def_used_intra;
  // -- Table: def_used_return_val_reg
  Own<t_btree_uuiuu__0_1_2_3_4__11111> rel_17_def_used_return_val_reg =
      mk<t_btree_uuiuu__0_1_2_3_4__11111>();
  souffle::RelationWrapper<12, t_btree_uuiuu__0_1_2_3_4__11111,
                           Tuple<RamDomain, 5>, 5, 0>
      wrapper_rel_17_def_used_return_val_reg;
  // -- Table: defined_in_block
  Own<t_btree_ui__0_1__11> rel_18_defined_in_block = mk<t_btree_ui__0_1__11>();
  souffle::RelationWrapper<13, t_btree_ui__0_1__11, Tuple<RamDomain, 2>, 2, 0>
      wrapper_rel_18_defined_in_block;
  // -- Table: direct_call
  Own<t_btree_uu__1_0__0__11__10__01> rel_19_direct_call =
      mk<t_btree_uu__1_0__0__11__10__01>();
  souffle::RelationWrapper<14, t_btree_uu__1_0__0__11__10__01,
                           Tuple<RamDomain, 2>, 2, 0>
      wrapper_rel_19_direct_call;
  // -- Table: direct_jump
  Own<t_btree_uu__0_1__11__10> rel_20_direct_jump =
      mk<t_btree_uu__0_1__11__10>();
  souffle::RelationWrapper<15, t_btree_uu__0_1__11__10, Tuple<RamDomain, 2>, 2,
                           0>
      wrapper_rel_20_direct_jump;
  // -- Table: fde_addresses
  Own<t_btree_uu__0_1__11__10> rel_21_fde_addresses =
      mk<t_btree_uu__0_1__11__10>();
  souffle::RelationWrapper<16, t_btree_uu__0_1__11__10, Tuple<RamDomain, 2>, 2,
                           0>
      wrapper_rel_21_fde_addresses;
  // -- Table: flow_def
  Own<t_btree_uiui__0_1_2_3__1110__1111> rel_22_flow_def =
      mk<t_btree_uiui__0_1_2_3__1110__1111>();
  souffle::RelationWrapper<17, t_btree_uiui__0_1_2_3__1110__1111,
                           Tuple<RamDomain, 4>, 4, 0>
      wrapper_rel_22_flow_def;
  // -- Table: function_non_maintained_reg
  Own<t_btree_i__0__1> rel_23_function_non_maintained_reg =
      mk<t_btree_i__0__1>();
  souffle::RelationWrapper<18, t_btree_i__0__1, Tuple<RamDomain, 1>, 1, 0>
      wrapper_rel_23_function_non_maintained_reg;
  // -- Table: get_pc_thunk
  Own<t_btree_ui__0_1__11__10> rel_24_get_pc_thunk =
      mk<t_btree_ui__0_1__11__10>();
  souffle::RelationWrapper<19, t_btree_ui__0_1__11__10, Tuple<RamDomain, 2>, 2,
                           0>
      wrapper_rel_24_get_pc_thunk;
  // -- Table: instruction
  Own<t_btree_uuiiuuuuuu__3_0_1_2_4_5_6_7_8_9__1111111111__1001000000__0001000000>
      rel_25_instruction = mk<
          t_btree_uuiiuuuuuu__3_0_1_2_4_5_6_7_8_9__1111111111__1001000000__0001000000>();
  souffle::RelationWrapper<
      20,
      t_btree_uuiiuuuuuu__3_0_1_2_4_5_6_7_8_9__1111111111__1001000000__0001000000,
      Tuple<RamDomain, 10>, 10, 0>
      wrapper_rel_25_instruction;
  // -- Table: instruction_get_dest_op
  Own<t_btree_uiu__0_1_2__111> rel_26_instruction_get_dest_op =
      mk<t_btree_uiu__0_1_2__111>();
  souffle::RelationWrapper<21, t_btree_uiu__0_1_2__111, Tuple<RamDomain, 3>, 3,
                           0>
      wrapper_rel_26_instruction_get_dest_op;
  // -- Table: instruction_get_op
  Own<t_btree_uuu__0_1_2__111> rel_27_instruction_get_op =
      mk<t_btree_uuu__0_1_2__111>();
  souffle::RelationWrapper<22, t_btree_uuu__0_1_2__111, Tuple<RamDomain, 3>, 3,
                           0>
      wrapper_rel_27_instruction_get_op;
  // -- Table: instruction_get_src_op
  Own<t_btree_uuu__0_1_2__111> rel_28_instruction_get_src_op =
      mk<t_btree_uuu__0_1_2__111>();
  souffle::RelationWrapper<23, t_btree_uuu__0_1_2__111, Tuple<RamDomain, 3>, 3,
                           0>
      wrapper_rel_28_instruction_get_src_op;
  // -- Table: inter_procedural_jump
  Own<t_btree_uu__0_1__11> rel_29_inter_procedural_jump =
      mk<t_btree_uu__0_1__11>();
  souffle::RelationWrapper<24, t_btree_uu__0_1__11, Tuple<RamDomain, 2>, 2, 0>
      wrapper_rel_29_inter_procedural_jump;
  // -- Table: jump_equal_operation
  Own<t_btree_i__0__1> rel_30_jump_equal_operation = mk<t_btree_i__0__1>();
  souffle::RelationWrapper<25, t_btree_i__0__1, Tuple<RamDomain, 1>, 1, 0>
      wrapper_rel_30_jump_equal_operation;
  // -- Table: jump_table_start
  Own<t_btree_uuuui__0_1_2_3_4__11111__10000> rel_31_jump_table_start =
      mk<t_btree_uuuui__0_1_2_3_4__11111__10000>();
  souffle::RelationWrapper<26, t_btree_uuuui__0_1_2_3_4__11111__10000,
                           Tuple<RamDomain, 5>, 5, 0>
      wrapper_rel_31_jump_table_start;
  // -- Table: jump_unequal_operation
  Own<t_btree_i__0__1> rel_32_jump_unequal_operation = mk<t_btree_i__0__1>();
  souffle::RelationWrapper<27, t_btree_i__0__1, Tuple<RamDomain, 1>, 1, 0>
      wrapper_rel_32_jump_unequal_operation;
  // -- Table: last_def
  Own<t_btree_uui__0_2_1__101__111> rel_33_last_def =
      mk<t_btree_uui__0_2_1__101__111>();
  souffle::RelationWrapper<28, t_btree_uui__0_2_1__101__111,
                           Tuple<RamDomain, 3>, 3, 0>
      wrapper_rel_33_last_def;
  // -- Table: local_next
  Own<t_btree_uu__0_1__11__10> rel_34_local_next =
      mk<t_btree_uu__0_1__11__10>();
  souffle::RelationWrapper<29, t_btree_uu__0_1__11__10, Tuple<RamDomain, 2>, 2,
                           0>
      wrapper_rel_34_local_next;
  // -- Table: may_fallthrough
  Own<t_btree_uu__0_1__11__10> rel_35_may_fallthrough =
      mk<t_btree_uu__0_1__11__10>();
  souffle::RelationWrapper<30, t_btree_uu__0_1__11__10, Tuple<RamDomain, 2>, 2,
                           0>
      wrapper_rel_35_may_fallthrough;
  // -- Table: must_def
  Own<t_btree_ui__0_1__11> rel_36_must_def = mk<t_btree_ui__0_1__11>();
  souffle::RelationWrapper<31, t_btree_ui__0_1__11, Tuple<RamDomain, 2>, 2, 0>
      wrapper_rel_36_must_def;
  // -- Table: next
  Own<t_btree_uu__0_1__11__10> rel_37_next = mk<t_btree_uu__0_1__11__10>();
  souffle::RelationWrapper<32, t_btree_uu__0_1__11__10, Tuple<RamDomain, 2>, 2,
                           0>
      wrapper_rel_37_next;
  // -- Table: op_indirect_contains_reg
  Own<t_btree_ui__0_1__11__10> rel_38_op_indirect_contains_reg =
      mk<t_btree_ui__0_1__11__10>();
  souffle::RelationWrapper<33, t_btree_ui__0_1__11__10, Tuple<RamDomain, 2>, 2,
                           0>
      wrapper_rel_38_op_indirect_contains_reg;
  // -- Table: op_regdirect_contains_reg
  Own<t_btree_ui__0_1__11__10> rel_39_op_regdirect_contains_reg =
      mk<t_btree_ui__0_1__11__10>();
  souffle::RelationWrapper<34, t_btree_ui__0_1__11__10, Tuple<RamDomain, 2>, 2,
                           0>
      wrapper_rel_39_op_regdirect_contains_reg;
  // -- Table: relative_address
  Own<t_btree_uuuuui__1_2_3_0_4_5__111111__011100> rel_40_relative_address =
      mk<t_btree_uuuuui__1_2_3_0_4_5__111111__011100>();
  souffle::RelationWrapper<35, t_btree_uuuuui__1_2_3_0_4_5__111111__011100,
                           Tuple<RamDomain, 6>, 6, 0>
      wrapper_rel_40_relative_address;
  // -- Table: return
  Own<t_btree_u__0__2__1> rel_41_return = mk<t_btree_u__0__2__1>();
  souffle::RelationWrapper<36, t_btree_u__0__2__1, Tuple<RamDomain, 1>, 1, 0>
      wrapper_rel_41_return;
  // -- Table: return_val_reg
  Own<t_btree_i__0__1> rel_42_return_val_reg = mk<t_btree_i__0__1>();
  souffle::RelationWrapper<37, t_btree_i__0__1, Tuple<RamDomain, 1>, 1, 0>
      wrapper_rel_42_return_val_reg;
  // -- Table: unconditional_jump
  Own<t_btree_u__0__1> rel_43_unconditional_jump = mk<t_btree_u__0__1>();
  souffle::RelationWrapper<38, t_btree_u__0__1, Tuple<RamDomain, 1>, 1, 0>
      wrapper_rel_43_unconditional_jump;
  // -- Table: used
  Own<t_btree_uiu__0_1_2__110__111> rel_44_used =
      mk<t_btree_uiu__0_1_2__110__111>();
  souffle::RelationWrapper<39, t_btree_uiu__0_1_2__110__111,
                           Tuple<RamDomain, 3>, 3, 0>
      wrapper_rel_44_used;
  // -- Table: used_in_block
  Own<t_btree_ui__0_1__11> rel_45_used_in_block = mk<t_btree_ui__0_1__11>();
  souffle::RelationWrapper<40, t_btree_ui__0_1__11, Tuple<RamDomain, 2>, 2, 0>
      wrapper_rel_45_used_in_block;

public:
  Sf_test_def()
      : wrapper_rel_5_block(*rel_5_block, symTable, "block",
                            std::array<const char *, 1>{{"u:address"}},
                            std::array<const char *, 1>{{"block"}}),

        wrapper_rel_6_block_last_def(
            *rel_6_block_last_def, symTable, "block_last_def",
            std::array<const char *, 3>{
                {"u:address", "u:address", "s:register"}},
            std::array<const char *, 3>{{"EA", "EA_def", "Reg"}}),

        wrapper_rel_7_block_last_instruction(
            *rel_7_block_last_instruction, symTable, "block_last_instruction",
            std::array<const char *, 2>{{"u:address", "u:address"}},
            std::array<const char *, 2>{{"block", "EA"}}),

        wrapper_rel_8_block_next(
            *rel_8_block_next, symTable, "block_next",
            std::array<const char *, 2>{{"u:address", "u:address"}},
            std::array<const char *, 2>{{"ea", "ea_next"}}),

        wrapper_rel_9_call_operation(
            *rel_9_call_operation, symTable, "call_operation",
            std::array<const char *, 1>{{"s:symbol"}},
            std::array<const char *, 1>{{"operation"}}),

        wrapper_rel_10_cmp_immediate_to_reg(
            *rel_10_cmp_immediate_to_reg, symTable, "cmp_immediate_to_reg",
            std::array<const char *, 4>{
                {"u:address", "s:register", "u:operand_index", "i:number"}},
            std::array<const char *, 4>{
                {"EA", "Reg", "Imm_index", "Immediate"}}),

        wrapper_rel_11_code(*rel_11_code, symTable, "code",
                            std::array<const char *, 1>{{"u:address"}},
                            std::array<const char *, 1>{{"ea"}}),

        wrapper_rel_12_code_in_block(
            *rel_12_code_in_block, symTable, "code_in_block",
            std::array<const char *, 2>{{"u:address", "u:address"}},
            std::array<const char *, 2>{{"ea", "block"}}),

        wrapper_rel_13_conditional_mov(
            *rel_13_conditional_mov, symTable, "conditional_mov",
            std::array<const char *, 1>{{"u:address"}},
            std::array<const char *, 1>{{"EA"}}),

        wrapper_rel_14_def(
            *rel_14_def, symTable, "def",
            std::array<const char *, 2>{{"u:address", "s:register"}},
            std::array<const char *, 2>{{"EA", "Reg"}}),

        wrapper_rel_15_def_used(
            *rel_15_def_used, symTable, "def_used",
            std::array<const char *, 4>{
                {"u:address", "s:register", "u:address", "u:operand_index"}},
            std::array<const char *, 4>{
                {"ea_def", "reg", "ea_used", "index_used"}}),

        wrapper_rel_16_def_used_intra(
            *rel_16_def_used_intra, symTable, "def_used_intra",
            std::array<const char *, 4>{
                {"u:address", "s:register", "u:address", "u:operand_index"}},
            std::array<const char *, 4>{
                {"ea_def", "reg", "ea_used", "index_used"}}),

        wrapper_rel_17_def_used_return_val_reg(
            *rel_17_def_used_return_val_reg, symTable,
            "def_used_return_val_reg",
            std::array<const char *, 5>{{"u:address", "u:address", "s:register",
                                         "u:address", "u:operand_index"}},
            std::array<const char *, 5>{
                {"ea_def", "ea_call", "reg", "ea_used", "index_used"}}),

        wrapper_rel_18_defined_in_block(
            *rel_18_defined_in_block, symTable, "defined_in_block",
            std::array<const char *, 2>{{"u:address", "s:register"}},
            std::array<const char *, 2>{{"EA", "Reg"}}),

        wrapper_rel_19_direct_call(
            *rel_19_direct_call, symTable, "direct_call",
            std::array<const char *, 2>{{"u:address", "u:address"}},
            std::array<const char *, 2>{{"src", "dest"}}),

        wrapper_rel_20_direct_jump(
            *rel_20_direct_jump, symTable, "direct_jump",
            std::array<const char *, 2>{{"u:address", "u:address"}},
            std::array<const char *, 2>{{"src", "dest"}}),

        wrapper_rel_21_fde_addresses(
            *rel_21_fde_addresses, symTable, "fde_addresses",
            std::array<const char *, 2>{{"u:address", "u:address"}},
            std::array<const char *, 2>{{"start", "end"}}),

        wrapper_rel_22_flow_def(
            *rel_22_flow_def, symTable, "flow_def",
            std::array<const char *, 4>{
                {"u:address", "s:register", "u:address", "i:number"}},
            std::array<const char *, 4>{{"EA", "Reg", "EA_next", "Value"}}),

        wrapper_rel_23_function_non_maintained_reg(
            *rel_23_function_non_maintained_reg, symTable,
            "function_non_maintained_reg",
            std::array<const char *, 1>{{"s:register"}},
            std::array<const char *, 1>{{"reg"}}),

        wrapper_rel_24_get_pc_thunk(
            *rel_24_get_pc_thunk, symTable, "get_pc_thunk",
            std::array<const char *, 2>{{"u:address", "s:register"}},
            std::array<const char *, 2>{{"EA", "Reg"}}),

        wrapper_rel_25_instruction(
            *rel_25_instruction, symTable, "instruction",
            std::array<const char *, 10>{
                {"u:address", "u:unsigned", "s:symbol", "s:symbol",
                 "u:operand_code", "u:operand_code", "u:operand_code",
                 "u:operand_code", "u:unsigned", "u:unsigned"}},
            std::array<const char *, 10>{{"ea", "size", "prefix", "opcode",
                                          "op1", "op2", "op3", "op4",
                                          "immOffset", "displacementOffset"}}),

        wrapper_rel_26_instruction_get_dest_op(
            *rel_26_instruction_get_dest_op, symTable,
            "instruction_get_dest_op",
            std::array<const char *, 3>{
                {"u:address", "i:number", "u:operand_code"}},
            std::array<const char *, 3>{{"ea", "index", "op"}}),

        wrapper_rel_27_instruction_get_op(
            *rel_27_instruction_get_op, symTable, "instruction_get_op",
            std::array<const char *, 3>{
                {"u:address", "u:operand_index", "u:operand_code"}},
            std::array<const char *, 3>{{"ea", "index", "operator"}}),

        wrapper_rel_28_instruction_get_src_op(
            *rel_28_instruction_get_src_op, symTable, "instruction_get_src_op",
            std::array<const char *, 3>{
                {"u:address", "u:operand_index", "u:operand_code"}},
            std::array<const char *, 3>{{"ea", "Index", "op"}}),

        wrapper_rel_29_inter_procedural_jump(
            *rel_29_inter_procedural_jump, symTable, "inter_procedural_jump",
            std::array<const char *, 2>{{"u:address", "u:address"}},
            std::array<const char *, 2>{{"Src", "Dest"}}),

        wrapper_rel_30_jump_equal_operation(
            *rel_30_jump_equal_operation, symTable, "jump_equal_operation",
            std::array<const char *, 1>{{"s:symbol"}},
            std::array<const char *, 1>{{"n"}}),

        wrapper_rel_31_jump_table_start(
            *rel_31_jump_table_start, symTable, "jump_table_start",
            std::array<const char *, 5>{{"u:address", "u:unsigned", "u:address",
                                         "u:address", "s:symbol"}},
            std::array<const char *, 5>{
                {"EA_jump", "Size", "TableStart", "TableRef", "Operation"}}),

        wrapper_rel_32_jump_unequal_operation(
            *rel_32_jump_unequal_operation, symTable, "jump_unequal_operation",
            std::array<const char *, 1>{{"s:symbol"}},
            std::array<const char *, 1>{{"n"}}),

        wrapper_rel_33_last_def(
            *rel_33_last_def, symTable, "last_def",
            std::array<const char *, 3>{
                {"u:address", "u:address", "s:register"}},
            std::array<const char *, 3>{{"EA", "EA_def", "Reg"}}),

        wrapper_rel_34_local_next(
            *rel_34_local_next, symTable, "local_next",
            std::array<const char *, 2>{{"u:address", "u:address"}},
            std::array<const char *, 2>{{"ea", "ea_next"}}),

        wrapper_rel_35_may_fallthrough(
            *rel_35_may_fallthrough, symTable, "may_fallthrough",
            std::array<const char *, 2>{{"u:address", "u:address"}},
            std::array<const char *, 2>{{"o", "d"}}),

        wrapper_rel_36_must_def(
            *rel_36_must_def, symTable, "must_def",
            std::array<const char *, 2>{{"u:address", "s:register"}},
            std::array<const char *, 2>{{"EA", "Reg"}}),

        wrapper_rel_37_next(
            *rel_37_next, symTable, "next",
            std::array<const char *, 2>{{"u:address", "u:address"}},
            std::array<const char *, 2>{{"n", "m"}}),

        wrapper_rel_38_op_indirect_contains_reg(
            *rel_38_op_indirect_contains_reg, symTable,
            "op_indirect_contains_reg",
            std::array<const char *, 2>{{"u:operand_code", "s:register"}},
            std::array<const char *, 2>{{"op", "reg"}}),

        wrapper_rel_39_op_regdirect_contains_reg(
            *rel_39_op_regdirect_contains_reg, symTable,
            "op_regdirect_contains_reg",
            std::array<const char *, 2>{{"u:operand_code", "s:register"}},
            std::array<const char *, 2>{{"op", "reg"}}),

        wrapper_rel_40_relative_address(
            *rel_40_relative_address, symTable, "relative_address",
            std::array<const char *, 6>{{"u:address", "u:unsigned", "u:address",
                                         "u:address", "u:address", "s:symbol"}},
            std::array<const char *, 6>{{"EA", "Size", "TableStart",
                                         "Reference", "Dest",
                                         "DestIsFirstOrSecond"}}),

        wrapper_rel_41_return(*rel_41_return, symTable, "return",
                              std::array<const char *, 1>{{"u:address"}},
                              std::array<const char *, 1>{{"n"}}),

        wrapper_rel_42_return_val_reg(
            *rel_42_return_val_reg, symTable, "return_val_reg",
            std::array<const char *, 1>{{"s:register"}},
            std::array<const char *, 1>{{"reg"}}),

        wrapper_rel_43_unconditional_jump(
            *rel_43_unconditional_jump, symTable, "unconditional_jump",
            std::array<const char *, 1>{{"u:address"}},
            std::array<const char *, 1>{{"n"}}),

        wrapper_rel_44_used(
            *rel_44_used, symTable, "used",
            std::array<const char *, 3>{
                {"u:address", "s:register", "u:operand_index"}},
            std::array<const char *, 3>{{"EA", "Reg", "Index"}}),

        wrapper_rel_45_used_in_block(
            *rel_45_used_in_block, symTable, "used_in_block",
            std::array<const char *, 2>{{"u:address", "s:register"}},
            std::array<const char *, 2>{{"EA", "Reg"}}) {
    addRelation("block", &wrapper_rel_5_block, true, true);
    addRelation("block_last_def", &wrapper_rel_6_block_last_def, false, true);
    addRelation("block_last_instruction", &wrapper_rel_7_block_last_instruction,
                true, true);
    addRelation("block_next", &wrapper_rel_8_block_next, false, true);
    addRelation("call_operation", &wrapper_rel_9_call_operation, false, false);
    addRelation("cmp_immediate_to_reg", &wrapper_rel_10_cmp_immediate_to_reg,
                true, true);
    addRelation("code", &wrapper_rel_11_code, true, true);
    addRelation("code_in_block", &wrapper_rel_12_code_in_block, true, true);
    addRelation("conditional_mov", &wrapper_rel_13_conditional_mov, true, true);
    addRelation("def", &wrapper_rel_14_def, false, true);
    addRelation("def_used", &wrapper_rel_15_def_used, false, true);
    addRelation("def_used_intra", &wrapper_rel_16_def_used_intra, false, true);
    addRelation("def_used_return_val_reg",
                &wrapper_rel_17_def_used_return_val_reg, false, true);
    addRelation("defined_in_block", &wrapper_rel_18_defined_in_block, false,
                true);
    addRelation("direct_call", &wrapper_rel_19_direct_call, true, true);
    addRelation("direct_jump", &wrapper_rel_20_direct_jump, true, true);
    addRelation("fde_addresses", &wrapper_rel_21_fde_addresses, true, true);
    addRelation("flow_def", &wrapper_rel_22_flow_def, false, true);
    addRelation("function_non_maintained_reg",
                &wrapper_rel_23_function_non_maintained_reg, false, true);
    addRelation("get_pc_thunk", &wrapper_rel_24_get_pc_thunk, true, true);
    addRelation("instruction", &wrapper_rel_25_instruction, true, true);
    addRelation("instruction_get_dest_op",
                &wrapper_rel_26_instruction_get_dest_op, true, true);
    addRelation("instruction_get_op", &wrapper_rel_27_instruction_get_op, true,
                true);
    addRelation("instruction_get_src_op",
                &wrapper_rel_28_instruction_get_src_op, true, true);
    addRelation("inter_procedural_jump", &wrapper_rel_29_inter_procedural_jump,
                false, true);
    addRelation("jump_equal_operation", &wrapper_rel_30_jump_equal_operation,
                false, true);
    addRelation("jump_table_start", &wrapper_rel_31_jump_table_start, true,
                true);
    addRelation("jump_unequal_operation",
                &wrapper_rel_32_jump_unequal_operation, false, true);
    addRelation("last_def", &wrapper_rel_33_last_def, false, true);
    addRelation("local_next", &wrapper_rel_34_local_next, false, true);
    addRelation("may_fallthrough", &wrapper_rel_35_may_fallthrough, true, true);
    addRelation("must_def", &wrapper_rel_36_must_def, false, true);
    addRelation("next", &wrapper_rel_37_next, true, true);
    addRelation("op_indirect_contains_reg",
                &wrapper_rel_38_op_indirect_contains_reg, true, true);
    addRelation("op_regdirect_contains_reg",
                &wrapper_rel_39_op_regdirect_contains_reg, true, false);
    addRelation("relative_address", &wrapper_rel_40_relative_address, true,
                true);
    addRelation("return", &wrapper_rel_41_return, true, true);
    addRelation("return_val_reg", &wrapper_rel_42_return_val_reg, false, true);
    addRelation("unconditional_jump", &wrapper_rel_43_unconditional_jump, true,
                true);
    addRelation("used", &wrapper_rel_44_used, false, true);
    addRelation("used_in_block", &wrapper_rel_45_used_in_block, false, true);
  }
  ~Sf_test_def() {}

private:
  std::string inputDirectory;
  std::string outputDirectory;
  bool performIO;
  std::atomic<RamDomain> ctr{};

  std::atomic<size_t> iter{};
  void runFunction(std::string inputDirectoryArg = "",
                   std::string outputDirectoryArg = "",
                   bool performIOArg = false) {
    this->inputDirectory = inputDirectoryArg;
    this->outputDirectory = outputDirectoryArg;
    this->performIO = performIOArg;
    SignalHandler::instance()->set();
#if defined(_OPENMP)
    if (getNumThreads() > 0) {
      omp_set_num_threads(getNumThreads());
    }
#endif

    // -- query evaluation --
    {
      std::vector<RamDomain> args, ret;
      subroutine_0(args, ret);
    }
    {
      std::vector<RamDomain> args, ret;
      subroutine_1(args, ret);
    }
    {
      std::vector<RamDomain> args, ret;
      subroutine_12(args, ret);
    }
    {
      std::vector<RamDomain> args, ret;
      subroutine_23(args, ret);
    }
    {
      std::vector<RamDomain> args, ret;
      subroutine_34(args, ret);
    }
    {
      std::vector<RamDomain> args, ret;
      subroutine_36(args, ret);
    }
    {
      std::vector<RamDomain> args, ret;
      subroutine_37(args, ret);
    }
    {
      std::vector<RamDomain> args, ret;
      subroutine_38(args, ret);
    }
    {
      std::vector<RamDomain> args, ret;
      subroutine_39(args, ret);
    }
    {
      std::vector<RamDomain> args, ret;
      subroutine_40(args, ret);
    }
    {
      std::vector<RamDomain> args, ret;
      subroutine_2(args, ret);
    }
    {
      std::vector<RamDomain> args, ret;
      subroutine_3(args, ret);
    }
    {
      std::vector<RamDomain> args, ret;
      subroutine_4(args, ret);
    }
    {
      std::vector<RamDomain> args, ret;
      subroutine_5(args, ret);
    }
    {
      std::vector<RamDomain> args, ret;
      subroutine_6(args, ret);
    }
    {
      std::vector<RamDomain> args, ret;
      subroutine_7(args, ret);
    }
    {
      std::vector<RamDomain> args, ret;
      subroutine_8(args, ret);
    }
    {
      std::vector<RamDomain> args, ret;
      subroutine_9(args, ret);
    }
    {
      std::vector<RamDomain> args, ret;
      subroutine_10(args, ret);
    }
    {
      std::vector<RamDomain> args, ret;
      subroutine_11(args, ret);
    }
    {
      std::vector<RamDomain> args, ret;
      subroutine_13(args, ret);
    }
    {
      std::vector<RamDomain> args, ret;
      subroutine_14(args, ret);
    }
    {
      std::vector<RamDomain> args, ret;
      subroutine_15(args, ret);
    }
    {
      std::vector<RamDomain> args, ret;
      subroutine_16(args, ret);
    }
    {
      std::vector<RamDomain> args, ret;
      subroutine_17(args, ret);
    }
    {
      std::vector<RamDomain> args, ret;
      subroutine_18(args, ret);
    }
    {
      std::vector<RamDomain> args, ret;
      subroutine_19(args, ret);
    }
    {
      std::vector<RamDomain> args, ret;
      subroutine_20(args, ret);
    }
    {
      std::vector<RamDomain> args, ret;
      subroutine_21(args, ret);
    }
    {
      std::vector<RamDomain> args, ret;
      subroutine_22(args, ret);
    }
    {
      std::vector<RamDomain> args, ret;
      subroutine_24(args, ret);
    }
    {
      std::vector<RamDomain> args, ret;
      subroutine_25(args, ret);
    }
    {
      std::vector<RamDomain> args, ret;
      subroutine_26(args, ret);
    }
    {
      std::vector<RamDomain> args, ret;
      subroutine_27(args, ret);
    }
    {
      std::vector<RamDomain> args, ret;
      subroutine_28(args, ret);
    }
    {
      std::vector<RamDomain> args, ret;
      subroutine_29(args, ret);
    }
    {
      std::vector<RamDomain> args, ret;
      subroutine_30(args, ret);
    }
    {
      std::vector<RamDomain> args, ret;
      subroutine_31(args, ret);
    }
    {
      std::vector<RamDomain> args, ret;
      subroutine_32(args, ret);
    }
    {
      std::vector<RamDomain> args, ret;
      subroutine_33(args, ret);
    }
    {
      std::vector<RamDomain> args, ret;
      subroutine_35(args, ret);
    }

    // -- relation hint statistics --
    SignalHandler::instance()->reset();
  }

public:
  void run() override { runFunction("", "", false); }

public:
  void runAll(std::string inputDirectoryArg = "",
              std::string outputDirectoryArg = "") override {
    runFunction(inputDirectoryArg, outputDirectoryArg, true);
  }

public:
  void printAll(std::string outputDirectoryArg = "") override {
    try {
      std::map<std::string, std::string> directiveMap(
          {{"IO", "file"},
           {"attributeNames", "EA\tReg"},
           {"name", "get_pc_thunk"},
           {"operation", "output"},
           {"output-dir", "."},
           {"params", "{\"records\": {}, \"relation\": {\"arity\": 2, "
                      "\"auxArity\": 0, \"params\": [\"EA\", \"Reg\"]}}"},
           {"types",
            "{\"ADTs\": {}, \"records\": {}, \"relation\": {\"arity\": 2, "
            "\"auxArity\": 0, \"types\": [\"u:address\", \"s:register\"]}}"}});
      if (!outputDirectoryArg.empty()) {
        directiveMap["output-dir"] = outputDirectoryArg;
      }
      IOSystem::getInstance()
          .getWriter(directiveMap, symTable, recordTable)
          ->writeAll(*rel_24_get_pc_thunk);
    } catch (std::exception &e) {
      std::cerr << e.what();
      exit(1);
    }
    try {
      std::map<std::string, std::string> directiveMap(
          {{"IO", "file"},
           {"attributeNames", "ea\tsize\tprefix\topcode\top1\top2\top3\top4\tim"
                              "mOffset\tdisplacementOffset"},
           {"name", "instruction"},
           {"operation", "output"},
           {"output-dir", "."},
           {"params", "{\"records\": {}, \"relation\": {\"arity\": 10, "
                      "\"auxArity\": 0, \"params\": [\"ea\", \"size\", "
                      "\"prefix\", \"opcode\", \"op1\", \"op2\", \"op3\", "
                      "\"op4\", \"immOffset\", \"displacementOffset\"]}}"},
           {"types",
            "{\"ADTs\": {}, \"records\": {}, \"relation\": {\"arity\": 10, "
            "\"auxArity\": 0, \"types\": [\"u:address\", \"u:unsigned\", "
            "\"s:symbol\", \"s:symbol\", \"u:operand_code\", "
            "\"u:operand_code\", \"u:operand_code\", \"u:operand_code\", "
            "\"u:unsigned\", \"u:unsigned\"]}}"}});
      if (!outputDirectoryArg.empty()) {
        directiveMap["output-dir"] = outputDirectoryArg;
      }
      IOSystem::getInstance()
          .getWriter(directiveMap, symTable, recordTable)
          ->writeAll(*rel_25_instruction);
    } catch (std::exception &e) {
      std::cerr << e.what();
      exit(1);
    }
    try {
      std::map<std::string, std::string> directiveMap(
          {{"IO", "file"},
           {"attributeNames", "ea\tindex\top"},
           {"name", "instruction_get_dest_op"},
           {"operation", "output"},
           {"output-dir", "."},
           {"params",
            "{\"records\": {}, \"relation\": {\"arity\": 3, \"auxArity\": 0, "
            "\"params\": [\"ea\", \"index\", \"op\"]}}"},
           {"types", "{\"ADTs\": {}, \"records\": {}, \"relation\": "
                     "{\"arity\": 3, \"auxArity\": 0, \"types\": "
                     "[\"u:address\", \"i:number\", \"u:operand_code\"]}}"}});
      if (!outputDirectoryArg.empty()) {
        directiveMap["output-dir"] = outputDirectoryArg;
      }
      IOSystem::getInstance()
          .getWriter(directiveMap, symTable, recordTable)
          ->writeAll(*rel_26_instruction_get_dest_op);
    } catch (std::exception &e) {
      std::cerr << e.what();
      exit(1);
    }
    try {
      std::map<std::string, std::string> directiveMap(
          {{"IO", "file"},
           {"attributeNames", "EA\tReg"},
           {"name", "def"},
           {"operation", "output"},
           {"output-dir", "."},
           {"params", "{\"records\": {}, \"relation\": {\"arity\": 2, "
                      "\"auxArity\": 0, \"params\": [\"EA\", \"Reg\"]}}"},
           {"types",
            "{\"ADTs\": {}, \"records\": {}, \"relation\": {\"arity\": 2, "
            "\"auxArity\": 0, \"types\": [\"u:address\", \"s:register\"]}}"}});
      if (!outputDirectoryArg.empty()) {
        directiveMap["output-dir"] = outputDirectoryArg;
      }
      IOSystem::getInstance()
          .getWriter(directiveMap, symTable, recordTable)
          ->writeAll(*rel_14_def);
    } catch (std::exception &e) {
      std::cerr << e.what();
      exit(1);
    }
    try {
      std::map<std::string, std::string> directiveMap(
          {{"IO", "file"},
           {"attributeNames", "ea\tblock"},
           {"name", "code_in_block"},
           {"operation", "output"},
           {"output-dir", "."},
           {"params", "{\"records\": {}, \"relation\": {\"arity\": 2, "
                      "\"auxArity\": 0, \"params\": [\"ea\", \"block\"]}}"},
           {"types",
            "{\"ADTs\": {}, \"records\": {}, \"relation\": {\"arity\": 2, "
            "\"auxArity\": 0, \"types\": [\"u:address\", \"u:address\"]}}"}});
      if (!outputDirectoryArg.empty()) {
        directiveMap["output-dir"] = outputDirectoryArg;
      }
      IOSystem::getInstance()
          .getWriter(directiveMap, symTable, recordTable)
          ->writeAll(*rel_12_code_in_block);
    } catch (std::exception &e) {
      std::cerr << e.what();
      exit(1);
    }
    try {
      std::map<std::string, std::string> directiveMap(
          {{"IO", "file"},
           {"attributeNames", "reg"},
           {"name", "function_non_maintained_reg"},
           {"operation", "output"},
           {"output-dir", "."},
           {"params", "{\"records\": {}, \"relation\": {\"arity\": 1, "
                      "\"auxArity\": 0, \"params\": [\"reg\"]}}"},
           {"types",
            "{\"ADTs\": {}, \"records\": {}, \"relation\": {\"arity\": 1, "
            "\"auxArity\": 0, \"types\": [\"s:register\"]}}"}});
      if (!outputDirectoryArg.empty()) {
        directiveMap["output-dir"] = outputDirectoryArg;
      }
      IOSystem::getInstance()
          .getWriter(directiveMap, symTable, recordTable)
          ->writeAll(*rel_23_function_non_maintained_reg);
    } catch (std::exception &e) {
      std::cerr << e.what();
      exit(1);
    }
    try {
      std::map<std::string, std::string> directiveMap(
          {{"IO", "file"},
           {"attributeNames", "EA\tReg"},
           {"name", "defined_in_block"},
           {"operation", "output"},
           {"output-dir", "."},
           {"params", "{\"records\": {}, \"relation\": {\"arity\": 2, "
                      "\"auxArity\": 0, \"params\": [\"EA\", \"Reg\"]}}"},
           {"types",
            "{\"ADTs\": {}, \"records\": {}, \"relation\": {\"arity\": 2, "
            "\"auxArity\": 0, \"types\": [\"u:address\", \"s:register\"]}}"}});
      if (!outputDirectoryArg.empty()) {
        directiveMap["output-dir"] = outputDirectoryArg;
      }
      IOSystem::getInstance()
          .getWriter(directiveMap, symTable, recordTable)
          ->writeAll(*rel_18_defined_in_block);
    } catch (std::exception &e) {
      std::cerr << e.what();
      exit(1);
    }
    try {
      std::map<std::string, std::string> directiveMap(
          {{"IO", "file"},
           {"attributeNames", "o\td"},
           {"name", "may_fallthrough"},
           {"operation", "output"},
           {"output-dir", "."},
           {"params", "{\"records\": {}, \"relation\": {\"arity\": 2, "
                      "\"auxArity\": 0, \"params\": [\"o\", \"d\"]}}"},
           {"types",
            "{\"ADTs\": {}, \"records\": {}, \"relation\": {\"arity\": 2, "
            "\"auxArity\": 0, \"types\": [\"u:address\", \"u:address\"]}}"}});
      if (!outputDirectoryArg.empty()) {
        directiveMap["output-dir"] = outputDirectoryArg;
      }
      IOSystem::getInstance()
          .getWriter(directiveMap, symTable, recordTable)
          ->writeAll(*rel_35_may_fallthrough);
    } catch (std::exception &e) {
      std::cerr << e.what();
      exit(1);
    }
    try {
      std::map<std::string, std::string> directiveMap(
          {{"IO", "file"},
           {"attributeNames", "ea\tea_next"},
           {"name", "local_next"},
           {"operation", "output"},
           {"output-dir", "."},
           {"params", "{\"records\": {}, \"relation\": {\"arity\": 2, "
                      "\"auxArity\": 0, \"params\": [\"ea\", \"ea_next\"]}}"},
           {"types",
            "{\"ADTs\": {}, \"records\": {}, \"relation\": {\"arity\": 2, "
            "\"auxArity\": 0, \"types\": [\"u:address\", \"u:address\"]}}"}});
      if (!outputDirectoryArg.empty()) {
        directiveMap["output-dir"] = outputDirectoryArg;
      }
      IOSystem::getInstance()
          .getWriter(directiveMap, symTable, recordTable)
          ->writeAll(*rel_34_local_next);
    } catch (std::exception &e) {
      std::cerr << e.what();
      exit(1);
    }
    try {
      std::map<std::string, std::string> directiveMap(
          {{"IO", "file"},
           {"attributeNames", "ea"},
           {"name", "code"},
           {"operation", "output"},
           {"output-dir", "."},
           {"params", "{\"records\": {}, \"relation\": {\"arity\": 1, "
                      "\"auxArity\": 0, \"params\": [\"ea\"]}}"},
           {"types",
            "{\"ADTs\": {}, \"records\": {}, \"relation\": {\"arity\": 1, "
            "\"auxArity\": 0, \"types\": [\"u:address\"]}}"}});
      if (!outputDirectoryArg.empty()) {
        directiveMap["output-dir"] = outputDirectoryArg;
      }
      IOSystem::getInstance()
          .getWriter(directiveMap, symTable, recordTable)
          ->writeAll(*rel_11_code);
    } catch (std::exception &e) {
      std::cerr << e.what();
      exit(1);
    }
    try {
      std::map<std::string, std::string> directiveMap(
          {{"IO", "file"},
           {"attributeNames", "src\tdest"},
           {"name", "direct_jump"},
           {"operation", "output"},
           {"output-dir", "."},
           {"params", "{\"records\": {}, \"relation\": {\"arity\": 2, "
                      "\"auxArity\": 0, \"params\": [\"src\", \"dest\"]}}"},
           {"types",
            "{\"ADTs\": {}, \"records\": {}, \"relation\": {\"arity\": 2, "
            "\"auxArity\": 0, \"types\": [\"u:address\", \"u:address\"]}}"}});
      if (!outputDirectoryArg.empty()) {
        directiveMap["output-dir"] = outputDirectoryArg;
      }
      IOSystem::getInstance()
          .getWriter(directiveMap, symTable, recordTable)
          ->writeAll(*rel_20_direct_jump);
    } catch (std::exception &e) {
      std::cerr << e.what();
      exit(1);
    }
    try {
      std::map<std::string, std::string> directiveMap(
          {{"IO", "file"},
           {"attributeNames", "src\tdest"},
           {"name", "direct_call"},
           {"operation", "output"},
           {"output-dir", "."},
           {"params", "{\"records\": {}, \"relation\": {\"arity\": 2, "
                      "\"auxArity\": 0, \"params\": [\"src\", \"dest\"]}}"},
           {"types",
            "{\"ADTs\": {}, \"records\": {}, \"relation\": {\"arity\": 2, "
            "\"auxArity\": 0, \"types\": [\"u:address\", \"u:address\"]}}"}});
      if (!outputDirectoryArg.empty()) {
        directiveMap["output-dir"] = outputDirectoryArg;
      }
      IOSystem::getInstance()
          .getWriter(directiveMap, symTable, recordTable)
          ->writeAll(*rel_19_direct_call);
    } catch (std::exception &e) {
      std::cerr << e.what();
      exit(1);
    }
    try {
      std::map<std::string, std::string> directiveMap(
          {{"IO", "file"},
           {"attributeNames", "n"},
           {"name", "unconditional_jump"},
           {"operation", "output"},
           {"output-dir", "."},
           {"params", "{\"records\": {}, \"relation\": {\"arity\": 1, "
                      "\"auxArity\": 0, \"params\": [\"n\"]}}"},
           {"types",
            "{\"ADTs\": {}, \"records\": {}, \"relation\": {\"arity\": 1, "
            "\"auxArity\": 0, \"types\": [\"u:address\"]}}"}});
      if (!outputDirectoryArg.empty()) {
        directiveMap["output-dir"] = outputDirectoryArg;
      }
      IOSystem::getInstance()
          .getWriter(directiveMap, symTable, recordTable)
          ->writeAll(*rel_43_unconditional_jump);
    } catch (std::exception &e) {
      std::cerr << e.what();
      exit(1);
    }
    try {
      std::map<std::string, std::string> directiveMap(
          {{"IO", "file"},
           {"attributeNames", "Src\tDest"},
           {"name", "inter_procedural_jump"},
           {"operation", "output"},
           {"output-dir", "."},
           {"params", "{\"records\": {}, \"relation\": {\"arity\": 2, "
                      "\"auxArity\": 0, \"params\": [\"Src\", \"Dest\"]}}"},
           {"types",
            "{\"ADTs\": {}, \"records\": {}, \"relation\": {\"arity\": 2, "
            "\"auxArity\": 0, \"types\": [\"u:address\", \"u:address\"]}}"}});
      if (!outputDirectoryArg.empty()) {
        directiveMap["output-dir"] = outputDirectoryArg;
      }
      IOSystem::getInstance()
          .getWriter(directiveMap, symTable, recordTable)
          ->writeAll(*rel_29_inter_procedural_jump);
    } catch (std::exception &e) {
      std::cerr << e.what();
      exit(1);
    }
    try {
      std::map<std::string, std::string> directiveMap(
          {{"IO", "file"},
           {"attributeNames", "block"},
           {"name", "block"},
           {"operation", "output"},
           {"output-dir", "."},
           {"params", "{\"records\": {}, \"relation\": {\"arity\": 1, "
                      "\"auxArity\": 0, \"params\": [\"block\"]}}"},
           {"types",
            "{\"ADTs\": {}, \"records\": {}, \"relation\": {\"arity\": 1, "
            "\"auxArity\": 0, \"types\": [\"u:address\"]}}"}});
      if (!outputDirectoryArg.empty()) {
        directiveMap["output-dir"] = outputDirectoryArg;
      }
      IOSystem::getInstance()
          .getWriter(directiveMap, symTable, recordTable)
          ->writeAll(*rel_5_block);
    } catch (std::exception &e) {
      std::cerr << e.what();
      exit(1);
    }
    try {
      std::map<std::string, std::string> directiveMap(
          {{"IO", "file"},
           {"attributeNames", "block\tEA"},
           {"name", "block_last_instruction"},
           {"operation", "output"},
           {"output-dir", "."},
           {"params", "{\"records\": {}, \"relation\": {\"arity\": 2, "
                      "\"auxArity\": 0, \"params\": [\"block\", \"EA\"]}}"},
           {"types",
            "{\"ADTs\": {}, \"records\": {}, \"relation\": {\"arity\": 2, "
            "\"auxArity\": 0, \"types\": [\"u:address\", \"u:address\"]}}"}});
      if (!outputDirectoryArg.empty()) {
        directiveMap["output-dir"] = outputDirectoryArg;
      }
      IOSystem::getInstance()
          .getWriter(directiveMap, symTable, recordTable)
          ->writeAll(*rel_7_block_last_instruction);
    } catch (std::exception &e) {
      std::cerr << e.what();
      exit(1);
    }
    try {
      std::map<std::string, std::string> directiveMap(
          {{"IO", "file"},
           {"attributeNames", "EA_jump\tSize\tTableStart\tTableRef\tOperation"},
           {"name", "jump_table_start"},
           {"operation", "output"},
           {"output-dir", "."},
           {"params", "{\"records\": {}, \"relation\": {\"arity\": 5, "
                      "\"auxArity\": 0, \"params\": [\"EA_jump\", \"Size\", "
                      "\"TableStart\", \"TableRef\", \"Operation\"]}}"},
           {"types",
            "{\"ADTs\": {}, \"records\": {}, \"relation\": {\"arity\": 5, "
            "\"auxArity\": 0, \"types\": [\"u:address\", \"u:unsigned\", "
            "\"u:address\", \"u:address\", \"s:symbol\"]}}"}});
      if (!outputDirectoryArg.empty()) {
        directiveMap["output-dir"] = outputDirectoryArg;
      }
      IOSystem::getInstance()
          .getWriter(directiveMap, symTable, recordTable)
          ->writeAll(*rel_31_jump_table_start);
    } catch (std::exception &e) {
      std::cerr << e.what();
      exit(1);
    }
    try {
      std::map<std::string, std::string> directiveMap(
          {{"IO", "file"},
           {"attributeNames",
            "EA\tSize\tTableStart\tReference\tDest\tDestIsFirstOrSecond"},
           {"name", "relative_address"},
           {"operation", "output"},
           {"output-dir", "."},
           {"params",
            "{\"records\": {}, \"relation\": {\"arity\": 6, \"auxArity\": 0, "
            "\"params\": [\"EA\", \"Size\", \"TableStart\", \"Reference\", "
            "\"Dest\", \"DestIsFirstOrSecond\"]}}"},
           {"types",
            "{\"ADTs\": {}, \"records\": {}, \"relation\": {\"arity\": 6, "
            "\"auxArity\": 0, \"types\": [\"u:address\", \"u:unsigned\", "
            "\"u:address\", \"u:address\", \"u:address\", \"s:symbol\"]}}"}});
      if (!outputDirectoryArg.empty()) {
        directiveMap["output-dir"] = outputDirectoryArg;
      }
      IOSystem::getInstance()
          .getWriter(directiveMap, symTable, recordTable)
          ->writeAll(*rel_40_relative_address);
    } catch (std::exception &e) {
      std::cerr << e.what();
      exit(1);
    }
    try {
      std::map<std::string, std::string> directiveMap(
          {{"IO", "file"},
           {"attributeNames", "ea\tea_next"},
           {"name", "block_next"},
           {"operation", "output"},
           {"output-dir", "."},
           {"params", "{\"records\": {}, \"relation\": {\"arity\": 2, "
                      "\"auxArity\": 0, \"params\": [\"ea\", \"ea_next\"]}}"},
           {"types",
            "{\"ADTs\": {}, \"records\": {}, \"relation\": {\"arity\": 2, "
            "\"auxArity\": 0, \"types\": [\"u:address\", \"u:address\"]}}"}});
      if (!outputDirectoryArg.empty()) {
        directiveMap["output-dir"] = outputDirectoryArg;
      }
      IOSystem::getInstance()
          .getWriter(directiveMap, symTable, recordTable)
          ->writeAll(*rel_8_block_next);
    } catch (std::exception &e) {
      std::cerr << e.what();
      exit(1);
    }
    try {
      std::map<std::string, std::string> directiveMap(
          {{"IO", "file"},
           {"attributeNames", "EA\tReg\tImm_index\tImmediate"},
           {"name", "cmp_immediate_to_reg"},
           {"operation", "output"},
           {"output-dir", "."},
           {"params",
            "{\"records\": {}, \"relation\": {\"arity\": 4, \"auxArity\": 0, "
            "\"params\": [\"EA\", \"Reg\", \"Imm_index\", \"Immediate\"]}}"},
           {"types",
            "{\"ADTs\": {}, \"records\": {}, \"relation\": {\"arity\": 4, "
            "\"auxArity\": 0, \"types\": [\"u:address\", \"s:register\", "
            "\"u:operand_index\", \"i:number\"]}}"}});
      if (!outputDirectoryArg.empty()) {
        directiveMap["output-dir"] = outputDirectoryArg;
      }
      IOSystem::getInstance()
          .getWriter(directiveMap, symTable, recordTable)
          ->writeAll(*rel_10_cmp_immediate_to_reg);
    } catch (std::exception &e) {
      std::cerr << e.what();
      exit(1);
    }
    try {
      std::map<std::string, std::string> directiveMap(
          {{"IO", "file"},
           {"attributeNames", "n"},
           {"name", "jump_equal_operation"},
           {"operation", "output"},
           {"output-dir", "."},
           {"params", "{\"records\": {}, \"relation\": {\"arity\": 1, "
                      "\"auxArity\": 0, \"params\": [\"n\"]}}"},
           {"types",
            "{\"ADTs\": {}, \"records\": {}, \"relation\": {\"arity\": 1, "
            "\"auxArity\": 0, \"types\": [\"s:symbol\"]}}"}});
      if (!outputDirectoryArg.empty()) {
        directiveMap["output-dir"] = outputDirectoryArg;
      }
      IOSystem::getInstance()
          .getWriter(directiveMap, symTable, recordTable)
          ->writeAll(*rel_30_jump_equal_operation);
    } catch (std::exception &e) {
      std::cerr << e.what();
      exit(1);
    }
    try {
      std::map<std::string, std::string> directiveMap(
          {{"IO", "file"},
           {"attributeNames", "n"},
           {"name", "jump_unequal_operation"},
           {"operation", "output"},
           {"output-dir", "."},
           {"params", "{\"records\": {}, \"relation\": {\"arity\": 1, "
                      "\"auxArity\": 0, \"params\": [\"n\"]}}"},
           {"types",
            "{\"ADTs\": {}, \"records\": {}, \"relation\": {\"arity\": 1, "
            "\"auxArity\": 0, \"types\": [\"s:symbol\"]}}"}});
      if (!outputDirectoryArg.empty()) {
        directiveMap["output-dir"] = outputDirectoryArg;
      }
      IOSystem::getInstance()
          .getWriter(directiveMap, symTable, recordTable)
          ->writeAll(*rel_32_jump_unequal_operation);
    } catch (std::exception &e) {
      std::cerr << e.what();
      exit(1);
    }
    try {
      std::map<std::string, std::string> directiveMap(
          {{"IO", "file"},
           {"attributeNames", "ea\tIndex\top"},
           {"name", "instruction_get_src_op"},
           {"operation", "output"},
           {"output-dir", "."},
           {"params",
            "{\"records\": {}, \"relation\": {\"arity\": 3, \"auxArity\": 0, "
            "\"params\": [\"ea\", \"Index\", \"op\"]}}"},
           {"types",
            "{\"ADTs\": {}, \"records\": {}, \"relation\": {\"arity\": 3, "
            "\"auxArity\": 0, \"types\": [\"u:address\", \"u:operand_index\", "
            "\"u:operand_code\"]}}"}});
      if (!outputDirectoryArg.empty()) {
        directiveMap["output-dir"] = outputDirectoryArg;
      }
      IOSystem::getInstance()
          .getWriter(directiveMap, symTable, recordTable)
          ->writeAll(*rel_28_instruction_get_src_op);
    } catch (std::exception &e) {
      std::cerr << e.what();
      exit(1);
    }
    try {
      std::map<std::string, std::string> directiveMap(
          {{"IO", "file"},
           {"attributeNames", "n\tm"},
           {"name", "next"},
           {"operation", "output"},
           {"output-dir", "."},
           {"params", "{\"records\": {}, \"relation\": {\"arity\": 2, "
                      "\"auxArity\": 0, \"params\": [\"n\", \"m\"]}}"},
           {"types",
            "{\"ADTs\": {}, \"records\": {}, \"relation\": {\"arity\": 2, "
            "\"auxArity\": 0, \"types\": [\"u:address\", \"u:address\"]}}"}});
      if (!outputDirectoryArg.empty()) {
        directiveMap["output-dir"] = outputDirectoryArg;
      }
      IOSystem::getInstance()
          .getWriter(directiveMap, symTable, recordTable)
          ->writeAll(*rel_37_next);
    } catch (std::exception &e) {
      std::cerr << e.what();
      exit(1);
    }
    try {
      std::map<std::string, std::string> directiveMap(
          {{"IO", "file"},
           {"attributeNames", "EA\tReg\tEA_next\tValue"},
           {"name", "flow_def"},
           {"operation", "output"},
           {"output-dir", "."},
           {"params",
            "{\"records\": {}, \"relation\": {\"arity\": 4, \"auxArity\": 0, "
            "\"params\": [\"EA\", \"Reg\", \"EA_next\", \"Value\"]}}"},
           {"types",
            "{\"ADTs\": {}, \"records\": {}, \"relation\": {\"arity\": 4, "
            "\"auxArity\": 0, \"types\": [\"u:address\", \"s:register\", "
            "\"u:address\", \"i:number\"]}}"}});
      if (!outputDirectoryArg.empty()) {
        directiveMap["output-dir"] = outputDirectoryArg;
      }
      IOSystem::getInstance()
          .getWriter(directiveMap, symTable, recordTable)
          ->writeAll(*rel_22_flow_def);
    } catch (std::exception &e) {
      std::cerr << e.what();
      exit(1);
    }
    try {
      std::map<std::string, std::string> directiveMap(
          {{"IO", "file"},
           {"attributeNames", "EA"},
           {"name", "conditional_mov"},
           {"operation", "output"},
           {"output-dir", "."},
           {"params", "{\"records\": {}, \"relation\": {\"arity\": 1, "
                      "\"auxArity\": 0, \"params\": [\"EA\"]}}"},
           {"types",
            "{\"ADTs\": {}, \"records\": {}, \"relation\": {\"arity\": 1, "
            "\"auxArity\": 0, \"types\": [\"u:address\"]}}"}});
      if (!outputDirectoryArg.empty()) {
        directiveMap["output-dir"] = outputDirectoryArg;
      }
      IOSystem::getInstance()
          .getWriter(directiveMap, symTable, recordTable)
          ->writeAll(*rel_13_conditional_mov);
    } catch (std::exception &e) {
      std::cerr << e.what();
      exit(1);
    }
    try {
      std::map<std::string, std::string> directiveMap(
          {{"IO", "file"},
           {"attributeNames", "EA\tReg"},
           {"name", "must_def"},
           {"operation", "output"},
           {"output-dir", "."},
           {"params", "{\"records\": {}, \"relation\": {\"arity\": 2, "
                      "\"auxArity\": 0, \"params\": [\"EA\", \"Reg\"]}}"},
           {"types",
            "{\"ADTs\": {}, \"records\": {}, \"relation\": {\"arity\": 2, "
            "\"auxArity\": 0, \"types\": [\"u:address\", \"s:register\"]}}"}});
      if (!outputDirectoryArg.empty()) {
        directiveMap["output-dir"] = outputDirectoryArg;
      }
      IOSystem::getInstance()
          .getWriter(directiveMap, symTable, recordTable)
          ->writeAll(*rel_36_must_def);
    } catch (std::exception &e) {
      std::cerr << e.what();
      exit(1);
    }
    try {
      std::map<std::string, std::string> directiveMap(
          {{"IO", "file"},
           {"attributeNames", "op\treg"},
           {"name", "op_indirect_contains_reg"},
           {"operation", "output"},
           {"output-dir", "."},
           {"params", "{\"records\": {}, \"relation\": {\"arity\": 2, "
                      "\"auxArity\": 0, \"params\": [\"op\", \"reg\"]}}"},
           {"types", "{\"ADTs\": {}, \"records\": {}, \"relation\": "
                     "{\"arity\": 2, \"auxArity\": 0, \"types\": "
                     "[\"u:operand_code\", \"s:register\"]}}"}});
      if (!outputDirectoryArg.empty()) {
        directiveMap["output-dir"] = outputDirectoryArg;
      }
      IOSystem::getInstance()
          .getWriter(directiveMap, symTable, recordTable)
          ->writeAll(*rel_38_op_indirect_contains_reg);
    } catch (std::exception &e) {
      std::cerr << e.what();
      exit(1);
    }
    try {
      std::map<std::string, std::string> directiveMap(
          {{"IO", "file"},
           {"attributeNames", "EA\tEA_def\tReg"},
           {"name", "block_last_def"},
           {"operation", "output"},
           {"output-dir", "."},
           {"params",
            "{\"records\": {}, \"relation\": {\"arity\": 3, \"auxArity\": 0, "
            "\"params\": [\"EA\", \"EA_def\", \"Reg\"]}}"},
           {"types", "{\"ADTs\": {}, \"records\": {}, \"relation\": "
                     "{\"arity\": 3, \"auxArity\": 0, \"types\": "
                     "[\"u:address\", \"u:address\", \"s:register\"]}}"}});
      if (!outputDirectoryArg.empty()) {
        directiveMap["output-dir"] = outputDirectoryArg;
      }
      IOSystem::getInstance()
          .getWriter(directiveMap, symTable, recordTable)
          ->writeAll(*rel_6_block_last_def);
    } catch (std::exception &e) {
      std::cerr << e.what();
      exit(1);
    }
    try {
      std::map<std::string, std::string> directiveMap(
          {{"IO", "file"},
           {"attributeNames", "ea_def\treg\tea_used\tindex_used"},
           {"name", "def_used_intra"},
           {"operation", "output"},
           {"output-dir", "."},
           {"params",
            "{\"records\": {}, \"relation\": {\"arity\": 4, \"auxArity\": 0, "
            "\"params\": [\"ea_def\", \"reg\", \"ea_used\", \"index_used\"]}}"},
           {"types",
            "{\"ADTs\": {}, \"records\": {}, \"relation\": {\"arity\": 4, "
            "\"auxArity\": 0, \"types\": [\"u:address\", \"s:register\", "
            "\"u:address\", \"u:operand_index\"]}}"}});
      if (!outputDirectoryArg.empty()) {
        directiveMap["output-dir"] = outputDirectoryArg;
      }
      IOSystem::getInstance()
          .getWriter(directiveMap, symTable, recordTable)
          ->writeAll(*rel_16_def_used_intra);
    } catch (std::exception &e) {
      std::cerr << e.what();
      exit(1);
    }
    try {
      std::map<std::string, std::string> directiveMap(
          {{"IO", "file"},
           {"attributeNames", "ea\tindex\toperator"},
           {"name", "instruction_get_op"},
           {"operation", "output"},
           {"output-dir", "."},
           {"params",
            "{\"records\": {}, \"relation\": {\"arity\": 3, \"auxArity\": 0, "
            "\"params\": [\"ea\", \"index\", \"operator\"]}}"},
           {"types",
            "{\"ADTs\": {}, \"records\": {}, \"relation\": {\"arity\": 3, "
            "\"auxArity\": 0, \"types\": [\"u:address\", \"u:operand_index\", "
            "\"u:operand_code\"]}}"}});
      if (!outputDirectoryArg.empty()) {
        directiveMap["output-dir"] = outputDirectoryArg;
      }
      IOSystem::getInstance()
          .getWriter(directiveMap, symTable, recordTable)
          ->writeAll(*rel_27_instruction_get_op);
    } catch (std::exception &e) {
      std::cerr << e.what();
      exit(1);
    }
    try {
      std::map<std::string, std::string> directiveMap(
          {{"IO", "file"},
           {"attributeNames", "EA\tEA_def\tReg"},
           {"name", "last_def"},
           {"operation", "output"},
           {"output-dir", "."},
           {"params",
            "{\"records\": {}, \"relation\": {\"arity\": 3, \"auxArity\": 0, "
            "\"params\": [\"EA\", \"EA_def\", \"Reg\"]}}"},
           {"types", "{\"ADTs\": {}, \"records\": {}, \"relation\": "
                     "{\"arity\": 3, \"auxArity\": 0, \"types\": "
                     "[\"u:address\", \"u:address\", \"s:register\"]}}"}});
      if (!outputDirectoryArg.empty()) {
        directiveMap["output-dir"] = outputDirectoryArg;
      }
      IOSystem::getInstance()
          .getWriter(directiveMap, symTable, recordTable)
          ->writeAll(*rel_33_last_def);
    } catch (std::exception &e) {
      std::cerr << e.what();
      exit(1);
    }
    try {
      std::map<std::string, std::string> directiveMap(
          {{"IO", "file"},
           {"attributeNames", "EA\tReg"},
           {"name", "used_in_block"},
           {"operation", "output"},
           {"output-dir", "."},
           {"params", "{\"records\": {}, \"relation\": {\"arity\": 2, "
                      "\"auxArity\": 0, \"params\": [\"EA\", \"Reg\"]}}"},
           {"types",
            "{\"ADTs\": {}, \"records\": {}, \"relation\": {\"arity\": 2, "
            "\"auxArity\": 0, \"types\": [\"u:address\", \"s:register\"]}}"}});
      if (!outputDirectoryArg.empty()) {
        directiveMap["output-dir"] = outputDirectoryArg;
      }
      IOSystem::getInstance()
          .getWriter(directiveMap, symTable, recordTable)
          ->writeAll(*rel_45_used_in_block);
    } catch (std::exception &e) {
      std::cerr << e.what();
      exit(1);
    }
    try {
      std::map<std::string, std::string> directiveMap(
          {{"IO", "file"},
           {"attributeNames", "EA\tReg\tIndex"},
           {"name", "used"},
           {"operation", "output"},
           {"output-dir", "."},
           {"params",
            "{\"records\": {}, \"relation\": {\"arity\": 3, \"auxArity\": 0, "
            "\"params\": [\"EA\", \"Reg\", \"Index\"]}}"},
           {"types",
            "{\"ADTs\": {}, \"records\": {}, \"relation\": {\"arity\": 3, "
            "\"auxArity\": 0, \"types\": [\"u:address\", \"s:register\", "
            "\"u:operand_index\"]}}"}});
      if (!outputDirectoryArg.empty()) {
        directiveMap["output-dir"] = outputDirectoryArg;
      }
      IOSystem::getInstance()
          .getWriter(directiveMap, symTable, recordTable)
          ->writeAll(*rel_44_used);
    } catch (std::exception &e) {
      std::cerr << e.what();
      exit(1);
    }
    try {
      std::map<std::string, std::string> directiveMap(
          {{"IO", "file"},
           {"attributeNames", "start\tend"},
           {"name", "fde_addresses"},
           {"operation", "output"},
           {"output-dir", "."},
           {"params", "{\"records\": {}, \"relation\": {\"arity\": 2, "
                      "\"auxArity\": 0, \"params\": [\"start\", \"end\"]}}"},
           {"types",
            "{\"ADTs\": {}, \"records\": {}, \"relation\": {\"arity\": 2, "
            "\"auxArity\": 0, \"types\": [\"u:address\", \"u:address\"]}}"}});
      if (!outputDirectoryArg.empty()) {
        directiveMap["output-dir"] = outputDirectoryArg;
      }
      IOSystem::getInstance()
          .getWriter(directiveMap, symTable, recordTable)
          ->writeAll(*rel_21_fde_addresses);
    } catch (std::exception &e) {
      std::cerr << e.what();
      exit(1);
    }
    try {
      std::map<std::string, std::string> directiveMap(
          {{"IO", "file"},
           {"attributeNames", "n"},
           {"name", "return"},
           {"operation", "output"},
           {"output-dir", "."},
           {"params", "{\"records\": {}, \"relation\": {\"arity\": 1, "
                      "\"auxArity\": 0, \"params\": [\"n\"]}}"},
           {"types",
            "{\"ADTs\": {}, \"records\": {}, \"relation\": {\"arity\": 1, "
            "\"auxArity\": 0, \"types\": [\"u:address\"]}}"}});
      if (!outputDirectoryArg.empty()) {
        directiveMap["output-dir"] = outputDirectoryArg;
      }
      IOSystem::getInstance()
          .getWriter(directiveMap, symTable, recordTable)
          ->writeAll(*rel_41_return);
    } catch (std::exception &e) {
      std::cerr << e.what();
      exit(1);
    }
    try {
      std::map<std::string, std::string> directiveMap(
          {{"IO", "file"},
           {"attributeNames", "reg"},
           {"name", "return_val_reg"},
           {"operation", "output"},
           {"output-dir", "."},
           {"params", "{\"records\": {}, \"relation\": {\"arity\": 1, "
                      "\"auxArity\": 0, \"params\": [\"reg\"]}}"},
           {"types",
            "{\"ADTs\": {}, \"records\": {}, \"relation\": {\"arity\": 1, "
            "\"auxArity\": 0, \"types\": [\"s:register\"]}}"}});
      if (!outputDirectoryArg.empty()) {
        directiveMap["output-dir"] = outputDirectoryArg;
      }
      IOSystem::getInstance()
          .getWriter(directiveMap, symTable, recordTable)
          ->writeAll(*rel_42_return_val_reg);
    } catch (std::exception &e) {
      std::cerr << e.what();
      exit(1);
    }
    try {
      std::map<std::string, std::string> directiveMap(
          {{"IO", "file"},
           {"attributeNames", "ea_def\tea_call\treg\tea_used\tindex_used"},
           {"name", "def_used_return_val_reg"},
           {"operation", "output"},
           {"output-dir", "."},
           {"params", "{\"records\": {}, \"relation\": {\"arity\": 5, "
                      "\"auxArity\": 0, \"params\": [\"ea_def\", \"ea_call\", "
                      "\"reg\", \"ea_used\", \"index_used\"]}}"},
           {"types",
            "{\"ADTs\": {}, \"records\": {}, \"relation\": {\"arity\": 5, "
            "\"auxArity\": 0, \"types\": [\"u:address\", \"u:address\", "
            "\"s:register\", \"u:address\", \"u:operand_index\"]}}"}});
      if (!outputDirectoryArg.empty()) {
        directiveMap["output-dir"] = outputDirectoryArg;
      }
      IOSystem::getInstance()
          .getWriter(directiveMap, symTable, recordTable)
          ->writeAll(*rel_17_def_used_return_val_reg);
    } catch (std::exception &e) {
      std::cerr << e.what();
      exit(1);
    }
    try {
      std::map<std::string, std::string> directiveMap(
          {{"IO", "file"},
           {"attributeNames", "ea_def\treg\tea_used\tindex_used"},
           {"name", "def_used"},
           {"operation", "output"},
           {"output-dir", "."},
           {"params",
            "{\"records\": {}, \"relation\": {\"arity\": 4, \"auxArity\": 0, "
            "\"params\": [\"ea_def\", \"reg\", \"ea_used\", \"index_used\"]}}"},
           {"types",
            "{\"ADTs\": {}, \"records\": {}, \"relation\": {\"arity\": 4, "
            "\"auxArity\": 0, \"types\": [\"u:address\", \"s:register\", "
            "\"u:address\", \"u:operand_index\"]}}"}});
      if (!outputDirectoryArg.empty()) {
        directiveMap["output-dir"] = outputDirectoryArg;
      }
      IOSystem::getInstance()
          .getWriter(directiveMap, symTable, recordTable)
          ->writeAll(*rel_15_def_used);
    } catch (std::exception &e) {
      std::cerr << e.what();
      exit(1);
    }
  }

public:
  void loadAll(std::string inputDirectoryArg = "") override {
    try {
      std::map<std::string, std::string> directiveMap(
          {{"IO", "file"},
           {"attributeNames", "ea\tsize\tprefix\topcode\top1\top2\top3\top4\tim"
                              "mOffset\tdisplacementOffset"},
           {"fact-dir", "."},
           {"filename", "instruction.csv"},
           {"name", "instruction"},
           {"operation", "input"},
           {"params", "{\"records\": {}, \"relation\": {\"arity\": 10, "
                      "\"auxArity\": 0, \"params\": [\"ea\", \"size\", "
                      "\"prefix\", \"opcode\", \"op1\", \"op2\", \"op3\", "
                      "\"op4\", \"immOffset\", \"displacementOffset\"]}}"},
           {"types",
            "{\"ADTs\": {}, \"records\": {}, \"relation\": {\"arity\": 10, "
            "\"auxArity\": 0, \"types\": [\"u:address\", \"u:unsigned\", "
            "\"s:symbol\", \"s:symbol\", \"u:operand_code\", "
            "\"u:operand_code\", \"u:operand_code\", \"u:operand_code\", "
            "\"u:unsigned\", \"u:unsigned\"]}}"}});
      if (!inputDirectoryArg.empty()) {
        directiveMap["fact-dir"] = inputDirectoryArg;
      }
      IOSystem::getInstance()
          .getReader(directiveMap, symTable, recordTable)
          ->readAll(*rel_25_instruction);
    } catch (std::exception &e) {
      std::cerr << "Error loading data: " << e.what() << '\n';
    }
    try {
      std::map<std::string, std::string> directiveMap(
          {{"IO", "file"},
           {"attributeNames", "ea\tblock"},
           {"fact-dir", "."},
           {"filename", "code_in_block.csv"},
           {"name", "code_in_block"},
           {"operation", "input"},
           {"params", "{\"records\": {}, \"relation\": {\"arity\": 2, "
                      "\"auxArity\": 0, \"params\": [\"ea\", \"block\"]}}"},
           {"types",
            "{\"ADTs\": {}, \"records\": {}, \"relation\": {\"arity\": 2, "
            "\"auxArity\": 0, \"types\": [\"u:address\", \"u:address\"]}}"}});
      if (!inputDirectoryArg.empty()) {
        directiveMap["fact-dir"] = inputDirectoryArg;
      }
      IOSystem::getInstance()
          .getReader(directiveMap, symTable, recordTable)
          ->readAll(*rel_12_code_in_block);
    } catch (std::exception &e) {
      std::cerr << "Error loading data: " << e.what() << '\n';
    }
    try {
      std::map<std::string, std::string> directiveMap(
          {{"IO", "file"},
           {"attributeNames", "EA\tReg"},
           {"fact-dir", "."},
           {"filename", "get_pc_thunk.csv"},
           {"name", "get_pc_thunk"},
           {"operation", "input"},
           {"params", "{\"records\": {}, \"relation\": {\"arity\": 2, "
                      "\"auxArity\": 0, \"params\": [\"EA\", \"Reg\"]}}"},
           {"types",
            "{\"ADTs\": {}, \"records\": {}, \"relation\": {\"arity\": 2, "
            "\"auxArity\": 0, \"types\": [\"u:address\", \"s:register\"]}}"}});
      if (!inputDirectoryArg.empty()) {
        directiveMap["fact-dir"] = inputDirectoryArg;
      }
      IOSystem::getInstance()
          .getReader(directiveMap, symTable, recordTable)
          ->readAll(*rel_24_get_pc_thunk);
    } catch (std::exception &e) {
      std::cerr << "Error loading data: " << e.what() << '\n';
    }
    try {
      std::map<std::string, std::string> directiveMap(
          {{"IO", "file"},
           {"attributeNames", "src\tdest"},
           {"fact-dir", "."},
           {"filename", "direct_call.csv"},
           {"name", "direct_call"},
           {"operation", "input"},
           {"params", "{\"records\": {}, \"relation\": {\"arity\": 2, "
                      "\"auxArity\": 0, \"params\": [\"src\", \"dest\"]}}"},
           {"types",
            "{\"ADTs\": {}, \"records\": {}, \"relation\": {\"arity\": 2, "
            "\"auxArity\": 0, \"types\": [\"u:address\", \"u:address\"]}}"}});
      if (!inputDirectoryArg.empty()) {
        directiveMap["fact-dir"] = inputDirectoryArg;
      }
      IOSystem::getInstance()
          .getReader(directiveMap, symTable, recordTable)
          ->readAll(*rel_19_direct_call);
    } catch (std::exception &e) {
      std::cerr << "Error loading data: " << e.what() << '\n';
    }
    try {
      std::map<std::string, std::string> directiveMap(
          {{"IO", "file"},
           {"attributeNames", "op\treg"},
           {"fact-dir", "."},
           {"filename", "op_regdirect_contains_reg.csv"},
           {"name", "op_regdirect_contains_reg"},
           {"operation", "input"},
           {"params", "{\"records\": {}, \"relation\": {\"arity\": 2, "
                      "\"auxArity\": 0, \"params\": [\"op\", \"reg\"]}}"},
           {"types", "{\"ADTs\": {}, \"records\": {}, \"relation\": "
                     "{\"arity\": 2, \"auxArity\": 0, \"types\": "
                     "[\"u:operand_code\", \"s:register\"]}}"}});
      if (!inputDirectoryArg.empty()) {
        directiveMap["fact-dir"] = inputDirectoryArg;
      }
      IOSystem::getInstance()
          .getReader(directiveMap, symTable, recordTable)
          ->readAll(*rel_39_op_regdirect_contains_reg);
    } catch (std::exception &e) {
      std::cerr << "Error loading data: " << e.what() << '\n';
    }
    try {
      std::map<std::string, std::string> directiveMap(
          {{"IO", "file"},
           {"attributeNames", "o\td"},
           {"fact-dir", "."},
           {"filename", "may_fallthrough.csv"},
           {"name", "may_fallthrough"},
           {"operation", "input"},
           {"params", "{\"records\": {}, \"relation\": {\"arity\": 2, "
                      "\"auxArity\": 0, \"params\": [\"o\", \"d\"]}}"},
           {"types",
            "{\"ADTs\": {}, \"records\": {}, \"relation\": {\"arity\": 2, "
            "\"auxArity\": 0, \"types\": [\"u:address\", \"u:address\"]}}"}});
      if (!inputDirectoryArg.empty()) {
        directiveMap["fact-dir"] = inputDirectoryArg;
      }
      IOSystem::getInstance()
          .getReader(directiveMap, symTable, recordTable)
          ->readAll(*rel_35_may_fallthrough);
    } catch (std::exception &e) {
      std::cerr << "Error loading data: " << e.what() << '\n';
    }
    try {
      std::map<std::string, std::string> directiveMap(
          {{"IO", "file"},
           {"attributeNames", "ea"},
           {"fact-dir", "."},
           {"filename", "code.csv"},
           {"name", "code"},
           {"operation", "input"},
           {"params", "{\"records\": {}, \"relation\": {\"arity\": 1, "
                      "\"auxArity\": 0, \"params\": [\"ea\"]}}"},
           {"types",
            "{\"ADTs\": {}, \"records\": {}, \"relation\": {\"arity\": 1, "
            "\"auxArity\": 0, \"types\": [\"u:address\"]}}"}});
      if (!inputDirectoryArg.empty()) {
        directiveMap["fact-dir"] = inputDirectoryArg;
      }
      IOSystem::getInstance()
          .getReader(directiveMap, symTable, recordTable)
          ->readAll(*rel_11_code);
    } catch (std::exception &e) {
      std::cerr << "Error loading data: " << e.what() << '\n';
    }
    try {
      std::map<std::string, std::string> directiveMap(
          {{"IO", "file"},
           {"attributeNames", "src\tdest"},
           {"fact-dir", "."},
           {"filename", "direct_jump.csv"},
           {"name", "direct_jump"},
           {"operation", "input"},
           {"params", "{\"records\": {}, \"relation\": {\"arity\": 2, "
                      "\"auxArity\": 0, \"params\": [\"src\", \"dest\"]}}"},
           {"types",
            "{\"ADTs\": {}, \"records\": {}, \"relation\": {\"arity\": 2, "
            "\"auxArity\": 0, \"types\": [\"u:address\", \"u:address\"]}}"}});
      if (!inputDirectoryArg.empty()) {
        directiveMap["fact-dir"] = inputDirectoryArg;
      }
      IOSystem::getInstance()
          .getReader(directiveMap, symTable, recordTable)
          ->readAll(*rel_20_direct_jump);
    } catch (std::exception &e) {
      std::cerr << "Error loading data: " << e.what() << '\n';
    }
    try {
      std::map<std::string, std::string> directiveMap(
          {{"IO", "file"},
           {"attributeNames", "n"},
           {"fact-dir", "."},
           {"filename", "unconditional_jump.csv"},
           {"name", "unconditional_jump"},
           {"operation", "input"},
           {"params", "{\"records\": {}, \"relation\": {\"arity\": 1, "
                      "\"auxArity\": 0, \"params\": [\"n\"]}}"},
           {"types",
            "{\"ADTs\": {}, \"records\": {}, \"relation\": {\"arity\": 1, "
            "\"auxArity\": 0, \"types\": [\"u:address\"]}}"}});
      if (!inputDirectoryArg.empty()) {
        directiveMap["fact-dir"] = inputDirectoryArg;
      }
      IOSystem::getInstance()
          .getReader(directiveMap, symTable, recordTable)
          ->readAll(*rel_43_unconditional_jump);
    } catch (std::exception &e) {
      std::cerr << "Error loading data: " << e.what() << '\n';
    }
    try {
      std::map<std::string, std::string> directiveMap(
          {{"IO", "file"},
           {"attributeNames", "ea\tindex\top"},
           {"fact-dir", "."},
           {"filename", "instruction_get_dest_op.csv"},
           {"name", "instruction_get_dest_op"},
           {"operation", "input"},
           {"params",
            "{\"records\": {}, \"relation\": {\"arity\": 3, \"auxArity\": 0, "
            "\"params\": [\"ea\", \"index\", \"op\"]}}"},
           {"types", "{\"ADTs\": {}, \"records\": {}, \"relation\": "
                     "{\"arity\": 3, \"auxArity\": 0, \"types\": "
                     "[\"u:address\", \"i:number\", \"u:operand_code\"]}}"}});
      if (!inputDirectoryArg.empty()) {
        directiveMap["fact-dir"] = inputDirectoryArg;
      }
      IOSystem::getInstance()
          .getReader(directiveMap, symTable, recordTable)
          ->readAll(*rel_26_instruction_get_dest_op);
    } catch (std::exception &e) {
      std::cerr << "Error loading data: " << e.what() << '\n';
    }
    try {
      std::map<std::string, std::string> directiveMap(
          {{"IO", "file"},
           {"attributeNames", "block"},
           {"fact-dir", "."},
           {"filename", "block.csv"},
           {"name", "block"},
           {"operation", "input"},
           {"params", "{\"records\": {}, \"relation\": {\"arity\": 1, "
                      "\"auxArity\": 0, \"params\": [\"block\"]}}"},
           {"types",
            "{\"ADTs\": {}, \"records\": {}, \"relation\": {\"arity\": 1, "
            "\"auxArity\": 0, \"types\": [\"u:address\"]}}"}});
      if (!inputDirectoryArg.empty()) {
        directiveMap["fact-dir"] = inputDirectoryArg;
      }
      IOSystem::getInstance()
          .getReader(directiveMap, symTable, recordTable)
          ->readAll(*rel_5_block);
    } catch (std::exception &e) {
      std::cerr << "Error loading data: " << e.what() << '\n';
    }
    try {
      std::map<std::string, std::string> directiveMap(
          {{"IO", "file"},
           {"attributeNames", "block\tEA"},
           {"fact-dir", "."},
           {"filename", "block_last_instruction.csv"},
           {"name", "block_last_instruction"},
           {"operation", "input"},
           {"params", "{\"records\": {}, \"relation\": {\"arity\": 2, "
                      "\"auxArity\": 0, \"params\": [\"block\", \"EA\"]}}"},
           {"types",
            "{\"ADTs\": {}, \"records\": {}, \"relation\": {\"arity\": 2, "
            "\"auxArity\": 0, \"types\": [\"u:address\", \"u:address\"]}}"}});
      if (!inputDirectoryArg.empty()) {
        directiveMap["fact-dir"] = inputDirectoryArg;
      }
      IOSystem::getInstance()
          .getReader(directiveMap, symTable, recordTable)
          ->readAll(*rel_7_block_last_instruction);
    } catch (std::exception &e) {
      std::cerr << "Error loading data: " << e.what() << '\n';
    }
    try {
      std::map<std::string, std::string> directiveMap(
          {{"IO", "file"},
           {"attributeNames", "EA_jump\tSize\tTableStart\tTableRef\tOperation"},
           {"fact-dir", "."},
           {"filename", "jump_table_start.csv"},
           {"name", "jump_table_start"},
           {"operation", "input"},
           {"params", "{\"records\": {}, \"relation\": {\"arity\": 5, "
                      "\"auxArity\": 0, \"params\": [\"EA_jump\", \"Size\", "
                      "\"TableStart\", \"TableRef\", \"Operation\"]}}"},
           {"types",
            "{\"ADTs\": {}, \"records\": {}, \"relation\": {\"arity\": 5, "
            "\"auxArity\": 0, \"types\": [\"u:address\", \"u:unsigned\", "
            "\"u:address\", \"u:address\", \"s:symbol\"]}}"}});
      if (!inputDirectoryArg.empty()) {
        directiveMap["fact-dir"] = inputDirectoryArg;
      }
      IOSystem::getInstance()
          .getReader(directiveMap, symTable, recordTable)
          ->readAll(*rel_31_jump_table_start);
    } catch (std::exception &e) {
      std::cerr << "Error loading data: " << e.what() << '\n';
    }
    try {
      std::map<std::string, std::string> directiveMap(
          {{"IO", "file"},
           {"attributeNames",
            "EA\tSize\tTableStart\tReference\tDest\tDestIsFirstOrSecond"},
           {"fact-dir", "."},
           {"filename", "relative_address.csv"},
           {"name", "relative_address"},
           {"operation", "input"},
           {"params",
            "{\"records\": {}, \"relation\": {\"arity\": 6, \"auxArity\": 0, "
            "\"params\": [\"EA\", \"Size\", \"TableStart\", \"Reference\", "
            "\"Dest\", \"DestIsFirstOrSecond\"]}}"},
           {"types",
            "{\"ADTs\": {}, \"records\": {}, \"relation\": {\"arity\": 6, "
            "\"auxArity\": 0, \"types\": [\"u:address\", \"u:unsigned\", "
            "\"u:address\", \"u:address\", \"u:address\", \"s:symbol\"]}}"}});
      if (!inputDirectoryArg.empty()) {
        directiveMap["fact-dir"] = inputDirectoryArg;
      }
      IOSystem::getInstance()
          .getReader(directiveMap, symTable, recordTable)
          ->readAll(*rel_40_relative_address);
    } catch (std::exception &e) {
      std::cerr << "Error loading data: " << e.what() << '\n';
    }
    try {
      std::map<std::string, std::string> directiveMap(
          {{"IO", "file"},
           {"attributeNames", "EA\tReg\tImm_index\tImmediate"},
           {"fact-dir", "."},
           {"filename", "cmp_immediate_to_reg.csv"},
           {"name", "cmp_immediate_to_reg"},
           {"operation", "input"},
           {"params",
            "{\"records\": {}, \"relation\": {\"arity\": 4, \"auxArity\": 0, "
            "\"params\": [\"EA\", \"Reg\", \"Imm_index\", \"Immediate\"]}}"},
           {"types",
            "{\"ADTs\": {}, \"records\": {}, \"relation\": {\"arity\": 4, "
            "\"auxArity\": 0, \"types\": [\"u:address\", \"s:register\", "
            "\"u:operand_index\", \"i:number\"]}}"}});
      if (!inputDirectoryArg.empty()) {
        directiveMap["fact-dir"] = inputDirectoryArg;
      }
      IOSystem::getInstance()
          .getReader(directiveMap, symTable, recordTable)
          ->readAll(*rel_10_cmp_immediate_to_reg);
    } catch (std::exception &e) {
      std::cerr << "Error loading data: " << e.what() << '\n';
    }
    try {
      std::map<std::string, std::string> directiveMap(
          {{"IO", "file"},
           {"attributeNames", "n\tm"},
           {"fact-dir", "."},
           {"filename", "next.csv"},
           {"name", "next"},
           {"operation", "input"},
           {"params", "{\"records\": {}, \"relation\": {\"arity\": 2, "
                      "\"auxArity\": 0, \"params\": [\"n\", \"m\"]}}"},
           {"types",
            "{\"ADTs\": {}, \"records\": {}, \"relation\": {\"arity\": 2, "
            "\"auxArity\": 0, \"types\": [\"u:address\", \"u:address\"]}}"}});
      if (!inputDirectoryArg.empty()) {
        directiveMap["fact-dir"] = inputDirectoryArg;
      }
      IOSystem::getInstance()
          .getReader(directiveMap, symTable, recordTable)
          ->readAll(*rel_37_next);
    } catch (std::exception &e) {
      std::cerr << "Error loading data: " << e.what() << '\n';
    }
    try {
      std::map<std::string, std::string> directiveMap(
          {{"IO", "file"},
           {"attributeNames", "EA"},
           {"fact-dir", "."},
           {"filename", "arch.conditional_mov.csv"},
           {"name", "conditional_mov"},
           {"operation", "input"},
           {"params", "{\"records\": {}, \"relation\": {\"arity\": 1, "
                      "\"auxArity\": 0, \"params\": [\"EA\"]}}"},
           {"types",
            "{\"ADTs\": {}, \"records\": {}, \"relation\": {\"arity\": 1, "
            "\"auxArity\": 0, \"types\": [\"u:address\"]}}"}});
      if (!inputDirectoryArg.empty()) {
        directiveMap["fact-dir"] = inputDirectoryArg;
      }
      IOSystem::getInstance()
          .getReader(directiveMap, symTable, recordTable)
          ->readAll(*rel_13_conditional_mov);
    } catch (std::exception &e) {
      std::cerr << "Error loading data: " << e.what() << '\n';
    }
    try {
      std::map<std::string, std::string> directiveMap(
          {{"IO", "file"},
           {"attributeNames", "ea\tindex\toperator"},
           {"fact-dir", "."},
           {"filename", "instruction_get_op.csv"},
           {"name", "instruction_get_op"},
           {"operation", "input"},
           {"params",
            "{\"records\": {}, \"relation\": {\"arity\": 3, \"auxArity\": 0, "
            "\"params\": [\"ea\", \"index\", \"operator\"]}}"},
           {"types",
            "{\"ADTs\": {}, \"records\": {}, \"relation\": {\"arity\": 3, "
            "\"auxArity\": 0, \"types\": [\"u:address\", \"u:operand_index\", "
            "\"u:operand_code\"]}}"}});
      if (!inputDirectoryArg.empty()) {
        directiveMap["fact-dir"] = inputDirectoryArg;
      }
      IOSystem::getInstance()
          .getReader(directiveMap, symTable, recordTable)
          ->readAll(*rel_27_instruction_get_op);
    } catch (std::exception &e) {
      std::cerr << "Error loading data: " << e.what() << '\n';
    }
    try {
      std::map<std::string, std::string> directiveMap(
          {{"IO", "file"},
           {"attributeNames", "op\treg"},
           {"fact-dir", "."},
           {"filename", "op_indirect_contains_reg.csv"},
           {"name", "op_indirect_contains_reg"},
           {"operation", "input"},
           {"params", "{\"records\": {}, \"relation\": {\"arity\": 2, "
                      "\"auxArity\": 0, \"params\": [\"op\", \"reg\"]}}"},
           {"types", "{\"ADTs\": {}, \"records\": {}, \"relation\": "
                     "{\"arity\": 2, \"auxArity\": 0, \"types\": "
                     "[\"u:operand_code\", \"s:register\"]}}"}});
      if (!inputDirectoryArg.empty()) {
        directiveMap["fact-dir"] = inputDirectoryArg;
      }
      IOSystem::getInstance()
          .getReader(directiveMap, symTable, recordTable)
          ->readAll(*rel_38_op_indirect_contains_reg);
    } catch (std::exception &e) {
      std::cerr << "Error loading data: " << e.what() << '\n';
    }
    try {
      std::map<std::string, std::string> directiveMap(
          {{"IO", "file"},
           {"attributeNames", "ea\tIndex\top"},
           {"fact-dir", "."},
           {"filename", "instruction_get_src_op.csv"},
           {"name", "instruction_get_src_op"},
           {"operation", "input"},
           {"params",
            "{\"records\": {}, \"relation\": {\"arity\": 3, \"auxArity\": 0, "
            "\"params\": [\"ea\", \"Index\", \"op\"]}}"},
           {"types",
            "{\"ADTs\": {}, \"records\": {}, \"relation\": {\"arity\": 3, "
            "\"auxArity\": 0, \"types\": [\"u:address\", \"u:operand_index\", "
            "\"u:operand_code\"]}}"}});
      if (!inputDirectoryArg.empty()) {
        directiveMap["fact-dir"] = inputDirectoryArg;
      }
      IOSystem::getInstance()
          .getReader(directiveMap, symTable, recordTable)
          ->readAll(*rel_28_instruction_get_src_op);
    } catch (std::exception &e) {
      std::cerr << "Error loading data: " << e.what() << '\n';
    }
    try {
      std::map<std::string, std::string> directiveMap(
          {{"IO", "file"},
           {"attributeNames", "start\tend"},
           {"fact-dir", "."},
           {"filename", "fde_addresses.csv"},
           {"name", "fde_addresses"},
           {"operation", "input"},
           {"params", "{\"records\": {}, \"relation\": {\"arity\": 2, "
                      "\"auxArity\": 0, \"params\": [\"start\", \"end\"]}}"},
           {"types",
            "{\"ADTs\": {}, \"records\": {}, \"relation\": {\"arity\": 2, "
            "\"auxArity\": 0, \"types\": [\"u:address\", \"u:address\"]}}"}});
      if (!inputDirectoryArg.empty()) {
        directiveMap["fact-dir"] = inputDirectoryArg;
      }
      IOSystem::getInstance()
          .getReader(directiveMap, symTable, recordTable)
          ->readAll(*rel_21_fde_addresses);
    } catch (std::exception &e) {
      std::cerr << "Error loading data: " << e.what() << '\n';
    }
    try {
      std::map<std::string, std::string> directiveMap(
          {{"IO", "file"},
           {"attributeNames", "n"},
           {"fact-dir", "."},
           {"filename", "arch.return.csv"},
           {"name", "return"},
           {"operation", "input"},
           {"params", "{\"records\": {}, \"relation\": {\"arity\": 1, "
                      "\"auxArity\": 0, \"params\": [\"n\"]}}"},
           {"types",
            "{\"ADTs\": {}, \"records\": {}, \"relation\": {\"arity\": 1, "
            "\"auxArity\": 0, \"types\": [\"u:address\"]}}"}});
      if (!inputDirectoryArg.empty()) {
        directiveMap["fact-dir"] = inputDirectoryArg;
      }
      IOSystem::getInstance()
          .getReader(directiveMap, symTable, recordTable)
          ->readAll(*rel_41_return);
    } catch (std::exception &e) {
      std::cerr << "Error loading data: " << e.what() << '\n';
    }
  }

public:
  void dumpInputs() override {
    try {
      std::map<std::string, std::string> rwOperation;
      rwOperation["IO"] = "stdout";
      rwOperation["name"] = "instruction";
      rwOperation["types"] =
          "{\"relation\": {\"arity\": 10, \"auxArity\": 0, \"types\": "
          "[\"u:address\", \"u:unsigned\", \"s:symbol\", \"s:symbol\", "
          "\"u:operand_code\", \"u:operand_code\", \"u:operand_code\", "
          "\"u:operand_code\", \"u:unsigned\", \"u:unsigned\"]}}";
      IOSystem::getInstance()
          .getWriter(rwOperation, symTable, recordTable)
          ->writeAll(*rel_25_instruction);
    } catch (std::exception &e) {
      std::cerr << e.what();
      exit(1);
    }
    try {
      std::map<std::string, std::string> rwOperation;
      rwOperation["IO"] = "stdout";
      rwOperation["name"] = "code_in_block";
      rwOperation["types"] = "{\"relation\": {\"arity\": 2, \"auxArity\": 0, "
                             "\"types\": [\"u:address\", \"u:address\"]}}";
      IOSystem::getInstance()
          .getWriter(rwOperation, symTable, recordTable)
          ->writeAll(*rel_12_code_in_block);
    } catch (std::exception &e) {
      std::cerr << e.what();
      exit(1);
    }
    try {
      std::map<std::string, std::string> rwOperation;
      rwOperation["IO"] = "stdout";
      rwOperation["name"] = "get_pc_thunk";
      rwOperation["types"] = "{\"relation\": {\"arity\": 2, \"auxArity\": 0, "
                             "\"types\": [\"u:address\", \"s:register\"]}}";
      IOSystem::getInstance()
          .getWriter(rwOperation, symTable, recordTable)
          ->writeAll(*rel_24_get_pc_thunk);
    } catch (std::exception &e) {
      std::cerr << e.what();
      exit(1);
    }
    try {
      std::map<std::string, std::string> rwOperation;
      rwOperation["IO"] = "stdout";
      rwOperation["name"] = "direct_call";
      rwOperation["types"] = "{\"relation\": {\"arity\": 2, \"auxArity\": 0, "
                             "\"types\": [\"u:address\", \"u:address\"]}}";
      IOSystem::getInstance()
          .getWriter(rwOperation, symTable, recordTable)
          ->writeAll(*rel_19_direct_call);
    } catch (std::exception &e) {
      std::cerr << e.what();
      exit(1);
    }
    try {
      std::map<std::string, std::string> rwOperation;
      rwOperation["IO"] = "stdout";
      rwOperation["name"] = "op_regdirect_contains_reg";
      rwOperation["types"] =
          "{\"relation\": {\"arity\": 2, \"auxArity\": 0, \"types\": "
          "[\"u:operand_code\", \"s:register\"]}}";
      IOSystem::getInstance()
          .getWriter(rwOperation, symTable, recordTable)
          ->writeAll(*rel_39_op_regdirect_contains_reg);
    } catch (std::exception &e) {
      std::cerr << e.what();
      exit(1);
    }
    try {
      std::map<std::string, std::string> rwOperation;
      rwOperation["IO"] = "stdout";
      rwOperation["name"] = "may_fallthrough";
      rwOperation["types"] = "{\"relation\": {\"arity\": 2, \"auxArity\": 0, "
                             "\"types\": [\"u:address\", \"u:address\"]}}";
      IOSystem::getInstance()
          .getWriter(rwOperation, symTable, recordTable)
          ->writeAll(*rel_35_may_fallthrough);
    } catch (std::exception &e) {
      std::cerr << e.what();
      exit(1);
    }
    try {
      std::map<std::string, std::string> rwOperation;
      rwOperation["IO"] = "stdout";
      rwOperation["name"] = "code";
      rwOperation["types"] = "{\"relation\": {\"arity\": 1, \"auxArity\": 0, "
                             "\"types\": [\"u:address\"]}}";
      IOSystem::getInstance()
          .getWriter(rwOperation, symTable, recordTable)
          ->writeAll(*rel_11_code);
    } catch (std::exception &e) {
      std::cerr << e.what();
      exit(1);
    }
    try {
      std::map<std::string, std::string> rwOperation;
      rwOperation["IO"] = "stdout";
      rwOperation["name"] = "direct_jump";
      rwOperation["types"] = "{\"relation\": {\"arity\": 2, \"auxArity\": 0, "
                             "\"types\": [\"u:address\", \"u:address\"]}}";
      IOSystem::getInstance()
          .getWriter(rwOperation, symTable, recordTable)
          ->writeAll(*rel_20_direct_jump);
    } catch (std::exception &e) {
      std::cerr << e.what();
      exit(1);
    }
    try {
      std::map<std::string, std::string> rwOperation;
      rwOperation["IO"] = "stdout";
      rwOperation["name"] = "unconditional_jump";
      rwOperation["types"] = "{\"relation\": {\"arity\": 1, \"auxArity\": 0, "
                             "\"types\": [\"u:address\"]}}";
      IOSystem::getInstance()
          .getWriter(rwOperation, symTable, recordTable)
          ->writeAll(*rel_43_unconditional_jump);
    } catch (std::exception &e) {
      std::cerr << e.what();
      exit(1);
    }
    try {
      std::map<std::string, std::string> rwOperation;
      rwOperation["IO"] = "stdout";
      rwOperation["name"] = "instruction_get_dest_op";
      rwOperation["types"] =
          "{\"relation\": {\"arity\": 3, \"auxArity\": 0, \"types\": "
          "[\"u:address\", \"i:number\", \"u:operand_code\"]}}";
      IOSystem::getInstance()
          .getWriter(rwOperation, symTable, recordTable)
          ->writeAll(*rel_26_instruction_get_dest_op);
    } catch (std::exception &e) {
      std::cerr << e.what();
      exit(1);
    }
    try {
      std::map<std::string, std::string> rwOperation;
      rwOperation["IO"] = "stdout";
      rwOperation["name"] = "block";
      rwOperation["types"] = "{\"relation\": {\"arity\": 1, \"auxArity\": 0, "
                             "\"types\": [\"u:address\"]}}";
      IOSystem::getInstance()
          .getWriter(rwOperation, symTable, recordTable)
          ->writeAll(*rel_5_block);
    } catch (std::exception &e) {
      std::cerr << e.what();
      exit(1);
    }
    try {
      std::map<std::string, std::string> rwOperation;
      rwOperation["IO"] = "stdout";
      rwOperation["name"] = "block_last_instruction";
      rwOperation["types"] = "{\"relation\": {\"arity\": 2, \"auxArity\": 0, "
                             "\"types\": [\"u:address\", \"u:address\"]}}";
      IOSystem::getInstance()
          .getWriter(rwOperation, symTable, recordTable)
          ->writeAll(*rel_7_block_last_instruction);
    } catch (std::exception &e) {
      std::cerr << e.what();
      exit(1);
    }
    try {
      std::map<std::string, std::string> rwOperation;
      rwOperation["IO"] = "stdout";
      rwOperation["name"] = "jump_table_start";
      rwOperation["types"] = "{\"relation\": {\"arity\": 5, \"auxArity\": 0, "
                             "\"types\": [\"u:address\", \"u:unsigned\", "
                             "\"u:address\", \"u:address\", \"s:symbol\"]}}";
      IOSystem::getInstance()
          .getWriter(rwOperation, symTable, recordTable)
          ->writeAll(*rel_31_jump_table_start);
    } catch (std::exception &e) {
      std::cerr << e.what();
      exit(1);
    }
    try {
      std::map<std::string, std::string> rwOperation;
      rwOperation["IO"] = "stdout";
      rwOperation["name"] = "relative_address";
      rwOperation["types"] =
          "{\"relation\": {\"arity\": 6, \"auxArity\": 0, \"types\": "
          "[\"u:address\", \"u:unsigned\", \"u:address\", \"u:address\", "
          "\"u:address\", \"s:symbol\"]}}";
      IOSystem::getInstance()
          .getWriter(rwOperation, symTable, recordTable)
          ->writeAll(*rel_40_relative_address);
    } catch (std::exception &e) {
      std::cerr << e.what();
      exit(1);
    }
    try {
      std::map<std::string, std::string> rwOperation;
      rwOperation["IO"] = "stdout";
      rwOperation["name"] = "cmp_immediate_to_reg";
      rwOperation["types"] = "{\"relation\": {\"arity\": 4, \"auxArity\": 0, "
                             "\"types\": [\"u:address\", \"s:register\", "
                             "\"u:operand_index\", \"i:number\"]}}";
      IOSystem::getInstance()
          .getWriter(rwOperation, symTable, recordTable)
          ->writeAll(*rel_10_cmp_immediate_to_reg);
    } catch (std::exception &e) {
      std::cerr << e.what();
      exit(1);
    }
    try {
      std::map<std::string, std::string> rwOperation;
      rwOperation["IO"] = "stdout";
      rwOperation["name"] = "next";
      rwOperation["types"] = "{\"relation\": {\"arity\": 2, \"auxArity\": 0, "
                             "\"types\": [\"u:address\", \"u:address\"]}}";
      IOSystem::getInstance()
          .getWriter(rwOperation, symTable, recordTable)
          ->writeAll(*rel_37_next);
    } catch (std::exception &e) {
      std::cerr << e.what();
      exit(1);
    }
    try {
      std::map<std::string, std::string> rwOperation;
      rwOperation["IO"] = "stdout";
      rwOperation["name"] = "conditional_mov";
      rwOperation["types"] = "{\"relation\": {\"arity\": 1, \"auxArity\": 0, "
                             "\"types\": [\"u:address\"]}}";
      IOSystem::getInstance()
          .getWriter(rwOperation, symTable, recordTable)
          ->writeAll(*rel_13_conditional_mov);
    } catch (std::exception &e) {
      std::cerr << e.what();
      exit(1);
    }
    try {
      std::map<std::string, std::string> rwOperation;
      rwOperation["IO"] = "stdout";
      rwOperation["name"] = "instruction_get_op";
      rwOperation["types"] =
          "{\"relation\": {\"arity\": 3, \"auxArity\": 0, \"types\": "
          "[\"u:address\", \"u:operand_index\", \"u:operand_code\"]}}";
      IOSystem::getInstance()
          .getWriter(rwOperation, symTable, recordTable)
          ->writeAll(*rel_27_instruction_get_op);
    } catch (std::exception &e) {
      std::cerr << e.what();
      exit(1);
    }
    try {
      std::map<std::string, std::string> rwOperation;
      rwOperation["IO"] = "stdout";
      rwOperation["name"] = "op_indirect_contains_reg";
      rwOperation["types"] =
          "{\"relation\": {\"arity\": 2, \"auxArity\": 0, \"types\": "
          "[\"u:operand_code\", \"s:register\"]}}";
      IOSystem::getInstance()
          .getWriter(rwOperation, symTable, recordTable)
          ->writeAll(*rel_38_op_indirect_contains_reg);
    } catch (std::exception &e) {
      std::cerr << e.what();
      exit(1);
    }
    try {
      std::map<std::string, std::string> rwOperation;
      rwOperation["IO"] = "stdout";
      rwOperation["name"] = "instruction_get_src_op";
      rwOperation["types"] =
          "{\"relation\": {\"arity\": 3, \"auxArity\": 0, \"types\": "
          "[\"u:address\", \"u:operand_index\", \"u:operand_code\"]}}";
      IOSystem::getInstance()
          .getWriter(rwOperation, symTable, recordTable)
          ->writeAll(*rel_28_instruction_get_src_op);
    } catch (std::exception &e) {
      std::cerr << e.what();
      exit(1);
    }
    try {
      std::map<std::string, std::string> rwOperation;
      rwOperation["IO"] = "stdout";
      rwOperation["name"] = "fde_addresses";
      rwOperation["types"] = "{\"relation\": {\"arity\": 2, \"auxArity\": 0, "
                             "\"types\": [\"u:address\", \"u:address\"]}}";
      IOSystem::getInstance()
          .getWriter(rwOperation, symTable, recordTable)
          ->writeAll(*rel_21_fde_addresses);
    } catch (std::exception &e) {
      std::cerr << e.what();
      exit(1);
    }
    try {
      std::map<std::string, std::string> rwOperation;
      rwOperation["IO"] = "stdout";
      rwOperation["name"] = "return";
      rwOperation["types"] = "{\"relation\": {\"arity\": 1, \"auxArity\": 0, "
                             "\"types\": [\"u:address\"]}}";
      IOSystem::getInstance()
          .getWriter(rwOperation, symTable, recordTable)
          ->writeAll(*rel_41_return);
    } catch (std::exception &e) {
      std::cerr << e.what();
      exit(1);
    }
  }

public:
  void dumpOutputs() override {
    try {
      std::map<std::string, std::string> rwOperation;
      rwOperation["IO"] = "stdout";
      rwOperation["name"] = "get_pc_thunk";
      rwOperation["types"] = "{\"relation\": {\"arity\": 2, \"auxArity\": 0, "
                             "\"types\": [\"u:address\", \"s:register\"]}}";
      IOSystem::getInstance()
          .getWriter(rwOperation, symTable, recordTable)
          ->writeAll(*rel_24_get_pc_thunk);
    } catch (std::exception &e) {
      std::cerr << e.what();
      exit(1);
    }
    try {
      std::map<std::string, std::string> rwOperation;
      rwOperation["IO"] = "stdout";
      rwOperation["name"] = "instruction";
      rwOperation["types"] =
          "{\"relation\": {\"arity\": 10, \"auxArity\": 0, \"types\": "
          "[\"u:address\", \"u:unsigned\", \"s:symbol\", \"s:symbol\", "
          "\"u:operand_code\", \"u:operand_code\", \"u:operand_code\", "
          "\"u:operand_code\", \"u:unsigned\", \"u:unsigned\"]}}";
      IOSystem::getInstance()
          .getWriter(rwOperation, symTable, recordTable)
          ->writeAll(*rel_25_instruction);
    } catch (std::exception &e) {
      std::cerr << e.what();
      exit(1);
    }
    try {
      std::map<std::string, std::string> rwOperation;
      rwOperation["IO"] = "stdout";
      rwOperation["name"] = "instruction_get_dest_op";
      rwOperation["types"] =
          "{\"relation\": {\"arity\": 3, \"auxArity\": 0, \"types\": "
          "[\"u:address\", \"i:number\", \"u:operand_code\"]}}";
      IOSystem::getInstance()
          .getWriter(rwOperation, symTable, recordTable)
          ->writeAll(*rel_26_instruction_get_dest_op);
    } catch (std::exception &e) {
      std::cerr << e.what();
      exit(1);
    }
    try {
      std::map<std::string, std::string> rwOperation;
      rwOperation["IO"] = "stdout";
      rwOperation["name"] = "def";
      rwOperation["types"] = "{\"relation\": {\"arity\": 2, \"auxArity\": 0, "
                             "\"types\": [\"u:address\", \"s:register\"]}}";
      IOSystem::getInstance()
          .getWriter(rwOperation, symTable, recordTable)
          ->writeAll(*rel_14_def);
    } catch (std::exception &e) {
      std::cerr << e.what();
      exit(1);
    }
    try {
      std::map<std::string, std::string> rwOperation;
      rwOperation["IO"] = "stdout";
      rwOperation["name"] = "code_in_block";
      rwOperation["types"] = "{\"relation\": {\"arity\": 2, \"auxArity\": 0, "
                             "\"types\": [\"u:address\", \"u:address\"]}}";
      IOSystem::getInstance()
          .getWriter(rwOperation, symTable, recordTable)
          ->writeAll(*rel_12_code_in_block);
    } catch (std::exception &e) {
      std::cerr << e.what();
      exit(1);
    }
    try {
      std::map<std::string, std::string> rwOperation;
      rwOperation["IO"] = "stdout";
      rwOperation["name"] = "function_non_maintained_reg";
      rwOperation["types"] = "{\"relation\": {\"arity\": 1, \"auxArity\": 0, "
                             "\"types\": [\"s:register\"]}}";
      IOSystem::getInstance()
          .getWriter(rwOperation, symTable, recordTable)
          ->writeAll(*rel_23_function_non_maintained_reg);
    } catch (std::exception &e) {
      std::cerr << e.what();
      exit(1);
    }
    try {
      std::map<std::string, std::string> rwOperation;
      rwOperation["IO"] = "stdout";
      rwOperation["name"] = "defined_in_block";
      rwOperation["types"] = "{\"relation\": {\"arity\": 2, \"auxArity\": 0, "
                             "\"types\": [\"u:address\", \"s:register\"]}}";
      IOSystem::getInstance()
          .getWriter(rwOperation, symTable, recordTable)
          ->writeAll(*rel_18_defined_in_block);
    } catch (std::exception &e) {
      std::cerr << e.what();
      exit(1);
    }
    try {
      std::map<std::string, std::string> rwOperation;
      rwOperation["IO"] = "stdout";
      rwOperation["name"] = "may_fallthrough";
      rwOperation["types"] = "{\"relation\": {\"arity\": 2, \"auxArity\": 0, "
                             "\"types\": [\"u:address\", \"u:address\"]}}";
      IOSystem::getInstance()
          .getWriter(rwOperation, symTable, recordTable)
          ->writeAll(*rel_35_may_fallthrough);
    } catch (std::exception &e) {
      std::cerr << e.what();
      exit(1);
    }
    try {
      std::map<std::string, std::string> rwOperation;
      rwOperation["IO"] = "stdout";
      rwOperation["name"] = "local_next";
      rwOperation["types"] = "{\"relation\": {\"arity\": 2, \"auxArity\": 0, "
                             "\"types\": [\"u:address\", \"u:address\"]}}";
      IOSystem::getInstance()
          .getWriter(rwOperation, symTable, recordTable)
          ->writeAll(*rel_34_local_next);
    } catch (std::exception &e) {
      std::cerr << e.what();
      exit(1);
    }
    try {
      std::map<std::string, std::string> rwOperation;
      rwOperation["IO"] = "stdout";
      rwOperation["name"] = "code";
      rwOperation["types"] = "{\"relation\": {\"arity\": 1, \"auxArity\": 0, "
                             "\"types\": [\"u:address\"]}}";
      IOSystem::getInstance()
          .getWriter(rwOperation, symTable, recordTable)
          ->writeAll(*rel_11_code);
    } catch (std::exception &e) {
      std::cerr << e.what();
      exit(1);
    }
    try {
      std::map<std::string, std::string> rwOperation;
      rwOperation["IO"] = "stdout";
      rwOperation["name"] = "direct_jump";
      rwOperation["types"] = "{\"relation\": {\"arity\": 2, \"auxArity\": 0, "
                             "\"types\": [\"u:address\", \"u:address\"]}}";
      IOSystem::getInstance()
          .getWriter(rwOperation, symTable, recordTable)
          ->writeAll(*rel_20_direct_jump);
    } catch (std::exception &e) {
      std::cerr << e.what();
      exit(1);
    }
    try {
      std::map<std::string, std::string> rwOperation;
      rwOperation["IO"] = "stdout";
      rwOperation["name"] = "direct_call";
      rwOperation["types"] = "{\"relation\": {\"arity\": 2, \"auxArity\": 0, "
                             "\"types\": [\"u:address\", \"u:address\"]}}";
      IOSystem::getInstance()
          .getWriter(rwOperation, symTable, recordTable)
          ->writeAll(*rel_19_direct_call);
    } catch (std::exception &e) {
      std::cerr << e.what();
      exit(1);
    }
    try {
      std::map<std::string, std::string> rwOperation;
      rwOperation["IO"] = "stdout";
      rwOperation["name"] = "unconditional_jump";
      rwOperation["types"] = "{\"relation\": {\"arity\": 1, \"auxArity\": 0, "
                             "\"types\": [\"u:address\"]}}";
      IOSystem::getInstance()
          .getWriter(rwOperation, symTable, recordTable)
          ->writeAll(*rel_43_unconditional_jump);
    } catch (std::exception &e) {
      std::cerr << e.what();
      exit(1);
    }
    try {
      std::map<std::string, std::string> rwOperation;
      rwOperation["IO"] = "stdout";
      rwOperation["name"] = "inter_procedural_jump";
      rwOperation["types"] = "{\"relation\": {\"arity\": 2, \"auxArity\": 0, "
                             "\"types\": [\"u:address\", \"u:address\"]}}";
      IOSystem::getInstance()
          .getWriter(rwOperation, symTable, recordTable)
          ->writeAll(*rel_29_inter_procedural_jump);
    } catch (std::exception &e) {
      std::cerr << e.what();
      exit(1);
    }
    try {
      std::map<std::string, std::string> rwOperation;
      rwOperation["IO"] = "stdout";
      rwOperation["name"] = "block";
      rwOperation["types"] = "{\"relation\": {\"arity\": 1, \"auxArity\": 0, "
                             "\"types\": [\"u:address\"]}}";
      IOSystem::getInstance()
          .getWriter(rwOperation, symTable, recordTable)
          ->writeAll(*rel_5_block);
    } catch (std::exception &e) {
      std::cerr << e.what();
      exit(1);
    }
    try {
      std::map<std::string, std::string> rwOperation;
      rwOperation["IO"] = "stdout";
      rwOperation["name"] = "block_last_instruction";
      rwOperation["types"] = "{\"relation\": {\"arity\": 2, \"auxArity\": 0, "
                             "\"types\": [\"u:address\", \"u:address\"]}}";
      IOSystem::getInstance()
          .getWriter(rwOperation, symTable, recordTable)
          ->writeAll(*rel_7_block_last_instruction);
    } catch (std::exception &e) {
      std::cerr << e.what();
      exit(1);
    }
    try {
      std::map<std::string, std::string> rwOperation;
      rwOperation["IO"] = "stdout";
      rwOperation["name"] = "jump_table_start";
      rwOperation["types"] = "{\"relation\": {\"arity\": 5, \"auxArity\": 0, "
                             "\"types\": [\"u:address\", \"u:unsigned\", "
                             "\"u:address\", \"u:address\", \"s:symbol\"]}}";
      IOSystem::getInstance()
          .getWriter(rwOperation, symTable, recordTable)
          ->writeAll(*rel_31_jump_table_start);
    } catch (std::exception &e) {
      std::cerr << e.what();
      exit(1);
    }
    try {
      std::map<std::string, std::string> rwOperation;
      rwOperation["IO"] = "stdout";
      rwOperation["name"] = "relative_address";
      rwOperation["types"] =
          "{\"relation\": {\"arity\": 6, \"auxArity\": 0, \"types\": "
          "[\"u:address\", \"u:unsigned\", \"u:address\", \"u:address\", "
          "\"u:address\", \"s:symbol\"]}}";
      IOSystem::getInstance()
          .getWriter(rwOperation, symTable, recordTable)
          ->writeAll(*rel_40_relative_address);
    } catch (std::exception &e) {
      std::cerr << e.what();
      exit(1);
    }
    try {
      std::map<std::string, std::string> rwOperation;
      rwOperation["IO"] = "stdout";
      rwOperation["name"] = "block_next";
      rwOperation["types"] = "{\"relation\": {\"arity\": 2, \"auxArity\": 0, "
                             "\"types\": [\"u:address\", \"u:address\"]}}";
      IOSystem::getInstance()
          .getWriter(rwOperation, symTable, recordTable)
          ->writeAll(*rel_8_block_next);
    } catch (std::exception &e) {
      std::cerr << e.what();
      exit(1);
    }
    try {
      std::map<std::string, std::string> rwOperation;
      rwOperation["IO"] = "stdout";
      rwOperation["name"] = "cmp_immediate_to_reg";
      rwOperation["types"] = "{\"relation\": {\"arity\": 4, \"auxArity\": 0, "
                             "\"types\": [\"u:address\", \"s:register\", "
                             "\"u:operand_index\", \"i:number\"]}}";
      IOSystem::getInstance()
          .getWriter(rwOperation, symTable, recordTable)
          ->writeAll(*rel_10_cmp_immediate_to_reg);
    } catch (std::exception &e) {
      std::cerr << e.what();
      exit(1);
    }
    try {
      std::map<std::string, std::string> rwOperation;
      rwOperation["IO"] = "stdout";
      rwOperation["name"] = "jump_equal_operation";
      rwOperation["types"] = "{\"relation\": {\"arity\": 1, \"auxArity\": 0, "
                             "\"types\": [\"s:symbol\"]}}";
      IOSystem::getInstance()
          .getWriter(rwOperation, symTable, recordTable)
          ->writeAll(*rel_30_jump_equal_operation);
    } catch (std::exception &e) {
      std::cerr << e.what();
      exit(1);
    }
    try {
      std::map<std::string, std::string> rwOperation;
      rwOperation["IO"] = "stdout";
      rwOperation["name"] = "jump_unequal_operation";
      rwOperation["types"] = "{\"relation\": {\"arity\": 1, \"auxArity\": 0, "
                             "\"types\": [\"s:symbol\"]}}";
      IOSystem::getInstance()
          .getWriter(rwOperation, symTable, recordTable)
          ->writeAll(*rel_32_jump_unequal_operation);
    } catch (std::exception &e) {
      std::cerr << e.what();
      exit(1);
    }
    try {
      std::map<std::string, std::string> rwOperation;
      rwOperation["IO"] = "stdout";
      rwOperation["name"] = "instruction_get_src_op";
      rwOperation["types"] =
          "{\"relation\": {\"arity\": 3, \"auxArity\": 0, \"types\": "
          "[\"u:address\", \"u:operand_index\", \"u:operand_code\"]}}";
      IOSystem::getInstance()
          .getWriter(rwOperation, symTable, recordTable)
          ->writeAll(*rel_28_instruction_get_src_op);
    } catch (std::exception &e) {
      std::cerr << e.what();
      exit(1);
    }
    try {
      std::map<std::string, std::string> rwOperation;
      rwOperation["IO"] = "stdout";
      rwOperation["name"] = "next";
      rwOperation["types"] = "{\"relation\": {\"arity\": 2, \"auxArity\": 0, "
                             "\"types\": [\"u:address\", \"u:address\"]}}";
      IOSystem::getInstance()
          .getWriter(rwOperation, symTable, recordTable)
          ->writeAll(*rel_37_next);
    } catch (std::exception &e) {
      std::cerr << e.what();
      exit(1);
    }
    try {
      std::map<std::string, std::string> rwOperation;
      rwOperation["IO"] = "stdout";
      rwOperation["name"] = "flow_def";
      rwOperation["types"] =
          "{\"relation\": {\"arity\": 4, \"auxArity\": 0, \"types\": "
          "[\"u:address\", \"s:register\", \"u:address\", \"i:number\"]}}";
      IOSystem::getInstance()
          .getWriter(rwOperation, symTable, recordTable)
          ->writeAll(*rel_22_flow_def);
    } catch (std::exception &e) {
      std::cerr << e.what();
      exit(1);
    }
    try {
      std::map<std::string, std::string> rwOperation;
      rwOperation["IO"] = "stdout";
      rwOperation["name"] = "conditional_mov";
      rwOperation["types"] = "{\"relation\": {\"arity\": 1, \"auxArity\": 0, "
                             "\"types\": [\"u:address\"]}}";
      IOSystem::getInstance()
          .getWriter(rwOperation, symTable, recordTable)
          ->writeAll(*rel_13_conditional_mov);
    } catch (std::exception &e) {
      std::cerr << e.what();
      exit(1);
    }
    try {
      std::map<std::string, std::string> rwOperation;
      rwOperation["IO"] = "stdout";
      rwOperation["name"] = "must_def";
      rwOperation["types"] = "{\"relation\": {\"arity\": 2, \"auxArity\": 0, "
                             "\"types\": [\"u:address\", \"s:register\"]}}";
      IOSystem::getInstance()
          .getWriter(rwOperation, symTable, recordTable)
          ->writeAll(*rel_36_must_def);
    } catch (std::exception &e) {
      std::cerr << e.what();
      exit(1);
    }
    try {
      std::map<std::string, std::string> rwOperation;
      rwOperation["IO"] = "stdout";
      rwOperation["name"] = "op_indirect_contains_reg";
      rwOperation["types"] =
          "{\"relation\": {\"arity\": 2, \"auxArity\": 0, \"types\": "
          "[\"u:operand_code\", \"s:register\"]}}";
      IOSystem::getInstance()
          .getWriter(rwOperation, symTable, recordTable)
          ->writeAll(*rel_38_op_indirect_contains_reg);
    } catch (std::exception &e) {
      std::cerr << e.what();
      exit(1);
    }
    try {
      std::map<std::string, std::string> rwOperation;
      rwOperation["IO"] = "stdout";
      rwOperation["name"] = "block_last_def";
      rwOperation["types"] =
          "{\"relation\": {\"arity\": 3, \"auxArity\": 0, \"types\": "
          "[\"u:address\", \"u:address\", \"s:register\"]}}";
      IOSystem::getInstance()
          .getWriter(rwOperation, symTable, recordTable)
          ->writeAll(*rel_6_block_last_def);
    } catch (std::exception &e) {
      std::cerr << e.what();
      exit(1);
    }
    try {
      std::map<std::string, std::string> rwOperation;
      rwOperation["IO"] = "stdout";
      rwOperation["name"] = "def_used_intra";
      rwOperation["types"] = "{\"relation\": {\"arity\": 4, \"auxArity\": 0, "
                             "\"types\": [\"u:address\", \"s:register\", "
                             "\"u:address\", \"u:operand_index\"]}}";
      IOSystem::getInstance()
          .getWriter(rwOperation, symTable, recordTable)
          ->writeAll(*rel_16_def_used_intra);
    } catch (std::exception &e) {
      std::cerr << e.what();
      exit(1);
    }
    try {
      std::map<std::string, std::string> rwOperation;
      rwOperation["IO"] = "stdout";
      rwOperation["name"] = "instruction_get_op";
      rwOperation["types"] =
          "{\"relation\": {\"arity\": 3, \"auxArity\": 0, \"types\": "
          "[\"u:address\", \"u:operand_index\", \"u:operand_code\"]}}";
      IOSystem::getInstance()
          .getWriter(rwOperation, symTable, recordTable)
          ->writeAll(*rel_27_instruction_get_op);
    } catch (std::exception &e) {
      std::cerr << e.what();
      exit(1);
    }
    try {
      std::map<std::string, std::string> rwOperation;
      rwOperation["IO"] = "stdout";
      rwOperation["name"] = "last_def";
      rwOperation["types"] =
          "{\"relation\": {\"arity\": 3, \"auxArity\": 0, \"types\": "
          "[\"u:address\", \"u:address\", \"s:register\"]}}";
      IOSystem::getInstance()
          .getWriter(rwOperation, symTable, recordTable)
          ->writeAll(*rel_33_last_def);
    } catch (std::exception &e) {
      std::cerr << e.what();
      exit(1);
    }
    try {
      std::map<std::string, std::string> rwOperation;
      rwOperation["IO"] = "stdout";
      rwOperation["name"] = "used_in_block";
      rwOperation["types"] = "{\"relation\": {\"arity\": 2, \"auxArity\": 0, "
                             "\"types\": [\"u:address\", \"s:register\"]}}";
      IOSystem::getInstance()
          .getWriter(rwOperation, symTable, recordTable)
          ->writeAll(*rel_45_used_in_block);
    } catch (std::exception &e) {
      std::cerr << e.what();
      exit(1);
    }
    try {
      std::map<std::string, std::string> rwOperation;
      rwOperation["IO"] = "stdout";
      rwOperation["name"] = "used";
      rwOperation["types"] =
          "{\"relation\": {\"arity\": 3, \"auxArity\": 0, \"types\": "
          "[\"u:address\", \"s:register\", \"u:operand_index\"]}}";
      IOSystem::getInstance()
          .getWriter(rwOperation, symTable, recordTable)
          ->writeAll(*rel_44_used);
    } catch (std::exception &e) {
      std::cerr << e.what();
      exit(1);
    }
    try {
      std::map<std::string, std::string> rwOperation;
      rwOperation["IO"] = "stdout";
      rwOperation["name"] = "fde_addresses";
      rwOperation["types"] = "{\"relation\": {\"arity\": 2, \"auxArity\": 0, "
                             "\"types\": [\"u:address\", \"u:address\"]}}";
      IOSystem::getInstance()
          .getWriter(rwOperation, symTable, recordTable)
          ->writeAll(*rel_21_fde_addresses);
    } catch (std::exception &e) {
      std::cerr << e.what();
      exit(1);
    }
    try {
      std::map<std::string, std::string> rwOperation;
      rwOperation["IO"] = "stdout";
      rwOperation["name"] = "return";
      rwOperation["types"] = "{\"relation\": {\"arity\": 1, \"auxArity\": 0, "
                             "\"types\": [\"u:address\"]}}";
      IOSystem::getInstance()
          .getWriter(rwOperation, symTable, recordTable)
          ->writeAll(*rel_41_return);
    } catch (std::exception &e) {
      std::cerr << e.what();
      exit(1);
    }
    try {
      std::map<std::string, std::string> rwOperation;
      rwOperation["IO"] = "stdout";
      rwOperation["name"] = "return_val_reg";
      rwOperation["types"] = "{\"relation\": {\"arity\": 1, \"auxArity\": 0, "
                             "\"types\": [\"s:register\"]}}";
      IOSystem::getInstance()
          .getWriter(rwOperation, symTable, recordTable)
          ->writeAll(*rel_42_return_val_reg);
    } catch (std::exception &e) {
      std::cerr << e.what();
      exit(1);
    }
    try {
      std::map<std::string, std::string> rwOperation;
      rwOperation["IO"] = "stdout";
      rwOperation["name"] = "def_used_return_val_reg";
      rwOperation["types"] =
          "{\"relation\": {\"arity\": 5, \"auxArity\": 0, \"types\": "
          "[\"u:address\", \"u:address\", \"s:register\", \"u:address\", "
          "\"u:operand_index\"]}}";
      IOSystem::getInstance()
          .getWriter(rwOperation, symTable, recordTable)
          ->writeAll(*rel_17_def_used_return_val_reg);
    } catch (std::exception &e) {
      std::cerr << e.what();
      exit(1);
    }
    try {
      std::map<std::string, std::string> rwOperation;
      rwOperation["IO"] = "stdout";
      rwOperation["name"] = "def_used";
      rwOperation["types"] = "{\"relation\": {\"arity\": 4, \"auxArity\": 0, "
                             "\"types\": [\"u:address\", \"s:register\", "
                             "\"u:address\", \"u:operand_index\"]}}";
      IOSystem::getInstance()
          .getWriter(rwOperation, symTable, recordTable)
          ->writeAll(*rel_15_def_used);
    } catch (std::exception &e) {
      std::cerr << e.what();
      exit(1);
    }
  }

public:
  SymbolTable &getSymbolTable() override { return symTable; }
  void executeSubroutine(std::string name, const std::vector<RamDomain> &args,
                         std::vector<RamDomain> &ret) override {
    if (name == "stratum_0") {
      subroutine_0(args, ret);
      return;
    }
    if (name == "stratum_1") {
      subroutine_1(args, ret);
      return;
    }
    if (name == "stratum_10") {
      subroutine_2(args, ret);
      return;
    }
    if (name == "stratum_11") {
      subroutine_3(args, ret);
      return;
    }
    if (name == "stratum_12") {
      subroutine_4(args, ret);
      return;
    }
    if (name == "stratum_13") {
      subroutine_5(args, ret);
      return;
    }
    if (name == "stratum_14") {
      subroutine_6(args, ret);
      return;
    }
    if (name == "stratum_15") {
      subroutine_7(args, ret);
      return;
    }
    if (name == "stratum_16") {
      subroutine_8(args, ret);
      return;
    }
    if (name == "stratum_17") {
      subroutine_9(args, ret);
      return;
    }
    if (name == "stratum_18") {
      subroutine_10(args, ret);
      return;
    }
    if (name == "stratum_19") {
      subroutine_11(args, ret);
      return;
    }
    if (name == "stratum_2") {
      subroutine_12(args, ret);
      return;
    }
    if (name == "stratum_20") {
      subroutine_13(args, ret);
      return;
    }
    if (name == "stratum_21") {
      subroutine_14(args, ret);
      return;
    }
    if (name == "stratum_22") {
      subroutine_15(args, ret);
      return;
    }
    if (name == "stratum_23") {
      subroutine_16(args, ret);
      return;
    }
    if (name == "stratum_24") {
      subroutine_17(args, ret);
      return;
    }
    if (name == "stratum_25") {
      subroutine_18(args, ret);
      return;
    }
    if (name == "stratum_26") {
      subroutine_19(args, ret);
      return;
    }
    if (name == "stratum_27") {
      subroutine_20(args, ret);
      return;
    }
    if (name == "stratum_28") {
      subroutine_21(args, ret);
      return;
    }
    if (name == "stratum_29") {
      subroutine_22(args, ret);
      return;
    }
    if (name == "stratum_3") {
      subroutine_23(args, ret);
      return;
    }
    if (name == "stratum_30") {
      subroutine_24(args, ret);
      return;
    }
    if (name == "stratum_31") {
      subroutine_25(args, ret);
      return;
    }
    if (name == "stratum_32") {
      subroutine_26(args, ret);
      return;
    }
    if (name == "stratum_33") {
      subroutine_27(args, ret);
      return;
    }
    if (name == "stratum_34") {
      subroutine_28(args, ret);
      return;
    }
    if (name == "stratum_35") {
      subroutine_29(args, ret);
      return;
    }
    if (name == "stratum_36") {
      subroutine_30(args, ret);
      return;
    }
    if (name == "stratum_37") {
      subroutine_31(args, ret);
      return;
    }
    if (name == "stratum_38") {
      subroutine_32(args, ret);
      return;
    }
    if (name == "stratum_39") {
      subroutine_33(args, ret);
      return;
    }
    if (name == "stratum_4") {
      subroutine_34(args, ret);
      return;
    }
    if (name == "stratum_40") {
      subroutine_35(args, ret);
      return;
    }
    if (name == "stratum_5") {
      subroutine_36(args, ret);
      return;
    }
    if (name == "stratum_6") {
      subroutine_37(args, ret);
      return;
    }
    if (name == "stratum_7") {
      subroutine_38(args, ret);
      return;
    }
    if (name == "stratum_8") {
      subroutine_39(args, ret);
      return;
    }
    if (name == "stratum_9") {
      subroutine_40(args, ret);
      return;
    }
    fatal("unknown subroutine");
  }
#ifdef _MSC_VER
#pragma warning(disable : 4100)
#endif // _MSC_VER
  void subroutine_0(const std::vector<RamDomain> &args,
                    std::vector<RamDomain> &ret) {
    SignalHandler::instance()->setMsg(R"_(call_operation("CALL").
in file /reversi/datalog/souffle/test_def.dl [17:1-17:24])_");
    [&]() {
      CREATE_OP_CONTEXT(rel_9_call_operation_op_ctxt,
                        rel_9_call_operation->createContext());
      Tuple<RamDomain, 1> tuple{{ramBitCast(RamSigned(0))}};
      rel_9_call_operation->insert(
          tuple, READ_OP_CONTEXT(rel_9_call_operation_op_ctxt));
    }();
  }
#ifdef _MSC_VER
#pragma warning(default : 4100)
#endif // _MSC_VER
#ifdef _MSC_VER
#pragma warning(disable : 4100)
#endif // _MSC_VER
  void subroutine_1(const std::vector<RamDomain> &args,
                    std::vector<RamDomain> &ret) {
    if (performIO) {
      try {
        std::map<std::string, std::string> directiveMap(
            {{"IO", "file"},
             {"attributeNames", "src\tdest"},
             {"fact-dir", "."},
             {"filename", "direct_call.csv"},
             {"name", "direct_call"},
             {"operation", "input"},
             {"params", "{\"records\": {}, \"relation\": {\"arity\": 2, "
                        "\"auxArity\": 0, \"params\": [\"src\", \"dest\"]}}"},
             {"types",
              "{\"ADTs\": {}, \"records\": {}, \"relation\": {\"arity\": 2, "
              "\"auxArity\": 0, \"types\": [\"u:address\", \"u:address\"]}}"}});
        if (!inputDirectory.empty()) {
          directiveMap["fact-dir"] = inputDirectory;
        }
        IOSystem::getInstance()
            .getReader(directiveMap, symTable, recordTable)
            ->readAll(*rel_19_direct_call);
      } catch (std::exception &e) {
        std::cerr << "Error loading data: " << e.what() << '\n';
      }
    }
    if (performIO) {
      try {
        std::map<std::string, std::string> directiveMap(
            {{"IO", "file"},
             {"attributeNames", "src\tdest"},
             {"name", "direct_call"},
             {"operation", "output"},
             {"output-dir", "."},
             {"params", "{\"records\": {}, \"relation\": {\"arity\": 2, "
                        "\"auxArity\": 0, \"params\": [\"src\", \"dest\"]}}"},
             {"types",
              "{\"ADTs\": {}, \"records\": {}, \"relation\": {\"arity\": 2, "
              "\"auxArity\": 0, \"types\": [\"u:address\", \"u:address\"]}}"}});
        if (!outputDirectory.empty()) {
          directiveMap["output-dir"] = outputDirectory;
        }
        IOSystem::getInstance()
            .getWriter(directiveMap, symTable, recordTable)
            ->writeAll(*rel_19_direct_call);
      } catch (std::exception &e) {
        std::cerr << e.what();
        exit(1);
      }
    }
  }
#ifdef _MSC_VER
#pragma warning(default : 4100)
#endif // _MSC_VER
#ifdef _MSC_VER
#pragma warning(disable : 4100)
#endif // _MSC_VER
  void subroutine_2(const std::vector<RamDomain> &args,
                    std::vector<RamDomain> &ret) {
    if (performIO) {
      try {
        std::map<std::string, std::string> directiveMap(
            {{"IO", "file"},
             {"attributeNames", "o\td"},
             {"fact-dir", "."},
             {"filename", "may_fallthrough.csv"},
             {"name", "may_fallthrough"},
             {"operation", "input"},
             {"params", "{\"records\": {}, \"relation\": {\"arity\": 2, "
                        "\"auxArity\": 0, \"params\": [\"o\", \"d\"]}}"},
             {"types",
              "{\"ADTs\": {}, \"records\": {}, \"relation\": {\"arity\": 2, "
              "\"auxArity\": 0, \"types\": [\"u:address\", \"u:address\"]}}"}});
        if (!inputDirectory.empty()) {
          directiveMap["fact-dir"] = inputDirectory;
        }
        IOSystem::getInstance()
            .getReader(directiveMap, symTable, recordTable)
            ->readAll(*rel_35_may_fallthrough);
      } catch (std::exception &e) {
        std::cerr << "Error loading data: " << e.what() << '\n';
      }
    }
    if (performIO) {
      try {
        std::map<std::string, std::string> directiveMap(
            {{"IO", "file"},
             {"attributeNames", "o\td"},
             {"name", "may_fallthrough"},
             {"operation", "output"},
             {"output-dir", "."},
             {"params", "{\"records\": {}, \"relation\": {\"arity\": 2, "
                        "\"auxArity\": 0, \"params\": [\"o\", \"d\"]}}"},
             {"types",
              "{\"ADTs\": {}, \"records\": {}, \"relation\": {\"arity\": 2, "
              "\"auxArity\": 0, \"types\": [\"u:address\", \"u:address\"]}}"}});
        if (!outputDirectory.empty()) {
          directiveMap["output-dir"] = outputDirectory;
        }
        IOSystem::getInstance()
            .getWriter(directiveMap, symTable, recordTable)
            ->writeAll(*rel_35_may_fallthrough);
      } catch (std::exception &e) {
        std::cerr << e.what();
        exit(1);
      }
    }
  }
#ifdef _MSC_VER
#pragma warning(default : 4100)
#endif // _MSC_VER
#ifdef _MSC_VER
#pragma warning(disable : 4100)
#endif // _MSC_VER
  void subroutine_3(const std::vector<RamDomain> &args,
                    std::vector<RamDomain> &ret) {
    SignalHandler::instance()->setMsg(R"_(local_next(EA,EA_next) :- 
   code_in_block(EA,Block),
   may_fallthrough(EA,EA_next),
   code_in_block(EA_next,Block).
in file /reversi/datalog/souffle/test_def.dl [59:1-62:34])_");
    if (!(rel_12_code_in_block->empty()) &&
        !(rel_35_may_fallthrough->empty())) {
      [&]() {
        CREATE_OP_CONTEXT(rel_34_local_next_op_ctxt,
                          rel_34_local_next->createContext());
        CREATE_OP_CONTEXT(rel_12_code_in_block_op_ctxt,
                          rel_12_code_in_block->createContext());
        CREATE_OP_CONTEXT(rel_35_may_fallthrough_op_ctxt,
                          rel_35_may_fallthrough->createContext());
        for (const auto &env0 : *rel_12_code_in_block) {
          auto range = rel_35_may_fallthrough->lowerUpperRange_10(
              Tuple<RamDomain, 2>{{ramBitCast(env0[0]),
                                   ramBitCast<RamDomain>(MIN_RAM_UNSIGNED)}},
              Tuple<RamDomain, 2>{{ramBitCast(env0[0]),
                                   ramBitCast<RamDomain>(MAX_RAM_UNSIGNED)}},
              READ_OP_CONTEXT(rel_35_may_fallthrough_op_ctxt));
          for (const auto &env1 : range) {
            if (rel_12_code_in_block->contains(
                    Tuple<RamDomain, 2>{
                        {ramBitCast(env1[1]), ramBitCast(env0[1])}},
                    READ_OP_CONTEXT(rel_12_code_in_block_op_ctxt))) {
              Tuple<RamDomain, 2> tuple{
                  {ramBitCast(env0[0]), ramBitCast(env1[1])}};
              rel_34_local_next->insert(
                  tuple, READ_OP_CONTEXT(rel_34_local_next_op_ctxt));
            }
          }
        }
      }();
    }
    if (performIO) {
      try {
        std::map<std::string, std::string> directiveMap(
            {{"IO", "file"},
             {"attributeNames", "ea\tea_next"},
             {"name", "local_next"},
             {"operation", "output"},
             {"output-dir", "."},
             {"params", "{\"records\": {}, \"relation\": {\"arity\": 2, "
                        "\"auxArity\": 0, \"params\": [\"ea\", \"ea_next\"]}}"},
             {"types",
              "{\"ADTs\": {}, \"records\": {}, \"relation\": {\"arity\": 2, "
              "\"auxArity\": 0, \"types\": [\"u:address\", \"u:address\"]}}"}});
        if (!outputDirectory.empty()) {
          directiveMap["output-dir"] = outputDirectory;
        }
        IOSystem::getInstance()
            .getWriter(directiveMap, symTable, recordTable)
            ->writeAll(*rel_34_local_next);
      } catch (std::exception &e) {
        std::cerr << e.what();
        exit(1);
      }
    }
  }
#ifdef _MSC_VER
#pragma warning(default : 4100)
#endif // _MSC_VER
#ifdef _MSC_VER
#pragma warning(disable : 4100)
#endif // _MSC_VER
  void subroutine_4(const std::vector<RamDomain> &args,
                    std::vector<RamDomain> &ret) {
    if (performIO) {
      try {
        std::map<std::string, std::string> directiveMap(
            {{"IO", "file"},
             {"attributeNames", "ea"},
             {"fact-dir", "."},
             {"filename", "code.csv"},
             {"name", "code"},
             {"operation", "input"},
             {"params", "{\"records\": {}, \"relation\": {\"arity\": 1, "
                        "\"auxArity\": 0, \"params\": [\"ea\"]}}"},
             {"types",
              "{\"ADTs\": {}, \"records\": {}, \"relation\": {\"arity\": 1, "
              "\"auxArity\": 0, \"types\": [\"u:address\"]}}"}});
        if (!inputDirectory.empty()) {
          directiveMap["fact-dir"] = inputDirectory;
        }
        IOSystem::getInstance()
            .getReader(directiveMap, symTable, recordTable)
            ->readAll(*rel_11_code);
      } catch (std::exception &e) {
        std::cerr << "Error loading data: " << e.what() << '\n';
      }
    }
    if (performIO) {
      try {
        std::map<std::string, std::string> directiveMap(
            {{"IO", "file"},
             {"attributeNames", "ea"},
             {"name", "code"},
             {"operation", "output"},
             {"output-dir", "."},
             {"params", "{\"records\": {}, \"relation\": {\"arity\": 1, "
                        "\"auxArity\": 0, \"params\": [\"ea\"]}}"},
             {"types",
              "{\"ADTs\": {}, \"records\": {}, \"relation\": {\"arity\": 1, "
              "\"auxArity\": 0, \"types\": [\"u:address\"]}}"}});
        if (!outputDirectory.empty()) {
          directiveMap["output-dir"] = outputDirectory;
        }
        IOSystem::getInstance()
            .getWriter(directiveMap, symTable, recordTable)
            ->writeAll(*rel_11_code);
      } catch (std::exception &e) {
        std::cerr << e.what();
        exit(1);
      }
    }
  }
#ifdef _MSC_VER
#pragma warning(default : 4100)
#endif // _MSC_VER
#ifdef _MSC_VER
#pragma warning(disable : 4100)
#endif // _MSC_VER
  void subroutine_5(const std::vector<RamDomain> &args,
                    std::vector<RamDomain> &ret) {
    if (performIO) {
      try {
        std::map<std::string, std::string> directiveMap(
            {{"IO", "file"},
             {"attributeNames", "src\tdest"},
             {"fact-dir", "."},
             {"filename", "direct_jump.csv"},
             {"name", "direct_jump"},
             {"operation", "input"},
             {"params", "{\"records\": {}, \"relation\": {\"arity\": 2, "
                        "\"auxArity\": 0, \"params\": [\"src\", \"dest\"]}}"},
             {"types",
              "{\"ADTs\": {}, \"records\": {}, \"relation\": {\"arity\": 2, "
              "\"auxArity\": 0, \"types\": [\"u:address\", \"u:address\"]}}"}});
        if (!inputDirectory.empty()) {
          directiveMap["fact-dir"] = inputDirectory;
        }
        IOSystem::getInstance()
            .getReader(directiveMap, symTable, recordTable)
            ->readAll(*rel_20_direct_jump);
      } catch (std::exception &e) {
        std::cerr << "Error loading data: " << e.what() << '\n';
      }
    }
    if (performIO) {
      try {
        std::map<std::string, std::string> directiveMap(
            {{"IO", "file"},
             {"attributeNames", "src\tdest"},
             {"name", "direct_jump"},
             {"operation", "output"},
             {"output-dir", "."},
             {"params", "{\"records\": {}, \"relation\": {\"arity\": 2, "
                        "\"auxArity\": 0, \"params\": [\"src\", \"dest\"]}}"},
             {"types",
              "{\"ADTs\": {}, \"records\": {}, \"relation\": {\"arity\": 2, "
              "\"auxArity\": 0, \"types\": [\"u:address\", \"u:address\"]}}"}});
        if (!outputDirectory.empty()) {
          directiveMap["output-dir"] = outputDirectory;
        }
        IOSystem::getInstance()
            .getWriter(directiveMap, symTable, recordTable)
            ->writeAll(*rel_20_direct_jump);
      } catch (std::exception &e) {
        std::cerr << e.what();
        exit(1);
      }
    }
  }
#ifdef _MSC_VER
#pragma warning(default : 4100)
#endif // _MSC_VER
#ifdef _MSC_VER
#pragma warning(disable : 4100)
#endif // _MSC_VER
  void subroutine_6(const std::vector<RamDomain> &args,
                    std::vector<RamDomain> &ret) {
    if (performIO) {
      try {
        std::map<std::string, std::string> directiveMap(
            {{"IO", "file"},
             {"attributeNames", "n"},
             {"fact-dir", "."},
             {"filename", "unconditional_jump.csv"},
             {"name", "unconditional_jump"},
             {"operation", "input"},
             {"params", "{\"records\": {}, \"relation\": {\"arity\": 1, "
                        "\"auxArity\": 0, \"params\": [\"n\"]}}"},
             {"types",
              "{\"ADTs\": {}, \"records\": {}, \"relation\": {\"arity\": 1, "
              "\"auxArity\": 0, \"types\": [\"u:address\"]}}"}});
        if (!inputDirectory.empty()) {
          directiveMap["fact-dir"] = inputDirectory;
        }
        IOSystem::getInstance()
            .getReader(directiveMap, symTable, recordTable)
            ->readAll(*rel_43_unconditional_jump);
      } catch (std::exception &e) {
        std::cerr << "Error loading data: " << e.what() << '\n';
      }
    }
    if (performIO) {
      try {
        std::map<std::string, std::string> directiveMap(
            {{"IO", "file"},
             {"attributeNames", "n"},
             {"name", "unconditional_jump"},
             {"operation", "output"},
             {"output-dir", "."},
             {"params", "{\"records\": {}, \"relation\": {\"arity\": 1, "
                        "\"auxArity\": 0, \"params\": [\"n\"]}}"},
             {"types",
              "{\"ADTs\": {}, \"records\": {}, \"relation\": {\"arity\": 1, "
              "\"auxArity\": 0, \"types\": [\"u:address\"]}}"}});
        if (!outputDirectory.empty()) {
          directiveMap["output-dir"] = outputDirectory;
        }
        IOSystem::getInstance()
            .getWriter(directiveMap, symTable, recordTable)
            ->writeAll(*rel_43_unconditional_jump);
      } catch (std::exception &e) {
        std::cerr << e.what();
        exit(1);
      }
    }
  }
#ifdef _MSC_VER
#pragma warning(default : 4100)
#endif // _MSC_VER
#ifdef _MSC_VER
#pragma warning(disable : 4100)
#endif // _MSC_VER
  void subroutine_7(const std::vector<RamDomain> &args,
                    std::vector<RamDomain> &ret) {
    SignalHandler::instance()->setMsg(R"_(inter_procedural_jump(Src,Dest) :- 
   unconditional_jump(Src),
   direct_jump(Src,Dest),
   direct_call(OtherSrc,Dest),
   code(OtherSrc).
in file /reversi/datalog/souffle/test_def.dl [79:1-83:20])_");
    if (!(rel_11_code->empty()) && !(rel_19_direct_call->empty()) &&
        !(rel_43_unconditional_jump->empty()) &&
        !(rel_20_direct_jump->empty())) {
      [&]() {
        CREATE_OP_CONTEXT(rel_11_code_op_ctxt, rel_11_code->createContext());
        CREATE_OP_CONTEXT(rel_20_direct_jump_op_ctxt,
                          rel_20_direct_jump->createContext());
        CREATE_OP_CONTEXT(rel_43_unconditional_jump_op_ctxt,
                          rel_43_unconditional_jump->createContext());
        CREATE_OP_CONTEXT(rel_19_direct_call_op_ctxt,
                          rel_19_direct_call->createContext());
        CREATE_OP_CONTEXT(rel_29_inter_procedural_jump_op_ctxt,
                          rel_29_inter_procedural_jump->createContext());
        for (const auto &env0 : *rel_43_unconditional_jump) {
          auto range = rel_20_direct_jump->lowerUpperRange_10(
              Tuple<RamDomain, 2>{{ramBitCast(env0[0]),
                                   ramBitCast<RamDomain>(MIN_RAM_UNSIGNED)}},
              Tuple<RamDomain, 2>{{ramBitCast(env0[0]),
                                   ramBitCast<RamDomain>(MAX_RAM_UNSIGNED)}},
              READ_OP_CONTEXT(rel_20_direct_jump_op_ctxt));
          for (const auto &env1 : range) {
            auto range = rel_19_direct_call->lowerUpperRange_01(
                Tuple<RamDomain, 2>{{ramBitCast<RamDomain>(MIN_RAM_UNSIGNED),
                                     ramBitCast(env1[1])}},
                Tuple<RamDomain, 2>{{ramBitCast<RamDomain>(MAX_RAM_UNSIGNED),
                                     ramBitCast(env1[1])}},
                READ_OP_CONTEXT(rel_19_direct_call_op_ctxt));
            for (const auto &env2 : range) {
              if (rel_11_code->contains(
                      Tuple<RamDomain, 1>{{ramBitCast(env2[0])}},
                      READ_OP_CONTEXT(rel_11_code_op_ctxt))) {
                Tuple<RamDomain, 2> tuple{
                    {ramBitCast(env0[0]), ramBitCast(env1[1])}};
                rel_29_inter_procedural_jump->insert(
                    tuple,
                    READ_OP_CONTEXT(rel_29_inter_procedural_jump_op_ctxt));
                break;
              }
            }
          }
        }
      }();
    }
    if (performIO) {
      try {
        std::map<std::string, std::string> directiveMap(
            {{"IO", "file"},
             {"attributeNames", "Src\tDest"},
             {"name", "inter_procedural_jump"},
             {"operation", "output"},
             {"output-dir", "."},
             {"params", "{\"records\": {}, \"relation\": {\"arity\": 2, "
                        "\"auxArity\": 0, \"params\": [\"Src\", \"Dest\"]}}"},
             {"types",
              "{\"ADTs\": {}, \"records\": {}, \"relation\": {\"arity\": 2, "
              "\"auxArity\": 0, \"types\": [\"u:address\", \"u:address\"]}}"}});
        if (!outputDirectory.empty()) {
          directiveMap["output-dir"] = outputDirectory;
        }
        IOSystem::getInstance()
            .getWriter(directiveMap, symTable, recordTable)
            ->writeAll(*rel_29_inter_procedural_jump);
      } catch (std::exception &e) {
        std::cerr << e.what();
        exit(1);
      }
    }
    if (performIO)
      rel_43_unconditional_jump->purge();
  }
#ifdef _MSC_VER
#pragma warning(default : 4100)
#endif // _MSC_VER
#ifdef _MSC_VER
#pragma warning(disable : 4100)
#endif // _MSC_VER
  void subroutine_8(const std::vector<RamDomain> &args,
                    std::vector<RamDomain> &ret) {
    if (performIO) {
      try {
        std::map<std::string, std::string> directiveMap(
            {{"IO", "file"},
             {"attributeNames", "block"},
             {"fact-dir", "."},
             {"filename", "block.csv"},
             {"name", "block"},
             {"operation", "input"},
             {"params", "{\"records\": {}, \"relation\": {\"arity\": 1, "
                        "\"auxArity\": 0, \"params\": [\"block\"]}}"},
             {"types",
              "{\"ADTs\": {}, \"records\": {}, \"relation\": {\"arity\": 1, "
              "\"auxArity\": 0, \"types\": [\"u:address\"]}}"}});
        if (!inputDirectory.empty()) {
          directiveMap["fact-dir"] = inputDirectory;
        }
        IOSystem::getInstance()
            .getReader(directiveMap, symTable, recordTable)
            ->readAll(*rel_5_block);
      } catch (std::exception &e) {
        std::cerr << "Error loading data: " << e.what() << '\n';
      }
    }
    if (performIO) {
      try {
        std::map<std::string, std::string> directiveMap(
            {{"IO", "file"},
             {"attributeNames", "block"},
             {"name", "block"},
             {"operation", "output"},
             {"output-dir", "."},
             {"params", "{\"records\": {}, \"relation\": {\"arity\": 1, "
                        "\"auxArity\": 0, \"params\": [\"block\"]}}"},
             {"types",
              "{\"ADTs\": {}, \"records\": {}, \"relation\": {\"arity\": 1, "
              "\"auxArity\": 0, \"types\": [\"u:address\"]}}"}});
        if (!outputDirectory.empty()) {
          directiveMap["output-dir"] = outputDirectory;
        }
        IOSystem::getInstance()
            .getWriter(directiveMap, symTable, recordTable)
            ->writeAll(*rel_5_block);
      } catch (std::exception &e) {
        std::cerr << e.what();
        exit(1);
      }
    }
  }
#ifdef _MSC_VER
#pragma warning(default : 4100)
#endif // _MSC_VER
#ifdef _MSC_VER
#pragma warning(disable : 4100)
#endif // _MSC_VER
  void subroutine_9(const std::vector<RamDomain> &args,
                    std::vector<RamDomain> &ret) {
    if (performIO) {
      try {
        std::map<std::string, std::string> directiveMap(
            {{"IO", "file"},
             {"attributeNames", "block\tEA"},
             {"fact-dir", "."},
             {"filename", "block_last_instruction.csv"},
             {"name", "block_last_instruction"},
             {"operation", "input"},
             {"params", "{\"records\": {}, \"relation\": {\"arity\": 2, "
                        "\"auxArity\": 0, \"params\": [\"block\", \"EA\"]}}"},
             {"types",
              "{\"ADTs\": {}, \"records\": {}, \"relation\": {\"arity\": 2, "
              "\"auxArity\": 0, \"types\": [\"u:address\", \"u:address\"]}}"}});
        if (!inputDirectory.empty()) {
          directiveMap["fact-dir"] = inputDirectory;
        }
        IOSystem::getInstance()
            .getReader(directiveMap, symTable, recordTable)
            ->readAll(*rel_7_block_last_instruction);
      } catch (std::exception &e) {
        std::cerr << "Error loading data: " << e.what() << '\n';
      }
    }
    if (performIO) {
      try {
        std::map<std::string, std::string> directiveMap(
            {{"IO", "file"},
             {"attributeNames", "block\tEA"},
             {"name", "block_last_instruction"},
             {"operation", "output"},
             {"output-dir", "."},
             {"params", "{\"records\": {}, \"relation\": {\"arity\": 2, "
                        "\"auxArity\": 0, \"params\": [\"block\", \"EA\"]}}"},
             {"types",
              "{\"ADTs\": {}, \"records\": {}, \"relation\": {\"arity\": 2, "
              "\"auxArity\": 0, \"types\": [\"u:address\", \"u:address\"]}}"}});
        if (!outputDirectory.empty()) {
          directiveMap["output-dir"] = outputDirectory;
        }
        IOSystem::getInstance()
            .getWriter(directiveMap, symTable, recordTable)
            ->writeAll(*rel_7_block_last_instruction);
      } catch (std::exception &e) {
        std::cerr << e.what();
        exit(1);
      }
    }
  }
#ifdef _MSC_VER
#pragma warning(default : 4100)
#endif // _MSC_VER
#ifdef _MSC_VER
#pragma warning(disable : 4100)
#endif // _MSC_VER
  void subroutine_10(const std::vector<RamDomain> &args,
                     std::vector<RamDomain> &ret) {
    if (performIO) {
      try {
        std::map<std::string, std::string> directiveMap(
            {{"IO", "file"},
             {"attributeNames",
              "EA_jump\tSize\tTableStart\tTableRef\tOperation"},
             {"fact-dir", "."},
             {"filename", "jump_table_start.csv"},
             {"name", "jump_table_start"},
             {"operation", "input"},
             {"params", "{\"records\": {}, \"relation\": {\"arity\": 5, "
                        "\"auxArity\": 0, \"params\": [\"EA_jump\", \"Size\", "
                        "\"TableStart\", \"TableRef\", \"Operation\"]}}"},
             {"types",
              "{\"ADTs\": {}, \"records\": {}, \"relation\": {\"arity\": 5, "
              "\"auxArity\": 0, \"types\": [\"u:address\", \"u:unsigned\", "
              "\"u:address\", \"u:address\", \"s:symbol\"]}}"}});
        if (!inputDirectory.empty()) {
          directiveMap["fact-dir"] = inputDirectory;
        }
        IOSystem::getInstance()
            .getReader(directiveMap, symTable, recordTable)
            ->readAll(*rel_31_jump_table_start);
      } catch (std::exception &e) {
        std::cerr << "Error loading data: " << e.what() << '\n';
      }
    }
    if (performIO) {
      try {
        std::map<std::string, std::string> directiveMap(
            {{"IO", "file"},
             {"attributeNames",
              "EA_jump\tSize\tTableStart\tTableRef\tOperation"},
             {"name", "jump_table_start"},
             {"operation", "output"},
             {"output-dir", "."},
             {"params", "{\"records\": {}, \"relation\": {\"arity\": 5, "
                        "\"auxArity\": 0, \"params\": [\"EA_jump\", \"Size\", "
                        "\"TableStart\", \"TableRef\", \"Operation\"]}}"},
             {"types",
              "{\"ADTs\": {}, \"records\": {}, \"relation\": {\"arity\": 5, "
              "\"auxArity\": 0, \"types\": [\"u:address\", \"u:unsigned\", "
              "\"u:address\", \"u:address\", \"s:symbol\"]}}"}});
        if (!outputDirectory.empty()) {
          directiveMap["output-dir"] = outputDirectory;
        }
        IOSystem::getInstance()
            .getWriter(directiveMap, symTable, recordTable)
            ->writeAll(*rel_31_jump_table_start);
      } catch (std::exception &e) {
        std::cerr << e.what();
        exit(1);
      }
    }
  }
#ifdef _MSC_VER
#pragma warning(default : 4100)
#endif // _MSC_VER
#ifdef _MSC_VER
#pragma warning(disable : 4100)
#endif // _MSC_VER
  void subroutine_11(const std::vector<RamDomain> &args,
                     std::vector<RamDomain> &ret) {
    if (performIO) {
      try {
        std::map<std::string, std::string> directiveMap(
            {{"IO", "file"},
             {"attributeNames",
              "EA\tSize\tTableStart\tReference\tDest\tDestIsFirstOrSecond"},
             {"fact-dir", "."},
             {"filename", "relative_address.csv"},
             {"name", "relative_address"},
             {"operation", "input"},
             {"params",
              "{\"records\": {}, \"relation\": {\"arity\": 6, \"auxArity\": 0, "
              "\"params\": [\"EA\", \"Size\", \"TableStart\", \"Reference\", "
              "\"Dest\", \"DestIsFirstOrSecond\"]}}"},
             {"types",
              "{\"ADTs\": {}, \"records\": {}, \"relation\": {\"arity\": 6, "
              "\"auxArity\": 0, \"types\": [\"u:address\", \"u:unsigned\", "
              "\"u:address\", \"u:address\", \"u:address\", \"s:symbol\"]}}"}});
        if (!inputDirectory.empty()) {
          directiveMap["fact-dir"] = inputDirectory;
        }
        IOSystem::getInstance()
            .getReader(directiveMap, symTable, recordTable)
            ->readAll(*rel_40_relative_address);
      } catch (std::exception &e) {
        std::cerr << "Error loading data: " << e.what() << '\n';
      }
    }
    if (performIO) {
      try {
        std::map<std::string, std::string> directiveMap(
            {{"IO", "file"},
             {"attributeNames",
              "EA\tSize\tTableStart\tReference\tDest\tDestIsFirstOrSecond"},
             {"name", "relative_address"},
             {"operation", "output"},
             {"output-dir", "."},
             {"params",
              "{\"records\": {}, \"relation\": {\"arity\": 6, \"auxArity\": 0, "
              "\"params\": [\"EA\", \"Size\", \"TableStart\", \"Reference\", "
              "\"Dest\", \"DestIsFirstOrSecond\"]}}"},
             {"types",
              "{\"ADTs\": {}, \"records\": {}, \"relation\": {\"arity\": 6, "
              "\"auxArity\": 0, \"types\": [\"u:address\", \"u:unsigned\", "
              "\"u:address\", \"u:address\", \"u:address\", \"s:symbol\"]}}"}});
        if (!outputDirectory.empty()) {
          directiveMap["output-dir"] = outputDirectory;
        }
        IOSystem::getInstance()
            .getWriter(directiveMap, symTable, recordTable)
            ->writeAll(*rel_40_relative_address);
      } catch (std::exception &e) {
        std::cerr << e.what();
        exit(1);
      }
    }
  }
#ifdef _MSC_VER
#pragma warning(default : 4100)
#endif // _MSC_VER
#ifdef _MSC_VER
#pragma warning(disable : 4100)
#endif // _MSC_VER
  void subroutine_12(const std::vector<RamDomain> &args,
                     std::vector<RamDomain> &ret) {
    SignalHandler::instance()->setMsg(R"_(function_non_maintained_reg("EAX").
in file /reversi/datalog/souffle/test_def.dl [22:1-22:36])_");
    [&]() {
      CREATE_OP_CONTEXT(rel_23_function_non_maintained_reg_op_ctxt,
                        rel_23_function_non_maintained_reg->createContext());
      Tuple<RamDomain, 1> tuple{{ramBitCast(RamSigned(1))}};
      rel_23_function_non_maintained_reg->insert(
          tuple, READ_OP_CONTEXT(rel_23_function_non_maintained_reg_op_ctxt));
    }();
    SignalHandler::instance()->setMsg(R"_(function_non_maintained_reg("ECX").
in file /reversi/datalog/souffle/test_def.dl [23:1-23:36])_");
    [&]() {
      CREATE_OP_CONTEXT(rel_23_function_non_maintained_reg_op_ctxt,
                        rel_23_function_non_maintained_reg->createContext());
      Tuple<RamDomain, 1> tuple{{ramBitCast(RamSigned(2))}};
      rel_23_function_non_maintained_reg->insert(
          tuple, READ_OP_CONTEXT(rel_23_function_non_maintained_reg_op_ctxt));
    }();
    SignalHandler::instance()->setMsg(R"_(function_non_maintained_reg("EDX").
in file /reversi/datalog/souffle/test_def.dl [24:1-24:36])_");
    [&]() {
      CREATE_OP_CONTEXT(rel_23_function_non_maintained_reg_op_ctxt,
                        rel_23_function_non_maintained_reg->createContext());
      Tuple<RamDomain, 1> tuple{{ramBitCast(RamSigned(3))}};
      rel_23_function_non_maintained_reg->insert(
          tuple, READ_OP_CONTEXT(rel_23_function_non_maintained_reg_op_ctxt));
    }();
    if (performIO) {
      try {
        std::map<std::string, std::string> directiveMap(
            {{"IO", "file"},
             {"attributeNames", "reg"},
             {"name", "function_non_maintained_reg"},
             {"operation", "output"},
             {"output-dir", "."},
             {"params", "{\"records\": {}, \"relation\": {\"arity\": 1, "
                        "\"auxArity\": 0, \"params\": [\"reg\"]}}"},
             {"types",
              "{\"ADTs\": {}, \"records\": {}, \"relation\": {\"arity\": 1, "
              "\"auxArity\": 0, \"types\": [\"s:register\"]}}"}});
        if (!outputDirectory.empty()) {
          directiveMap["output-dir"] = outputDirectory;
        }
        IOSystem::getInstance()
            .getWriter(directiveMap, symTable, recordTable)
            ->writeAll(*rel_23_function_non_maintained_reg);
      } catch (std::exception &e) {
        std::cerr << e.what();
        exit(1);
      }
    }
  }
#ifdef _MSC_VER
#pragma warning(default : 4100)
#endif // _MSC_VER
#ifdef _MSC_VER
#pragma warning(disable : 4100)
#endif // _MSC_VER
  void subroutine_13(const std::vector<RamDomain> &args,
                     std::vector<RamDomain> &ret) {
    SignalHandler::instance()->setMsg(R"_(block_next(EA,Block2) :- 
   block_last_instruction(_,EA),
   may_fallthrough(EA,Block2),
   block(Block2).
in file /reversi/datalog/souffle/test_def.dl [108:1-111:19])_");
    if (!(rel_5_block->empty()) && !(rel_7_block_last_instruction->empty()) &&
        !(rel_35_may_fallthrough->empty())) {
      [&]() {
        CREATE_OP_CONTEXT(rel_5_block_op_ctxt, rel_5_block->createContext());
        CREATE_OP_CONTEXT(rel_7_block_last_instruction_op_ctxt,
                          rel_7_block_last_instruction->createContext());
        CREATE_OP_CONTEXT(rel_35_may_fallthrough_op_ctxt,
                          rel_35_may_fallthrough->createContext());
        CREATE_OP_CONTEXT(rel_8_block_next_op_ctxt,
                          rel_8_block_next->createContext());
        for (const auto &env0 : *rel_7_block_last_instruction) {
          auto range = rel_35_may_fallthrough->lowerUpperRange_10(
              Tuple<RamDomain, 2>{{ramBitCast(env0[1]),
                                   ramBitCast<RamDomain>(MIN_RAM_UNSIGNED)}},
              Tuple<RamDomain, 2>{{ramBitCast(env0[1]),
                                   ramBitCast<RamDomain>(MAX_RAM_UNSIGNED)}},
              READ_OP_CONTEXT(rel_35_may_fallthrough_op_ctxt));
          for (const auto &env1 : range) {
            if (rel_5_block->contains(
                    Tuple<RamDomain, 1>{{ramBitCast(env1[1])}},
                    READ_OP_CONTEXT(rel_5_block_op_ctxt))) {
              Tuple<RamDomain, 2> tuple{
                  {ramBitCast(env0[1]), ramBitCast(env1[1])}};
              rel_8_block_next->insert(
                  tuple, READ_OP_CONTEXT(rel_8_block_next_op_ctxt));
            }
          }
        }
      }();
    }
    SignalHandler::instance()->setMsg(R"_(block_next(EA,EA_next) :- 
   block_last_instruction(_,EA),
   jump_table_start(EA,Size,TableStart,TableReference,_),
   relative_address(_,Size,TableStart,TableReference,EA_next,_).
in file /reversi/datalog/souffle/test_def.dl [125:1-129:66])_");
    if (!(rel_40_relative_address->empty()) &&
        !(rel_7_block_last_instruction->empty()) &&
        !(rel_31_jump_table_start->empty())) {
      [&]() {
        CREATE_OP_CONTEXT(rel_7_block_last_instruction_op_ctxt,
                          rel_7_block_last_instruction->createContext());
        CREATE_OP_CONTEXT(rel_31_jump_table_start_op_ctxt,
                          rel_31_jump_table_start->createContext());
        CREATE_OP_CONTEXT(rel_40_relative_address_op_ctxt,
                          rel_40_relative_address->createContext());
        CREATE_OP_CONTEXT(rel_8_block_next_op_ctxt,
                          rel_8_block_next->createContext());
        for (const auto &env0 : *rel_7_block_last_instruction) {
          auto range = rel_31_jump_table_start->lowerUpperRange_10000(
              Tuple<RamDomain, 5>{{ramBitCast(env0[1]),
                                   ramBitCast<RamDomain>(MIN_RAM_UNSIGNED),
                                   ramBitCast<RamDomain>(MIN_RAM_UNSIGNED),
                                   ramBitCast<RamDomain>(MIN_RAM_UNSIGNED),
                                   ramBitCast<RamDomain>(MIN_RAM_SIGNED)}},
              Tuple<RamDomain, 5>{{ramBitCast(env0[1]),
                                   ramBitCast<RamDomain>(MAX_RAM_UNSIGNED),
                                   ramBitCast<RamDomain>(MAX_RAM_UNSIGNED),
                                   ramBitCast<RamDomain>(MAX_RAM_UNSIGNED),
                                   ramBitCast<RamDomain>(MAX_RAM_SIGNED)}},
              READ_OP_CONTEXT(rel_31_jump_table_start_op_ctxt));
          for (const auto &env1 : range) {
            auto range = rel_40_relative_address->lowerUpperRange_011100(
                Tuple<RamDomain, 6>{{ramBitCast<RamDomain>(MIN_RAM_UNSIGNED),
                                     ramBitCast(env1[1]), ramBitCast(env1[2]),
                                     ramBitCast(env1[3]),
                                     ramBitCast<RamDomain>(MIN_RAM_UNSIGNED),
                                     ramBitCast<RamDomain>(MIN_RAM_SIGNED)}},
                Tuple<RamDomain, 6>{{ramBitCast<RamDomain>(MAX_RAM_UNSIGNED),
                                     ramBitCast(env1[1]), ramBitCast(env1[2]),
                                     ramBitCast(env1[3]),
                                     ramBitCast<RamDomain>(MAX_RAM_UNSIGNED),
                                     ramBitCast<RamDomain>(MAX_RAM_SIGNED)}},
                READ_OP_CONTEXT(rel_40_relative_address_op_ctxt));
            for (const auto &env2 : range) {
              Tuple<RamDomain, 2> tuple{
                  {ramBitCast(env0[1]), ramBitCast(env2[4])}};
              rel_8_block_next->insert(
                  tuple, READ_OP_CONTEXT(rel_8_block_next_op_ctxt));
            }
          }
        }
      }();
    }
    SignalHandler::instance()->setMsg(R"_(block_next(EA,EA_next) :- 
   block_last_instruction(_,EA),
   direct_jump(EA,EA_next),
   !inter_procedural_jump(EA,EA_next).
in file /reversi/datalog/souffle/test_def.dl [113:1-117:40])_");
    if (!(rel_7_block_last_instruction->empty()) &&
        !(rel_20_direct_jump->empty())) {
      [&]() {
        CREATE_OP_CONTEXT(rel_20_direct_jump_op_ctxt,
                          rel_20_direct_jump->createContext());
        CREATE_OP_CONTEXT(rel_29_inter_procedural_jump_op_ctxt,
                          rel_29_inter_procedural_jump->createContext());
        CREATE_OP_CONTEXT(rel_7_block_last_instruction_op_ctxt,
                          rel_7_block_last_instruction->createContext());
        CREATE_OP_CONTEXT(rel_8_block_next_op_ctxt,
                          rel_8_block_next->createContext());
        for (const auto &env0 : *rel_7_block_last_instruction) {
          auto range = rel_20_direct_jump->lowerUpperRange_10(
              Tuple<RamDomain, 2>{{ramBitCast(env0[1]),
                                   ramBitCast<RamDomain>(MIN_RAM_UNSIGNED)}},
              Tuple<RamDomain, 2>{{ramBitCast(env0[1]),
                                   ramBitCast<RamDomain>(MAX_RAM_UNSIGNED)}},
              READ_OP_CONTEXT(rel_20_direct_jump_op_ctxt));
          for (const auto &env1 : range) {
            if (!(rel_29_inter_procedural_jump->contains(
                    Tuple<RamDomain, 2>{
                        {ramBitCast(env0[1]), ramBitCast(env1[1])}},
                    READ_OP_CONTEXT(rel_29_inter_procedural_jump_op_ctxt)))) {
              Tuple<RamDomain, 2> tuple{
                  {ramBitCast(env0[1]), ramBitCast(env1[1])}};
              rel_8_block_next->insert(
                  tuple, READ_OP_CONTEXT(rel_8_block_next_op_ctxt));
            }
          }
        }
      }();
    }
    if (performIO) {
      try {
        std::map<std::string, std::string> directiveMap(
            {{"IO", "file"},
             {"attributeNames", "ea\tea_next"},
             {"name", "block_next"},
             {"operation", "output"},
             {"output-dir", "."},
             {"params", "{\"records\": {}, \"relation\": {\"arity\": 2, "
                        "\"auxArity\": 0, \"params\": [\"ea\", \"ea_next\"]}}"},
             {"types",
              "{\"ADTs\": {}, \"records\": {}, \"relation\": {\"arity\": 2, "
              "\"auxArity\": 0, \"types\": [\"u:address\", \"u:address\"]}}"}});
        if (!outputDirectory.empty()) {
          directiveMap["output-dir"] = outputDirectory;
        }
        IOSystem::getInstance()
            .getWriter(directiveMap, symTable, recordTable)
            ->writeAll(*rel_8_block_next);
      } catch (std::exception &e) {
        std::cerr << e.what();
        exit(1);
      }
    }
    if (performIO)
      rel_35_may_fallthrough->purge();
    if (performIO)
      rel_29_inter_procedural_jump->purge();
    if (performIO)
      rel_5_block->purge();
    if (performIO)
      rel_31_jump_table_start->purge();
    if (performIO)
      rel_40_relative_address->purge();
  }
#ifdef _MSC_VER
#pragma warning(default : 4100)
#endif // _MSC_VER
#ifdef _MSC_VER
#pragma warning(disable : 4100)
#endif // _MSC_VER
  void subroutine_14(const std::vector<RamDomain> &args,
                     std::vector<RamDomain> &ret) {
    if (performIO) {
      try {
        std::map<std::string, std::string> directiveMap(
            {{"IO", "file"},
             {"attributeNames", "EA\tReg\tImm_index\tImmediate"},
             {"fact-dir", "."},
             {"filename", "cmp_immediate_to_reg.csv"},
             {"name", "cmp_immediate_to_reg"},
             {"operation", "input"},
             {"params",
              "{\"records\": {}, \"relation\": {\"arity\": 4, \"auxArity\": 0, "
              "\"params\": [\"EA\", \"Reg\", \"Imm_index\", \"Immediate\"]}}"},
             {"types",
              "{\"ADTs\": {}, \"records\": {}, \"relation\": {\"arity\": 4, "
              "\"auxArity\": 0, \"types\": [\"u:address\", \"s:register\", "
              "\"u:operand_index\", \"i:number\"]}}"}});
        if (!inputDirectory.empty()) {
          directiveMap["fact-dir"] = inputDirectory;
        }
        IOSystem::getInstance()
            .getReader(directiveMap, symTable, recordTable)
            ->readAll(*rel_10_cmp_immediate_to_reg);
      } catch (std::exception &e) {
        std::cerr << "Error loading data: " << e.what() << '\n';
      }
    }
    if (performIO) {
      try {
        std::map<std::string, std::string> directiveMap(
            {{"IO", "file"},
             {"attributeNames", "EA\tReg\tImm_index\tImmediate"},
             {"name", "cmp_immediate_to_reg"},
             {"operation", "output"},
             {"output-dir", "."},
             {"params",
              "{\"records\": {}, \"relation\": {\"arity\": 4, \"auxArity\": 0, "
              "\"params\": [\"EA\", \"Reg\", \"Imm_index\", \"Immediate\"]}}"},
             {"types",
              "{\"ADTs\": {}, \"records\": {}, \"relation\": {\"arity\": 4, "
              "\"auxArity\": 0, \"types\": [\"u:address\", \"s:register\", "
              "\"u:operand_index\", \"i:number\"]}}"}});
        if (!outputDirectory.empty()) {
          directiveMap["output-dir"] = outputDirectory;
        }
        IOSystem::getInstance()
            .getWriter(directiveMap, symTable, recordTable)
            ->writeAll(*rel_10_cmp_immediate_to_reg);
      } catch (std::exception &e) {
        std::cerr << e.what();
        exit(1);
      }
    }
  }
#ifdef _MSC_VER
#pragma warning(default : 4100)
#endif // _MSC_VER
#ifdef _MSC_VER
#pragma warning(disable : 4100)
#endif // _MSC_VER
  void subroutine_15(const std::vector<RamDomain> &args,
                     std::vector<RamDomain> &ret) {
    SignalHandler::instance()->setMsg(R"_(jump_equal_operation("JE").
in file /reversi/datalog/souffle/test_def.dl [170:1-170:28])_");
    [&]() {
      CREATE_OP_CONTEXT(rel_30_jump_equal_operation_op_ctxt,
                        rel_30_jump_equal_operation->createContext());
      Tuple<RamDomain, 1> tuple{{ramBitCast(RamSigned(4))}};
      rel_30_jump_equal_operation->insert(
          tuple, READ_OP_CONTEXT(rel_30_jump_equal_operation_op_ctxt));
    }();
    SignalHandler::instance()->setMsg(R"_(jump_equal_operation("JZ").
in file /reversi/datalog/souffle/test_def.dl [171:1-171:28])_");
    [&]() {
      CREATE_OP_CONTEXT(rel_30_jump_equal_operation_op_ctxt,
                        rel_30_jump_equal_operation->createContext());
      Tuple<RamDomain, 1> tuple{{ramBitCast(RamSigned(5))}};
      rel_30_jump_equal_operation->insert(
          tuple, READ_OP_CONTEXT(rel_30_jump_equal_operation_op_ctxt));
    }();
    if (performIO) {
      try {
        std::map<std::string, std::string> directiveMap(
            {{"IO", "file"},
             {"attributeNames", "n"},
             {"name", "jump_equal_operation"},
             {"operation", "output"},
             {"output-dir", "."},
             {"params", "{\"records\": {}, \"relation\": {\"arity\": 1, "
                        "\"auxArity\": 0, \"params\": [\"n\"]}}"},
             {"types",
              "{\"ADTs\": {}, \"records\": {}, \"relation\": {\"arity\": 1, "
              "\"auxArity\": 0, \"types\": [\"s:symbol\"]}}"}});
        if (!outputDirectory.empty()) {
          directiveMap["output-dir"] = outputDirectory;
        }
        IOSystem::getInstance()
            .getWriter(directiveMap, symTable, recordTable)
            ->writeAll(*rel_30_jump_equal_operation);
      } catch (std::exception &e) {
        std::cerr << e.what();
        exit(1);
      }
    }
  }
#ifdef _MSC_VER
#pragma warning(default : 4100)
#endif // _MSC_VER
#ifdef _MSC_VER
#pragma warning(disable : 4100)
#endif // _MSC_VER
  void subroutine_16(const std::vector<RamDomain> &args,
                     std::vector<RamDomain> &ret) {
    SignalHandler::instance()->setMsg(R"_(jump_unequal_operation("JNE").
in file /reversi/datalog/souffle/test_def.dl [173:1-173:31])_");
    [&]() {
      CREATE_OP_CONTEXT(rel_32_jump_unequal_operation_op_ctxt,
                        rel_32_jump_unequal_operation->createContext());
      Tuple<RamDomain, 1> tuple{{ramBitCast(RamSigned(6))}};
      rel_32_jump_unequal_operation->insert(
          tuple, READ_OP_CONTEXT(rel_32_jump_unequal_operation_op_ctxt));
    }();
    SignalHandler::instance()->setMsg(R"_(jump_unequal_operation("JNZ").
in file /reversi/datalog/souffle/test_def.dl [174:1-174:31])_");
    [&]() {
      CREATE_OP_CONTEXT(rel_32_jump_unequal_operation_op_ctxt,
                        rel_32_jump_unequal_operation->createContext());
      Tuple<RamDomain, 1> tuple{{ramBitCast(RamSigned(7))}};
      rel_32_jump_unequal_operation->insert(
          tuple, READ_OP_CONTEXT(rel_32_jump_unequal_operation_op_ctxt));
    }();
    if (performIO) {
      try {
        std::map<std::string, std::string> directiveMap(
            {{"IO", "file"},
             {"attributeNames", "n"},
             {"name", "jump_unequal_operation"},
             {"operation", "output"},
             {"output-dir", "."},
             {"params", "{\"records\": {}, \"relation\": {\"arity\": 1, "
                        "\"auxArity\": 0, \"params\": [\"n\"]}}"},
             {"types",
              "{\"ADTs\": {}, \"records\": {}, \"relation\": {\"arity\": 1, "
              "\"auxArity\": 0, \"types\": [\"s:symbol\"]}}"}});
        if (!outputDirectory.empty()) {
          directiveMap["output-dir"] = outputDirectory;
        }
        IOSystem::getInstance()
            .getWriter(directiveMap, symTable, recordTable)
            ->writeAll(*rel_32_jump_unequal_operation);
      } catch (std::exception &e) {
        std::cerr << e.what();
        exit(1);
      }
    }
  }
#ifdef _MSC_VER
#pragma warning(default : 4100)
#endif // _MSC_VER
#ifdef _MSC_VER
#pragma warning(disable : 4100)
#endif // _MSC_VER
  void subroutine_17(const std::vector<RamDomain> &args,
                     std::vector<RamDomain> &ret) {
    if (performIO) {
      try {
        std::map<std::string, std::string> directiveMap(
            {{"IO", "file"},
             {"attributeNames", "n\tm"},
             {"fact-dir", "."},
             {"filename", "next.csv"},
             {"name", "next"},
             {"operation", "input"},
             {"params", "{\"records\": {}, \"relation\": {\"arity\": 2, "
                        "\"auxArity\": 0, \"params\": [\"n\", \"m\"]}}"},
             {"types",
              "{\"ADTs\": {}, \"records\": {}, \"relation\": {\"arity\": 2, "
              "\"auxArity\": 0, \"types\": [\"u:address\", \"u:address\"]}}"}});
        if (!inputDirectory.empty()) {
          directiveMap["fact-dir"] = inputDirectory;
        }
        IOSystem::getInstance()
            .getReader(directiveMap, symTable, recordTable)
            ->readAll(*rel_37_next);
      } catch (std::exception &e) {
        std::cerr << "Error loading data: " << e.what() << '\n';
      }
    }
    if (performIO) {
      try {
        std::map<std::string, std::string> directiveMap(
            {{"IO", "file"},
             {"attributeNames", "n\tm"},
             {"name", "next"},
             {"operation", "output"},
             {"output-dir", "."},
             {"params", "{\"records\": {}, \"relation\": {\"arity\": 2, "
                        "\"auxArity\": 0, \"params\": [\"n\", \"m\"]}}"},
             {"types",
              "{\"ADTs\": {}, \"records\": {}, \"relation\": {\"arity\": 2, "
              "\"auxArity\": 0, \"types\": [\"u:address\", \"u:address\"]}}"}});
        if (!outputDirectory.empty()) {
          directiveMap["output-dir"] = outputDirectory;
        }
        IOSystem::getInstance()
            .getWriter(directiveMap, symTable, recordTable)
            ->writeAll(*rel_37_next);
      } catch (std::exception &e) {
        std::cerr << e.what();
        exit(1);
      }
    }
  }
#ifdef _MSC_VER
#pragma warning(default : 4100)
#endif // _MSC_VER
#ifdef _MSC_VER
#pragma warning(disable : 4100)
#endif // _MSC_VER
  void subroutine_18(const std::vector<RamDomain> &args,
                     std::vector<RamDomain> &ret) {
    SignalHandler::instance()->setMsg(
        R"_(flow_def(EA_jump,Reg,EA_target,Immediate) :- 
   code(EA),
   cmp_immediate_to_reg(EA,Reg,_,Immediate),
   next(EA,EA_jump),
   direct_jump(EA_jump,EA_target),
   jump_equal_operation(Operation),
   instruction(EA_jump,_,_,Operation,_,_,_,_,_,_).
in file /reversi/datalog/souffle/test_def.dl [176:1-182:37])_");
    if (!(rel_25_instruction->empty()) &&
        !(rel_30_jump_equal_operation->empty()) &&
        !(rel_20_direct_jump->empty()) && !(rel_37_next->empty()) &&
        !(rel_11_code->empty()) && !(rel_10_cmp_immediate_to_reg->empty())) {
      [&]() {
        CREATE_OP_CONTEXT(rel_10_cmp_immediate_to_reg_op_ctxt,
                          rel_10_cmp_immediate_to_reg->createContext());
        CREATE_OP_CONTEXT(rel_30_jump_equal_operation_op_ctxt,
                          rel_30_jump_equal_operation->createContext());
        CREATE_OP_CONTEXT(rel_37_next_op_ctxt, rel_37_next->createContext());
        CREATE_OP_CONTEXT(rel_11_code_op_ctxt, rel_11_code->createContext());
        CREATE_OP_CONTEXT(rel_20_direct_jump_op_ctxt,
                          rel_20_direct_jump->createContext());
        CREATE_OP_CONTEXT(rel_25_instruction_op_ctxt,
                          rel_25_instruction->createContext());
        CREATE_OP_CONTEXT(rel_22_flow_def_op_ctxt,
                          rel_22_flow_def->createContext());
        for (const auto &env0 : *rel_11_code) {
          auto range = rel_10_cmp_immediate_to_reg->lowerUpperRange_1000(
              Tuple<RamDomain, 4>{{ramBitCast(env0[0]),
                                   ramBitCast<RamDomain>(MIN_RAM_SIGNED),
                                   ramBitCast<RamDomain>(MIN_RAM_UNSIGNED),
                                   ramBitCast<RamDomain>(MIN_RAM_SIGNED)}},
              Tuple<RamDomain, 4>{{ramBitCast(env0[0]),
                                   ramBitCast<RamDomain>(MAX_RAM_SIGNED),
                                   ramBitCast<RamDomain>(MAX_RAM_UNSIGNED),
                                   ramBitCast<RamDomain>(MAX_RAM_SIGNED)}},
              READ_OP_CONTEXT(rel_10_cmp_immediate_to_reg_op_ctxt));
          for (const auto &env1 : range) {
            auto range = rel_37_next->lowerUpperRange_10(
                Tuple<RamDomain, 2>{{ramBitCast(env0[0]),
                                     ramBitCast<RamDomain>(MIN_RAM_UNSIGNED)}},
                Tuple<RamDomain, 2>{{ramBitCast(env0[0]),
                                     ramBitCast<RamDomain>(MAX_RAM_UNSIGNED)}},
                READ_OP_CONTEXT(rel_37_next_op_ctxt));
            for (const auto &env2 : range) {
              auto range = rel_20_direct_jump->lowerUpperRange_10(
                  Tuple<RamDomain, 2>{
                      {ramBitCast(env2[1]),
                       ramBitCast<RamDomain>(MIN_RAM_UNSIGNED)}},
                  Tuple<RamDomain, 2>{
                      {ramBitCast(env2[1]),
                       ramBitCast<RamDomain>(MAX_RAM_UNSIGNED)}},
                  READ_OP_CONTEXT(rel_20_direct_jump_op_ctxt));
              for (const auto &env3 : range) {
                for (const auto &env4 : *rel_30_jump_equal_operation) {
                  if (!rel_25_instruction
                           ->lowerUpperRange_1001000000(
                               Tuple<RamDomain, 10>{
                                   {ramBitCast(env2[1]),
                                    ramBitCast<RamDomain>(MIN_RAM_UNSIGNED),
                                    ramBitCast<RamDomain>(MIN_RAM_SIGNED),
                                    ramBitCast(env4[0]),
                                    ramBitCast<RamDomain>(MIN_RAM_UNSIGNED),
                                    ramBitCast<RamDomain>(MIN_RAM_UNSIGNED),
                                    ramBitCast<RamDomain>(MIN_RAM_UNSIGNED),
                                    ramBitCast<RamDomain>(MIN_RAM_UNSIGNED),
                                    ramBitCast<RamDomain>(MIN_RAM_UNSIGNED),
                                    ramBitCast<RamDomain>(MIN_RAM_UNSIGNED)}},
                               Tuple<RamDomain, 10>{
                                   {ramBitCast(env2[1]),
                                    ramBitCast<RamDomain>(MAX_RAM_UNSIGNED),
                                    ramBitCast<RamDomain>(MAX_RAM_SIGNED),
                                    ramBitCast(env4[0]),
                                    ramBitCast<RamDomain>(MAX_RAM_UNSIGNED),
                                    ramBitCast<RamDomain>(MAX_RAM_UNSIGNED),
                                    ramBitCast<RamDomain>(MAX_RAM_UNSIGNED),
                                    ramBitCast<RamDomain>(MAX_RAM_UNSIGNED),
                                    ramBitCast<RamDomain>(MAX_RAM_UNSIGNED),
                                    ramBitCast<RamDomain>(MAX_RAM_UNSIGNED)}},
                               READ_OP_CONTEXT(rel_25_instruction_op_ctxt))
                           .empty()) {
                    Tuple<RamDomain, 4> tuple{
                        {ramBitCast(env2[1]), ramBitCast(env1[1]),
                         ramBitCast(env3[1]), ramBitCast(env1[3])}};
                    rel_22_flow_def->insert(
                        tuple, READ_OP_CONTEXT(rel_22_flow_def_op_ctxt));
                    break;
                  }
                }
              }
            }
          }
        }
      }();
    }
    SignalHandler::instance()->setMsg(
        R"_(flow_def(EA_jump,Reg,EA_target,Immediate) :- 
   code(EA),
   cmp_immediate_to_reg(EA,Reg,_,Immediate),
   next(EA,EA_jump),
   direct_jump(EA_jump,_),
   next(EA_jump,EA_target),
   jump_unequal_operation(Operation),
   instruction(EA_jump,_,_,Operation,_,_,_,_,_,_).
in file /reversi/datalog/souffle/test_def.dl [184:1-191:39])_");
    if (!(rel_10_cmp_immediate_to_reg->empty()) && !(rel_11_code->empty()) &&
        !(rel_20_direct_jump->empty()) && !(rel_37_next->empty()) &&
        !(rel_25_instruction->empty()) &&
        !(rel_32_jump_unequal_operation->empty())) {
      [&]() {
        CREATE_OP_CONTEXT(rel_10_cmp_immediate_to_reg_op_ctxt,
                          rel_10_cmp_immediate_to_reg->createContext());
        CREATE_OP_CONTEXT(rel_32_jump_unequal_operation_op_ctxt,
                          rel_32_jump_unequal_operation->createContext());
        CREATE_OP_CONTEXT(rel_37_next_op_ctxt, rel_37_next->createContext());
        CREATE_OP_CONTEXT(rel_11_code_op_ctxt, rel_11_code->createContext());
        CREATE_OP_CONTEXT(rel_20_direct_jump_op_ctxt,
                          rel_20_direct_jump->createContext());
        CREATE_OP_CONTEXT(rel_25_instruction_op_ctxt,
                          rel_25_instruction->createContext());
        CREATE_OP_CONTEXT(rel_22_flow_def_op_ctxt,
                          rel_22_flow_def->createContext());
        for (const auto &env0 : *rel_11_code) {
          auto range = rel_10_cmp_immediate_to_reg->lowerUpperRange_1000(
              Tuple<RamDomain, 4>{{ramBitCast(env0[0]),
                                   ramBitCast<RamDomain>(MIN_RAM_SIGNED),
                                   ramBitCast<RamDomain>(MIN_RAM_UNSIGNED),
                                   ramBitCast<RamDomain>(MIN_RAM_SIGNED)}},
              Tuple<RamDomain, 4>{{ramBitCast(env0[0]),
                                   ramBitCast<RamDomain>(MAX_RAM_SIGNED),
                                   ramBitCast<RamDomain>(MAX_RAM_UNSIGNED),
                                   ramBitCast<RamDomain>(MAX_RAM_SIGNED)}},
              READ_OP_CONTEXT(rel_10_cmp_immediate_to_reg_op_ctxt));
          for (const auto &env1 : range) {
            auto range = rel_37_next->lowerUpperRange_10(
                Tuple<RamDomain, 2>{{ramBitCast(env0[0]),
                                     ramBitCast<RamDomain>(MIN_RAM_UNSIGNED)}},
                Tuple<RamDomain, 2>{{ramBitCast(env0[0]),
                                     ramBitCast<RamDomain>(MAX_RAM_UNSIGNED)}},
                READ_OP_CONTEXT(rel_37_next_op_ctxt));
            for (const auto &env2 : range) {
              if (!rel_20_direct_jump
                       ->lowerUpperRange_10(
                           Tuple<RamDomain, 2>{
                               {ramBitCast(env2[1]),
                                ramBitCast<RamDomain>(MIN_RAM_UNSIGNED)}},
                           Tuple<RamDomain, 2>{
                               {ramBitCast(env2[1]),
                                ramBitCast<RamDomain>(MAX_RAM_UNSIGNED)}},
                           READ_OP_CONTEXT(rel_20_direct_jump_op_ctxt))
                       .empty()) {
                auto range = rel_37_next->lowerUpperRange_10(
                    Tuple<RamDomain, 2>{
                        {ramBitCast(env2[1]),
                         ramBitCast<RamDomain>(MIN_RAM_UNSIGNED)}},
                    Tuple<RamDomain, 2>{
                        {ramBitCast(env2[1]),
                         ramBitCast<RamDomain>(MAX_RAM_UNSIGNED)}},
                    READ_OP_CONTEXT(rel_37_next_op_ctxt));
                for (const auto &env3 : range) {
                  for (const auto &env4 : *rel_32_jump_unequal_operation) {
                    if (!rel_25_instruction
                             ->lowerUpperRange_1001000000(
                                 Tuple<RamDomain, 10>{
                                     {ramBitCast(env2[1]),
                                      ramBitCast<RamDomain>(MIN_RAM_UNSIGNED),
                                      ramBitCast<RamDomain>(MIN_RAM_SIGNED),
                                      ramBitCast(env4[0]),
                                      ramBitCast<RamDomain>(MIN_RAM_UNSIGNED),
                                      ramBitCast<RamDomain>(MIN_RAM_UNSIGNED),
                                      ramBitCast<RamDomain>(MIN_RAM_UNSIGNED),
                                      ramBitCast<RamDomain>(MIN_RAM_UNSIGNED),
                                      ramBitCast<RamDomain>(MIN_RAM_UNSIGNED),
                                      ramBitCast<RamDomain>(MIN_RAM_UNSIGNED)}},
                                 Tuple<RamDomain, 10>{
                                     {ramBitCast(env2[1]),
                                      ramBitCast<RamDomain>(MAX_RAM_UNSIGNED),
                                      ramBitCast<RamDomain>(MAX_RAM_SIGNED),
                                      ramBitCast(env4[0]),
                                      ramBitCast<RamDomain>(MAX_RAM_UNSIGNED),
                                      ramBitCast<RamDomain>(MAX_RAM_UNSIGNED),
                                      ramBitCast<RamDomain>(MAX_RAM_UNSIGNED),
                                      ramBitCast<RamDomain>(MAX_RAM_UNSIGNED),
                                      ramBitCast<RamDomain>(MAX_RAM_UNSIGNED),
                                      ramBitCast<RamDomain>(MAX_RAM_UNSIGNED)}},
                                 READ_OP_CONTEXT(rel_25_instruction_op_ctxt))
                             .empty()) {
                      Tuple<RamDomain, 4> tuple{
                          {ramBitCast(env2[1]), ramBitCast(env1[1]),
                           ramBitCast(env3[1]), ramBitCast(env1[3])}};
                      rel_22_flow_def->insert(
                          tuple, READ_OP_CONTEXT(rel_22_flow_def_op_ctxt));
                      break;
                    }
                  }
                }
              }
            }
          }
        }
      }();
    }
    if (performIO) {
      try {
        std::map<std::string, std::string> directiveMap(
            {{"IO", "file"},
             {"attributeNames", "EA\tReg\tEA_next\tValue"},
             {"name", "flow_def"},
             {"operation", "output"},
             {"output-dir", "."},
             {"params",
              "{\"records\": {}, \"relation\": {\"arity\": 4, \"auxArity\": 0, "
              "\"params\": [\"EA\", \"Reg\", \"EA_next\", \"Value\"]}}"},
             {"types",
              "{\"ADTs\": {}, \"records\": {}, \"relation\": {\"arity\": 4, "
              "\"auxArity\": 0, \"types\": [\"u:address\", \"s:register\", "
              "\"u:address\", \"i:number\"]}}"}});
        if (!outputDirectory.empty()) {
          directiveMap["output-dir"] = outputDirectory;
        }
        IOSystem::getInstance()
            .getWriter(directiveMap, symTable, recordTable)
            ->writeAll(*rel_22_flow_def);
      } catch (std::exception &e) {
        std::cerr << e.what();
        exit(1);
      }
    }
    if (performIO)
      rel_25_instruction->purge();
    if (performIO)
      rel_20_direct_jump->purge();
    if (performIO)
      rel_11_code->purge();
    if (performIO)
      rel_10_cmp_immediate_to_reg->purge();
    if (performIO)
      rel_37_next->purge();
    if (performIO)
      rel_30_jump_equal_operation->purge();
    if (performIO)
      rel_32_jump_unequal_operation->purge();
  }
#ifdef _MSC_VER
#pragma warning(default : 4100)
#endif // _MSC_VER
#ifdef _MSC_VER
#pragma warning(disable : 4100)
#endif // _MSC_VER
  void subroutine_19(const std::vector<RamDomain> &args,
                     std::vector<RamDomain> &ret) {
    if (performIO) {
      try {
        std::map<std::string, std::string> directiveMap(
            {{"IO", "file"},
             {"attributeNames", "EA"},
             {"fact-dir", "."},
             {"filename", "arch.conditional_mov.csv"},
             {"name", "conditional_mov"},
             {"operation", "input"},
             {"params", "{\"records\": {}, \"relation\": {\"arity\": 1, "
                        "\"auxArity\": 0, \"params\": [\"EA\"]}}"},
             {"types",
              "{\"ADTs\": {}, \"records\": {}, \"relation\": {\"arity\": 1, "
              "\"auxArity\": 0, \"types\": [\"u:address\"]}}"}});
        if (!inputDirectory.empty()) {
          directiveMap["fact-dir"] = inputDirectory;
        }
        IOSystem::getInstance()
            .getReader(directiveMap, symTable, recordTable)
            ->readAll(*rel_13_conditional_mov);
      } catch (std::exception &e) {
        std::cerr << "Error loading data: " << e.what() << '\n';
      }
    }
    if (performIO) {
      try {
        std::map<std::string, std::string> directiveMap(
            {{"IO", "file"},
             {"attributeNames", "EA"},
             {"name", "conditional_mov"},
             {"operation", "output"},
             {"output-dir", "."},
             {"params", "{\"records\": {}, \"relation\": {\"arity\": 1, "
                        "\"auxArity\": 0, \"params\": [\"EA\"]}}"},
             {"types",
              "{\"ADTs\": {}, \"records\": {}, \"relation\": {\"arity\": 1, "
              "\"auxArity\": 0, \"types\": [\"u:address\"]}}"}});
        if (!outputDirectory.empty()) {
          directiveMap["output-dir"] = outputDirectory;
        }
        IOSystem::getInstance()
            .getWriter(directiveMap, symTable, recordTable)
            ->writeAll(*rel_13_conditional_mov);
      } catch (std::exception &e) {
        std::cerr << e.what();
        exit(1);
      }
    }
  }
#ifdef _MSC_VER
#pragma warning(default : 4100)
#endif // _MSC_VER
#ifdef _MSC_VER
#pragma warning(disable : 4100)
#endif // _MSC_VER
  void subroutine_20(const std::vector<RamDomain> &args,
                     std::vector<RamDomain> &ret) {
    SignalHandler::instance()->setMsg(R"_(must_def(EA,Reg) :- 
   def(EA,Reg),
   !conditional_mov(EA).
in file /reversi/datalog/souffle/test_def.dl [200:1-202:26])_");
    if (!(rel_14_def->empty())) {
      [&]() {
        CREATE_OP_CONTEXT(rel_13_conditional_mov_op_ctxt,
                          rel_13_conditional_mov->createContext());
        CREATE_OP_CONTEXT(rel_36_must_def_op_ctxt,
                          rel_36_must_def->createContext());
        CREATE_OP_CONTEXT(rel_14_def_op_ctxt, rel_14_def->createContext());
        for (const auto &env0 : *rel_14_def) {
          if (!(rel_13_conditional_mov->contains(
                  Tuple<RamDomain, 1>{{ramBitCast(env0[0])}},
                  READ_OP_CONTEXT(rel_13_conditional_mov_op_ctxt)))) {
            Tuple<RamDomain, 2> tuple{
                {ramBitCast(env0[0]), ramBitCast(env0[1])}};
            rel_36_must_def->insert(tuple,
                                    READ_OP_CONTEXT(rel_36_must_def_op_ctxt));
          }
        }
      }();
    }
    if (performIO) {
      try {
        std::map<std::string, std::string> directiveMap(
            {{"IO", "file"},
             {"attributeNames", "EA\tReg"},
             {"name", "must_def"},
             {"operation", "output"},
             {"output-dir", "."},
             {"params", "{\"records\": {}, \"relation\": {\"arity\": 2, "
                        "\"auxArity\": 0, \"params\": [\"EA\", \"Reg\"]}}"},
             {"types", "{\"ADTs\": {}, \"records\": {}, \"relation\": "
                       "{\"arity\": 2, \"auxArity\": 0, \"types\": "
                       "[\"u:address\", \"s:register\"]}}"}});
        if (!outputDirectory.empty()) {
          directiveMap["output-dir"] = outputDirectory;
        }
        IOSystem::getInstance()
            .getWriter(directiveMap, symTable, recordTable)
            ->writeAll(*rel_36_must_def);
      } catch (std::exception &e) {
        std::cerr << e.what();
        exit(1);
      }
    }
    if (performIO)
      rel_13_conditional_mov->purge();
  }
#ifdef _MSC_VER
#pragma warning(default : 4100)
#endif // _MSC_VER
#ifdef _MSC_VER
#pragma warning(disable : 4100)
#endif // _MSC_VER
  void subroutine_21(const std::vector<RamDomain> &args,
                     std::vector<RamDomain> &ret) {
    SignalHandler::instance()->setMsg(R"_(block_last_def(EA_next,EA,Reg) :- 
   def(EA,Reg),
   local_next(EA,EA_next).
in file /reversi/datalog/souffle/test_def.dl [244:1-246:28])_");
    if (!(rel_14_def->empty()) && !(rel_34_local_next->empty())) {
      [&]() {
        CREATE_OP_CONTEXT(rel_34_local_next_op_ctxt,
                          rel_34_local_next->createContext());
        CREATE_OP_CONTEXT(rel_14_def_op_ctxt, rel_14_def->createContext());
        CREATE_OP_CONTEXT(rel_6_block_last_def_op_ctxt,
                          rel_6_block_last_def->createContext());
        for (const auto &env0 : *rel_14_def) {
          auto range = rel_34_local_next->lowerUpperRange_10(
              Tuple<RamDomain, 2>{{ramBitCast(env0[0]),
                                   ramBitCast<RamDomain>(MIN_RAM_UNSIGNED)}},
              Tuple<RamDomain, 2>{{ramBitCast(env0[0]),
                                   ramBitCast<RamDomain>(MAX_RAM_UNSIGNED)}},
              READ_OP_CONTEXT(rel_34_local_next_op_ctxt));
          for (const auto &env1 : range) {
            Tuple<RamDomain, 3> tuple{{ramBitCast(env1[1]), ramBitCast(env0[0]),
                                       ramBitCast(env0[1])}};
            rel_6_block_last_def->insert(
                tuple, READ_OP_CONTEXT(rel_6_block_last_def_op_ctxt));
          }
        }
      }();
    }
    [&]() {
      CREATE_OP_CONTEXT(rel_6_block_last_def_op_ctxt,
                        rel_6_block_last_def->createContext());
      CREATE_OP_CONTEXT(rel_1_delta_block_last_def_op_ctxt,
                        rel_1_delta_block_last_def->createContext());
      for (const auto &env0 : *rel_6_block_last_def) {
        Tuple<RamDomain, 3> tuple{
            {ramBitCast(env0[0]), ramBitCast(env0[1]), ramBitCast(env0[2])}};
        rel_1_delta_block_last_def->insert(
            tuple, READ_OP_CONTEXT(rel_1_delta_block_last_def_op_ctxt));
      }
    }();
    iter = 0;
    for (;;) {
      SignalHandler::instance()->setMsg(
          R"_(block_last_def(EA_next,EA_def,Reg) :- 
   block_last_def(EA,EA_def,Reg),
   !must_def(EA,Reg),
   local_next(EA,EA_next).
in file /reversi/datalog/souffle/test_def.dl [248:1-251:28])_");
      if (!(rel_1_delta_block_last_def->empty()) &&
          !(rel_34_local_next->empty())) {
        [&]() {
          CREATE_OP_CONTEXT(rel_34_local_next_op_ctxt,
                            rel_34_local_next->createContext());
          CREATE_OP_CONTEXT(rel_36_must_def_op_ctxt,
                            rel_36_must_def->createContext());
          CREATE_OP_CONTEXT(rel_6_block_last_def_op_ctxt,
                            rel_6_block_last_def->createContext());
          CREATE_OP_CONTEXT(rel_1_delta_block_last_def_op_ctxt,
                            rel_1_delta_block_last_def->createContext());
          CREATE_OP_CONTEXT(rel_3_new_block_last_def_op_ctxt,
                            rel_3_new_block_last_def->createContext());
          for (const auto &env0 : *rel_1_delta_block_last_def) {
            if (!(rel_36_must_def->contains(
                    Tuple<RamDomain, 2>{
                        {ramBitCast(env0[0]), ramBitCast(env0[2])}},
                    READ_OP_CONTEXT(rel_36_must_def_op_ctxt)))) {
              auto range = rel_34_local_next->lowerUpperRange_10(
                  Tuple<RamDomain, 2>{
                      {ramBitCast(env0[0]),
                       ramBitCast<RamDomain>(MIN_RAM_UNSIGNED)}},
                  Tuple<RamDomain, 2>{
                      {ramBitCast(env0[0]),
                       ramBitCast<RamDomain>(MAX_RAM_UNSIGNED)}},
                  READ_OP_CONTEXT(rel_34_local_next_op_ctxt));
              for (const auto &env1 : range) {
                if (!(rel_6_block_last_def->contains(
                        Tuple<RamDomain, 3>{{ramBitCast(env1[1]),
                                             ramBitCast(env0[1]),
                                             ramBitCast(env0[2])}},
                        READ_OP_CONTEXT(rel_6_block_last_def_op_ctxt)))) {
                  Tuple<RamDomain, 3> tuple{{ramBitCast(env1[1]),
                                             ramBitCast(env0[1]),
                                             ramBitCast(env0[2])}};
                  rel_3_new_block_last_def->insert(
                      tuple, READ_OP_CONTEXT(rel_3_new_block_last_def_op_ctxt));
                }
              }
            }
          }
        }();
      }
      if (rel_3_new_block_last_def->empty())
        break;
      [&]() {
        CREATE_OP_CONTEXT(rel_6_block_last_def_op_ctxt,
                          rel_6_block_last_def->createContext());
        CREATE_OP_CONTEXT(rel_3_new_block_last_def_op_ctxt,
                          rel_3_new_block_last_def->createContext());
        for (const auto &env0 : *rel_3_new_block_last_def) {
          Tuple<RamDomain, 3> tuple{
              {ramBitCast(env0[0]), ramBitCast(env0[1]), ramBitCast(env0[2])}};
          rel_6_block_last_def->insert(
              tuple, READ_OP_CONTEXT(rel_6_block_last_def_op_ctxt));
        }
      }();
      std::swap(rel_1_delta_block_last_def, rel_3_new_block_last_def);
      rel_3_new_block_last_def->purge();
      iter++;
    }
    iter = 0;
    rel_1_delta_block_last_def->purge();
    rel_3_new_block_last_def->purge();
    if (performIO) {
      try {
        std::map<std::string, std::string> directiveMap(
            {{"IO", "file"},
             {"attributeNames", "EA\tEA_def\tReg"},
             {"name", "block_last_def"},
             {"operation", "output"},
             {"output-dir", "."},
             {"params",
              "{\"records\": {}, \"relation\": {\"arity\": 3, \"auxArity\": 0, "
              "\"params\": [\"EA\", \"EA_def\", \"Reg\"]}}"},
             {"types", "{\"ADTs\": {}, \"records\": {}, \"relation\": "
                       "{\"arity\": 3, \"auxArity\": 0, \"types\": "
                       "[\"u:address\", \"u:address\", \"s:register\"]}}"}});
        if (!outputDirectory.empty()) {
          directiveMap["output-dir"] = outputDirectory;
        }
        IOSystem::getInstance()
            .getWriter(directiveMap, symTable, recordTable)
            ->writeAll(*rel_6_block_last_def);
      } catch (std::exception &e) {
        std::cerr << e.what();
        exit(1);
      }
    }
    if (performIO)
      rel_34_local_next->purge();
  }
#ifdef _MSC_VER
#pragma warning(default : 4100)
#endif // _MSC_VER
#ifdef _MSC_VER
#pragma warning(disable : 4100)
#endif // _MSC_VER
  void subroutine_22(const std::vector<RamDomain> &args,
                     std::vector<RamDomain> &ret) {
    SignalHandler::instance()->setMsg(R"_(last_def(Block,EA,Reg) :- 
   def(EA,Reg),
   block_next(EA,Block),
   !flow_def(EA,Reg,Block,_).
in file /reversi/datalog/souffle/test_def.dl [256:1-259:31])_");
    if (!(rel_14_def->empty()) && !(rel_8_block_next->empty())) {
      [&]() {
        CREATE_OP_CONTEXT(rel_33_last_def_op_ctxt,
                          rel_33_last_def->createContext());
        CREATE_OP_CONTEXT(rel_22_flow_def_op_ctxt,
                          rel_22_flow_def->createContext());
        CREATE_OP_CONTEXT(rel_8_block_next_op_ctxt,
                          rel_8_block_next->createContext());
        CREATE_OP_CONTEXT(rel_14_def_op_ctxt, rel_14_def->createContext());
        for (const auto &env0 : *rel_14_def) {
          auto range = rel_8_block_next->lowerUpperRange_10(
              Tuple<RamDomain, 2>{{ramBitCast(env0[0]),
                                   ramBitCast<RamDomain>(MIN_RAM_UNSIGNED)}},
              Tuple<RamDomain, 2>{{ramBitCast(env0[0]),
                                   ramBitCast<RamDomain>(MAX_RAM_UNSIGNED)}},
              READ_OP_CONTEXT(rel_8_block_next_op_ctxt));
          for (const auto &env1 : range) {
            if (!(!rel_22_flow_def
                       ->lowerUpperRange_1110(
                           Tuple<RamDomain, 4>{
                               {ramBitCast(env0[0]), ramBitCast(env0[1]),
                                ramBitCast(env1[1]),
                                ramBitCast<RamDomain>(MIN_RAM_SIGNED)}},
                           Tuple<RamDomain, 4>{
                               {ramBitCast(env0[0]), ramBitCast(env0[1]),
                                ramBitCast(env1[1]),
                                ramBitCast<RamDomain>(MAX_RAM_SIGNED)}},
                           READ_OP_CONTEXT(rel_22_flow_def_op_ctxt))
                       .empty())) {
              Tuple<RamDomain, 3> tuple{{ramBitCast(env1[1]),
                                         ramBitCast(env0[0]),
                                         ramBitCast(env0[1])}};
              rel_33_last_def->insert(tuple,
                                      READ_OP_CONTEXT(rel_33_last_def_op_ctxt));
            }
          }
        }
      }();
    }
    SignalHandler::instance()->setMsg(R"_(last_def(Block,EA_def,Reg) :- 
   block_last_def(Block_end,EA_def,Reg),
   !must_def(Block_end,Reg),
   block_next(Block_end,Block),
   !flow_def(Block_end,Reg,Block,_).
in file /reversi/datalog/souffle/test_def.dl [261:1-265:38])_");
    if (!(rel_6_block_last_def->empty()) && !(rel_8_block_next->empty())) {
      [&]() {
        CREATE_OP_CONTEXT(rel_33_last_def_op_ctxt,
                          rel_33_last_def->createContext());
        CREATE_OP_CONTEXT(rel_22_flow_def_op_ctxt,
                          rel_22_flow_def->createContext());
        CREATE_OP_CONTEXT(rel_36_must_def_op_ctxt,
                          rel_36_must_def->createContext());
        CREATE_OP_CONTEXT(rel_8_block_next_op_ctxt,
                          rel_8_block_next->createContext());
        CREATE_OP_CONTEXT(rel_6_block_last_def_op_ctxt,
                          rel_6_block_last_def->createContext());
        for (const auto &env0 : *rel_6_block_last_def) {
          if (!(rel_36_must_def->contains(
                  Tuple<RamDomain, 2>{
                      {ramBitCast(env0[0]), ramBitCast(env0[2])}},
                  READ_OP_CONTEXT(rel_36_must_def_op_ctxt)))) {
            auto range = rel_8_block_next->lowerUpperRange_10(
                Tuple<RamDomain, 2>{{ramBitCast(env0[0]),
                                     ramBitCast<RamDomain>(MIN_RAM_UNSIGNED)}},
                Tuple<RamDomain, 2>{{ramBitCast(env0[0]),
                                     ramBitCast<RamDomain>(MAX_RAM_UNSIGNED)}},
                READ_OP_CONTEXT(rel_8_block_next_op_ctxt));
            for (const auto &env1 : range) {
              if (!(!rel_22_flow_def
                         ->lowerUpperRange_1110(
                             Tuple<RamDomain, 4>{
                                 {ramBitCast(env0[0]), ramBitCast(env0[2]),
                                  ramBitCast(env1[1]),
                                  ramBitCast<RamDomain>(MIN_RAM_SIGNED)}},
                             Tuple<RamDomain, 4>{
                                 {ramBitCast(env0[0]), ramBitCast(env0[2]),
                                  ramBitCast(env1[1]),
                                  ramBitCast<RamDomain>(MAX_RAM_SIGNED)}},
                             READ_OP_CONTEXT(rel_22_flow_def_op_ctxt))
                         .empty())) {
                Tuple<RamDomain, 3> tuple{{ramBitCast(env1[1]),
                                           ramBitCast(env0[1]),
                                           ramBitCast(env0[2])}};
                rel_33_last_def->insert(
                    tuple, READ_OP_CONTEXT(rel_33_last_def_op_ctxt));
              }
            }
          }
        }
      }();
    }
    SignalHandler::instance()->setMsg(R"_(last_def(Block_next,Block_end,Reg) :- 
   flow_def(Block_end,Reg,Block_next,_).
in file /reversi/datalog/souffle/test_def.dl [274:1-275:42])_");
    if (!(rel_22_flow_def->empty())) {
      [&]() {
        CREATE_OP_CONTEXT(rel_33_last_def_op_ctxt,
                          rel_33_last_def->createContext());
        CREATE_OP_CONTEXT(rel_22_flow_def_op_ctxt,
                          rel_22_flow_def->createContext());
        for (const auto &env0 : *rel_22_flow_def) {
          Tuple<RamDomain, 3> tuple{
              {ramBitCast(env0[2]), ramBitCast(env0[0]), ramBitCast(env0[1])}};
          rel_33_last_def->insert(tuple,
                                  READ_OP_CONTEXT(rel_33_last_def_op_ctxt));
        }
      }();
    }
    [&]() {
      CREATE_OP_CONTEXT(rel_33_last_def_op_ctxt,
                        rel_33_last_def->createContext());
      CREATE_OP_CONTEXT(rel_2_delta_last_def_op_ctxt,
                        rel_2_delta_last_def->createContext());
      for (const auto &env0 : *rel_33_last_def) {
        Tuple<RamDomain, 3> tuple{
            {ramBitCast(env0[0]), ramBitCast(env0[1]), ramBitCast(env0[2])}};
        rel_2_delta_last_def->insert(
            tuple, READ_OP_CONTEXT(rel_2_delta_last_def_op_ctxt));
      }
    }();
    iter = 0;
    for (;;) {
      SignalHandler::instance()->setMsg(R"_(last_def(Block_next,EA_def,Reg) :- 
   last_def(Block,EA_def,Reg),
   !defined_in_block(Block,Reg),
   block_last_instruction(Block,Block_end),
   block_next(Block_end,Block_next),
   !flow_def(Block_end,Reg,Block,_).
in file /reversi/datalog/souffle/test_def.dl [267:1-272:38])_");
      if (!(rel_8_block_next->empty()) && !(rel_2_delta_last_def->empty()) &&
          !(rel_7_block_last_instruction->empty())) {
        [&]() {
          CREATE_OP_CONTEXT(rel_33_last_def_op_ctxt,
                            rel_33_last_def->createContext());
          CREATE_OP_CONTEXT(rel_2_delta_last_def_op_ctxt,
                            rel_2_delta_last_def->createContext());
          CREATE_OP_CONTEXT(rel_4_new_last_def_op_ctxt,
                            rel_4_new_last_def->createContext());
          CREATE_OP_CONTEXT(rel_7_block_last_instruction_op_ctxt,
                            rel_7_block_last_instruction->createContext());
          CREATE_OP_CONTEXT(rel_18_defined_in_block_op_ctxt,
                            rel_18_defined_in_block->createContext());
          CREATE_OP_CONTEXT(rel_22_flow_def_op_ctxt,
                            rel_22_flow_def->createContext());
          CREATE_OP_CONTEXT(rel_8_block_next_op_ctxt,
                            rel_8_block_next->createContext());
          for (const auto &env0 : *rel_2_delta_last_def) {
            if (!(rel_18_defined_in_block->contains(
                    Tuple<RamDomain, 2>{
                        {ramBitCast(env0[0]), ramBitCast(env0[2])}},
                    READ_OP_CONTEXT(rel_18_defined_in_block_op_ctxt)))) {
              auto range = rel_7_block_last_instruction->lowerUpperRange_10(
                  Tuple<RamDomain, 2>{
                      {ramBitCast(env0[0]),
                       ramBitCast<RamDomain>(MIN_RAM_UNSIGNED)}},
                  Tuple<RamDomain, 2>{
                      {ramBitCast(env0[0]),
                       ramBitCast<RamDomain>(MAX_RAM_UNSIGNED)}},
                  READ_OP_CONTEXT(rel_7_block_last_instruction_op_ctxt));
              for (const auto &env1 : range) {
                if (!(!rel_22_flow_def
                           ->lowerUpperRange_1110(
                               Tuple<RamDomain, 4>{
                                   {ramBitCast(env1[1]), ramBitCast(env0[2]),
                                    ramBitCast(env0[0]),
                                    ramBitCast<RamDomain>(MIN_RAM_SIGNED)}},
                               Tuple<RamDomain, 4>{
                                   {ramBitCast(env1[1]), ramBitCast(env0[2]),
                                    ramBitCast(env0[0]),
                                    ramBitCast<RamDomain>(MAX_RAM_SIGNED)}},
                               READ_OP_CONTEXT(rel_22_flow_def_op_ctxt))
                           .empty())) {
                  auto range = rel_8_block_next->lowerUpperRange_10(
                      Tuple<RamDomain, 2>{
                          {ramBitCast(env1[1]),
                           ramBitCast<RamDomain>(MIN_RAM_UNSIGNED)}},
                      Tuple<RamDomain, 2>{
                          {ramBitCast(env1[1]),
                           ramBitCast<RamDomain>(MAX_RAM_UNSIGNED)}},
                      READ_OP_CONTEXT(rel_8_block_next_op_ctxt));
                  for (const auto &env2 : range) {
                    if (!(rel_33_last_def->contains(
                            Tuple<RamDomain, 3>{{ramBitCast(env2[1]),
                                                 ramBitCast(env0[1]),
                                                 ramBitCast(env0[2])}},
                            READ_OP_CONTEXT(rel_33_last_def_op_ctxt)))) {
                      Tuple<RamDomain, 3> tuple{{ramBitCast(env2[1]),
                                                 ramBitCast(env0[1]),
                                                 ramBitCast(env0[2])}};
                      rel_4_new_last_def->insert(
                          tuple, READ_OP_CONTEXT(rel_4_new_last_def_op_ctxt));
                    }
                  }
                }
              }
            }
          }
        }();
      }
      if (rel_4_new_last_def->empty())
        break;
      [&]() {
        CREATE_OP_CONTEXT(rel_33_last_def_op_ctxt,
                          rel_33_last_def->createContext());
        CREATE_OP_CONTEXT(rel_4_new_last_def_op_ctxt,
                          rel_4_new_last_def->createContext());
        for (const auto &env0 : *rel_4_new_last_def) {
          Tuple<RamDomain, 3> tuple{
              {ramBitCast(env0[0]), ramBitCast(env0[1]), ramBitCast(env0[2])}};
          rel_33_last_def->insert(tuple,
                                  READ_OP_CONTEXT(rel_33_last_def_op_ctxt));
        }
      }();
      std::swap(rel_2_delta_last_def, rel_4_new_last_def);
      rel_4_new_last_def->purge();
      iter++;
    }
    iter = 0;
    rel_2_delta_last_def->purge();
    rel_4_new_last_def->purge();
    if (performIO) {
      try {
        std::map<std::string, std::string> directiveMap(
            {{"IO", "file"},
             {"attributeNames", "EA\tEA_def\tReg"},
             {"name", "last_def"},
             {"operation", "output"},
             {"output-dir", "."},
             {"params",
              "{\"records\": {}, \"relation\": {\"arity\": 3, \"auxArity\": 0, "
              "\"params\": [\"EA\", \"EA_def\", \"Reg\"]}}"},
             {"types", "{\"ADTs\": {}, \"records\": {}, \"relation\": "
                       "{\"arity\": 3, \"auxArity\": 0, \"types\": "
                       "[\"u:address\", \"u:address\", \"s:register\"]}}"}});
        if (!outputDirectory.empty()) {
          directiveMap["output-dir"] = outputDirectory;
        }
        IOSystem::getInstance()
            .getWriter(directiveMap, symTable, recordTable)
            ->writeAll(*rel_33_last_def);
      } catch (std::exception &e) {
        std::cerr << e.what();
        exit(1);
      }
    }
    if (performIO)
      rel_14_def->purge();
    if (performIO)
      rel_8_block_next->purge();
    if (performIO)
      rel_22_flow_def->purge();
    if (performIO)
      rel_36_must_def->purge();
    if (performIO)
      rel_18_defined_in_block->purge();
  }
#ifdef _MSC_VER
#pragma warning(default : 4100)
#endif // _MSC_VER
#ifdef _MSC_VER
#pragma warning(disable : 4100)
#endif // _MSC_VER
  void subroutine_23(const std::vector<RamDomain> &args,
                     std::vector<RamDomain> &ret) {
    if (performIO) {
      try {
        std::map<std::string, std::string> directiveMap(
            {{"IO", "file"},
             {"attributeNames", "EA\tReg"},
             {"fact-dir", "."},
             {"filename", "get_pc_thunk.csv"},
             {"name", "get_pc_thunk"},
             {"operation", "input"},
             {"params", "{\"records\": {}, \"relation\": {\"arity\": 2, "
                        "\"auxArity\": 0, \"params\": [\"EA\", \"Reg\"]}}"},
             {"types", "{\"ADTs\": {}, \"records\": {}, \"relation\": "
                       "{\"arity\": 2, \"auxArity\": 0, \"types\": "
                       "[\"u:address\", \"s:register\"]}}"}});
        if (!inputDirectory.empty()) {
          directiveMap["fact-dir"] = inputDirectory;
        }
        IOSystem::getInstance()
            .getReader(directiveMap, symTable, recordTable)
            ->readAll(*rel_24_get_pc_thunk);
      } catch (std::exception &e) {
        std::cerr << "Error loading data: " << e.what() << '\n';
      }
    }
    if (performIO) {
      try {
        std::map<std::string, std::string> directiveMap(
            {{"IO", "file"},
             {"attributeNames", "EA\tReg"},
             {"name", "get_pc_thunk"},
             {"operation", "output"},
             {"output-dir", "."},
             {"params", "{\"records\": {}, \"relation\": {\"arity\": 2, "
                        "\"auxArity\": 0, \"params\": [\"EA\", \"Reg\"]}}"},
             {"types", "{\"ADTs\": {}, \"records\": {}, \"relation\": "
                       "{\"arity\": 2, \"auxArity\": 0, \"types\": "
                       "[\"u:address\", \"s:register\"]}}"}});
        if (!outputDirectory.empty()) {
          directiveMap["output-dir"] = outputDirectory;
        }
        IOSystem::getInstance()
            .getWriter(directiveMap, symTable, recordTable)
            ->writeAll(*rel_24_get_pc_thunk);
      } catch (std::exception &e) {
        std::cerr << e.what();
        exit(1);
      }
    }
  }
#ifdef _MSC_VER
#pragma warning(default : 4100)
#endif // _MSC_VER
#ifdef _MSC_VER
#pragma warning(disable : 4100)
#endif // _MSC_VER
  void subroutine_24(const std::vector<RamDomain> &args,
                     std::vector<RamDomain> &ret) {
    if (performIO) {
      try {
        std::map<std::string, std::string> directiveMap(
            {{"IO", "file"},
             {"attributeNames", "ea\tindex\toperator"},
             {"fact-dir", "."},
             {"filename", "instruction_get_op.csv"},
             {"name", "instruction_get_op"},
             {"operation", "input"},
             {"params",
              "{\"records\": {}, \"relation\": {\"arity\": 3, \"auxArity\": 0, "
              "\"params\": [\"ea\", \"index\", \"operator\"]}}"},
             {"types",
              "{\"ADTs\": {}, \"records\": {}, \"relation\": {\"arity\": 3, "
              "\"auxArity\": 0, \"types\": [\"u:address\", "
              "\"u:operand_index\", \"u:operand_code\"]}}"}});
        if (!inputDirectory.empty()) {
          directiveMap["fact-dir"] = inputDirectory;
        }
        IOSystem::getInstance()
            .getReader(directiveMap, symTable, recordTable)
            ->readAll(*rel_27_instruction_get_op);
      } catch (std::exception &e) {
        std::cerr << "Error loading data: " << e.what() << '\n';
      }
    }
    if (performIO) {
      try {
        std::map<std::string, std::string> directiveMap(
            {{"IO", "file"},
             {"attributeNames", "ea\tindex\toperator"},
             {"name", "instruction_get_op"},
             {"operation", "output"},
             {"output-dir", "."},
             {"params",
              "{\"records\": {}, \"relation\": {\"arity\": 3, \"auxArity\": 0, "
              "\"params\": [\"ea\", \"index\", \"operator\"]}}"},
             {"types",
              "{\"ADTs\": {}, \"records\": {}, \"relation\": {\"arity\": 3, "
              "\"auxArity\": 0, \"types\": [\"u:address\", "
              "\"u:operand_index\", \"u:operand_code\"]}}"}});
        if (!outputDirectory.empty()) {
          directiveMap["output-dir"] = outputDirectory;
        }
        IOSystem::getInstance()
            .getWriter(directiveMap, symTable, recordTable)
            ->writeAll(*rel_27_instruction_get_op);
      } catch (std::exception &e) {
        std::cerr << e.what();
        exit(1);
      }
    }
  }
#ifdef _MSC_VER
#pragma warning(default : 4100)
#endif // _MSC_VER
#ifdef _MSC_VER
#pragma warning(disable : 4100)
#endif // _MSC_VER
  void subroutine_25(const std::vector<RamDomain> &args,
                     std::vector<RamDomain> &ret) {
    if (performIO) {
      try {
        std::map<std::string, std::string> directiveMap(
            {{"IO", "file"},
             {"attributeNames", "ea\tIndex\top"},
             {"fact-dir", "."},
             {"filename", "instruction_get_src_op.csv"},
             {"name", "instruction_get_src_op"},
             {"operation", "input"},
             {"params",
              "{\"records\": {}, \"relation\": {\"arity\": 3, \"auxArity\": 0, "
              "\"params\": [\"ea\", \"Index\", \"op\"]}}"},
             {"types",
              "{\"ADTs\": {}, \"records\": {}, \"relation\": {\"arity\": 3, "
              "\"auxArity\": 0, \"types\": [\"u:address\", "
              "\"u:operand_index\", \"u:operand_code\"]}}"}});
        if (!inputDirectory.empty()) {
          directiveMap["fact-dir"] = inputDirectory;
        }
        IOSystem::getInstance()
            .getReader(directiveMap, symTable, recordTable)
            ->readAll(*rel_28_instruction_get_src_op);
      } catch (std::exception &e) {
        std::cerr << "Error loading data: " << e.what() << '\n';
      }
    }
    if (performIO) {
      try {
        std::map<std::string, std::string> directiveMap(
            {{"IO", "file"},
             {"attributeNames", "ea\tIndex\top"},
             {"name", "instruction_get_src_op"},
             {"operation", "output"},
             {"output-dir", "."},
             {"params",
              "{\"records\": {}, \"relation\": {\"arity\": 3, \"auxArity\": 0, "
              "\"params\": [\"ea\", \"Index\", \"op\"]}}"},
             {"types",
              "{\"ADTs\": {}, \"records\": {}, \"relation\": {\"arity\": 3, "
              "\"auxArity\": 0, \"types\": [\"u:address\", "
              "\"u:operand_index\", \"u:operand_code\"]}}"}});
        if (!outputDirectory.empty()) {
          directiveMap["output-dir"] = outputDirectory;
        }
        IOSystem::getInstance()
            .getWriter(directiveMap, symTable, recordTable)
            ->writeAll(*rel_28_instruction_get_src_op);
      } catch (std::exception &e) {
        std::cerr << e.what();
        exit(1);
      }
    }
  }
#ifdef _MSC_VER
#pragma warning(default : 4100)
#endif // _MSC_VER
#ifdef _MSC_VER
#pragma warning(disable : 4100)
#endif // _MSC_VER
  void subroutine_26(const std::vector<RamDomain> &args,
                     std::vector<RamDomain> &ret) {
    if (performIO) {
      try {
        std::map<std::string, std::string> directiveMap(
            {{"IO", "file"},
             {"attributeNames", "op\treg"},
             {"fact-dir", "."},
             {"filename", "op_indirect_contains_reg.csv"},
             {"name", "op_indirect_contains_reg"},
             {"operation", "input"},
             {"params", "{\"records\": {}, \"relation\": {\"arity\": 2, "
                        "\"auxArity\": 0, \"params\": [\"op\", \"reg\"]}}"},
             {"types", "{\"ADTs\": {}, \"records\": {}, \"relation\": "
                       "{\"arity\": 2, \"auxArity\": 0, \"types\": "
                       "[\"u:operand_code\", \"s:register\"]}}"}});
        if (!inputDirectory.empty()) {
          directiveMap["fact-dir"] = inputDirectory;
        }
        IOSystem::getInstance()
            .getReader(directiveMap, symTable, recordTable)
            ->readAll(*rel_38_op_indirect_contains_reg);
      } catch (std::exception &e) {
        std::cerr << "Error loading data: " << e.what() << '\n';
      }
    }
    if (performIO) {
      try {
        std::map<std::string, std::string> directiveMap(
            {{"IO", "file"},
             {"attributeNames", "op\treg"},
             {"name", "op_indirect_contains_reg"},
             {"operation", "output"},
             {"output-dir", "."},
             {"params", "{\"records\": {}, \"relation\": {\"arity\": 2, "
                        "\"auxArity\": 0, \"params\": [\"op\", \"reg\"]}}"},
             {"types", "{\"ADTs\": {}, \"records\": {}, \"relation\": "
                       "{\"arity\": 2, \"auxArity\": 0, \"types\": "
                       "[\"u:operand_code\", \"s:register\"]}}"}});
        if (!outputDirectory.empty()) {
          directiveMap["output-dir"] = outputDirectory;
        }
        IOSystem::getInstance()
            .getWriter(directiveMap, symTable, recordTable)
            ->writeAll(*rel_38_op_indirect_contains_reg);
      } catch (std::exception &e) {
        std::cerr << e.what();
        exit(1);
      }
    }
  }
#ifdef _MSC_VER
#pragma warning(default : 4100)
#endif // _MSC_VER
#ifdef _MSC_VER
#pragma warning(disable : 4100)
#endif // _MSC_VER
  void subroutine_27(const std::vector<RamDomain> &args,
                     std::vector<RamDomain> &ret) {
    SignalHandler::instance()->setMsg(R"_(used(EA,Reg,Index) :- 
   instruction_get_src_op(EA,Index,Op),
   op_regdirect_contains_reg(Op,Reg).
in file /reversi/datalog/souffle/test_def.dl [226:1-228:39])_");
    if (!(rel_28_instruction_get_src_op->empty()) &&
        !(rel_39_op_regdirect_contains_reg->empty())) {
      [&]() {
        CREATE_OP_CONTEXT(rel_44_used_op_ctxt, rel_44_used->createContext());
        CREATE_OP_CONTEXT(rel_39_op_regdirect_contains_reg_op_ctxt,
                          rel_39_op_regdirect_contains_reg->createContext());
        CREATE_OP_CONTEXT(rel_28_instruction_get_src_op_op_ctxt,
                          rel_28_instruction_get_src_op->createContext());
        for (const auto &env0 : *rel_28_instruction_get_src_op) {
          auto range = rel_39_op_regdirect_contains_reg->lowerUpperRange_10(
              Tuple<RamDomain, 2>{
                  {ramBitCast(env0[2]), ramBitCast<RamDomain>(MIN_RAM_SIGNED)}},
              Tuple<RamDomain, 2>{
                  {ramBitCast(env0[2]), ramBitCast<RamDomain>(MAX_RAM_SIGNED)}},
              READ_OP_CONTEXT(rel_39_op_regdirect_contains_reg_op_ctxt));
          for (const auto &env1 : range) {
            Tuple<RamDomain, 3> tuple{{ramBitCast(env0[0]), ramBitCast(env1[1]),
                                       ramBitCast(env0[1])}};
            rel_44_used->insert(tuple, READ_OP_CONTEXT(rel_44_used_op_ctxt));
          }
        }
      }();
    }
    SignalHandler::instance()->setMsg(R"_(used(EA,Reg,Index) :- 
   instruction_get_op(EA,Index,Op),
   op_indirect_contains_reg(Op,Reg).
in file /reversi/datalog/souffle/test_def.dl [230:1-232:38])_");
    if (!(rel_27_instruction_get_op->empty()) &&
        !(rel_38_op_indirect_contains_reg->empty())) {
      [&]() {
        CREATE_OP_CONTEXT(rel_38_op_indirect_contains_reg_op_ctxt,
                          rel_38_op_indirect_contains_reg->createContext());
        CREATE_OP_CONTEXT(rel_44_used_op_ctxt, rel_44_used->createContext());
        CREATE_OP_CONTEXT(rel_27_instruction_get_op_op_ctxt,
                          rel_27_instruction_get_op->createContext());
        for (const auto &env0 : *rel_27_instruction_get_op) {
          auto range = rel_38_op_indirect_contains_reg->lowerUpperRange_10(
              Tuple<RamDomain, 2>{
                  {ramBitCast(env0[2]), ramBitCast<RamDomain>(MIN_RAM_SIGNED)}},
              Tuple<RamDomain, 2>{
                  {ramBitCast(env0[2]), ramBitCast<RamDomain>(MAX_RAM_SIGNED)}},
              READ_OP_CONTEXT(rel_38_op_indirect_contains_reg_op_ctxt));
          for (const auto &env1 : range) {
            Tuple<RamDomain, 3> tuple{{ramBitCast(env0[0]), ramBitCast(env1[1]),
                                       ramBitCast(env0[1])}};
            rel_44_used->insert(tuple, READ_OP_CONTEXT(rel_44_used_op_ctxt));
          }
        }
      }();
    }
    if (performIO) {
      try {
        std::map<std::string, std::string> directiveMap(
            {{"IO", "file"},
             {"attributeNames", "EA\tReg\tIndex"},
             {"name", "used"},
             {"operation", "output"},
             {"output-dir", "."},
             {"params",
              "{\"records\": {}, \"relation\": {\"arity\": 3, \"auxArity\": 0, "
              "\"params\": [\"EA\", \"Reg\", \"Index\"]}}"},
             {"types",
              "{\"ADTs\": {}, \"records\": {}, \"relation\": {\"arity\": 3, "
              "\"auxArity\": 0, \"types\": [\"u:address\", \"s:register\", "
              "\"u:operand_index\"]}}"}});
        if (!outputDirectory.empty()) {
          directiveMap["output-dir"] = outputDirectory;
        }
        IOSystem::getInstance()
            .getWriter(directiveMap, symTable, recordTable)
            ->writeAll(*rel_44_used);
      } catch (std::exception &e) {
        std::cerr << e.what();
        exit(1);
      }
    }
    if (performIO)
      rel_39_op_regdirect_contains_reg->purge();
    if (performIO)
      rel_28_instruction_get_src_op->purge();
    if (performIO)
      rel_38_op_indirect_contains_reg->purge();
    if (performIO)
      rel_27_instruction_get_op->purge();
  }
#ifdef _MSC_VER
#pragma warning(default : 4100)
#endif // _MSC_VER
#ifdef _MSC_VER
#pragma warning(disable : 4100)
#endif // _MSC_VER
  void subroutine_28(const std::vector<RamDomain> &args,
                     std::vector<RamDomain> &ret) {
    SignalHandler::instance()->setMsg(R"_(used_in_block(Block,Reg) :- 
   used(EA,Reg,_),
   code_in_block(EA,Block).
in file /reversi/datalog/souffle/test_def.dl [237:1-239:29])_");
    if (!(rel_44_used->empty()) && !(rel_12_code_in_block->empty())) {
      [&]() {
        CREATE_OP_CONTEXT(rel_44_used_op_ctxt, rel_44_used->createContext());
        CREATE_OP_CONTEXT(rel_45_used_in_block_op_ctxt,
                          rel_45_used_in_block->createContext());
        CREATE_OP_CONTEXT(rel_12_code_in_block_op_ctxt,
                          rel_12_code_in_block->createContext());
        for (const auto &env0 : *rel_44_used) {
          auto range = rel_12_code_in_block->lowerUpperRange_10(
              Tuple<RamDomain, 2>{{ramBitCast(env0[0]),
                                   ramBitCast<RamDomain>(MIN_RAM_UNSIGNED)}},
              Tuple<RamDomain, 2>{{ramBitCast(env0[0]),
                                   ramBitCast<RamDomain>(MAX_RAM_UNSIGNED)}},
              READ_OP_CONTEXT(rel_12_code_in_block_op_ctxt));
          for (const auto &env1 : range) {
            Tuple<RamDomain, 2> tuple{
                {ramBitCast(env1[1]), ramBitCast(env0[1])}};
            rel_45_used_in_block->insert(
                tuple, READ_OP_CONTEXT(rel_45_used_in_block_op_ctxt));
          }
        }
      }();
    }
    if (performIO) {
      try {
        std::map<std::string, std::string> directiveMap(
            {{"IO", "file"},
             {"attributeNames", "EA\tReg"},
             {"name", "used_in_block"},
             {"operation", "output"},
             {"output-dir", "."},
             {"params", "{\"records\": {}, \"relation\": {\"arity\": 2, "
                        "\"auxArity\": 0, \"params\": [\"EA\", \"Reg\"]}}"},
             {"types", "{\"ADTs\": {}, \"records\": {}, \"relation\": "
                       "{\"arity\": 2, \"auxArity\": 0, \"types\": "
                       "[\"u:address\", \"s:register\"]}}"}});
        if (!outputDirectory.empty()) {
          directiveMap["output-dir"] = outputDirectory;
        }
        IOSystem::getInstance()
            .getWriter(directiveMap, symTable, recordTable)
            ->writeAll(*rel_45_used_in_block);
      } catch (std::exception &e) {
        std::cerr << e.what();
        exit(1);
      }
    }
  }
#ifdef _MSC_VER
#pragma warning(default : 4100)
#endif // _MSC_VER
#ifdef _MSC_VER
#pragma warning(disable : 4100)
#endif // _MSC_VER
  void subroutine_29(const std::vector<RamDomain> &args,
                     std::vector<RamDomain> &ret) {
    SignalHandler::instance()->setMsg(
        R"_(def_used_intra(EA_def,Reg,EA_used,Index) :- 
   used(EA_used,Reg,Index),
   block_last_def(EA_used,EA_def,Reg).
in file /reversi/datalog/souffle/test_def.dl [283:1-285:40])_");
    if (!(rel_44_used->empty()) && !(rel_6_block_last_def->empty())) {
      [&]() {
        CREATE_OP_CONTEXT(rel_44_used_op_ctxt, rel_44_used->createContext());
        CREATE_OP_CONTEXT(rel_16_def_used_intra_op_ctxt,
                          rel_16_def_used_intra->createContext());
        CREATE_OP_CONTEXT(rel_6_block_last_def_op_ctxt,
                          rel_6_block_last_def->createContext());
        for (const auto &env0 : *rel_44_used) {
          auto range = rel_6_block_last_def->lowerUpperRange_101(
              Tuple<RamDomain, 3>{{ramBitCast(env0[0]),
                                   ramBitCast<RamDomain>(MIN_RAM_UNSIGNED),
                                   ramBitCast(env0[1])}},
              Tuple<RamDomain, 3>{{ramBitCast(env0[0]),
                                   ramBitCast<RamDomain>(MAX_RAM_UNSIGNED),
                                   ramBitCast(env0[1])}},
              READ_OP_CONTEXT(rel_6_block_last_def_op_ctxt));
          for (const auto &env1 : range) {
            Tuple<RamDomain, 4> tuple{{ramBitCast(env1[1]), ramBitCast(env0[1]),
                                       ramBitCast(env0[0]),
                                       ramBitCast(env0[2])}};
            rel_16_def_used_intra->insert(
                tuple, READ_OP_CONTEXT(rel_16_def_used_intra_op_ctxt));
          }
        }
      }();
    }
    SignalHandler::instance()->setMsg(
        R"_(def_used_intra(EA_def,Reg,EA_used,Index) :- 
   last_def(Block,EA_def,Reg),
   code_in_block(EA_used,Block),
   !block_last_def(EA_used,_,Reg),
   used(EA_used,Reg,Index).
in file /reversi/datalog/souffle/test_def.dl [287:1-291:29])_");
    if (!(rel_44_used->empty()) && !(rel_33_last_def->empty()) &&
        !(rel_12_code_in_block->empty())) {
      [&]() {
        CREATE_OP_CONTEXT(rel_33_last_def_op_ctxt,
                          rel_33_last_def->createContext());
        CREATE_OP_CONTEXT(rel_44_used_op_ctxt, rel_44_used->createContext());
        CREATE_OP_CONTEXT(rel_16_def_used_intra_op_ctxt,
                          rel_16_def_used_intra->createContext());
        CREATE_OP_CONTEXT(rel_12_code_in_block_op_ctxt,
                          rel_12_code_in_block->createContext());
        CREATE_OP_CONTEXT(rel_6_block_last_def_op_ctxt,
                          rel_6_block_last_def->createContext());
        for (const auto &env0 : *rel_33_last_def) {
          auto range = rel_12_code_in_block->lowerUpperRange_01(
              Tuple<RamDomain, 2>{{ramBitCast<RamDomain>(MIN_RAM_UNSIGNED),
                                   ramBitCast(env0[0])}},
              Tuple<RamDomain, 2>{{ramBitCast<RamDomain>(MAX_RAM_UNSIGNED),
                                   ramBitCast(env0[0])}},
              READ_OP_CONTEXT(rel_12_code_in_block_op_ctxt));
          for (const auto &env1 : range) {
            if (!(!rel_6_block_last_def
                       ->lowerUpperRange_101(
                           Tuple<RamDomain, 3>{
                               {ramBitCast(env1[0]),
                                ramBitCast<RamDomain>(MIN_RAM_UNSIGNED),
                                ramBitCast(env0[2])}},
                           Tuple<RamDomain, 3>{
                               {ramBitCast(env1[0]),
                                ramBitCast<RamDomain>(MAX_RAM_UNSIGNED),
                                ramBitCast(env0[2])}},
                           READ_OP_CONTEXT(rel_6_block_last_def_op_ctxt))
                       .empty())) {
              auto range = rel_44_used->lowerUpperRange_110(
                  Tuple<RamDomain, 3>{
                      {ramBitCast(env1[0]), ramBitCast(env0[2]),
                       ramBitCast<RamDomain>(MIN_RAM_UNSIGNED)}},
                  Tuple<RamDomain, 3>{
                      {ramBitCast(env1[0]), ramBitCast(env0[2]),
                       ramBitCast<RamDomain>(MAX_RAM_UNSIGNED)}},
                  READ_OP_CONTEXT(rel_44_used_op_ctxt));
              for (const auto &env2 : range) {
                Tuple<RamDomain, 4> tuple{
                    {ramBitCast(env0[1]), ramBitCast(env0[2]),
                     ramBitCast(env1[0]), ramBitCast(env2[2])}};
                rel_16_def_used_intra->insert(
                    tuple, READ_OP_CONTEXT(rel_16_def_used_intra_op_ctxt));
              }
            }
          }
        }
      }();
    }
    if (performIO) {
      try {
        std::map<std::string, std::string> directiveMap(
            {{"IO", "file"},
             {"attributeNames", "ea_def\treg\tea_used\tindex_used"},
             {"name", "def_used_intra"},
             {"operation", "output"},
             {"output-dir", "."},
             {"params", "{\"records\": {}, \"relation\": {\"arity\": 4, "
                        "\"auxArity\": 0, \"params\": [\"ea_def\", \"reg\", "
                        "\"ea_used\", \"index_used\"]}}"},
             {"types",
              "{\"ADTs\": {}, \"records\": {}, \"relation\": {\"arity\": 4, "
              "\"auxArity\": 0, \"types\": [\"u:address\", \"s:register\", "
              "\"u:address\", \"u:operand_index\"]}}"}});
        if (!outputDirectory.empty()) {
          directiveMap["output-dir"] = outputDirectory;
        }
        IOSystem::getInstance()
            .getWriter(directiveMap, symTable, recordTable)
            ->writeAll(*rel_16_def_used_intra);
      } catch (std::exception &e) {
        std::cerr << e.what();
        exit(1);
      }
    }
    if (performIO)
      rel_44_used->purge();
  }
#ifdef _MSC_VER
#pragma warning(default : 4100)
#endif // _MSC_VER
#ifdef _MSC_VER
#pragma warning(disable : 4100)
#endif // _MSC_VER
  void subroutine_30(const std::vector<RamDomain> &args,
                     std::vector<RamDomain> &ret) {
    if (performIO) {
      try {
        std::map<std::string, std::string> directiveMap(
            {{"IO", "file"},
             {"attributeNames", "start\tend"},
             {"fact-dir", "."},
             {"filename", "fde_addresses.csv"},
             {"name", "fde_addresses"},
             {"operation", "input"},
             {"params", "{\"records\": {}, \"relation\": {\"arity\": 2, "
                        "\"auxArity\": 0, \"params\": [\"start\", \"end\"]}}"},
             {"types",
              "{\"ADTs\": {}, \"records\": {}, \"relation\": {\"arity\": 2, "
              "\"auxArity\": 0, \"types\": [\"u:address\", \"u:address\"]}}"}});
        if (!inputDirectory.empty()) {
          directiveMap["fact-dir"] = inputDirectory;
        }
        IOSystem::getInstance()
            .getReader(directiveMap, symTable, recordTable)
            ->readAll(*rel_21_fde_addresses);
      } catch (std::exception &e) {
        std::cerr << "Error loading data: " << e.what() << '\n';
      }
    }
    if (performIO) {
      try {
        std::map<std::string, std::string> directiveMap(
            {{"IO", "file"},
             {"attributeNames", "start\tend"},
             {"name", "fde_addresses"},
             {"operation", "output"},
             {"output-dir", "."},
             {"params", "{\"records\": {}, \"relation\": {\"arity\": 2, "
                        "\"auxArity\": 0, \"params\": [\"start\", \"end\"]}}"},
             {"types",
              "{\"ADTs\": {}, \"records\": {}, \"relation\": {\"arity\": 2, "
              "\"auxArity\": 0, \"types\": [\"u:address\", \"u:address\"]}}"}});
        if (!outputDirectory.empty()) {
          directiveMap["output-dir"] = outputDirectory;
        }
        IOSystem::getInstance()
            .getWriter(directiveMap, symTable, recordTable)
            ->writeAll(*rel_21_fde_addresses);
      } catch (std::exception &e) {
        std::cerr << e.what();
        exit(1);
      }
    }
  }
#ifdef _MSC_VER
#pragma warning(default : 4100)
#endif // _MSC_VER
#ifdef _MSC_VER
#pragma warning(disable : 4100)
#endif // _MSC_VER
  void subroutine_31(const std::vector<RamDomain> &args,
                     std::vector<RamDomain> &ret) {
    if (performIO) {
      try {
        std::map<std::string, std::string> directiveMap(
            {{"IO", "file"},
             {"attributeNames", "n"},
             {"fact-dir", "."},
             {"filename", "arch.return.csv"},
             {"name", "return"},
             {"operation", "input"},
             {"params", "{\"records\": {}, \"relation\": {\"arity\": 1, "
                        "\"auxArity\": 0, \"params\": [\"n\"]}}"},
             {"types",
              "{\"ADTs\": {}, \"records\": {}, \"relation\": {\"arity\": 1, "
              "\"auxArity\": 0, \"types\": [\"u:address\"]}}"}});
        if (!inputDirectory.empty()) {
          directiveMap["fact-dir"] = inputDirectory;
        }
        IOSystem::getInstance()
            .getReader(directiveMap, symTable, recordTable)
            ->readAll(*rel_41_return);
      } catch (std::exception &e) {
        std::cerr << "Error loading data: " << e.what() << '\n';
      }
    }
    if (performIO) {
      try {
        std::map<std::string, std::string> directiveMap(
            {{"IO", "file"},
             {"attributeNames", "n"},
             {"name", "return"},
             {"operation", "output"},
             {"output-dir", "."},
             {"params", "{\"records\": {}, \"relation\": {\"arity\": 1, "
                        "\"auxArity\": 0, \"params\": [\"n\"]}}"},
             {"types",
              "{\"ADTs\": {}, \"records\": {}, \"relation\": {\"arity\": 1, "
              "\"auxArity\": 0, \"types\": [\"u:address\"]}}"}});
        if (!outputDirectory.empty()) {
          directiveMap["output-dir"] = outputDirectory;
        }
        IOSystem::getInstance()
            .getWriter(directiveMap, symTable, recordTable)
            ->writeAll(*rel_41_return);
      } catch (std::exception &e) {
        std::cerr << e.what();
        exit(1);
      }
    }
  }
#ifdef _MSC_VER
#pragma warning(default : 4100)
#endif // _MSC_VER
#ifdef _MSC_VER
#pragma warning(disable : 4100)
#endif // _MSC_VER
  void subroutine_32(const std::vector<RamDomain> &args,
                     std::vector<RamDomain> &ret) {
    SignalHandler::instance()->setMsg(R"_(return_val_reg("RAX").
in file /reversi/datalog/souffle/test_def.dl [306:1-306:23])_");
    [&]() {
      CREATE_OP_CONTEXT(rel_42_return_val_reg_op_ctxt,
                        rel_42_return_val_reg->createContext());
      Tuple<RamDomain, 1> tuple{{ramBitCast(RamSigned(8))}};
      rel_42_return_val_reg->insert(
          tuple, READ_OP_CONTEXT(rel_42_return_val_reg_op_ctxt));
    }();
    if (performIO) {
      try {
        std::map<std::string, std::string> directiveMap(
            {{"IO", "file"},
             {"attributeNames", "reg"},
             {"name", "return_val_reg"},
             {"operation", "output"},
             {"output-dir", "."},
             {"params", "{\"records\": {}, \"relation\": {\"arity\": 1, "
                        "\"auxArity\": 0, \"params\": [\"reg\"]}}"},
             {"types",
              "{\"ADTs\": {}, \"records\": {}, \"relation\": {\"arity\": 1, "
              "\"auxArity\": 0, \"types\": [\"s:register\"]}}"}});
        if (!outputDirectory.empty()) {
          directiveMap["output-dir"] = outputDirectory;
        }
        IOSystem::getInstance()
            .getWriter(directiveMap, symTable, recordTable)
            ->writeAll(*rel_42_return_val_reg);
      } catch (std::exception &e) {
        std::cerr << e.what();
        exit(1);
      }
    }
  }
#ifdef _MSC_VER
#pragma warning(default : 4100)
#endif // _MSC_VER
#ifdef _MSC_VER
#pragma warning(disable : 4100)
#endif // _MSC_VER
  void subroutine_33(const std::vector<RamDomain> &args,
                     std::vector<RamDomain> &ret) {
    SignalHandler::instance()->setMsg(
        R"_(def_used_return_val_reg(EA_def,EA_call,Reg,EA_used,Index_used) :- 
   return_val_reg(Reg),
   def_used_intra(EA_call,Reg,EA_used,Index_used),
   direct_call(EA_call,Callee),
   fde_addresses(Callee,End),
   return(CalleeReturn),
   Callee <= CalleeReturn,
   CalleeReturn < End,
   code_in_block(CalleeReturn,CalleeReturnBlock),
   block_last_instruction(CalleeReturnBlock,CalleeReturnBlock_end),
   block_last_def(CalleeReturnBlock_end,EA_def,Reg).
in file /reversi/datalog/souffle/test_def.dl [316:1-331:7])_");
    if (!(rel_6_block_last_def->empty()) &&
        !(rel_7_block_last_instruction->empty()) &&
        !(rel_12_code_in_block->empty()) && !(rel_41_return->empty()) &&
        !(rel_21_fde_addresses->empty()) && !(rel_19_direct_call->empty()) &&
        !(rel_42_return_val_reg->empty()) &&
        !(rel_16_def_used_intra->empty())) {
      [&]() {
        CREATE_OP_CONTEXT(rel_19_direct_call_op_ctxt,
                          rel_19_direct_call->createContext());
        CREATE_OP_CONTEXT(rel_16_def_used_intra_op_ctxt,
                          rel_16_def_used_intra->createContext());
        CREATE_OP_CONTEXT(rel_21_fde_addresses_op_ctxt,
                          rel_21_fde_addresses->createContext());
        CREATE_OP_CONTEXT(rel_41_return_op_ctxt,
                          rel_41_return->createContext());
        CREATE_OP_CONTEXT(rel_42_return_val_reg_op_ctxt,
                          rel_42_return_val_reg->createContext());
        CREATE_OP_CONTEXT(rel_17_def_used_return_val_reg_op_ctxt,
                          rel_17_def_used_return_val_reg->createContext());
        CREATE_OP_CONTEXT(rel_7_block_last_instruction_op_ctxt,
                          rel_7_block_last_instruction->createContext());
        CREATE_OP_CONTEXT(rel_12_code_in_block_op_ctxt,
                          rel_12_code_in_block->createContext());
        CREATE_OP_CONTEXT(rel_6_block_last_def_op_ctxt,
                          rel_6_block_last_def->createContext());
        for (const auto &env0 : *rel_42_return_val_reg) {
          auto range = rel_16_def_used_intra->lowerUpperRange_0100(
              Tuple<RamDomain, 4>{{ramBitCast<RamDomain>(MIN_RAM_UNSIGNED),
                                   ramBitCast(env0[0]),
                                   ramBitCast<RamDomain>(MIN_RAM_UNSIGNED),
                                   ramBitCast<RamDomain>(MIN_RAM_UNSIGNED)}},
              Tuple<RamDomain, 4>{{ramBitCast<RamDomain>(MAX_RAM_UNSIGNED),
                                   ramBitCast(env0[0]),
                                   ramBitCast<RamDomain>(MAX_RAM_UNSIGNED),
                                   ramBitCast<RamDomain>(MAX_RAM_UNSIGNED)}},
              READ_OP_CONTEXT(rel_16_def_used_intra_op_ctxt));
          for (const auto &env1 : range) {
            auto range = rel_19_direct_call->lowerUpperRange_10(
                Tuple<RamDomain, 2>{{ramBitCast(env1[0]),
                                     ramBitCast<RamDomain>(MIN_RAM_UNSIGNED)}},
                Tuple<RamDomain, 2>{{ramBitCast(env1[0]),
                                     ramBitCast<RamDomain>(MAX_RAM_UNSIGNED)}},
                READ_OP_CONTEXT(rel_19_direct_call_op_ctxt));
            for (const auto &env2 : range) {
              auto range = rel_21_fde_addresses->lowerUpperRange_10(
                  Tuple<RamDomain, 2>{
                      {ramBitCast(env2[1]),
                       ramBitCast<RamDomain>(MIN_RAM_UNSIGNED)}},
                  Tuple<RamDomain, 2>{
                      {ramBitCast(env2[1]),
                       ramBitCast<RamDomain>(MAX_RAM_UNSIGNED)}},
                  READ_OP_CONTEXT(rel_21_fde_addresses_op_ctxt));
              for (const auto &env3 : range) {
                auto range = rel_41_return->lowerUpperRange_2(
                    Tuple<RamDomain, 1>{{ramBitCast(env2[1])}},
                    Tuple<RamDomain, 1>{{ramBitCast(env3[1])}},
                    READ_OP_CONTEXT(rel_41_return_op_ctxt));
                for (const auto &env4 : range) {
                  if ((ramBitCast<RamDomain>(env4[0]) !=
                       ramBitCast<RamDomain>(env3[1]))) {
                    auto range = rel_12_code_in_block->lowerUpperRange_10(
                        Tuple<RamDomain, 2>{
                            {ramBitCast(env4[0]),
                             ramBitCast<RamDomain>(MIN_RAM_UNSIGNED)}},
                        Tuple<RamDomain, 2>{
                            {ramBitCast(env4[0]),
                             ramBitCast<RamDomain>(MAX_RAM_UNSIGNED)}},
                        READ_OP_CONTEXT(rel_12_code_in_block_op_ctxt));
                    for (const auto &env5 : range) {
                      auto range =
                          rel_7_block_last_instruction->lowerUpperRange_10(
                              Tuple<RamDomain, 2>{
                                  {ramBitCast(env5[1]),
                                   ramBitCast<RamDomain>(MIN_RAM_UNSIGNED)}},
                              Tuple<RamDomain, 2>{
                                  {ramBitCast(env5[1]),
                                   ramBitCast<RamDomain>(MAX_RAM_UNSIGNED)}},
                              READ_OP_CONTEXT(
                                  rel_7_block_last_instruction_op_ctxt));
                      for (const auto &env6 : range) {
                        auto range = rel_6_block_last_def->lowerUpperRange_101(
                            Tuple<RamDomain, 3>{
                                {ramBitCast(env6[1]),
                                 ramBitCast<RamDomain>(MIN_RAM_UNSIGNED),
                                 ramBitCast(env0[0])}},
                            Tuple<RamDomain, 3>{
                                {ramBitCast(env6[1]),
                                 ramBitCast<RamDomain>(MAX_RAM_UNSIGNED),
                                 ramBitCast(env0[0])}},
                            READ_OP_CONTEXT(rel_6_block_last_def_op_ctxt));
                        for (const auto &env7 : range) {
                          Tuple<RamDomain, 5> tuple{
                              {ramBitCast(env7[1]), ramBitCast(env1[0]),
                               ramBitCast(env0[0]), ramBitCast(env1[2]),
                               ramBitCast(env1[3])}};
                          rel_17_def_used_return_val_reg->insert(
                              tuple,
                              READ_OP_CONTEXT(
                                  rel_17_def_used_return_val_reg_op_ctxt));
                        }
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }();
    }
    SignalHandler::instance()->setMsg(
        R"_(def_used_return_val_reg(EA_def,EA_call,Reg,EA_used,Index_used) :- 
   return_val_reg(Reg),
   def_used_intra(EA_call,Reg,EA_used,Index_used),
   direct_call(EA_call,Callee),
   fde_addresses(Callee,End),
   return(CalleeReturn),
   Callee <= CalleeReturn,
   CalleeReturn < End,
   code_in_block(CalleeReturn,CalleeReturnBlock),
   block_last_instruction(CalleeReturnBlock,CalleeReturnBlock_end),
   !block_last_def(CalleeReturnBlock_end,_,Reg),
   last_def(CalleeReturnBlock,EA_def,Reg).
in file /reversi/datalog/souffle/test_def.dl [316:1-331:7])_");
    if (!(rel_33_last_def->empty()) &&
        !(rel_7_block_last_instruction->empty()) &&
        !(rel_12_code_in_block->empty()) && !(rel_41_return->empty()) &&
        !(rel_21_fde_addresses->empty()) && !(rel_19_direct_call->empty()) &&
        !(rel_42_return_val_reg->empty()) &&
        !(rel_16_def_used_intra->empty())) {
      [&]() {
        CREATE_OP_CONTEXT(rel_33_last_def_op_ctxt,
                          rel_33_last_def->createContext());
        CREATE_OP_CONTEXT(rel_19_direct_call_op_ctxt,
                          rel_19_direct_call->createContext());
        CREATE_OP_CONTEXT(rel_16_def_used_intra_op_ctxt,
                          rel_16_def_used_intra->createContext());
        CREATE_OP_CONTEXT(rel_21_fde_addresses_op_ctxt,
                          rel_21_fde_addresses->createContext());
        CREATE_OP_CONTEXT(rel_41_return_op_ctxt,
                          rel_41_return->createContext());
        CREATE_OP_CONTEXT(rel_42_return_val_reg_op_ctxt,
                          rel_42_return_val_reg->createContext());
        CREATE_OP_CONTEXT(rel_17_def_used_return_val_reg_op_ctxt,
                          rel_17_def_used_return_val_reg->createContext());
        CREATE_OP_CONTEXT(rel_7_block_last_instruction_op_ctxt,
                          rel_7_block_last_instruction->createContext());
        CREATE_OP_CONTEXT(rel_12_code_in_block_op_ctxt,
                          rel_12_code_in_block->createContext());
        CREATE_OP_CONTEXT(rel_6_block_last_def_op_ctxt,
                          rel_6_block_last_def->createContext());
        for (const auto &env0 : *rel_42_return_val_reg) {
          auto range = rel_16_def_used_intra->lowerUpperRange_0100(
              Tuple<RamDomain, 4>{{ramBitCast<RamDomain>(MIN_RAM_UNSIGNED),
                                   ramBitCast(env0[0]),
                                   ramBitCast<RamDomain>(MIN_RAM_UNSIGNED),
                                   ramBitCast<RamDomain>(MIN_RAM_UNSIGNED)}},
              Tuple<RamDomain, 4>{{ramBitCast<RamDomain>(MAX_RAM_UNSIGNED),
                                   ramBitCast(env0[0]),
                                   ramBitCast<RamDomain>(MAX_RAM_UNSIGNED),
                                   ramBitCast<RamDomain>(MAX_RAM_UNSIGNED)}},
              READ_OP_CONTEXT(rel_16_def_used_intra_op_ctxt));
          for (const auto &env1 : range) {
            auto range = rel_19_direct_call->lowerUpperRange_10(
                Tuple<RamDomain, 2>{{ramBitCast(env1[0]),
                                     ramBitCast<RamDomain>(MIN_RAM_UNSIGNED)}},
                Tuple<RamDomain, 2>{{ramBitCast(env1[0]),
                                     ramBitCast<RamDomain>(MAX_RAM_UNSIGNED)}},
                READ_OP_CONTEXT(rel_19_direct_call_op_ctxt));
            for (const auto &env2 : range) {
              auto range = rel_21_fde_addresses->lowerUpperRange_10(
                  Tuple<RamDomain, 2>{
                      {ramBitCast(env2[1]),
                       ramBitCast<RamDomain>(MIN_RAM_UNSIGNED)}},
                  Tuple<RamDomain, 2>{
                      {ramBitCast(env2[1]),
                       ramBitCast<RamDomain>(MAX_RAM_UNSIGNED)}},
                  READ_OP_CONTEXT(rel_21_fde_addresses_op_ctxt));
              for (const auto &env3 : range) {
                auto range = rel_41_return->lowerUpperRange_2(
                    Tuple<RamDomain, 1>{{ramBitCast(env2[1])}},
                    Tuple<RamDomain, 1>{{ramBitCast(env3[1])}},
                    READ_OP_CONTEXT(rel_41_return_op_ctxt));
                for (const auto &env4 : range) {
                  if ((ramBitCast<RamDomain>(env4[0]) !=
                       ramBitCast<RamDomain>(env3[1]))) {
                    auto range = rel_12_code_in_block->lowerUpperRange_10(
                        Tuple<RamDomain, 2>{
                            {ramBitCast(env4[0]),
                             ramBitCast<RamDomain>(MIN_RAM_UNSIGNED)}},
                        Tuple<RamDomain, 2>{
                            {ramBitCast(env4[0]),
                             ramBitCast<RamDomain>(MAX_RAM_UNSIGNED)}},
                        READ_OP_CONTEXT(rel_12_code_in_block_op_ctxt));
                    for (const auto &env5 : range) {
                      auto range =
                          rel_7_block_last_instruction->lowerUpperRange_10(
                              Tuple<RamDomain, 2>{
                                  {ramBitCast(env5[1]),
                                   ramBitCast<RamDomain>(MIN_RAM_UNSIGNED)}},
                              Tuple<RamDomain, 2>{
                                  {ramBitCast(env5[1]),
                                   ramBitCast<RamDomain>(MAX_RAM_UNSIGNED)}},
                              READ_OP_CONTEXT(
                                  rel_7_block_last_instruction_op_ctxt));
                      for (const auto &env6 : range) {
                        if (!(!rel_6_block_last_def
                                   ->lowerUpperRange_101(
                                       Tuple<RamDomain, 3>{
                                           {ramBitCast(env6[1]),
                                            ramBitCast<RamDomain>(
                                                MIN_RAM_UNSIGNED),
                                            ramBitCast(env0[0])}},
                                       Tuple<RamDomain, 3>{
                                           {ramBitCast(env6[1]),
                                            ramBitCast<RamDomain>(
                                                MAX_RAM_UNSIGNED),
                                            ramBitCast(env0[0])}},
                                       READ_OP_CONTEXT(
                                           rel_6_block_last_def_op_ctxt))
                                   .empty())) {
                          auto range = rel_33_last_def->lowerUpperRange_101(
                              Tuple<RamDomain, 3>{
                                  {ramBitCast(env5[1]),
                                   ramBitCast<RamDomain>(MIN_RAM_UNSIGNED),
                                   ramBitCast(env0[0])}},
                              Tuple<RamDomain, 3>{
                                  {ramBitCast(env5[1]),
                                   ramBitCast<RamDomain>(MAX_RAM_UNSIGNED),
                                   ramBitCast(env0[0])}},
                              READ_OP_CONTEXT(rel_33_last_def_op_ctxt));
                          for (const auto &env7 : range) {
                            Tuple<RamDomain, 5> tuple{
                                {ramBitCast(env7[1]), ramBitCast(env1[0]),
                                 ramBitCast(env0[0]), ramBitCast(env1[2]),
                                 ramBitCast(env1[3])}};
                            rel_17_def_used_return_val_reg->insert(
                                tuple,
                                READ_OP_CONTEXT(
                                    rel_17_def_used_return_val_reg_op_ctxt));
                          }
                          break;
                        }
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }();
    }
    if (performIO) {
      try {
        std::map<std::string, std::string> directiveMap(
            {{"IO", "file"},
             {"attributeNames", "ea_def\tea_call\treg\tea_used\tindex_used"},
             {"name", "def_used_return_val_reg"},
             {"operation", "output"},
             {"output-dir", "."},
             {"params", "{\"records\": {}, \"relation\": {\"arity\": 5, "
                        "\"auxArity\": 0, \"params\": [\"ea_def\", "
                        "\"ea_call\", \"reg\", \"ea_used\", \"index_used\"]}}"},
             {"types",
              "{\"ADTs\": {}, \"records\": {}, \"relation\": {\"arity\": 5, "
              "\"auxArity\": 0, \"types\": [\"u:address\", \"u:address\", "
              "\"s:register\", \"u:address\", \"u:operand_index\"]}}"}});
        if (!outputDirectory.empty()) {
          directiveMap["output-dir"] = outputDirectory;
        }
        IOSystem::getInstance()
            .getWriter(directiveMap, symTable, recordTable)
            ->writeAll(*rel_17_def_used_return_val_reg);
      } catch (std::exception &e) {
        std::cerr << e.what();
        exit(1);
      }
    }
    if (performIO)
      rel_19_direct_call->purge();
    if (performIO)
      rel_12_code_in_block->purge();
    if (performIO)
      rel_7_block_last_instruction->purge();
    if (performIO)
      rel_6_block_last_def->purge();
    if (performIO)
      rel_33_last_def->purge();
    if (performIO)
      rel_42_return_val_reg->purge();
    if (performIO)
      rel_21_fde_addresses->purge();
    if (performIO)
      rel_41_return->purge();
  }
#ifdef _MSC_VER
#pragma warning(default : 4100)
#endif // _MSC_VER
#ifdef _MSC_VER
#pragma warning(disable : 4100)
#endif // _MSC_VER
  void subroutine_34(const std::vector<RamDomain> &args,
                     std::vector<RamDomain> &ret) {
    if (performIO) {
      try {
        std::map<std::string, std::string> directiveMap(
            {{"IO", "file"},
             {"attributeNames", "ea\tsize\tprefix\topcode\top1\top2\top3\top4\t"
                                "immOffset\tdisplacementOffset"},
             {"fact-dir", "."},
             {"filename", "instruction.csv"},
             {"name", "instruction"},
             {"operation", "input"},
             {"params", "{\"records\": {}, \"relation\": {\"arity\": 10, "
                        "\"auxArity\": 0, \"params\": [\"ea\", \"size\", "
                        "\"prefix\", \"opcode\", \"op1\", \"op2\", \"op3\", "
                        "\"op4\", \"immOffset\", \"displacementOffset\"]}}"},
             {"types",
              "{\"ADTs\": {}, \"records\": {}, \"relation\": {\"arity\": 10, "
              "\"auxArity\": 0, \"types\": [\"u:address\", \"u:unsigned\", "
              "\"s:symbol\", \"s:symbol\", \"u:operand_code\", "
              "\"u:operand_code\", \"u:operand_code\", \"u:operand_code\", "
              "\"u:unsigned\", \"u:unsigned\"]}}"}});
        if (!inputDirectory.empty()) {
          directiveMap["fact-dir"] = inputDirectory;
        }
        IOSystem::getInstance()
            .getReader(directiveMap, symTable, recordTable)
            ->readAll(*rel_25_instruction);
      } catch (std::exception &e) {
        std::cerr << "Error loading data: " << e.what() << '\n';
      }
    }
    if (performIO) {
      try {
        std::map<std::string, std::string> directiveMap(
            {{"IO", "file"},
             {"attributeNames", "ea\tsize\tprefix\topcode\top1\top2\top3\top4\t"
                                "immOffset\tdisplacementOffset"},
             {"name", "instruction"},
             {"operation", "output"},
             {"output-dir", "."},
             {"params", "{\"records\": {}, \"relation\": {\"arity\": 10, "
                        "\"auxArity\": 0, \"params\": [\"ea\", \"size\", "
                        "\"prefix\", \"opcode\", \"op1\", \"op2\", \"op3\", "
                        "\"op4\", \"immOffset\", \"displacementOffset\"]}}"},
             {"types",
              "{\"ADTs\": {}, \"records\": {}, \"relation\": {\"arity\": 10, "
              "\"auxArity\": 0, \"types\": [\"u:address\", \"u:unsigned\", "
              "\"s:symbol\", \"s:symbol\", \"u:operand_code\", "
              "\"u:operand_code\", \"u:operand_code\", \"u:operand_code\", "
              "\"u:unsigned\", \"u:unsigned\"]}}"}});
        if (!outputDirectory.empty()) {
          directiveMap["output-dir"] = outputDirectory;
        }
        IOSystem::getInstance()
            .getWriter(directiveMap, symTable, recordTable)
            ->writeAll(*rel_25_instruction);
      } catch (std::exception &e) {
        std::cerr << e.what();
        exit(1);
      }
    }
  }
#ifdef _MSC_VER
#pragma warning(default : 4100)
#endif // _MSC_VER
#ifdef _MSC_VER
#pragma warning(disable : 4100)
#endif // _MSC_VER
  void subroutine_35(const std::vector<RamDomain> &args,
                     std::vector<RamDomain> &ret) {
    SignalHandler::instance()->setMsg(R"_(def_used(EA_def,Reg,EA_used,Index) :- 
   def_used_intra(EA_def,Reg,EA_used,Index).
in file /reversi/datalog/souffle/test_def.dl [293:1-294:46])_");
    if (!(rel_16_def_used_intra->empty())) {
      [&]() {
        CREATE_OP_CONTEXT(rel_16_def_used_intra_op_ctxt,
                          rel_16_def_used_intra->createContext());
        CREATE_OP_CONTEXT(rel_15_def_used_op_ctxt,
                          rel_15_def_used->createContext());
        for (const auto &env0 : *rel_16_def_used_intra) {
          Tuple<RamDomain, 4> tuple{{ramBitCast(env0[0]), ramBitCast(env0[1]),
                                     ramBitCast(env0[2]), ramBitCast(env0[3])}};
          rel_15_def_used->insert(tuple,
                                  READ_OP_CONTEXT(rel_15_def_used_op_ctxt));
        }
      }();
    }
    SignalHandler::instance()->setMsg(
        R"_(def_used(EA_def,Reg,EA_used,Index_used) :- 
   def_used_return_val_reg(EA_def,_,Reg,EA_used,Index_used).
in file /reversi/datalog/souffle/test_def.dl [296:1-297:62])_");
    if (!(rel_17_def_used_return_val_reg->empty())) {
      [&]() {
        CREATE_OP_CONTEXT(rel_17_def_used_return_val_reg_op_ctxt,
                          rel_17_def_used_return_val_reg->createContext());
        CREATE_OP_CONTEXT(rel_15_def_used_op_ctxt,
                          rel_15_def_used->createContext());
        for (const auto &env0 : *rel_17_def_used_return_val_reg) {
          Tuple<RamDomain, 4> tuple{{ramBitCast(env0[0]), ramBitCast(env0[2]),
                                     ramBitCast(env0[3]), ramBitCast(env0[4])}};
          rel_15_def_used->insert(tuple,
                                  READ_OP_CONTEXT(rel_15_def_used_op_ctxt));
        }
      }();
    }
    if (performIO) {
      try {
        std::map<std::string, std::string> directiveMap(
            {{"IO", "file"},
             {"attributeNames", "ea_def\treg\tea_used\tindex_used"},
             {"name", "def_used"},
             {"operation", "output"},
             {"output-dir", "."},
             {"params", "{\"records\": {}, \"relation\": {\"arity\": 4, "
                        "\"auxArity\": 0, \"params\": [\"ea_def\", \"reg\", "
                        "\"ea_used\", \"index_used\"]}}"},
             {"types",
              "{\"ADTs\": {}, \"records\": {}, \"relation\": {\"arity\": 4, "
              "\"auxArity\": 0, \"types\": [\"u:address\", \"s:register\", "
              "\"u:address\", \"u:operand_index\"]}}"}});
        if (!outputDirectory.empty()) {
          directiveMap["output-dir"] = outputDirectory;
        }
        IOSystem::getInstance()
            .getWriter(directiveMap, symTable, recordTable)
            ->writeAll(*rel_15_def_used);
      } catch (std::exception &e) {
        std::cerr << e.what();
        exit(1);
      }
    }
    if (performIO)
      rel_16_def_used_intra->purge();
    if (performIO)
      rel_17_def_used_return_val_reg->purge();
  }
#ifdef _MSC_VER
#pragma warning(default : 4100)
#endif // _MSC_VER
#ifdef _MSC_VER
#pragma warning(disable : 4100)
#endif // _MSC_VER
  void subroutine_36(const std::vector<RamDomain> &args,
                     std::vector<RamDomain> &ret) {
    if (performIO) {
      try {
        std::map<std::string, std::string> directiveMap(
            {{"IO", "file"},
             {"attributeNames", "ea\tindex\top"},
             {"fact-dir", "."},
             {"filename", "instruction_get_dest_op.csv"},
             {"name", "instruction_get_dest_op"},
             {"operation", "input"},
             {"params",
              "{\"records\": {}, \"relation\": {\"arity\": 3, \"auxArity\": 0, "
              "\"params\": [\"ea\", \"index\", \"op\"]}}"},
             {"types", "{\"ADTs\": {}, \"records\": {}, \"relation\": "
                       "{\"arity\": 3, \"auxArity\": 0, \"types\": "
                       "[\"u:address\", \"i:number\", \"u:operand_code\"]}}"}});
        if (!inputDirectory.empty()) {
          directiveMap["fact-dir"] = inputDirectory;
        }
        IOSystem::getInstance()
            .getReader(directiveMap, symTable, recordTable)
            ->readAll(*rel_26_instruction_get_dest_op);
      } catch (std::exception &e) {
        std::cerr << "Error loading data: " << e.what() << '\n';
      }
    }
    if (performIO) {
      try {
        std::map<std::string, std::string> directiveMap(
            {{"IO", "file"},
             {"attributeNames", "ea\tindex\top"},
             {"name", "instruction_get_dest_op"},
             {"operation", "output"},
             {"output-dir", "."},
             {"params",
              "{\"records\": {}, \"relation\": {\"arity\": 3, \"auxArity\": 0, "
              "\"params\": [\"ea\", \"index\", \"op\"]}}"},
             {"types", "{\"ADTs\": {}, \"records\": {}, \"relation\": "
                       "{\"arity\": 3, \"auxArity\": 0, \"types\": "
                       "[\"u:address\", \"i:number\", \"u:operand_code\"]}}"}});
        if (!outputDirectory.empty()) {
          directiveMap["output-dir"] = outputDirectory;
        }
        IOSystem::getInstance()
            .getWriter(directiveMap, symTable, recordTable)
            ->writeAll(*rel_26_instruction_get_dest_op);
      } catch (std::exception &e) {
        std::cerr << e.what();
        exit(1);
      }
    }
  }
#ifdef _MSC_VER
#pragma warning(default : 4100)
#endif // _MSC_VER
#ifdef _MSC_VER
#pragma warning(disable : 4100)
#endif // _MSC_VER
  void subroutine_37(const std::vector<RamDomain> &args,
                     std::vector<RamDomain> &ret) {
    if (performIO) {
      try {
        std::map<std::string, std::string> directiveMap(
            {{"IO", "file"},
             {"attributeNames", "op\treg"},
             {"fact-dir", "."},
             {"filename", "op_regdirect_contains_reg.csv"},
             {"name", "op_regdirect_contains_reg"},
             {"operation", "input"},
             {"params", "{\"records\": {}, \"relation\": {\"arity\": 2, "
                        "\"auxArity\": 0, \"params\": [\"op\", \"reg\"]}}"},
             {"types", "{\"ADTs\": {}, \"records\": {}, \"relation\": "
                       "{\"arity\": 2, \"auxArity\": 0, \"types\": "
                       "[\"u:operand_code\", \"s:register\"]}}"}});
        if (!inputDirectory.empty()) {
          directiveMap["fact-dir"] = inputDirectory;
        }
        IOSystem::getInstance()
            .getReader(directiveMap, symTable, recordTable)
            ->readAll(*rel_39_op_regdirect_contains_reg);
      } catch (std::exception &e) {
        std::cerr << "Error loading data: " << e.what() << '\n';
      }
    }
  }
#ifdef _MSC_VER
#pragma warning(default : 4100)
#endif // _MSC_VER
#ifdef _MSC_VER
#pragma warning(disable : 4100)
#endif // _MSC_VER
  void subroutine_38(const std::vector<RamDomain> &args,
                     std::vector<RamDomain> &ret) {
    SignalHandler::instance()->setMsg(R"_(def(EA,Reg) :- 
   instruction_get_dest_op(EA,_,Op),
   op_regdirect_contains_reg(Op,Reg).
in file /reversi/datalog/souffle/test_def.dl [131:1-133:39])_");
    if (!(rel_26_instruction_get_dest_op->empty()) &&
        !(rel_39_op_regdirect_contains_reg->empty())) {
      [&]() {
        CREATE_OP_CONTEXT(rel_39_op_regdirect_contains_reg_op_ctxt,
                          rel_39_op_regdirect_contains_reg->createContext());
        CREATE_OP_CONTEXT(rel_26_instruction_get_dest_op_op_ctxt,
                          rel_26_instruction_get_dest_op->createContext());
        CREATE_OP_CONTEXT(rel_14_def_op_ctxt, rel_14_def->createContext());
        for (const auto &env0 : *rel_26_instruction_get_dest_op) {
          auto range = rel_39_op_regdirect_contains_reg->lowerUpperRange_10(
              Tuple<RamDomain, 2>{
                  {ramBitCast(env0[2]), ramBitCast<RamDomain>(MIN_RAM_SIGNED)}},
              Tuple<RamDomain, 2>{
                  {ramBitCast(env0[2]), ramBitCast<RamDomain>(MAX_RAM_SIGNED)}},
              READ_OP_CONTEXT(rel_39_op_regdirect_contains_reg_op_ctxt));
          for (const auto &env1 : range) {
            Tuple<RamDomain, 2> tuple{
                {ramBitCast(env0[0]), ramBitCast(env1[1])}};
            rel_14_def->insert(tuple, READ_OP_CONTEXT(rel_14_def_op_ctxt));
          }
        }
      }();
    }
    SignalHandler::instance()->setMsg(R"_(def(EA,Reg) :- 
   direct_call(EA,EA_pc_thunk),
   get_pc_thunk(EA_pc_thunk,Reg).
in file /reversi/datalog/souffle/test_def.dl [147:1-149:35])_");
    if (!(rel_19_direct_call->empty()) && !(rel_24_get_pc_thunk->empty())) {
      [&]() {
        CREATE_OP_CONTEXT(rel_19_direct_call_op_ctxt,
                          rel_19_direct_call->createContext());
        CREATE_OP_CONTEXT(rel_24_get_pc_thunk_op_ctxt,
                          rel_24_get_pc_thunk->createContext());
        CREATE_OP_CONTEXT(rel_14_def_op_ctxt, rel_14_def->createContext());
        for (const auto &env0 : *rel_19_direct_call) {
          auto range = rel_24_get_pc_thunk->lowerUpperRange_10(
              Tuple<RamDomain, 2>{
                  {ramBitCast(env0[1]), ramBitCast<RamDomain>(MIN_RAM_SIGNED)}},
              Tuple<RamDomain, 2>{
                  {ramBitCast(env0[1]), ramBitCast<RamDomain>(MAX_RAM_SIGNED)}},
              READ_OP_CONTEXT(rel_24_get_pc_thunk_op_ctxt));
          for (const auto &env1 : range) {
            Tuple<RamDomain, 2> tuple{
                {ramBitCast(env0[0]), ramBitCast(env1[1])}};
            rel_14_def->insert(tuple, READ_OP_CONTEXT(rel_14_def_op_ctxt));
          }
        }
      }();
    }
    SignalHandler::instance()->setMsg(R"_(def(EA,Reg) :- 
   call_operation(Operation),
   function_non_maintained_reg(Reg),
   instruction(EA,_,_,Operation,_,_,_,_,_,_).
in file /reversi/datalog/souffle/test_def.dl [135:1-139:38])_");
    if (!(rel_25_instruction->empty()) && !(rel_9_call_operation->empty()) &&
        !(rel_23_function_non_maintained_reg->empty())) {
      [&]() {
        CREATE_OP_CONTEXT(rel_25_instruction_op_ctxt,
                          rel_25_instruction->createContext());
        CREATE_OP_CONTEXT(rel_9_call_operation_op_ctxt,
                          rel_9_call_operation->createContext());
        CREATE_OP_CONTEXT(rel_14_def_op_ctxt, rel_14_def->createContext());
        CREATE_OP_CONTEXT(rel_23_function_non_maintained_reg_op_ctxt,
                          rel_23_function_non_maintained_reg->createContext());
        for (const auto &env0 : *rel_9_call_operation) {
          for (const auto &env1 : *rel_23_function_non_maintained_reg) {
            auto range = rel_25_instruction->lowerUpperRange_0001000000(
                Tuple<RamDomain, 10>{{ramBitCast<RamDomain>(MIN_RAM_UNSIGNED),
                                      ramBitCast<RamDomain>(MIN_RAM_UNSIGNED),
                                      ramBitCast<RamDomain>(MIN_RAM_SIGNED),
                                      ramBitCast(env0[0]),
                                      ramBitCast<RamDomain>(MIN_RAM_UNSIGNED),
                                      ramBitCast<RamDomain>(MIN_RAM_UNSIGNED),
                                      ramBitCast<RamDomain>(MIN_RAM_UNSIGNED),
                                      ramBitCast<RamDomain>(MIN_RAM_UNSIGNED),
                                      ramBitCast<RamDomain>(MIN_RAM_UNSIGNED),
                                      ramBitCast<RamDomain>(MIN_RAM_UNSIGNED)}},
                Tuple<RamDomain, 10>{{ramBitCast<RamDomain>(MAX_RAM_UNSIGNED),
                                      ramBitCast<RamDomain>(MAX_RAM_UNSIGNED),
                                      ramBitCast<RamDomain>(MAX_RAM_SIGNED),
                                      ramBitCast(env0[0]),
                                      ramBitCast<RamDomain>(MAX_RAM_UNSIGNED),
                                      ramBitCast<RamDomain>(MAX_RAM_UNSIGNED),
                                      ramBitCast<RamDomain>(MAX_RAM_UNSIGNED),
                                      ramBitCast<RamDomain>(MAX_RAM_UNSIGNED),
                                      ramBitCast<RamDomain>(MAX_RAM_UNSIGNED),
                                      ramBitCast<RamDomain>(MAX_RAM_UNSIGNED)}},
                READ_OP_CONTEXT(rel_25_instruction_op_ctxt));
            for (const auto &env2 : range) {
              Tuple<RamDomain, 2> tuple{
                  {ramBitCast(env2[0]), ramBitCast(env1[0])}};
              rel_14_def->insert(tuple, READ_OP_CONTEXT(rel_14_def_op_ctxt));
            }
          }
        }
      }();
    }
    if (performIO) {
      try {
        std::map<std::string, std::string> directiveMap(
            {{"IO", "file"},
             {"attributeNames", "EA\tReg"},
             {"name", "def"},
             {"operation", "output"},
             {"output-dir", "."},
             {"params", "{\"records\": {}, \"relation\": {\"arity\": 2, "
                        "\"auxArity\": 0, \"params\": [\"EA\", \"Reg\"]}}"},
             {"types", "{\"ADTs\": {}, \"records\": {}, \"relation\": "
                       "{\"arity\": 2, \"auxArity\": 0, \"types\": "
                       "[\"u:address\", \"s:register\"]}}"}});
        if (!outputDirectory.empty()) {
          directiveMap["output-dir"] = outputDirectory;
        }
        IOSystem::getInstance()
            .getWriter(directiveMap, symTable, recordTable)
            ->writeAll(*rel_14_def);
      } catch (std::exception &e) {
        std::cerr << e.what();
        exit(1);
      }
    }
    if (performIO)
      rel_26_instruction_get_dest_op->purge();
    if (performIO)
      rel_9_call_operation->purge();
    if (performIO)
      rel_23_function_non_maintained_reg->purge();
    if (performIO)
      rel_24_get_pc_thunk->purge();
  }
#ifdef _MSC_VER
#pragma warning(default : 4100)
#endif // _MSC_VER
#ifdef _MSC_VER
#pragma warning(disable : 4100)
#endif // _MSC_VER
  void subroutine_39(const std::vector<RamDomain> &args,
                     std::vector<RamDomain> &ret) {
    if (performIO) {
      try {
        std::map<std::string, std::string> directiveMap(
            {{"IO", "file"},
             {"attributeNames", "ea\tblock"},
             {"fact-dir", "."},
             {"filename", "code_in_block.csv"},
             {"name", "code_in_block"},
             {"operation", "input"},
             {"params", "{\"records\": {}, \"relation\": {\"arity\": 2, "
                        "\"auxArity\": 0, \"params\": [\"ea\", \"block\"]}}"},
             {"types",
              "{\"ADTs\": {}, \"records\": {}, \"relation\": {\"arity\": 2, "
              "\"auxArity\": 0, \"types\": [\"u:address\", \"u:address\"]}}"}});
        if (!inputDirectory.empty()) {
          directiveMap["fact-dir"] = inputDirectory;
        }
        IOSystem::getInstance()
            .getReader(directiveMap, symTable, recordTable)
            ->readAll(*rel_12_code_in_block);
      } catch (std::exception &e) {
        std::cerr << "Error loading data: " << e.what() << '\n';
      }
    }
    if (performIO) {
      try {
        std::map<std::string, std::string> directiveMap(
            {{"IO", "file"},
             {"attributeNames", "ea\tblock"},
             {"name", "code_in_block"},
             {"operation", "output"},
             {"output-dir", "."},
             {"params", "{\"records\": {}, \"relation\": {\"arity\": 2, "
                        "\"auxArity\": 0, \"params\": [\"ea\", \"block\"]}}"},
             {"types",
              "{\"ADTs\": {}, \"records\": {}, \"relation\": {\"arity\": 2, "
              "\"auxArity\": 0, \"types\": [\"u:address\", \"u:address\"]}}"}});
        if (!outputDirectory.empty()) {
          directiveMap["output-dir"] = outputDirectory;
        }
        IOSystem::getInstance()
            .getWriter(directiveMap, symTable, recordTable)
            ->writeAll(*rel_12_code_in_block);
      } catch (std::exception &e) {
        std::cerr << e.what();
        exit(1);
      }
    }
  }
#ifdef _MSC_VER
#pragma warning(default : 4100)
#endif // _MSC_VER
#ifdef _MSC_VER
#pragma warning(disable : 4100)
#endif // _MSC_VER
  void subroutine_40(const std::vector<RamDomain> &args,
                     std::vector<RamDomain> &ret) {
    SignalHandler::instance()->setMsg(R"_(defined_in_block(Block,Reg) :- 
   def(EA,Reg),
   code_in_block(EA,Block).
in file /reversi/datalog/souffle/test_def.dl [207:1-209:29])_");
    if (!(rel_14_def->empty()) && !(rel_12_code_in_block->empty())) {
      [&]() {
        CREATE_OP_CONTEXT(rel_12_code_in_block_op_ctxt,
                          rel_12_code_in_block->createContext());
        CREATE_OP_CONTEXT(rel_18_defined_in_block_op_ctxt,
                          rel_18_defined_in_block->createContext());
        CREATE_OP_CONTEXT(rel_14_def_op_ctxt, rel_14_def->createContext());
        for (const auto &env0 : *rel_14_def) {
          auto range = rel_12_code_in_block->lowerUpperRange_10(
              Tuple<RamDomain, 2>{{ramBitCast(env0[0]),
                                   ramBitCast<RamDomain>(MIN_RAM_UNSIGNED)}},
              Tuple<RamDomain, 2>{{ramBitCast(env0[0]),
                                   ramBitCast<RamDomain>(MAX_RAM_UNSIGNED)}},
              READ_OP_CONTEXT(rel_12_code_in_block_op_ctxt));
          for (const auto &env1 : range) {
            Tuple<RamDomain, 2> tuple{
                {ramBitCast(env1[1]), ramBitCast(env0[1])}};
            rel_18_defined_in_block->insert(
                tuple, READ_OP_CONTEXT(rel_18_defined_in_block_op_ctxt));
          }
        }
      }();
    }
    if (performIO) {
      try {
        std::map<std::string, std::string> directiveMap(
            {{"IO", "file"},
             {"attributeNames", "EA\tReg"},
             {"name", "defined_in_block"},
             {"operation", "output"},
             {"output-dir", "."},
             {"params", "{\"records\": {}, \"relation\": {\"arity\": 2, "
                        "\"auxArity\": 0, \"params\": [\"EA\", \"Reg\"]}}"},
             {"types", "{\"ADTs\": {}, \"records\": {}, \"relation\": "
                       "{\"arity\": 2, \"auxArity\": 0, \"types\": "
                       "[\"u:address\", \"s:register\"]}}"}});
        if (!outputDirectory.empty()) {
          directiveMap["output-dir"] = outputDirectory;
        }
        IOSystem::getInstance()
            .getWriter(directiveMap, symTable, recordTable)
            ->writeAll(*rel_18_defined_in_block);
      } catch (std::exception &e) {
        std::cerr << e.what();
        exit(1);
      }
    }
  }
#ifdef _MSC_VER
#pragma warning(default : 4100)
#endif // _MSC_VER
};
SouffleProgram *newInstance_test_def() { return new Sf_test_def; }
SymbolTable *getST_test_def(SouffleProgram *p) {
  return &reinterpret_cast<Sf_test_def *>(p)->symTable;
}

#ifdef __EMBEDDED_SOUFFLE__
class factory_Sf_test_def : public souffle::ProgramFactory {
  SouffleProgram *newInstance() { return new Sf_test_def(); };

public:
  factory_Sf_test_def() : ProgramFactory("test_def") {}
};
extern "C" {
factory_Sf_test_def __factory_Sf_test_def_instance;
}
}
#else
}
int main(int argc, char **argv) {
  try {
    souffle::CmdOptions opt(R"(./test_def.dl)", R"()", R"()", false, R"()", 1);
    if (!opt.parse(argc, argv))
      return 1;
    souffle::Sf_test_def obj;
#if defined(_OPENMP)
    obj.setNumThreads(opt.getNumJobs());

#endif
    obj.runAll(opt.getInputFileDir(), opt.getOutputFileDir());
    return 0;
  } catch (std::exception &e) {
    souffle::SignalHandler::instance()->error(e.what());
  }
}

#endif
