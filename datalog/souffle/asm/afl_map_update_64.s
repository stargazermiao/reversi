
  xorq __afl_prev_loc(%rip), %rcx
  xorq %rcx, __afl_prev_loc(%rip)
  shrq $1, __afl_prev_loc(%rip)

  addb $1, (%rdx, %rcx, 1)
  adcb $0, (%rdx, %rcx, 1)
