FROM stargazermiao/ddisasm

RUN apt-get update && apt-get install -y python3 python3-pip libz3-dev
RUN update-alternatives --install /usr/bin/python python /usr/bin/python3 1
RUN update-alternatives --install /usr/bin/pip pip /usr/bin/pip3 1

RUN git clone https://github.com/StarGazerM/gtirb-rewriting.git /gtirb-rewriting
RUN cd /gtirb-rewriting && git checkout rewrite-data && pip install -r requirements.txt && python setup.py install

COPY . /reversi
ENV LD_LIBRARY_PATH=${LD_LIBRARY_PATH:+$LD_LIBRARY_PATH:}/datalog/souffle/functor
