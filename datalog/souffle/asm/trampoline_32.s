/* AFL trampoline code
 *  - `unique_make`: id of a basic block
 *  - `payload_name`: which payload will be called 
 */

.align 4

leal -16(%esp), %esp
movl %edi,  0(%esp)
movl %edx,  4(%esp)
movl %ecx,  8(%esp)
movl %eax, 12(%esp)
movl {unique_mark}, %ecx
call {payload_name}
movl 12(%esp), %eax
movl  8(%esp), %ecx
movl  4(%esp), %edx
movl  0(%esp), %edi
leal 16(%esp), %esp
