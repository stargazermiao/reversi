"""
merge all relation inside 2 dirs and add an extra binary name column at the end of each relation

Yihao Sun
"""

import argparse
import os

def merge_rel(bin_name1, rel_path1, out_path):
    rel_file1 = open(rel_path1, "r")
    out_file = open(out_path, "a+")
    for l in rel_file1:
        out_file.write(l[:-1] + f"\t{bin_name1}\n")
    out_file.close()
    rel_file1.close()

def merge_dir(in_dirs, output_dir):
    # bin_name1 = os.path.basename(dir1)
    # bin_name2 = os.path.basename(dir2)
    # for rel_file_name in os.listdir(dir1):
    #     merge_rel(bin_name1, os.path.join(dir1, rel_file_name),
    #               bin_name2, os.path.join(dir2, rel_file_name),
    #               os.path.join(output_dir, rel_file_name))
    for in_dir in in_dirs:
        in_dir = os.path.realpath(in_dir)
        bin_name = os.path.basename(in_dir)
        for rel_file_name in os.listdir(in_dir):
            merge_rel(bin_name,
                      os.path.join(in_dir, rel_file_name),
                      os.path.join(output_dir, rel_file_name))


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='merge all relation inside 2 dirs and add an extra binary name column at the end of each relation')
    parser.add_argument('out_dir', metavar='out', type=str, help='output dir')
    parser.add_argument('in_dirs', metavar='in', type=str, nargs='+', help='input dirs')
    args = parser.parse_args()
    merge_dir(args.in_dirs,
              os.path.realpath(args.out_dir))
