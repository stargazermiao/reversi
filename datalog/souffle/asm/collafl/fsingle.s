
  movq %rcx, __afl_prev_loc(%rip)
  movq __coll_afl_x(%rip), %rcx
  /* do nothing but `shared_mem[cur_hash]++;` */
  addb $1, (%rdx, %rcx, 1)
  adcb $0, (%rdx, %rcx, 1)
  nop
